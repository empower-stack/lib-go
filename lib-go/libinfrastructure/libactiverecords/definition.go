package libactiverecords

import (
	"fmt"
	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

type Definitions struct {
	RepositoryPath string
	PersistencePath string
	// UseTenants bool
	slice []*ActiveRecordDefinition
	byIds map[string]*ActiveRecordDefinition
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(a *ActiveRecordDefinition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*ActiveRecordDefinition{}
	}

	// a.AggregateRoot.isAggregateRoot = true

	// EntitiesInAggregate := a.GetEntities()
	// a.entitiesByName = map[string]*EntityDefinition{}
	// a.valueObjectsByName = map[string]*ValueObjectDefinition{}
	// a.commandsByName = map[string]*CustomRequest{}

	// for _, e := range EntitiesInAggregate {

	// 	e.aggregateDefinition = a
	// 	a.entitiesByName[e.Name] = e

	// 	e.domainPath = ds.Repository
	// 	e.externalDatasourcesByPropertyName = map[string]DatasourceClient{}

	// 	if e.Queries == nil {
	// 		e.Queries = &EntityQueriesDefinition{}
	// 	}
	// 	if e.Commands == nil {
	// 		e.Commands = &EntityCommandsDefinition{}
	// 	}

	// 	for _, f := range e.Keys {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 		if f.IsTranslatable() {
	// 			e.hasTranslatable = true
	// 		}
	// 	}
	// 	for _, f := range e.Properties {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 		if f.IsTranslatable() {
	// 			e.hasTranslatable = true
	// 		}
	// 		if f.GetExternalDatasource() != nil {
	// 			e.externalDatasourcesByPropertyName[f.GetName()] = f.GetExternalDatasource()
	// 		}
	// 	}

	// 	// e.setUseTenants(e.AggregateDefinition.UseTenants)
	// }

	// for _, v := range a.ValueObjects {
	// 	v.aggregateDefinition = a
	// 	v.aggregateIDProperty = SetAggregateIDProperty(a.AggregateRoot.Name)
	// 	a.valueObjectsByName[v.Name] = v

	// 	v.domainPath = ds.Repository

	// 	for _, f := range v.GetKeys() {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 	}
	// 	for _, f := range v.Properties {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 	}
	// }

	// for _, c := range a.Queries {
	// 	a.commandsByName[c.Name] = c
	// }
	// for _, c := range a.Commands {
	// 	c.isCommand = true
	// 	a.commandsByName[c.Name] = c
	// }

	ds.slice = append(ds.slice, a)
	ds.byIds[a.Name()] = a

	// for _, old := range ds.Slice() {
	// 	// fmt.Println("old ", old.AggregateRoot.Name)
	// 	for _, e := range old.GetEntities() {
	// 		for _, f := range e.Keys {
	// 			if f.GetReferenceName() == a.AggregateRoot.Name {
	// 				f.SetReference(a.AggregateRoot)
	// 			}
	// 		}
	// 		for _, f := range e.Properties {
	// 			if f.GetReferenceName() == a.AggregateRoot.Name {
	// 				f.SetReference(a.AggregateRoot)
	// 			}
	// 		}
	// 		for _, f := range e.Keys {
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 		for _, f := range e.Properties {
	// 			// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	for _, v := range old.ValueObjects {
	// 		// fmt.Println("\nvalueObject ", v.Name)
	// 		for _, f := range v.GetKeys() {
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 		for _, f := range v.Properties {
	// 			// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}

	// 	for _, c := range old.GetCustomRequests() {
	// 		for _, a = range ds.slice {
	// 			for _, e := range a.GetEntities() {
	// 				for _, f := range c.Args {
	// 					// fmt.Printf("%s %s\n", f.GetReferenceName(), e.GetName())
	// 					if f.GetReferenceName() == e.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(e)
	// 					}
	// 				}
	// 				for _, f := range c.Results {
	// 					// fmt.Printf("%s %s\n", f.GetReferenceName(), e.GetName())
	// 					if f.GetReferenceName() == e.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(e)
	// 					}
	// 				}
	// 			}
	// 			for _, v := range a.ValueObjects {
	// 				for _, f := range c.Args {
	// 					// fmt.Printf("%s %s\n", f.GetReferenceName(), v.GetName())
	// 					if f.GetReferenceName() == v.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(v)
	// 					}
	// 				}
	// 				for _, f := range c.Results {
	// 					// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
	// 					if f.GetReferenceName() == v.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(v)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }

}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*ActiveRecordDefinition {
	result := ds.slice

	return result
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *ActiveRecordDefinition {
	d := ds.byIds[id]

	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

type ActiveRecordDefinition struct {
	Aggregate *libdomain.AggregateDefinition
	RenamedEntities map[string]string
}

func (r *ActiveRecordDefinition) Name() string {
	return r.Aggregate.AggregateRoot.Name
}
func (r *ActiveRecordDefinition) Title() string {
	return r.Aggregate.AggregateRoot.Title()
}
func (r *ActiveRecordDefinition) GetRenamedEntity(domainName string) string {
	if r.RenamedEntities == nil {
		return domainName
	}
	if v, ok := r.RenamedEntities[domainName]; ok {
		return v
	}
	return domainName
}
func (r *ActiveRecordDefinition) GetRenamedEntityTitle(domainName string) string {
	return strings.Title(r.GetRenamedEntity(domainName))
}
