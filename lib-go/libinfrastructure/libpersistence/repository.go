package libpersistence

type RepositoryInterface interface {
	// Insert(WorkflowInterface, interface{}) ([]DomainObjectInterface, error)
	// // Select(TableInterface, WorkflowInterface, *baserepositorypb.ListQuery) ([]DomainObjectInterface, error)
	// Update(*AggregateDefinition, WorkflowInterface, []FilterInterface, interface{}) ([]DomainObjectInterface, error)
	// Delete(*AggregateDefinition, WorkflowInterface, []FilterInterface) error
	GetCache(string) ([]byte, error)
	SetCache(string, []byte) error
	GetRecordCache(string, string) ([]byte, error)
	SetRecordCache(string, string, []byte) error
	InvalidateRecordCache(string, string)
	DefinitionInterface() interface{}
}

type FilterInterface interface{}

type ListQueryInterface interface {
	GetOptions() *OptionsListQuery
}
