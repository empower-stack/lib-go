package libpersistence

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/generator/baserepositorypb"
	"google.golang.org/protobuf/proto"
)

type Definitions struct {
	Repository       string
	Prefix           string
	GitRepository    string
	DatasourceClient DatasourceClient
	CacheClient      libapplication.CacheClient
	Libraries []*Definitions

	// UseTenants bool
	slice []*AggregateDefinition
	byIds map[string]*AggregateDefinition

	tablesByName map[string]TableInterface
}

var AddEntitiesProperties func(*TableDefinition)

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(a *AggregateDefinition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*AggregateDefinition{}
	}

	if ds.tablesByName == nil {
		ds.tablesByName = map[string]TableInterface{}
	}

	a.AggregateRoot.isAggregateRoot = true

	EntitiesInAggregate := a.GetEntities()
	a.entitiesByName = map[string]*TableDefinition{}
	a.valueObjectsByName = map[string]*ValueObjectDefinition{}
	a.commandsByName = map[string]*CustomRequest{}

	for _, e := range EntitiesInAggregate {

		e.aggregateDefinition = a
		a.entitiesByName[e.Name] = e

		e.domainPath = ds.Repository
		e.externalDatasourcesByPropertyName = map[string]DatasourceClient{}

		if e.Queries == nil {
			e.Queries = &EntityQueriesDefinition{}
		}
		if e.Commands == nil {
			e.Commands = &EntityCommandsDefinition{}
		}

		ds.tablesByName[e.Name] = e

		if AddEntitiesProperties != nil {
			AddEntitiesProperties(e)
		}

		for _, f := range e.Keys {

			// for _, old := range EntitiesInAggregate {
			// 	if f.GetReferenceName() == old.Name {
			// 		f.SetReference(old)
			// 	}
			// }
			// for _, old := range ds.Slice() {
			// 	if f.GetReferenceName() == old.AggregateRoot.Name {
			// 		f.SetReference(old.AggregateRoot)
			// 	}
			// }
			if f.IsTranslatable() {
				e.hasTranslatable = true
			}
		}
		for _, f := range e.Properties {
			// for _, old := range EntitiesInAggregate {
			// 	if f.GetReferenceName() == old.Name {
			// 		f.SetReference(old)
			// 	}
			// }
			// for _, old := range ds.Slice() {
			// 	if f.GetReferenceName() == old.AggregateRoot.Name {
			// 		f.SetReference(old.AggregateRoot)
			// 	}
			// }
			if f.IsTranslatable() {
				e.hasTranslatable = true
			}
			if f.GetExternalDatasource() != nil {
				e.externalDatasourcesByPropertyName[f.GetName()] = f.GetExternalDatasource()
			}
		}

		// e.setUseTenants(e.AggregateDefinition.UseTenants)
	}

	for _, v := range a.ValueObjects {
		v.aggregateDefinition = a
		v.aggregateIDProperty = SetAggregateIDProperty(a.AggregateRoot.Name)
		a.valueObjectsByName[v.Name] = v

		v.domainPath = ds.Repository

		ds.tablesByName[v.Name] = v

		// for _, f := range v.GetKeys() {
		// 	for _, old := range EntitiesInAggregate {
		// 		if f.GetReferenceName() == old.Name {
		// 			f.SetReference(old)
		// 		}
		// 	}
		// 	for _, old := range ds.Slice() {
		// 		if f.GetReferenceName() == old.AggregateRoot.Name {
		// 			f.SetReference(old.AggregateRoot)
		// 		}
		// 	}
		// }
		// for _, f := range v.Properties {
		// 	for _, old := range EntitiesInAggregate {
		// 		if f.GetReferenceName() == old.Name {
		// 			f.SetReference(old)
		// 		}
		// 	}
		// 	for _, old := range ds.Slice() {
		// 		if f.GetReferenceName() == old.AggregateRoot.Name {
		// 			f.SetReference(old.AggregateRoot)
		// 		}
		// 	}
		// }
	}

	for _, c := range a.Queries {
		a.commandsByName[c.Name] = c
	}
	for _, c := range a.Commands {
		c.isCommand = true
		a.commandsByName[c.Name] = c
	}

	for _, addProperties := range a.AddEntitiesProperties {
		for _, old := range ds.Slice() {
			for _, e := range old.GetEntities() {
				if e.Name == addProperties.Entity {
					for _, p := range addProperties.Keys {
						e.Keys = append(e.Keys, p)
					}
					for _, p := range addProperties.Properties {
						e.Properties = append(e.Properties, p)
					}
				}
			}
		}
	}

	ds.slice = append(ds.slice, a)
	ds.byIds[a.AggregateRoot.Name] = a

	for _, old := range ds.Slice() {
		// fmt.Println("old ", old.AggregateRoot.Name)
		for _, e := range old.GetEntities() {
			for _, f := range e.Keys {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.tablesByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.tablesByName[f.GetReferenceName()])
					}
					if f.GetReference() == nil {
						panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), e.Name, f.GetReferenceName()))
					}
				}
			}
			for _, f := range e.Properties {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.tablesByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.tablesByName[f.GetReferenceName()])
					}
					if f.GetReference() == nil {
						panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), e.Name, f.GetReferenceName()))
					}
				}
			}
		}
		for _, v := range old.ValueObjects {
			// fmt.Println("\nvalueObject ", v.Name)
			for _, f := range v.GetKeys() {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.tablesByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.tablesByName[f.GetReferenceName()])
					}
					if f.GetReference() == nil {
						panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), v.Name, f.GetReferenceName()))
					}
				}
			}
			for _, f := range v.Properties {
				// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.tablesByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.tablesByName[f.GetReferenceName()])
					}
					if f.GetReference() == nil {
						panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), v.Name, f.GetReferenceName()))
					}
				}
			}
		}

		for _, c := range old.GetCustomRequests() {

			for _, f := range c.Args {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.tablesByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.tablesByName[f.GetReferenceName()])
					}
					// if f.GetReference() == nil {
					// 	panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
					// }
				}
			}
			for _, f := range c.Results {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.tablesByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.tablesByName[f.GetReferenceName()])
					}
					// if f.GetReference() == nil {
					// 	panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
					// }
				}
			}
		}
	}

}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*AggregateDefinition {
	result := ds.slice

	return result
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *AggregateDefinition {
	d := ds.byIds[id]

	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) MigrateAggregates() error {

	fmt.Printf("Migrating aggregates\n")
	fmt.Printf("Migrating aggregates %+v\n", ds.DatasourceClient)

	defs := map[*TableDefinition]*TableDefinition{}
	for _, def := range ds.Slice() {
		for _, entity := range def.GetEntities() {
			if entity.ExternalDatasource == nil {
				defs[entity] = entity
			}
		}

	}
	err := ds.DatasourceClient.MigrateTables(defs, true)
	if err != nil {
		return err
	}

	externalDefs := map[DatasourceClient]map[*TableDefinition]*TableDefinition{}
	for _, def := range ds.Slice() {
		for _, entity := range def.GetEntities() {
			if entity.ExternalDatasource != nil {
				if _, ok := externalDefs[entity.ExternalDatasource]; !ok {
					externalDefs[entity.ExternalDatasource] = map[*TableDefinition]*TableDefinition{}
				}
				externalDefs[entity.ExternalDatasource][entity] = entity
			}
			for _, property := range entity.StoredProperties() {
				if property.GetExternalDatasource() != nil {
					if _, ok := externalDefs[property.GetExternalDatasource()]; !ok {
						externalDefs[property.GetExternalDatasource()] = map[*TableDefinition]*TableDefinition{}
					}
					externalDefs[property.GetExternalDatasource()][entity] = entity
				}
			}
		}
	}
	for client, entities := range externalDefs {
		err := client.MigrateTables(entities, false)
		if err != nil {
			return err
		}
	}

	tenantIDByExternalID := map[string]string{}
	for _, def := range ds.Slice() {

		for _, data := range def.ExternalData {

			if data.Aggregate == nil {
				data.Aggregate = def.GetAggregate()
			}

			var tenantID string
			if def.UseTenants {
				if data.TenantID == "" {
					return errors.New("The tenantID is required for external data")
				} else {

					tenantID = tenantIDByExternalID[data.TenantID]

					// workflow := &libapplication.ApplicationContext{
					// 	TransactionClient: data.Aggregate.RepositoryInterface().DefinitionInterface().(*Definition).DatasourceClient,
					// }
					// workflow.SetWorkflow(&libdomain.Workflow{
					// 	Context: context.Background(),
					// })

					// err := data.Aggregate.RepositoryInterface().DefinitionInterface().(*Definition).DatasourceClient.BeginTransaction(workflow, false)
					// if err != nil {
					// 	return err
					// }

					// tenants, err := gentenant.TenantEntity.Select(workflow, &pb.TenantListQuery{
					// 	Filters: []*pbtenant.TenantFilter{{Filter: &pbtenant.TenantFilter_Condition{Condition: &pbtenant.TenantCondition{
					// 		Property: pbtenant.TenantProperties_TENANT_EXTERNAL_ID,
					// 		Operator: &pbtenant.TenantCondition_IsEqualTo{IsEqualTo: &basepb.IsEqualTo{Value: data.TenantID}},
					// 	}}}},
					// })
					// if err != nil {
					// 	data.Aggregate.RepositoryInterface().DefinitionInterface().(*Definition).DatasourceClient.RollbackTransaction(workflow)
					// 	return err
					// } else {
					// 	data.Aggregate.RepositoryInterface().DefinitionInterface().(*Definition).DatasourceClient.CommitTransaction(workflow)
					// }

					// tenantID = tenants.Slice()[0].ID()
				}
			}

			workflow := libapplication.NewApplicationContext(context.Background(), ds.DatasourceClient, nil, nil, true)
			workflow.Workflow().TenantID = libdomain.TenantID(tenantID)

			err := ds.DatasourceClient.BeginTransaction(workflow, false)
			if err != nil {
				return err
			}

			// fmt.Println("!!!!!")
			fmt.Println(workflow.Transaction)

			err = data.Aggregate.LoadExternalData(
				workflow, data.Module, data.Data,
			)
			if err != nil {
				ds.DatasourceClient.RollbackTransaction(workflow)
				return err
			} else {
				ds.DatasourceClient.CommitTransaction(workflow)
			}

			for aggregate, cache := range workflow.GetCache() {
				for _, cacheItem := range cache {
					if cacheItem.CacheClient != nil {
						fmt.Printf("item %+v\n", cacheItem.Item)

						item, err := proto.Marshal(cacheItem.Item)
						if err != nil {
							fmt.Println("Error while marshalling cache", err)
						}
						cacheItem.CacheClient.Set(workflow.Workflow().Context, cacheItem.Key, item, 0)
						if err != nil {
							fmt.Println("Error while setting cache", err)
						// } else {
						// 	fmt.Println("Cache set for " + cacheItem.Key)
						}
					}
					fmt.Println(aggregate)
					if aggregate == "tenants" {
						tenantIDByExternalID[cacheItem.Item.(libapplication.EntityInterface).GetExternalID()] = cacheItem.Item.(libapplication.EntityInterface).GetID()
					}
				}
			}
		}
	}

	return nil

}

// Definition is used to declare the information of a model, so it can generate its code.
type AggregateDefinition struct {
	AggregateRoot         *TableDefinition
	Children              []*TableDefinition
	UseTenants            bool
	entitiesByName        map[string]*TableDefinition
	ValueObjects          []*ValueObjectDefinition
	valueObjectsByName    map[string]*ValueObjectDefinition
	Queries               []*CustomRequest
	Commands              []*CustomRequest
	commandsByName        map[string]*CustomRequest
	EventStoreClient      EventStoreClient
	EventDispatchClient   EventDispatchClient
	AddEntitiesProperties []*AddPropertiesDefinition
	ExternalData          []*ExternalData

	aggregate AggregateInterface
}

func (a *AggregateDefinition) GetEntities() []*TableDefinition {
	EntitiesInAggregate := []*TableDefinition{
		a.AggregateRoot,
	}
	for _, c := range a.Children {
		EntitiesInAggregate = append(EntitiesInAggregate, c)
	}
	return EntitiesInAggregate
}
func (a *AggregateDefinition) GetTableByName(name string) *TableDefinition {
	return a.entitiesByName[name]
}
func (a *AggregateDefinition) GetValueObjectByName(name string) *ValueObjectDefinition {
	return a.valueObjectsByName[name]
}
func (a *AggregateDefinition) GetCommandByName(name string) *CustomRequest {
	return a.commandsByName[name]
}
func (a *AggregateDefinition) GetTables() []TableInterface {
	var tables []TableInterface
	for _, c := range a.GetEntities() {
		tables = append(tables, c)
	}
	for _, c := range a.ValueObjects {
		tables = append(tables, c)
	}
	return tables
}
func (a *AggregateDefinition) GetCustomRequests() []*CustomRequest {
	var requests []*CustomRequest
	for _, c := range a.Queries {
		requests = append(requests, c)
	}
	for _, c := range a.Commands {
		requests = append(requests, c)
	}
	return requests
}
func (a *AggregateDefinition) SetAggregate(aggregate AggregateInterface) {
	a.aggregate = aggregate
}
func (a *AggregateDefinition) GetAggregate() AggregateInterface {
	return a.aggregate
}

// Definition is used to declare the information of a model, so it can generate its code.
type AggregateInterface interface {
	Definition() *AggregateDefinition
	RepositoryInterface() RepositoryInterface
	// SetRepository(RepositoryInterface)
	AggregateRootInterface() AggregateRootInterface
	LoadExternalData(
		*libapplication.ApplicationContext, string, []map[string]string,
	) error
}

type UniqueConstraint struct {
	Name       string
	Columns []string
}

type TableDefinition struct {
	Aggregate           AggregateInterface
	aggregateDefinition *AggregateDefinition
	Name                string
	Keys                []Property
	Properties          []Property
	DisableID           bool
	DisableDatetime     bool
	Queries             *EntityQueriesDefinition
	Commands            *EntityCommandsDefinition
	UseOneOf            bool
	// useTenants bool
	DisableDatabaseStore              bool
	SafeDelete                        bool
	Abstract                          bool
	NoCache                           bool
	isAggregateRoot                   bool
	hasTranslatable                   bool
	domainPath                        string
	Indexes                           []*Index
	PrimaryKey                        string
	UniqueConstraints                 []UniqueConstraint
	ExternalDatasource                DatasourceClient
	externalDatasourcesByPropertyName map[string]DatasourceClient
	// Select               func(
	// 	WorkflowInterface, *baserepositorypb.ListQuery,
	// ) (*Collection, error)
}

func (t *TableDefinition) AggregateDefinition() *AggregateDefinition {
	return t.aggregateDefinition
}
func (t *TableDefinition) GetName() string {
	return t.Name
}
func (t *TableDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *TableDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *TableDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *TableDefinition) GetType() string {
	return "Table"
}
func (t *TableDefinition) IsEntity() bool {
	return true
}
func (t *TableDefinition) UseTenants() bool {
	if t.AggregateDefinition() == nil {
		return false
	}
	return t.AggregateDefinition().UseTenants
}
func (t *TableDefinition) GetDisableID() bool {
	return t.DisableID
}
func (t *TableDefinition) GetDisableDatetime() bool {
	return t.DisableDatetime
}
func (t *TableDefinition) GetDisableDatabaseStore() bool {
	return t.DisableDatabaseStore
}
func (t *TableDefinition) GetUseOneOf() bool {
	return t.UseOneOf
}
func (t *TableDefinition) IsAggregateRoot() bool {
	return t.isAggregateRoot
}
func (t *TableDefinition) HasTranslatable() bool {
	return t.hasTranslatable
}
func (t *TableDefinition) GetAbstract() bool {
	return t.Abstract
}
func (t *TableDefinition) GetNoCache() bool {
	return t.NoCache
}
func (t *TableDefinition) GetIndexes() []*Index {
	return t.Indexes
}
func (t *TableDefinition) GetPrimaryKey() string {
	return t.PrimaryKey
}
func (t *TableDefinition) GetExternalDatasource() DatasourceClient {
	return t.ExternalDatasource
}
func (t *TableDefinition) GetExternalDatasources() map[DatasourceClient][]Property {
	results := map[DatasourceClient][]Property{}
	for _, property := range t.Properties {
		if property.GetExternalDatasource() != nil {
			if _, ok := results[property.GetExternalDatasource()]; !ok {
				results[property.GetExternalDatasource()] = []Property{}
			}
			results[property.GetExternalDatasource()] = append(
				results[property.GetExternalDatasource()], property)
		}
	}
	return results
}
func (t *TableDefinition) GetPropertyExternalDatasource(propertyName string) DatasourceClient {
	return t.externalDatasourcesByPropertyName[propertyName]
}

//	func (t *TableDefinition) setUseTenants(useTenants bool) {
//		t.useTenants = useTenants
//	}
func (t *TableDefinition) StoredProperties() []Property {
	var storedProperties []Property
	for _, property := range t.Keys {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *TableDefinition) NestedProperties() []Property {
	var nestedProperties []Property
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *TableDefinition) GetCommands() *EntityCommandsDefinition {
	return t.Commands
}
func (t *TableDefinition) GetKeys() []Property {
	return t.Keys
}
func (t *TableDefinition) DomainPath() string {
	return t.domainPath
}

type RequestConfig struct {
	Groups *[]*baserepositorypb.GetByExternalIDQuery
}

type EntityQueriesDefinition struct {
	Get            *RequestConfig
	List           *RequestConfig
	GetFromEvents  *RequestConfig
	CustomCommands []*CustomRequest
}

type EntityCommandsDefinition struct {
	Create         *RequestConfig
	Update         *RequestConfig
	Delete         *RequestConfig
	CustomCommands []*CustomRequest
}

type Index struct {
	DatasourceClient
	Name       string
	Properties []string
}

type CustomRequest struct {
	Name       string
	Event      string
	OnFactory  bool
	Repository bool
	Args       []ArgsInterface
	Results    []ArgsInterface
	isCommand  bool
}

func (f *CustomRequest) Title() string {
	return strings.Title(f.Name)
}
func (f *CustomRequest) IsCommand() bool {
	return f.isCommand
}

type ArgsInterface interface {
	GetName() string
	Title() string
	ProtoType() string
	GoType() string
	GoNil() string
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() TableInterface
	SetReference(e TableInterface)
	Type() PropertyType
	IsRepeated() bool
}

const RepeatedPropertyType = PropertyType("RepeatedPropertyType")

type RepeatedProperty struct {
	Property Property
}

func (r *RepeatedProperty) GetName() string {
	return r.Property.GetName()
}
func (r *RepeatedProperty) Title() string {
	return r.Property.Title()
}
func (r *RepeatedProperty) Upper() string {
	return r.Property.Upper()
}
func (r *RepeatedProperty) ProtoType() string {
	prototype := r.Property.ProtoType()
	if r.Property.Type() == "Many2oneType" {
		prototype = strcase.ToCamel(r.Property.GetReference().Title())
	}
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) ProtoTypeArg() string {
	prototype := r.Property.ProtoTypeArg()
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) GoType() string {
	return fmt.Sprintf("[]%s", r.Property.GoType())
}
func (r *RepeatedProperty) GoTypeID() string {
	return fmt.Sprintf("[]%s", r.Property.GoTypeID())
}
func (r *RepeatedProperty) GoNil() string {
	return "nil"
}
func (r *RepeatedProperty) GetReferenceName() string {
	return r.Property.GetReferenceName()
}
func (r *RepeatedProperty) Type() PropertyType {
	return r.Property.Type()
}
func (r *RepeatedProperty) DBType() *DBType {
	return r.Property.DBType()
}
func (r *RepeatedProperty) GetInverseProperty() string {
	return ""
}
func (r *RepeatedProperty) GetPrimaryKey() bool {
	return false
}
func (r *RepeatedProperty) GetReference() TableInterface {
	return r.Property.GetReference()
}
func (r *RepeatedProperty) GetRequired() bool {
	return false
}
func (r *RepeatedProperty) GraphqlSchemaType() string {
	return "[" + r.Property.GraphqlSchemaType() + "!]"
}
func (r *RepeatedProperty) GraphqlType() string {
	return ""
}
func (r *RepeatedProperty) IsNested() bool {
	return false
}
func (r *RepeatedProperty) IsStored() bool {
	return true
}
func (r *RepeatedProperty) JSONType() string {
	return ""
}
func (r *RepeatedProperty) NameWithoutID() string {
	return r.Property.NameWithoutID()
}
func (r *RepeatedProperty) ProtoTypeOptional() string {
	return r.ProtoType()
}
func (r *RepeatedProperty) SetPosition(position int) {
	// r.Position = position
}
func (r *RepeatedProperty) GetPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) SetReference(e TableInterface) {
	r.Property.SetReference(e)
}
func (r *RepeatedProperty) Snake() string {
	return ""
}
func (r *RepeatedProperty) TitleWithoutID() string {
	return r.Property.TitleWithoutID()
}
func (r *RepeatedProperty) IsRepeated() bool {
	return true
}
func (r *RepeatedProperty) IsTranslatable() bool {
	return false
}
func (r *RepeatedProperty) GetReturnDetailsInTests() bool {
	return r.Property.GetReturnDetailsInTests()
}
func (r *RepeatedProperty) LoadedPosition() int {
	return r.Property.GetLoadedPosition()
}
func (r *RepeatedProperty) Position() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) LoadedPositionEntity() int {
	return r.Property.GetLoadedPositionEntity()
}
func (r *RepeatedProperty) PositionMany2one() int {
	return r.Property.GetPositionEntity()
}
func (r *RepeatedProperty) GetLoadedPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) GetLoadedPositionEntity() int {
	return r.Property.GetLoadedPositionEntity()
}
func (r *RepeatedProperty) GetPositionEntity() int {
	return r.Property.GetPositionEntity()
}

// STRING is a contains reprensenting the widely used string type.
const STRING = "string"

// FieldType is the generic type for a data field.
type PropertyType string

type DBType struct {
	Type  string
	Value string
}

// Field is an interface to get the data from the field.
type Property interface {
	GetName() string
	NameWithoutID() string
	Title() string
	TitleWithoutID() string
	Snake() string
	Upper() string
	Type() PropertyType
	GoType() string
	GoTypeID() string
	GoNil() string
	JSONType() string
	ProtoType() string
	ProtoTypeArg() string
	ProtoTypeOptional() string
	DBType() *DBType
	DiffTypeInterface() string
	GraphqlType() string
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() TableInterface
	// GetReferenceDefinition() *ModelDefinition
	SetReference(TableInterface)
	GetInverseProperty() string
	GetRequired() bool
	GetPrimaryKey() bool
	GetWithEntity() bool
	// GetFieldData() *FieldData
	IsStored() bool
	IsNested() bool
	SetPosition(int)
	GetPosition() int
	IsRepeated() bool
	IsTranslatable() bool
	IsIndexed() bool
	GetReturnDetailsInTests() bool
	GetLoadedPosition() int
	GetLoadedPositionEntity() int
	GetPositionEntity() int
	GetExternalDatasource() DatasourceClient
	GetExcludeFromInterface() bool
}

type ValueObjectDefinition struct {
	Aggregate           AggregateInterface
	aggregateDefinition *AggregateDefinition
	Name                string
	Keys                []Property
	Properties          []Property
	PrimaryKey		  string
	Abstract            bool
	aggregateIDProperty Property
	domainPath          string
	// Select              func(
	// 	WorkflowInterface, *baserepositorypb.ListQuery,
	// ) (*Collection, error)
}

func (t *ValueObjectDefinition) AggregateDefinition() *AggregateDefinition {
	return t.aggregateDefinition
}
func (t *ValueObjectDefinition) GetName() string {
	return t.Name
}
func (t *ValueObjectDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *ValueObjectDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *ValueObjectDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *ValueObjectDefinition) GetType() string {
	return "ValueObject"
}
func (t *ValueObjectDefinition) IsEntity() bool {
	return false
}
func (t *ValueObjectDefinition) UseTenants() bool {
	return t.aggregateDefinition.UseTenants
}
func (t *ValueObjectDefinition) GetDisableID() bool {
	return true
}
func (t *ValueObjectDefinition) GetDisableDatetime() bool {
	return true
}
func (t *ValueObjectDefinition) GetDisableDatabaseStore() bool {
	return false
}
func (t *ValueObjectDefinition) GetUseOneOf() bool {
	return false
}
func (t *ValueObjectDefinition) StoredProperties() []Property {
	var storedProperties []Property
	for _, property := range t.GetKeys() {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *ValueObjectDefinition) NestedProperties() []Property {
	var nestedProperties []Property
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *ValueObjectDefinition) GetCommands() *EntityCommandsDefinition {
	return &EntityCommandsDefinition{}
}

var SetAggregateIDProperty func(string) Property

func (t *ValueObjectDefinition) GetKeys() []Property {
	keys := []Property{
		// t.aggregateIDProperty,
	}
	for _, key := range t.Keys {
		keys = append(keys, key)
	}
	return keys
}
func (t *ValueObjectDefinition) IsAggregateRoot() bool {
	return false
}
func (t *ValueObjectDefinition) HasTranslatable() bool {
	return false
}
func (t *ValueObjectDefinition) GetAbstract() bool {
	return t.Abstract
}
func (t *ValueObjectDefinition) DomainPath() string {
	return t.domainPath
}
func (t *ValueObjectDefinition) GetNoCache() bool {
	return false
}
func (t *ValueObjectDefinition) GetExternalDatasource() DatasourceClient {
	return nil
}
func (t *ValueObjectDefinition) GetIndexes() []*Index {
	return []*Index{}
}
func (t *ValueObjectDefinition) GetPrimaryKey() string {
	return ""
}

type TableInterface interface {
	GetName() string
	Title() string
	Snake() string
	Upper() string
	GetType() string
	IsEntity() bool
	UseTenants() bool
	GetDisableID() bool
	GetDisableDatetime() bool
	GetDisableDatabaseStore() bool
	GetUseOneOf() bool
	StoredProperties() []Property
	NestedProperties() []Property
	GetCommands() *EntityCommandsDefinition
	GetKeys() []Property
	GetIndexes() []*Index
	GetPrimaryKey() string
	IsAggregateRoot() bool
	HasTranslatable() bool
	GetAbstract() bool
	GetNoCache() bool
	DomainPath() string
	GetExternalDatasource() DatasourceClient
}

type OnDeleteValue string

const OnDeleteCascade = OnDeleteValue("CASCADE")
const OnDeleteProtect = OnDeleteValue("PROTECT")
const OnDeleteSetNull = OnDeleteValue("SET NULL")

type JWTClaims struct {
	*jwt.StandardClaims
	TenantID  string
	UserID    string
	UserEmail string
	Groups    []*baserepositorypb.GetByExternalIDQuery
}

type ExternalData struct {
	Aggregate AggregateInterface
	TenantID  string
	Module    string
	Data      []map[string]string
}

var IDProperty = &baserepositorypb.BaseProperty{Snake: "id"}
var TenantIDProperty = &baserepositorypb.BaseProperty{Snake: "tenant_id"}
var ExternalModuleProperty = &baserepositorypb.BaseProperty{Snake: "external_module"}
var ExternalIDProperty = &baserepositorypb.BaseProperty{Snake: "external_id"}

// Driver is an interface for the drivers providing crud operations.
type DatasourceClient interface {
	GenerateSchema([]*TableDefinition) string 
	MigrateTables(map[*TableDefinition]*TableDefinition, bool) error
	// InitDB(writeInfo *libdata.ClusterInfo, readInfo *libdata.ClusterInfo) (*libdata.Cluster, error)
	// CreateTenant(*libdata.Workflow, []*libdata.ModelDefinition, *libdata.Tenant) error
	// GetTenant(*libdata.Workflow, *libdata.Tenant) (*libdata.Tenant, error)
	GetImportRepository() string
	GetScanType() string
	GetDriverName() string
	GenerateInsertBuildArgsFunc(DatasourceClient, TableInterface) string
	GenerateScanFunc(DatasourceClient, string, TableInterface) string
	Get(*TableDefinition, *libapplication.ApplicationContext, string) (interface{}, error)
	Select(TableInterface, *libapplication.ApplicationContext, []*Filter, []string, *OptionsListQuery) (interface{}, error)
	Insert(
		TableInterface, *libapplication.ApplicationContext, []interface{},
		[]string, map[string]interface{},
		[]string,
	) (interface{}, error)
	Update(
		TableInterface, *libapplication.ApplicationContext, []*Filter, interface{},
		[]string, map[string]interface{}, []string,
	) (interface{}, error)
	Delete(TableInterface, *libapplication.ApplicationContext, []*Filter) error
	BeginTransaction(*libapplication.ApplicationContext, bool) error
	RollbackTransaction(*libapplication.ApplicationContext) error
	CommitTransaction(*libapplication.ApplicationContext) error
	// CheckIDExist(*libpersistence.TableDefinition, *libpersistence.Workflow, string) error
	// SupportTransaction() bool
	RegisterEvents(*libapplication.ApplicationContext, interface{}, []*libapplication.Event) error
	MarkDispatchedEvent(*libapplication.ApplicationContext, *libapplication.Event) error
	GetAggregateHistory(*libapplication.ApplicationContext, *libdomain.AggregateDefinition, string) ([]*libdomain.EventHistory, error)
	GetAggregateEvents(*libapplication.ApplicationContext, *libdomain.AggregateDefinition, string) ([]*libapplication.Event, error)
}

type Filter struct {
	Field    string
	JSONKey  string
	Operator Operator
}

type AddPropertiesDefinition struct {
	Entity     string
	Keys       []Property
	Properties []Property
}

type BooleanDiffInterface interface {
	Old() bool
	New() bool
}

type TextDiffInterface interface {
	Old() string
	New() string
}

type IntegerDiffInterface interface {
	Old() int
	New() int
}

type FloatDiffInterface interface {
	Old() float64
	New() float64
}

type TimeDiffInterface interface {
	Old() time.Time
	New() time.Time
}

type GetWYSIWYGProperties struct {
	TranslatedTerms bool
	Assets          bool
}

type OptionsListQuery struct {
	Lang string
	Offset int
	Limit int
	OrderBy string
	// After string
	// Before string
	// First int
	// Last int
	GlobalIgnoreTenants bool
	AllowFiltering bool
}

type OperatorType string
type Operator interface {
	Type() OperatorType
}

type IsEqualTo struct {
	Value string
}
const OPERATOR_TYPE_IS_EQUAL_TO = OperatorType("IS_EQUAL_TO")
func (o *IsEqualTo) Type() OperatorType {
	return OPERATOR_TYPE_IS_EQUAL_TO
}
type IsDifferentTo struct {
	Value string
}
const OPERATOR_TYPE_IS_DIFFERENT_TO = OperatorType("IS_DIFFERENT_TO")
func (o *IsDifferentTo) Type() OperatorType {
	return OPERATOR_TYPE_IS_DIFFERENT_TO
}
type Contains struct {
	Value string
}
const OPERATOR_TYPE_CONTAINS = OperatorType("CONTAINS")
func (o *Contains) Type() OperatorType {
	return OPERATOR_TYPE_CONTAINS
}
type DoesNotContain struct {
	Value string
}
const OPERATOR_TYPE_DOES_NOT_CONTAIN = OperatorType("DOES_NOT_CONTAIN")
func (o *DoesNotContain) Type() OperatorType {
	return OPERATOR_TYPE_DOES_NOT_CONTAIN
}
type IsIn struct {
	Values []string
}
const OPERATOR_TYPE_IS_IN = OperatorType("IS_IN")
func (o *IsIn) Type() OperatorType {
	return OPERATOR_TYPE_IS_IN
}
type IsNotIn struct {
	Values []string
}
const OPERATOR_TYPE_IS_NOT_IN = OperatorType("IS_NOT_IN")
func (o *IsNotIn) Type() OperatorType {
	return OPERATOR_TYPE_IS_NOT_IN
}
type IsNull struct {}
const OPERATOR_TYPE_IS_NULL = OperatorType("IS_NULL")
func (o *IsNull) Type() OperatorType {
	return OPERATOR_TYPE_IS_NULL
}
type IsNotNull struct {}
const OPERATOR_TYPE_IS_NOT_NULL = OperatorType("IS_NOT_NULL")
func (o *IsNotNull) Type() OperatorType {
	return OPERATOR_TYPE_IS_NOT_NULL
}
type IsGreaterThan struct {
	Value float64
}
const OPERATOR_TYPE_IS_GREATER_THAN = OperatorType("IS_GREATER_THAN")
func (o *IsGreaterThan) Type() OperatorType {
	return OPERATOR_TYPE_IS_GREATER_THAN
}
type IsGreaterThanOrEqualTo struct {
	Value float64
}
const OPERATOR_TYPE_IS_GREATER_THAN_OR_EQUAL_TO = OperatorType("IS_GREATER_THAN_OR_EQUAL_TO")
func (o *IsGreaterThanOrEqualTo) Type() OperatorType {
	return OPERATOR_TYPE_IS_GREATER_THAN_OR_EQUAL_TO
}
type IsLessThan struct {
	Value float64
}
const OPERATOR_TYPE_IS_LESS_THAN = OperatorType("IS_LESS_THAN")
func (o *IsLessThan) Type() OperatorType {
	return OPERATOR_TYPE_IS_LESS_THAN
}
type IsLessThanOrEqualTo struct {
	Value float64
}
const OPERATOR_TYPE_IS_LESS_THAN_OR_EQUAL_TO = OperatorType("IS_LESS_THAN_OR_EQUAL_TO")
func (o *IsLessThanOrEqualTo) Type() OperatorType {
	return OPERATOR_TYPE_IS_LESS_THAN_OR_EQUAL_TO
}
type IsTrue struct {}
const OPERATOR_TYPE_IS_TRUE = OperatorType("IS_TRUE")
func (o *IsTrue) Type() OperatorType {
	return OPERATOR_TYPE_IS_TRUE
}
type IsFalse struct {}
const OPERATOR_TYPE_IS_FALSE = OperatorType("IS_FALSE")
func (o *IsFalse) Type() OperatorType {
	return OPERATOR_TYPE_IS_FALSE
}
type IsBefore struct {
	Value time.Time
}
const OPERATOR_TYPE_IS_BEFORE = OperatorType("IS_BEFORE")
func (o *IsBefore) Type() OperatorType {
	return OPERATOR_TYPE_IS_BEFORE
}
type IsAfter struct {
	Value time.Time
}
const OPERATOR_TYPE_IS_AFTER = OperatorType("IS_AFTER")
func (o *IsAfter) Type() OperatorType {
	return OPERATOR_TYPE_IS_AFTER
}
type NativeOR struct {
	Filters []*Filter
}
const OPERATOR_TYPE_NATIVE_OR = OperatorType("NATIVE_OR")
func (o *NativeOR) Type() OperatorType {
	return OPERATOR_TYPE_NATIVE_OR
}