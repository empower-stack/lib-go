package libpersistence

import (
	// "github.com/pkg/errors"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/generator/baserepositorypb"
	// "gitlab.com/empowerlab/stack/lib-go/libutils"
)

type AggregateRootInterface interface {
	CheckIDExist(*libapplication.ApplicationContext, string) error
}

// Init will initialize the pool.
// func (d *TableDefinition) Init() {
// 	d.Select = d.selectDefault
// }

// nolint: unparam
func (d *TableDefinition) selectDefault(
	wk *libapplication.ApplicationContext, query *baserepositorypb.OptionsListQuery,
) (*Collection, error) {

	// fmt.Printf("filters %+v\n", *filters[0])

	// var orderBy []*libdata.OrderByInput
	// if orderByArg != nil { //|| after != nil {
	// 	if orderByArg != nil {
	// 		//nolint: gocritic
	// 		switch *orderByArg {
	// 		case "created_at_DESC":
	// 			orderBy = append(orderBy, &libdata.OrderByInput{Field: "created_at", Desc: true})
	// 		}
	// 	}
	// } else {
	// 	if !d.Aggregate.Definition().AggregateRoot.DisableDatetime {
	// 		orderBy = append(orderBy, &libdata.OrderByInput{Field: "created_at", Desc: false})
	// 	}
	// }
	// // nolint: dupl
	// if after != nil {
	// 	template, err := d.Getter(ctx, tx, *after, with)
	// 	if err != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't get cursor template")
	// 	}
	// 	firstOrderBy := orderBy[0]

	// 	// nolint: dupl
	// 	switch firstOrderBy.Field {
	// 	case "id":
	// 		filters = append(filters, &libdata.Filter{
	// 			Field: firstOrderBy.Field, Operator: "<", Operande: template.(ObjectInterface).GetID()})
	// 	case libdata.CreatedAtField:
	// 		filters = append(filters, &libdata.Filter{
	// 			Field: firstOrderBy.Field, Operator: "<",
	// Operande: template.(ObjectInterface).GetCreatedAt()})
	// 	case libdata.UpdatedAtField:
	// 		filters = append(filters, &libdata.Filter{
	// 			Field: firstOrderBy.Field, Operator: "<",
	// Operande: template.(ObjectInterface).GetUpdatedAt()})
	// 	default:
	// 		return nil, nil, &libdata.OrderByError{O: firstOrderBy.Field}
	// 	}
	// }

	// records, err := d.Aggregate.RepositoryInterface().Select(
	// 	d, wk, query)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't select templates")
	// }

	// var records []interface{}
	// recordByIds := map[string]interface{}{}
	// switch tx.Type {
	// case libdata.CassandraType:
	// 	templateMaps, errC := rows.(*gocqlx.Iterx).Iter.SliceMap()
	// 	if errC != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't scan to map")
	// 	}
	// 	for _, r := range templateMaps {
	// 		record := d.Newer()
	// 		dest := reflect.ValueOf(record).Elem()
	// 		typeOfDest := dest.Type()
	// 		for i := 0; i < typeOfDest.NumField(); i++ {
	// 			f := dest.Field(i)
	// 			name := typeOfDest.Field(i).Name
	// 			fieldType, _ := dest.Type().FieldByName(name)
	// 			if val, ok := r[fieldType.Tag.Get("db")]; ok {
	// 				if f.IsValid() {
	// 					if f.CanSet() {
	// 						switch f.Kind() {
	// 						case reflect.Int32:
	// 							f.SetInt(int64(val.(int)))
	// 						case reflect.String:
	// 							// fmt.Println("kind", reflect.ValueOf(val).Kind().String())
	// 							switch reflect.ValueOf(val).Kind() {
	// 							case reflect.String:
	// 								f.SetString(val.(string))
	// 							}
	// 						case reflect.Bool:
	// 							f.SetBool(val.(bool))
	// 						case reflect.Ptr:
	// 							if val.(string) != "" {
	// 								v := val.(string)
	// 								f.Set(reflect.ValueOf(&v))
	// 							} else {
	// 								f.Set(reflect.Zero(f.Type()))
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 		fmt.Printf("record cassandra %+v\n", record)
	// 		records = append(records, record)
	// 		if !d.DBInfo.NoID {
	// 			recordByIds[r["id"].(string)] = record
	// 		}
	// 	}
	// nolint: dupl
	// case libdata.PostgresType:
	// for rows.(*sqlx.Rows).Next() {
	// 	record := d.New()
	// 	err = rows.(*sqlx.Rows).StructScan(record)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 		return nil, errors.Wrap(err, "Couldn't scan to templates")
	// 	}
	// 	records = append(records, record)
	// 	recordByIds[record.(ObjectInterface).GetID()] = record
	// 	fmt.Printf("record %+v\n", record)
	// }
	// records, err := d.Scan(rows, nil)
	// if err != nil {
	// 	return nil, err
	// }

	// }

	// tx.CacheInsert(ctx, d.Model, records)

	// if d.TranslamodelFields != nil {
	// 	// recordntIds := utils.MapKeys(recordByIds)

	// 	lang := ctx.Value(LangKey).(string)
	// 	languageDefault, err := SettingGet(ctx, tx, "languageDefault")
	// 	if err != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't get language default")
	// 	}
	// 	if len(recordByIds) > 0 {
	// 		fmt.Println("recordByIds", libutils.MapKeys(recordByIds))
	// 		recordTrs, _, err := d.TranslateModel.Selecter(ctx, tx, []*libdata.Filter{
	// 			{Field: "id", Operator: "IN", Operande: libutils.MapKeys(recordByIds)},
	// 			{Field: "lang", Operator: "IN", Operande: []string{lang, *languageDefault}},
	// 		}, []string{}, nil, nil, nil)
	// 		if err != nil {
	// 			return nil, nil, errors.Wrap(err, "Couldn't get record translations")
	// 		}

	// 		recordTrByRL := map[string]map[string]interface{}{}
	// 		for _, recordTr := range recordTrs {
	// 			rtr := recordTr.(ObjectInterface)
	// 			if _, ok := recordTrByRL[rtr.GetID()]; !ok {
	// 				recordTrByRL[rtr.GetID()] = map[string]interface{}{}
	// 			}
	// 			recordTrByRL[rtr.GetID()][recordTr.(modelTrInterface).GetLang()] = recordTr
	// 		}

	// 		// nolint: dupl
	// 		for _, record := range records {
	// 			d.SelectTrFielder(record, recordTrByRL, lang, *languageDefault)
	// 		}
	// 	}
	// }

	// for _, record := range records {

	// 	// data, err := record.Marshal()
	// 	// if err != nil {
	// 	// 	return nil, err
	// 	// }

	// 	// if d.Definition.Model.CacheClient != nil {
	// 	// err = d.Definition.Repository.SetRecordCache(
	// 	// 	d.Workflow.TenantID, record.GetID(), data)
	// 	// if err != nil {
	// 	// 	return nil, err
	// 	// }
	// 	// }
	// }

	results := &Collection{
		Aggregate: d.Aggregate,
		Workflow:  wk,
	}
	// results.Init(records)
	return results, nil
}

// Init will initialize the pool.
// func (d *ValueObjectDefinition) Init() {
// 	d.Select = d.selectDefault
// }

// nolint: unparam
func (d *ValueObjectDefinition) selectDefault(
	wk *libapplication.ApplicationContext, query *baserepositorypb.OptionsListQuery,
) (*Collection, error) {

	// fmt.Printf("filters %+v\n", *filters[0])

	// var orderBy []*libdata.OrderByInput
	// if orderByArg != nil { //|| after != nil {
	// 	if orderByArg != nil {
	// 		//nolint: gocritic
	// 		switch *orderByArg {
	// 		case "created_at_DESC":
	// 			orderBy = append(orderBy, &libdata.OrderByInput{Field: "created_at", Desc: true})
	// 		}
	// 	}
	// } else {
	// 	if !d.Aggregate.Definition().AggregateRoot.DisableDatetime {
	// 		orderBy = append(orderBy, &libdata.OrderByInput{Field: "created_at", Desc: false})
	// 	}
	// }
	// // nolint: dupl
	// if after != nil {
	// 	template, err := d.Getter(ctx, tx, *after, with)
	// 	if err != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't get cursor template")
	// 	}
	// 	firstOrderBy := orderBy[0]

	// 	// nolint: dupl
	// 	switch firstOrderBy.Field {
	// 	case "id":
	// 		filters = append(filters, &libdata.Filter{
	// 			Field: firstOrderBy.Field, Operator: "<", Operande: template.(ObjectInterface).GetID()})
	// 	case libdata.CreatedAtField:
	// 		filters = append(filters, &libdata.Filter{
	// 			Field: firstOrderBy.Field, Operator: "<",
	// Operande: template.(ObjectInterface).GetCreatedAt()})
	// 	case libdata.UpdatedAtField:
	// 		filters = append(filters, &libdata.Filter{
	// 			Field: firstOrderBy.Field, Operator: "<",
	// Operande: template.(ObjectInterface).GetUpdatedAt()})
	// 	default:
	// 		return nil, nil, &libdata.OrderByError{O: firstOrderBy.Field}
	// 	}
	// }

	// records, err := d.Aggregate.RepositoryInterface().Select(
	// 	d, wk, query)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't select templates")
	// }

	// var records []interface{}
	// recordByIds := map[string]interface{}{}
	// switch tx.Type {
	// case libdata.CassandraType:
	// 	templateMaps, errC := rows.(*gocqlx.Iterx).Iter.SliceMap()
	// 	if errC != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't scan to map")
	// 	}
	// 	for _, r := range templateMaps {
	// 		record := d.Newer()
	// 		dest := reflect.ValueOf(record).Elem()
	// 		typeOfDest := dest.Type()
	// 		for i := 0; i < typeOfDest.NumField(); i++ {
	// 			f := dest.Field(i)
	// 			name := typeOfDest.Field(i).Name
	// 			fieldType, _ := dest.Type().FieldByName(name)
	// 			if val, ok := r[fieldType.Tag.Get("db")]; ok {
	// 				if f.IsValid() {
	// 					if f.CanSet() {
	// 						switch f.Kind() {
	// 						case reflect.Int32:
	// 							f.SetInt(int64(val.(int)))
	// 						case reflect.String:
	// 							// fmt.Println("kind", reflect.ValueOf(val).Kind().String())
	// 							switch reflect.ValueOf(val).Kind() {
	// 							case reflect.String:
	// 								f.SetString(val.(string))
	// 							}
	// 						case reflect.Bool:
	// 							f.SetBool(val.(bool))
	// 						case reflect.Ptr:
	// 							if val.(string) != "" {
	// 								v := val.(string)
	// 								f.Set(reflect.ValueOf(&v))
	// 							} else {
	// 								f.Set(reflect.Zero(f.Type()))
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 		fmt.Printf("record cassandra %+v\n", record)
	// 		records = append(records, record)
	// 		if !d.DBInfo.NoID {
	// 			recordByIds[r["id"].(string)] = record
	// 		}
	// 	}
	// nolint: dupl
	// case libdata.PostgresType:
	// for rows.(*sqlx.Rows).Next() {
	// 	record := d.New()
	// 	err = rows.(*sqlx.Rows).StructScan(record)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 		return nil, errors.Wrap(err, "Couldn't scan to templates")
	// 	}
	// 	records = append(records, record)
	// 	recordByIds[record.(ObjectInterface).GetID()] = record
	// 	fmt.Printf("record %+v\n", record)
	// }
	// records, err := d.Scan(rows, nil)
	// if err != nil {
	// 	return nil, err
	// }

	// }

	// tx.CacheInsert(ctx, d.Model, records)

	// if d.TranslamodelFields != nil {
	// 	// recordntIds := utils.MapKeys(recordByIds)

	// 	lang := ctx.Value(LangKey).(string)
	// 	languageDefault, err := SettingGet(ctx, tx, "languageDefault")
	// 	if err != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't get language default")
	// 	}
	// 	if len(recordByIds) > 0 {
	// 		fmt.Println("recordByIds", libutils.MapKeys(recordByIds))
	// 		recordTrs, _, err := d.TranslateModel.Selecter(ctx, tx, []*libdata.Filter{
	// 			{Field: "id", Operator: "IN", Operande: libutils.MapKeys(recordByIds)},
	// 			{Field: "lang", Operator: "IN", Operande: []string{lang, *languageDefault}},
	// 		}, []string{}, nil, nil, nil)
	// 		if err != nil {
	// 			return nil, nil, errors.Wrap(err, "Couldn't get record translations")
	// 		}

	// 		recordTrByRL := map[string]map[string]interface{}{}
	// 		for _, recordTr := range recordTrs {
	// 			rtr := recordTr.(ObjectInterface)
	// 			if _, ok := recordTrByRL[rtr.GetID()]; !ok {
	// 				recordTrByRL[rtr.GetID()] = map[string]interface{}{}
	// 			}
	// 			recordTrByRL[rtr.GetID()][recordTr.(modelTrInterface).GetLang()] = recordTr
	// 		}

	// 		// nolint: dupl
	// 		for _, record := range records {
	// 			d.SelectTrFielder(record, recordTrByRL, lang, *languageDefault)
	// 		}
	// 	}
	// }

	// for _, record := range records {

	// 	// data, err := record.Marshal()
	// 	// if err != nil {
	// 	// 	return nil, err
	// 	// }

	// 	// if d.Definition.Model.CacheClient != nil {
	// 	// err = d.Definition.Repository.SetRecordCache(
	// 	// 	d.Workflow.TenantID, record.GetID(), data)
	// 	// if err != nil {
	// 	// 	return nil, err
	// 	// }
	// 	// }
	// }

	results := &Collection{
		Aggregate: d.Aggregate,
		Workflow:  wk,
	}
	// results.Init(records)
	return results, nil
}
