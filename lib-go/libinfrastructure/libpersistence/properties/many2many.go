package properties

import (
	// "strings"

	"fmt"
	"strings"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// Many2manyPropertyType contains the field type for Many2many
const Many2manyPropertyType = libpersistence.PropertyType("Many2manyType")

/*Many2many is the field type you can use in definition to declare a Many2many field.
 */
type Many2manyDefinition struct {
	*properties.SliceDefinition
	*PersistenceOptions
	ReferenceDefinition libpersistence.TableInterface
	Relation            string
	InverseProperty     string
	TargetProperty      string
	OnDelete            libpersistence.OnDeleteValue
	// WithEntityInDomain  bool
	// LoadedPositionMany2many int
	// PositionMany2many       int
}

func Many2many(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, relation string, inverseProperty string, targetProperty string, onDelete libdomain.OnDeleteValue, withEntity bool, position int, loadedPosition int,  options *properties.IDOptions, persistenceOptions *PersistenceOptions) *Many2manyDefinition {

	if referenceDefinition != nil {
		if referenceName != "" {
			panic(fmt.Sprintf("You can't set a reference name and a reference definition: %s", name))
		}
		referenceName = referenceDefinition.GetName()
	}

	idProperty := properties.ID(name, required, referenceName, referenceDefinition, onDelete, position, loadedPosition, options)
	if  withEntity {
		idProperty = properties.IDWithEntity(name, required, referenceName, referenceDefinition, onDelete, position, loadedPosition, position, loadedPosition, options)
	}
	
	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	return &Many2manyDefinition{
		SliceDefinition: properties.Slice(idProperty, targetProperty ),
		PersistenceOptions: persistenceOptions,
		Relation: relation,
		InverseProperty: inverseProperty,
		TargetProperty: targetProperty,
	}	
}

// // GetName return the name of the field.
// func (f *Many2many) GetName() string {
// 	return f.Name
// }

// // Title return the title of the field.
// func (f *Many2many) Title() string {
// 	titleName := strcase.ToCamel(f.Name)
// 	if f.TitleName != "" {
// 		titleName = f.TitleName
// 	}
// 	return titleName
// }

// // Title return the title of the field.
// func (f *Many2many) NameWithoutID() string {
// 	return strings.Replace(f.Name, "Id", "", -1)
// }

// // Title return the title of the field.
// func (f *Many2many) TitleWithoutID() string {
// 	return strings.Replace(f.Title(), "Id", "", -1)
// }

// // Snake return the name of the field, in snake_case. This is essentially used for database.
// func (f *Many2many) Snake() string {
// 	dbName := strcase.ToSnake(f.Name)
// 	if f.DBName != "" {
// 		dbName = f.DBName
// 	}
// 	return dbName
// }

// func (f *Many2many) Upper() string {
// 	return strings.ToUpper(f.Snake())
// }

// // Type return the type of the field.
func (f *Many2manyDefinition) Type() libpersistence.PropertyType {
	return Many2manyPropertyType
}

// // Type return the type of the field.
func (f *Many2manyDefinition) GoType() string {
	return libpersistence.STRING
}

// // Type return the type of the field.
func (f *Many2manyDefinition) GoTypeID() string {
	return f.GoType()
}

// // Type return the type of the field.
// func (f *Many2many) GoNil() string {
// 	return "nil"
// }

// // Type return the type of the field.
// func (f *Many2many) JSONType() string {
// 	return f.GoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Many2many) ProtoType() string {
// 	return f.GoType()
// }

// func (f *Many2many) ProtoTypeArg() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Many2many) ProtoTypeOptional() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
func (f *Many2manyDefinition) DBType() *libpersistence.DBType {
	return nil
}

// // Type return the type of the field.
// func (f *Many2many) DiffTypeInterface() string {
// 	return "libpersistence.TextDiffInterface"
// }

// // Type return the type of the field.
// func (f *Many2many) GraphqlType() string {
// 	return f.GetReference().Title()
// }

// // Type return the type of the field.
// func (f *Many2many) GraphqlSchemaType() string {
// 	return f.GetReference().Title()
// }

// // GetReference return the name of referenced model, if this field is linked to another model.
// func (f *Many2many) GetReferenceName() string {
// 	return f.Reference
// }

// // GetReferenceDefinition return the definition of referenced model,
// // if this field is linked to another model.
func (f *Many2manyDefinition) GetReference() libpersistence.TableInterface {
	return f.ReferenceDefinition
}

// // SetReferenceDefinition set the definition of referenced model,
// // if this field is linked to another model.
func (f *Many2manyDefinition) SetReference(d libpersistence.TableInterface) {
	f.ReferenceDefinition = d
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) RelationTitle() string {
	return strcase.ToCamel(f.Relation)
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) RelationUpper() string {
	return strings.ToUpper(strcase.ToSnake(f.Relation))
}

// // GetInverseProperty return the inverse field, if applicable.
// func (f *Many2many) GetInverseProperty() string {
// 	return f.InverseProperty
// }

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) InverseTitle() string {
	return strcase.ToCamel(f.InverseProperty)
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) InverseSnake() string {
	return strcase.ToSnake(f.InverseProperty)
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) InverseUpper() string {
	return strings.ToUpper(f.InverseSnake())
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) TargetTitle() string {
	return strcase.ToCamel(f.TargetProperty)
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *Many2manyDefinition) TargetSnake() string {
	return strcase.ToSnake(f.TargetProperty)
}

// // GetRequired return if this field is required or not.
// func (f *Many2many) GetRequired() bool {
// 	return false
// }

// // GetPrimaryKey return if this field is a primary key or not.
// func (f *Many2many) GetPrimaryKey() bool {
// 	return false
// }

// func (f *Many2many) GetWithEntityInDomain() bool {
// 	return f.WithEntityInDomain
// }

func (f *Many2manyDefinition) IsStored() bool {
	return false
}

func (f *Many2manyDefinition) IsNested() bool {
	return true
}

// //GetFieldData return the field information in a format exploimodel by templates.
// // func (f *Many2many) GetFieldData() *libpersistence.FieldData {

// // 	// var reference *ReferenceData
// // 	// referenceDefinition := f.GetReferenceDefinition()
// // 	// if referenceDefinition != nil {
// // 	// 	reference = &ReferenceData{}
// // 	// 	if referenceDefinition != f.Model {
// // 	// 		reference = &ReferenceData{
// // 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// // 	// 			Title: referenceDefinition.GetTemplateData().Title,
// // 	// 		}
// // 	// 	} else {
// // 	// 		reference = &ReferenceData{
// // 	// 			Name:  result.Name,
// // 	// 			Title: result.Title,
// // 	// 		}
// // 	// 	}
// // 	// }

// // 	return &libpersistence.FieldData{
// // 		Name:            f.Name,
// // 		NameWithoutID: f.Name,
// // 		// Type:             Many2manyPropertyType.GoType(),
// // 		// GoType:           Many2manyPropertyType.GoType(),
// // 		// ProtoType:        Many2manyPropertyType.ProtoType(),
// // 		Title:            f.Title(),
// // 		TitleWithoutID: f.Title(),
// // 		LesserTitle:      strings.Title(f.GetName()),
// // 		Snake:            f.Snake(),
// // 		Required:         false,
// // 		Nested:           true,
// // 		InverseProperty:     "",
// // 		// Definition:       f,
// // 	}
// // }

// func (f *Many2many) SetPosition(position int) {
// 	f.Position = position
// }

// func (f *Many2many) GetPosition() int {
// 	return f.Position
// }

// func (r *Many2many) IsRepeated() bool {
// 	return false
// }

// func (r *Many2many) IsTranslatable() bool {
// 	return false
// }

// func (r *Many2many) IsIndexed() bool {
// 	return false
// }

// func (r *Many2many) GetReturnDetailsInTests() bool {
// 	return false
// }

// func (r *Many2many) GetLoadedPosition() int {
// 	return r.LoadedPosition
// }
// func (r *Many2many) GetLoadedPositionMany2one() int {
// 	return 0
// }
// func (r *Many2many) GetPositionMany2one() int {
// 	return 0
// }
// func (r *Many2many) GetExternalDatasource() libpersistence.DatasourceClient {
// 	return r.ExternalDatasource
// }
// func (r *Many2many) GetExcludeFromInterface() bool {
// 	return r.ExcludeFromInterface
// }
