package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// JSONPropertyType contains the field type for JSON
const JSONPropertyType = libpersistence.PropertyType("JSONType")

/*JSON is the field type you can use in definition to declare a JSON field.
 */
type JSONDefinition struct {
	*properties.MapDefinition
	*PersistenceOptions
}

func JSON(name string, required bool, position int, loadedPosition int,  options *properties.MapOptions, persistenceOptions *PersistenceOptions) *JSONDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &JSONDefinition{
		MapDefinition: properties.Map(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *JSONDefinition) Type() libpersistence.PropertyType {
	return JSONPropertyType
}

func (f *JSONDefinition) ProtoType() string {
	return "google.protobuf.Struct"
}

func (f *JSONDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *JSONDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}
func (f *JSONDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}
func (f *JSONDefinition) DiffTypeInterface() string {
	return "libpersistence.TextDiffInterface"
}
