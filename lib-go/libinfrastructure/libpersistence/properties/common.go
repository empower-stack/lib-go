package properties

import (
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

type PersistenceOptions struct {	
	ExternalDatasource libpersistence.DatasourceClient
	ExcludeFromInterface bool
	Index bool
}

func (r *PersistenceOptions) GetExternalDatasource() libpersistence.DatasourceClient {
	if r == nil {
		return nil
	}
	return r.ExternalDatasource
}
func (r *PersistenceOptions) GetExcludeFromInterface() bool {
	if r == nil {
		return false
	}
	return r.ExcludeFromInterface
}
func (r *PersistenceOptions) IsIndexed() bool {
	if r == nil {
		return false
	}
	return r.Index
}
func (f *PersistenceOptions) GetReference() libpersistence.TableInterface {
	return nil
}
func (f *PersistenceOptions) SetReference(d libpersistence.TableInterface) {}