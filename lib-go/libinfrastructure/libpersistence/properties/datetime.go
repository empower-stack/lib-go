package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// DatetimePropertyType contains the field type for Datetime
const DatetimePropertyType = libpersistence.PropertyType("DatetimeType")

/*Datetime is the field type you can use in definition to declare a Datetime field.
 */
type DatetimeDefinition struct {
	*properties.DatetimeDefinition
	*PersistenceOptions
}

func Datetime(name string, required bool, position int, loadedPosition int,  options *properties.DatetimeOptions, persistenceOptions *PersistenceOptions) *DatetimeDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &DatetimeDefinition{
		DatetimeDefinition: properties.Datetime(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *DatetimeDefinition) Type() libpersistence.PropertyType {
	return DatetimePropertyType
}

func (f *DatetimeDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

func (f *DatetimeDefinition) DiffTypeInterface() string {
	return "*libdomain.TimeDiff"
}