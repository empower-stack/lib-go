package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// FloatPropertyType contains the field type for Float
const FloatPropertyType = libpersistence.PropertyType("FloatType")

/*Float is the field type you can use in definition to declare a Float field.
 */
type FloatDefinition struct {
	*properties.FloatDefinition
	*PersistenceOptions
}

func Float(name string, required bool, position int, loadedPosition int,  options *properties.FloatOptions, persistenceOptions *PersistenceOptions) *FloatDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &FloatDefinition{
		FloatDefinition: properties.Float(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *FloatDefinition) Type() libpersistence.PropertyType {
	return FloatPropertyType
}
func (f *FloatDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullFloat64",
		Value: "Float64",
	}
}

func (f *FloatDefinition) DiffTypeInterface() string {
	return "*libdomain.FloatDiff"
}
