package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// TextPropertyType contains the field type for Text
const TextPropertyType = libpersistence.PropertyType("TextType")

/*Text is the field type you can use in definition to declare a Text field.
 */
type TextDefinition struct {
	*properties.TextDefinition
	*PersistenceOptions
	// LoadedTermsPosition int
	// TermsPosition       int
	// datasource          libpersistence.DatasourceClient
}

func Text(name string, required bool, position int, loadedPosition int,  options *properties.TextOptions, persistenceOptions *PersistenceOptions) *TextDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	return &TextDefinition{
		TextDefinition: properties.Text(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}
func TextTranslatable(name string, required bool, position int, loadedPosition int,  options *properties.TextOptions, persistenceOptions *PersistenceOptions) *TextDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	return &TextDefinition{
		TextDefinition: properties.TextTranslatable(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}
func TextPatch(name string, required bool, position int, loadedPosition int,  options *properties.TextOptions, persistenceOptions *PersistenceOptions) *TextDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	return &TextDefinition{
		TextDefinition: properties.TextPatch(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}
func TextWYSIWYG(name string, required bool, patch bool, position int, loadedPosition int, positionTerms int, loadedPositionTerms int,  options *properties.TextOptions, persistenceOptions *PersistenceOptions) *TextDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &TextDefinition{
		TextDefinition: properties.TextWYSIWYG(name, required, patch, position, loadedPosition, positionTerms, loadedPositionTerms, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *TextDefinition) Type() libpersistence.PropertyType {
	return TextPropertyType
}

func (f *TextDefinition) ProtoType() string {
	if f.Translatable == properties.TranslatableSimple {
		return "map<string, string>"
	}
	return f.GoType()
}

func (f *TextDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *TextDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}
func (f *TextDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

func (f *TextDefinition) DiffTypeInterface() string {
	if f.Translatable == properties.TranslatableSimple {
		return "map[string]*libdomain.TextDiff"
	}
	if f.Patch {
		return "*libdomain.TextPatchDiff"
	}
	return "*libdomain.TextDiff"
}
