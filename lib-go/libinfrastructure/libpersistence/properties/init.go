package properties

import "gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"

func init() {
	libpersistence.AddEntitiesProperties = func(e *libpersistence.TableDefinition) {
		if e.ExternalDatasource == nil && !e.DisableDatetime {
			e.Properties = append(e.Properties, Boolean( "archived", false,  1001, 1002, nil, nil))
			e.Properties = append(e.Properties, Datetime( "createdAt", true, 1003, 1004, nil, nil))
			e.Properties = append(e.Properties, Text( "createdBy",false, 1005, 1006, nil, nil))
			e.Properties = append(e.Properties, Datetime( "updatedAt", true, 1007, 1008, nil, nil))
			e.Properties = append(e.Properties, Text( "updatedBy",false,   1009, 1010, nil, nil))
			e.Properties = append(e.Properties, Datetime( "archivedAt", false, 1011, 1012, nil, nil))
			e.Properties = append(e.Properties, Text( "archivedBy", false, 1013, 1014, nil, nil))
			if e.IsAggregateRoot() {
				e.Properties = append(e.Properties, Integer( "version", false, 1015, 1016, nil, nil))
			}
		}
	}
}
