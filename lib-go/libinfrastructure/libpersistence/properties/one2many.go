package properties

import (
	// "strings"

	"fmt"
	"strings"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// One2manyPropertyType contains the field type for One2many
const One2manyPropertyType = libpersistence.PropertyType("One2manyType")

/*One2many is the field type you can use in definition to declare a One2many field.
 */
type One2manyDefinition struct {
	*properties.OrderedMapDefinition
	*PersistenceOptions
	InverseProperty    string
	ChildDefinition    libpersistence.TableInterface
}

func One2many(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, inverseProperty string, position int, loadedPosition int,  options *properties.EntityOptions, persistenceOptions *PersistenceOptions) *One2manyDefinition {

	if referenceDefinition != nil {
		if referenceName != "" {
			panic(fmt.Sprintf("You can't set a reference name and a reference definition: %s", name))
		}
		referenceName = referenceDefinition.GetName()
	}
	
	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	return &One2manyDefinition{
		OrderedMapDefinition: properties.OrderedMap(properties.Entity(name, required, referenceName, referenceDefinition, position, loadedPosition, options), inverseProperty),
		PersistenceOptions: persistenceOptions,
	}	
}

// // GetName return the name of the field.
// func (f *One2many) GetName() string {
// 	return f.Name
// }

// // Title return the title of the field.
// func (f *One2many) Title() string {
// 	titleName := strcase.ToCamel(f.Name)
// 	if f.TitleName != "" {
// 		titleName = f.TitleName
// 	}
// 	return titleName
// }

// // Title return the title of the field.
// func (f *One2many) NameWithoutID() string {
// 	return f.Name
// }

// // Title return the title of the field.
// func (f *One2many) TitleWithoutID() string {
// 	return f.Title()
// }

// // Snake return the name of the field, in snake_case. This is essentially used for database.
// func (f *One2many) Snake() string {
// 	dbName := strcase.ToSnake(f.Name)
// 	if f.DBName != "" {
// 		dbName = f.DBName
// 	}
// 	return dbName
// }

// func (f *One2many) Upper() string {
// 	return strings.ToUpper(f.Snake())
// }

// // Type return the type of the field.
func (f *One2manyDefinition) Type() libpersistence.PropertyType {
	return One2manyPropertyType
}

// // Type return the type of the field.
func (f *One2manyDefinition) GoType() string {
	return libpersistence.STRING
}

// // Type return the type of the field.
func (f *One2manyDefinition) GoTypeID() string {
	return f.GoType()
}

// // Type return the type of the field.
// func (f *One2many) GoNil() string {
// 	return "nil"
// }

// // Type return the type of the field.
// func (f *One2many) JSONType() string {
// 	return f.GoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *One2many) ProtoType() string {
// 	return f.GoType()
// }

// func (f *One2many) ProtoTypeArg() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *One2many) ProtoTypeOptional() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
func (f *One2manyDefinition) DBType() *libpersistence.DBType {
	return nil
}

// // Type return the type of the field.
// func (f *One2many) DiffTypeInterface() string {
// 	return "libpersistence.TextDiffInterface"
// }

// // Type return the type of the field.
// func (f *One2many) GraphqlType() string {
// 	return f.GetReference().Title()
// }

// // Type return the type of the field.
// func (f *One2many) GraphqlSchemaType() string {
// 	return f.GetReference().Title()
// }

// // func (f *One2many) GoType() string {
// // 	reference := ""
// // 	referenceDefinition := f.GetReferenceDefinition()
// // 	if referenceDefinition != nil {
// // 		reference = referenceDefinition.GetTemplateData().Title
// // 	}
// // 	return reference
// // }

// // GetReference return the name of referenced model, if this field is linked to another model.
// func (f *One2many) GetReferenceName() string {
// 	return f.Child
// }

// // GetReferenceDefinition return the definition of referenced model,
// // if this field is linked to another model.
func (f *One2manyDefinition) GetReference() libpersistence.TableInterface {
	return f.ChildDefinition
}

// // SetReferenceDefinition set the definition of referenced model,
// // if this field is linked to another model.
func (f *One2manyDefinition) SetReference(d libpersistence.TableInterface) {
	f.ChildDefinition = d
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *One2manyDefinition) GetInverseProperty() string {
	return strcase.ToCamel(f.OrderedMapDefinition.InverseProperty)
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *One2manyDefinition) InversePropertySnake() string {
	return strcase.ToSnake(f.OrderedMapDefinition.InverseProperty)
}

// // GetInverseProperty return the inverse field, if applicable.
func (f *One2manyDefinition) InversePropertyUpper() string {
	return strings.ToUpper(f.InversePropertySnake())
}

// // GetRequired return if this field is required or not.
// func (f *One2many) GetRequired() bool {
// 	return false
// }

// // GetPrimaryKey return if this field is a primary key or not.
// func (f *One2many) GetPrimaryKey() bool {
// 	return false
// }

// func (f *One2many) GetWithEntityInDomain() bool {
// 	return false
// }

func (f *One2manyDefinition) IsStored() bool {
	return false
}

func (f *One2manyDefinition) IsNested() bool {
	return true
}

// //GetFieldData return the field information in a format exploimodel by templates.
// // func (f *One2many) GetFieldData() *libpersistence.FieldData {

// // 	// var reference *ReferenceData
// // 	// referenceDefinition := f.GetReferenceDefinition()
// // 	// if referenceDefinition != nil {
// // 	// 	reference = &ReferenceData{}
// // 	// 	if referenceDefinition != f.Model {
// // 	// 		reference = &ReferenceData{
// // 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// // 	// 			Title: referenceDefinition.GetTemplateData().Title,
// // 	// 		}
// // 	// 	} else {
// // 	// 		reference = &ReferenceData{
// // 	// 			Name:  result.Name,
// // 	// 			Title: result.Title,
// // 	// 		}
// // 	// 	}
// // 	// }

// // 	return &libpersistence.FieldData{
// // 		Name:            f.Name,
// // 		NameWithoutID: f.Name,
// // 		// Type:             One2manyPropertyType.GoType(),
// // 		// GoType:           One2manyPropertyType.GoType(),
// // 		// ProtoType:        One2manyPropertyType.ProtoType(),
// // 		Title:            f.Title(),
// // 		TitleWithoutID: f.Title(),
// // 		LesserTitle:      strings.Title(f.GetName()),
// // 		Snake:            f.Snake(),
// // 		Required:         false,
// // 		Nested:           true,
// // 		InverseProperty:     strings.Title(f.InverseProperty),
// // 		// Definition:       f,
// // 	}
// // }

// func (f *One2many) SetPosition(position int) {
// 	f.Position = position
// }

// func (f *One2many) GetPosition() int {
// 	return f.Position
// }

// func (r *One2many) IsRepeated() bool {
// 	return false
// }

// func (r *One2many) IsTranslatable() bool {
// 	return false
// }

// func (r *One2many) IsIndexed() bool {
// 	return false
// }

// func (r *One2many) GetReturnDetailsInTests() bool {
// 	return false
// }

// func (r *One2many) GetLoadedPosition() int {
// 	return r.LoadedPosition
// }
// func (r *One2many) GetLoadedPositionMany2one() int {
// 	return 0
// }
// func (r *One2many) GetPositionMany2one() int {
// 	return 0
// }
// func (r *One2many) GetExternalDatasource() libpersistence.DatasourceClient {
// 	return r.ExternalDatasource
// }
// func (r *One2many) GetExcludeFromInterface() bool {
// 	return r.ExcludeFromInterface
// }
