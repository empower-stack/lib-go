package generator

import (
	"bytes"
	"fmt"
	"strings"

	"io/ioutil"
	"os"

	"embed"

	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/properties"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:generate protoc -I=. -I=$GOPATH/src --go_out=$GOPATH/src baserepositorypb/main.proto

//go:embed proto.go.tmpl
var fproto embed.FS

// Protos will generate the protobuf files for the models.
func Protos(defs *libpersistence.Definitions) {

	var err error
	file, _ := fproto.ReadFile("proto.go.tmpl")
	protoTemplate := template.Must(protoFuncs.Parse(string(file)))

	d := struct {
		Repository string
		// UseTenants      bool
		Aggregates      []*libpersistence.AggregateDefinition
		EntityPositions map[string]int
		Import          string
	}{
		Repository: defs.Repository,
		// UseTenants: defs.UseTenants,
	}

	imports := map[string]string{}
	for _, definition := range defs.Slice() {

		d.Aggregates = append(d.Aggregates, definition)

		for _, entity := range definition.GetEntities() {
			for _, property := range entity.Properties {
				switch v := property.(type) {
				case *properties.Many2oneDefinition:
					if v.GetReference().DomainPath() != defs.Repository {
						path := "import \"" + v.GetReference().DomainPath() + "/gen/pb/main.proto\";"
						imports[path] = path
					}
				case *properties.JSONDefinition:
					path := "import \"google/protobuf/struct.proto\";"
					imports[path] = path
				}
			}
		}
		for _, request := range definition.GetCustomRequests() {
			for _, result := range request.Results {
				if result.GetReference() != nil {
					if result.GetReference().DomainPath() != defs.Repository {
						path := "import \"" + result.GetReference().DomainPath() + "/gen/pb/main.proto\";"
						imports[path] = path
					}
				}
			}
		}

	}

	d.Import = strings.Join(libutils.MapKeys(imports), "\n")

	entityPositions := map[string]int{}
	for _, a := range d.Aggregates {
		for _, entity := range a.GetEntities() {
			position := 3
			for _, property := range entity.StoredProperties() {
				// property.SetPosition(position)

				position = position + 1
				if property.Type() == properties.Many2onePropertyType {
					position = position + 1
				}
				if property.IsTranslatable() {
					position = position + 1
				}
			}

			entityPositions[entity.Name] = position
		}
		for _, valueObject := range a.ValueObjects {
			position := 3
			for _, property := range valueObject.StoredProperties() {
				// property.SetPosition(position)

				position = position + 1
				if property.Type() == properties.Many2onePropertyType {
					position = position + 1
				}
				if property.IsTranslatable() {
					position = position + 1
				}
			}
			for _, property := range valueObject.NestedProperties() {
				// property.SetPosition(position)

				position = position + 1
				if property.Type() == properties.Many2onePropertyType {
					position = position + 1
				}
				if property.IsTranslatable() {
					position = position + 1
				}
			}

			entityPositions[valueObject.Name] = position
		}
	}
	d.EntityPositions = entityPositions

	err = os.MkdirAll("./gen/pb", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf := &bytes.Buffer{}
	err = protoTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("model proto", err)
	}
	err = ioutil.WriteFile(
		"./gen/pb/main.proto",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

}

// nolint:lll
var protoFuncs = template.New("").Funcs(template.FuncMap{
	"add": func(i int, j int) int {
		return i + j
	},
	"position": func(entityPositions map[string]int, entity libpersistence.TableInterface, j int) int {
		return entityPositions[entity.GetName()] + j
	},
	"getProperty": func(table libpersistence.TableInterface, property libpersistence.Property, i int, j int) string {

		var content string

		if property.GetRequired() {
			content = `{{.Property.ProtoTypeArg}} {{.Property.GetName}} = {{.Position}};`
		} else {
			if table.GetUseOneOf() {
				content = `{ {oneof has{{.Property.Title}} { {{.Property.ProtoType}} {{.Property.GetName}} = {{.Position}};}`
			} else {
				content = `{{.Property.ProtoTypeArg}} {{.Property.GetName}} = {{.Position}};`
			}
		}

		positionDefault := property.GetPosition() + 1

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Property               libpersistence.Property
			Position               int
			PositionMany2one       int
			LoadedPositionMany2one int
			PositionDefault        int
		}{
			Property:               property,
			Position:               property.GetPosition(),
			PositionMany2one:       property.GetPositionEntity(),
			LoadedPositionMany2one: property.GetLoadedPositionEntity(),
			PositionDefault:        positionDefault,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
})
