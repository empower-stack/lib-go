package libpersistence

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

type Event struct {
	ID          string
	Aggregate   AggregateInterface
	AggregateID string
	Name        string
	Payload     map[string]interface{}
}

type EventStoreClient interface {
	RegisterEvents(libdomain.ApplicationContext, AggregateInterface, []*Event) error
	MarkDispatchedEvent(libdomain.ApplicationContext, *Event) error
	GetAggregateEvents(libdomain.ApplicationContext, *libdomain.AggregateDefinition, string, string) ([]*Event, error)
}

type EventDispatchClient interface {
	DispatchEvent(
		// wk *Workflow,
		event *Event) error
}
