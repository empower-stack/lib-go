package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/arguments"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/librepository"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed domain-*.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Domains(defs *librepository.Definitions) {

	var err error
	fileInit, _ := fdomain.ReadFile("domain-init.go.tmpl")
	initTemplate := template.Must(initFuncs.Parse(string(fileInit)))

	file, _ := fdomain.ReadFile("domain-main.go.tmpl")
	domainTemplate := template.Must(aggregateFuncs.Parse(string(file)))

	event := false
	// if defs.EventClient != nil {
	// 	event = true
	// }

	err = os.MkdirAll("../gen", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	var aggregates []*libpersistence.AggregateDefinition
	for _, definition := range defs.Slice() {

		d := struct {
			Path       string
			Event      bool
			Repository *librepository.RepositoryDefinition
			Import string
		}{
			Path:       defs.Repository,
			Event:      event,
			Repository: definition,
		}

		imports := map[string]string{}
		for _, table := range definition.Aggregate.GetObjects() {
			path := table.DomainPackage() + "identities \"" + table.DomainPath() + "/identities\""
			imports[path] = path
			path = table.DomainPackage() + " \"" + table.DomainPath() + "\""
			imports[path] = path
			for _, property := range table.GetProperties() {
				switch v := property.(type) {
				case *properties.OrderedMapDefinition:
					if v.GetReference().DomainPath() != "" {
						path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
						imports[path] = path
						path = v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
						imports[path] = path
					}
				case *properties.SliceDefinition:
					switch vv := v.PropertyDefinition.(type) {
					case *properties.IDDefinition:
						if vv.GetReference().DomainPath() != "" {
							if vv.WithEntity {
								path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
								imports[path] = path
							}
						}
					}					
				case *properties.ValueObjectDefinition:
					if v.GetReference().DomainPath() != "" {
						path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
						imports[path] = path
					}
					for _, property2 := range v.GetReference().GetProperties() {
						switch vv := property2.(type) {
						case *properties.OrderedMapDefinition:
							if vv.GetReference().DomainPath() != "" {
								// path := vv.GetReference().DomainPackage() + "identities \"" + vv.GetReference().DomainPath() + "/identities\""
								// imports[path] = path
								// path = vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
								// imports[path] = path
							}
						case *properties.IDDefinition:
							if vv.GetReference().DomainPath() != "" {
								path := vv.GetReference().DomainPackage() + "identities \"" + vv.GetReference().DomainPath() + "/identities\""
								imports[path] = path								
								// if vv.WithEntity {
								// 	path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
								// 	imports[path] = path
								// }
							}
						// case *properties.ValueObjectDefinition:
						// 	if vv.GetReference().DomainPath() != "" {
						// 		path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
						// 		imports[path] = path
						// 	}
						}
					}
				case *properties.IDDefinition:
					if v.GetReference().DomainPath() != "" && table.IsEntity() {
						path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
						imports[path] = path
						// if v.WithEntity {
						// 	path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
						// 	imports[path] = path
						// }
					}
				case *properties.SelectionDefinition:
					if v.GetReference().DomainPath() != "" {
						path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
						imports[path] = path
					}
				}
			}
		}
		for _, function := range definition.Aggregate.RepositoryFunctions {
			for _, property := range function.Args {
				switch v := property.(type) {
				case *arguments.EntityDefinition, *arguments.GetPropertiesDefinition:
					if v.GetReference().DomainPath() != "" {
						path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
						imports[path] = path
					}
				case *arguments.IDDefinition:
					if v.GetReference().DomainPath() != "" {
						path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
						imports[path] = path
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		buf := &bytes.Buffer{}
		err := domainTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("../gen/%s.gen.go", strcase.ToSnake(definition.Name())),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

	}

	d := struct {
		Repository string
		Aggregates []*libpersistence.AggregateDefinition
	}{
		Repository: defs.Repository,
		Aggregates: aggregates,
	}
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("../gen/init.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

}

var initFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

func generatePropertyPersistenceInterface(prefix string, property libdomain.PropertyDefinition) string {

	title := prefix + property.Title()
	titleWithoutID := prefix + property.TitleWithoutID()

	var content string

	if property.CollectionType() == properties.OrderedMapPropertyType {
		content = `{{.Title}}() []{{.Property.GetReference.Title}}EntityPersistenceInterface
			{{.Title}}Loaded() bool	
			`
	} else if property.CollectionType() == properties.SlicePropertyType {
		if property.Type() == properties.IDPropertyType {
			content = `{{.Title}}String() []string
				{{.Title}}Loaded() bool
				{{- if .Property.GetWithEntity}}
					{{.TitleWithoutID}}() []{{.Property.GetReference.Title}}EntityPersistenceInterface
					{{.TitleWithoutID}}Loaded() bool
				{{- end}}
				`
		} else {
			content = `{{.Title}}() []string
				{{.Title}}Loaded() bool
				`
		}
	} else {
		if property.Type() == properties.EntityPropertyType {
			content = `{{.Title}}() {{.Property.GetReference.Title}}EntityPersistenceInterface
			{{.Title}}Loaded() bool
			`
		} else if property.Type() == properties.ValueObjectPropertyType {
			content = `{{.Title}}Loaded() bool
			`
			for _, p := range property.GetReference().GetProperties() {
				content += generatePropertyPersistenceInterface(prefix + property.Title(), p)
			}
		} else if property.Type() == properties.IDPropertyType {
			content = `{{.Title}}String() string
				{{.Title}}Loaded() bool
				{{- if .Property.GetWithEntity}}
					{{.TitleWithoutID}}() {{.Property.GetReference.Title}}EntityPersistenceInterface
					{{.TitleWithoutID}}Loaded() bool
				{{- end}}
				`
		} else if property.Type() == properties.SelectionPropertyType {
			content = `{{.Title}}() string
				{{.Title}}Loaded() bool
				`
		} else {
			content = `{{.Title}}() {{.Property.GoTypeWithPackage}}
				{{.Title}}Loaded() bool
				`
		}
	}

	if property.Type() == properties.TextPropertyType && property.GetTranslatable() == properties.TranslatableWYSIWYG {
		content += `{{.Title}}WYSIWYG() *libdomain.WYSIWYG
			{{.Title}}WYSIWYGLoaded() bool
			`
	}

	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		Title            string
		TitleWithoutID   string
		Property 		libdomain.PropertyDefinition
	}{
		Title:            title,
		TitleWithoutID:   titleWithoutID,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()
}

func generatePropertyConversionToDomain(inputName string, entityName string, prefix string, property libdomain.PropertyDefinition) string {

	name := prefix + property.GetName()
	nameWithoutID := prefix + property.NameWithoutID()
	title := prefix + property.Title()
	titleWithoutID := prefix + property.TitleWithoutID()

	var content string

	if property.CollectionType() == properties.OrderedMapPropertyType {
		content = `//var {{.Name}}Ids []{{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID
			var {{.Name}} []*{{.Property.GetReference.DomainPackage}}.{{.Property.GetReference.Title}}Data
			for _, {{.Property.GetReference.Name}} := range {{.EntityName}}Persistence.{{.Title}}() {
				{{if .Property.GetReference.IsAggregateRoot}}_,{{end}} {{.Property.GetReference.Name}}ID, {{.Property.GetReference.Name}}Domain{{if .Property.GetReference.IsAggregateRoot}}, _{{end}} := Convert{{.Property.GetReference.Title}}ToDomainImplementation({{.Property.GetReference.Name}})
				//{{.Name}}Ids = append({{.Name}}Ids, {{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID({{.Property.GetReference.Name}}ID))
				{{.Property.GetReference.Name}}Domain.ID = {{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID({{.Property.GetReference.Name}}ID)
				{{.Name}} = append({{.Name}}, {{.Property.GetReference.Name}}Domain)
			}
			//{{.InputName}}Data.{{.Property.Title}}Ids = {{.Name}}Ids
			{{.InputName}}Data.{{.Property.Title}} = {{.Name}}
			`
	} else if property.CollectionType() == properties.SlicePropertyType {
		if property.Type() == properties.IDPropertyType {
			content = `var {{.Name}} []{{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID
			for _, {{.Property.GetReference.Name}} := range {{.EntityName}}Persistence.{{.Title}}String() {
				{{.Name}} = append({{.Name}}, {{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID({{.Property.GetReference.Name}}))
			}
			{{.InputName}}Data.{{.Property.Title}} = {{.Name}}
			`
		} else {
			content = `{{.InputName}}Data.{{.Property.Title}} = {{.EntityName}}Persistence.{{.Title}}()
				`
		}
	} else {
		if property.Type() == properties.EntityPropertyType {
			content = `{{.InputName}}Data.{{.Property.Title}} = Convert{{.Property.GetReference.Title}}ToDomain({{.EntityName}}Persistence.{{.Title}}())
				`
		} else if property.Type() == properties.ValueObjectPropertyType {
			content = `{{.Property.GetReference.Name}}Data := &{{.Property.GetReference.DomainPackage}}.{{.Property.GetReference.Title}}Data{}
			`
			for _, p := range property.GetReference().GetProperties() {
				content += generatePropertyConversionToDomain(property.GetReference().GetName(), entityName, prefix + property.Title(), p)
			}
			content += `{{.InputName}}Data.{{.Property.Title}} = {{.Property.GetReference.Name}}Data
			`
		} else if property.Type() == properties.IDPropertyType {
			content = `{{.InputName}}Data.{{.Property.Title}} = {{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID({{.EntityName}}Persistence.{{.Title}}String())
				`
		} else if property.Type() == properties.SelectionPropertyType {
			content = `{{.Name}}, err := {{.Property.GetReference.DomainPackage}}.Select{{.Property.GetReference.Title}}({{.EntityName}}Persistence.{{.Title}}())
			if err != nil {
				panic(err)
			}
			{{.InputName}}Data.{{.Property.Title}} = {{.Name}}
			`
		} else {
			content = `{{.InputName}}Data.{{.Property.Title}} = {{.EntityName}}Persistence.{{.Title}}()
				`
		}
	}

	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		InputName 	 string
		EntityName 	 string
		Name            string
		NameWithoutID   string
		Title            string
		TitleWithoutID   string
		Property 		libdomain.PropertyDefinition
	}{
		InputName: inputName,
		EntityName: entityName,
		Name:            name,
		NameWithoutID:   nameWithoutID,
		Title:            title,
		TitleWithoutID:   titleWithoutID,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	contentProperty := buf.String()

	content = `if {{.EntityName}}Persistence.{{.Title}}Loaded() {
			{{.ContentProperty}}
			{{.InputName}}Data.{{.Property.Title}}IsLoaded = true
		}
		{{- if and (eq .Property.Type "IDType") .Property.GetWithEntity}}
		if {{.EntityName}}Persistence.{{.TitleWithoutID}}Loaded() {
			{{- if eq .Property.CollectionType "Slice"}}
			var {{.NameWithoutID}} []*{{.Property.GetReference.DomainPackage}}.{{.Property.GetReference.Title}}
			for _, {{.Property.GetReference.Name}} := range {{.EntityName}}Persistence.{{.TitleWithoutID}}() {
				{{.NameWithoutID}} = append({{.NameWithoutID}}, Convert{{.Property.GetReference.Title}}ToDomain({{.Property.GetReference.Name}}))
			}
			{{.InputName}}Data.{{.Property.TitleWithoutID}} = {{.NameWithoutID}}
			{{- else}}
			if {{.EntityName}}Persistence.{{.TitleWithoutID}}() != nil {
				{{.InputName}}Data.{{.Property.TitleWithoutID}} = Convert{{.Property.GetReference.Title}}ToDomain({{.EntityName}}Persistence.{{.TitleWithoutID}}())
			}
			{{- end}}
			{{.InputName}}Data.{{.Property.TitleWithoutID}}IsLoaded = true
		}
		{{- end}}
		{{- if and (eq .Property.Type "TextType") (eq .Property.GetTranslatable "TranslatableWYSIWYG")}}
		if {{.EntityName}}Persistence.{{.Title}}WYSIWYGLoaded() {
			{{.InputName}}Data.{{.Property.Title}}WYSIWYG = {{.EntityName}}Persistence.{{.Title}}WYSIWYG()
			{{.InputName}}Data.{{.Property.Title}}WYSIWYGIsLoaded = true
		}
		{{- end}}
		`

	buf = &bytes.Buffer{}
	err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
		InputName 	 string
		EntityName		string
		ContentProperty 	 string
		Name			string
		NameWithoutID	string
		Title			string
		TitleWithoutID	string
		Property 		libdomain.PropertyDefinition
	}{
		InputName: inputName,
		EntityName: entityName,
		ContentProperty: contentProperty,
		Name:            name,
		NameWithoutID:   nameWithoutID,
		Title:            title,
		TitleWithoutID:   titleWithoutID,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()


}

// nolint:lll
var aggregateFuncs = template.New("").Funcs(template.FuncMap{
	"generateDeletedEvent": func(aggregate *libdomain.AggregateDefinition, entity *libdomain.EntityDefinition) bool {
		for _, event := range aggregate.Events {
			if event.Name == entity.Title()+"Deleted" {
				return false
			}
		}
		return true
	},
	"generatePropertyPersistenceInterface": func(property libdomain.PropertyDefinition) string {
		return generatePropertyPersistenceInterface("", property)
	},
	"generatePropertyConversionToDomain": func(entityName string, property libdomain.PropertyDefinition) string {
		return generatePropertyConversionToDomain(entityName, entityName, "", property)
	},
	"getCustomCommandSingleton": func(aggregate *libpersistence.AggregateDefinition, table libpersistence.TableInterface, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (t *{{.ModelTitle}}) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   collection := &{{.ModelTitle}}Collection{}
		   collection.Init([]libpersistence.DomainObjectInterface{t})
		   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(collection, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandPrototype": func(customFunc *libpersistence.CustomRequest) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandCollection": func(aggregate *libpersistence.AggregateDefinition, table libpersistence.TableInterface, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (c *{{.ModelTitle}}Collection) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(c, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})
