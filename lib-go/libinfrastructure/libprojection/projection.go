package libprojection

type StreamingClient interface {
	Subscribe(stream string, aggregate string, function func([]byte)) (interface{}, error)
	Publish(string, []byte) error
}
