package librest

// import (
// 	"context"
// 	"fmt"
// 	// "os"
// 	"reflect"

// 	graphql "github.com/graph-gophers/graphql-go"
// 	// "github.com/ory/hydra/sdk/go/hydra"
// 	"github.com/pkg/errors"
// 	"gitlab.com/empowerlab/stack/lib-go/libdata"
// 	"gitlab.com/empowerlab/stack/lib-go/libmodel"
// 	"gitlab.com/empowerlab/stack/lib-go/libutils"
// )

// // Definition todo
// type Definition struct {
// 	Model *libmodel.Definition
// 	NewResulter      func() interface{}
// 	NewCreateMutater func() interface{}
// 	// NewCreateWithoutParentMutater func() interface{}
// 	NewGetQuerier        func() interface{}
// 	NewUpdateMutater     func() interface{}
// 	NewDeleteMutater     func() interface{}
// 	NewSelectQuerier     func() interface{}
// 	NewParentSearchInput func(string) interface{}
// 	GenerateParamer      func(string, bool, string) interface{}
// }

// // GraphqlGet todo
// func (d *Definition) Get(
// 	ctx context.Context, cluster *libdata.Cluster, tenant string, id graphql.ID, with []string,
// ) (interface{}, error) {
// 	tx, err := cluster.BeginTransaction(ctx, tenant)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// 	}
// 	defer func() {
// 		err = tx.CommitTransaction(ctx, err)
// 	}()

// 	return d.Model.Getter(ctx, tx, string(id), with)
// }

// // GraphqlSelect todo
// // nolint: unparam
// func (d *Definition) Select(
// 	ctx context.Context,
// 	cluster *libdata.Cluster,
// 	tenant string,
// 	Filters interface{},
// 	with []string,
// 	First *int32,
// 	After *graphql.ID,
// 	OrderBy *string,
// ) (interface{}, error) {

// 	// if os.Getenv("DISABLE_AUTH") != "True" {
// 	// 	fmt.Println(ctx)
// 	// 	fmt.Printf("|%s|\n", ctx.Value("token"))

// 	// 	// conf := &oauth2.Config{
// 	// 	// 	ClientID:     "42daf780-976a-42bd-94f4-f8caf90a2ade",
// 	// 	// 	ClientSecret: "X8BTcDs~AStCGG_pUBWqr7l2Ej",
// 	// 	// 	Scopes:       []string{"openid"},
// 	// 	// 	Endpoint: oauth2.Endpoint{
// 	// 	// 		TokenURL: "http://localhost:3003/oauth2/token",
// 	// 	// 		AuthURL:  "http://localhost:3003/oauth2/auth",
// 	// 	// 	},
// 	// 	// }
// 	// 	sdk, _ := hydra.NewSDK(&hydra.Configuration{
// 	// 		ClientID:     os.Getenv("HYDRA_CLIENT_ID"),
// 	// 		ClientSecret: os.Getenv("HYDRA_CLIENT_SECRET"),
// 	// 		AdminURL:  os.Getenv("HYDRA_CLUSTER_URL"),
// 	// 		Scopes:       []string{"hydra.*", "openid", "offline"},
// 	// 	})
// 	// 	intro, resp, err := sdk.IntrospectOAuth2Token(ctx.Value("token").(string), "")
// 	// 	fmt.Printf("%+v\n", intro)
// 	// 	fmt.Println(resp)
// 	// 	fmt.Println(err)
// 	// }

// 	var filters []*libdata.Filter
// 	fmt.Printf("filters %+v\n", Filters)
// 	s := reflect.ValueOf(Filters)
// 	if !s.IsNil() {
// 		s = s.Elem()
// 		typeOfFilters := s.Type()
// 		for i := 0; i < s.NumField(); i++ {
// 			f := s.Field(i)
// 			if f.Kind() == reflect.Ptr {
// 				if f.IsNil() {
// 					continue
// 				}
// 				f = f.Elem()
// 			}
// 			name := typeOfFilters.Field(i).Name
// 			fmt.Printf("name %s\n", name)
// 			fmt.Printf("db %s\n", typeOfFilters.Field(i).Tag.Get("db"))
// 			fmt.Printf("f %s\n", f.String())
// 			fmt.Printf("type %s\n", f.Type())
// 			var filter *libdata.Filter
// 			switch f.Type().String() {
// 				case "[]*graphql.ID":
// 					// fmt.Printf("kind %s\n", f.Kind())
// 					operande := []string{}
// 					// s := reflect.ValueOf(f)
// 					for is := 0; is < f.Len(); is++ {
// 						fmt.Println(f.Index(is).Elem())
// 						operande = append(operande, f.Index(is).Elem().String())
// 					}
// 					filter = &libdata.Filter{
// 						Field: typeOfFilters.Field(i).Tag.Get("db"), Operator: "IN", Operande: operande}
// 				default:
// 					filter = &libdata.Filter{
// 						Field: typeOfFilters.Field(i).Tag.Get("db"), Operator: "=", Operande: f.String()}
// 			}
// 			filters = append(filters, filter)
// 			// switch name {
// 			// case "ID":
// 			// 	filters = append(filters, &db.Filter{Field: "id", Operator: "IN", Operande: f.String()})
// 			// }
// 		}
// 	}
// 	for _, filter := range filters {
// 		fmt.Printf("filter %+v\n", filter)
// 	}

// 	var limit *uint
// 	if First != nil {
// 		l := uint(*First)
// 		limit = &l
// 	}

// 	var after *string
// 	if After != nil {
// 		a := string(*After)
// 		if a != "" {
// 			after = &a
// 		}
// 	}

// 	var orderBy *string
// 	if OrderBy != nil {
// 		a := *OrderBy
// 		if a != "" {
// 			orderBy = &a
// 		}
// 	}

// 	tx, err := cluster.BeginTransaction(ctx, tenant)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// 	}
// 	defer func() {
// 		err = tx.CommitTransaction(ctx, err)
// 	}()

// 	templates, _, err := d.Model.Selecter(ctx, tx, filters, with, limit, after, orderBy)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get templates")
// 	}

// 	return templates, nil
// }

// // GraphqlCreate todo
// // nolint: unparam
// func (d *Definition) Create(
// 	ctx context.Context, cluster *libdata.Cluster,
// tenant string, args interface{}, record interface{},
// ) (interface{}, error) {

// 	fmt.Printf("TEST\n")

// 	_, err := libutils.ConvertStruct(args, record, []string{})
// 	if err != nil {
// 		return nil, err
// 	}

// 	// record := args.Record
// 	fmt.Printf("args %+v\n", args)
// 	fmt.Printf("record %+v\n", record)

// 	tx, err := cluster.BeginTransaction(ctx, tenant)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// 	}
// 	defer func() {
// 		err = tx.CommitTransaction(ctx, err)
// 	}()

// 	// res := []*Template{{
// 	// 	ID:         string(args.ID),
// 	// 	ProviderID: string(args.ProviderID),
// 	// 	Content:    args.Content,
// 	// }}

// 	templates, _, err := d.Model.Creater(ctx, tx, []interface{}{record})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't insert template")
// 	}

// 	fmt.Printf("g record %+v\n", templates[0])

// 	return templates[0], nil
// }

// // GraphqlUpdate todo
// // nolint: unparam
// func (d *Definition) Update(
// 	ctx context.Context, cluster *libdata.Cluster, tenant string,
// 	args interface{}, record interface{}, fields []string, with []string,
// ) (interface{}, error) {

// 	// fields := utils.GetDBFields(record)

// 	// // record := args.Record
// 	// var providerID string
// 	// if args.ProviderID != nil {
// 	// 	fields = append(fields, "provider_id")
// 	// 	providerID = string(*args.ProviderID)
// 	// }

// 	// var content string
// 	// if args.Content != nil {
// 	// 	fields = append(fields, "content")
// 	// 	content = *args.Content
// 	// }

// 	tx, err := cluster.BeginTransaction(ctx, tenant)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// 	}
// 	defer func() {
// 		err = tx.CommitTransaction(ctx, err)
// 	}()

// 	fields, err = libutils.ConvertStruct(args, record, fields)
// 	if err != nil {
// 		return nil, err
// 	}
// 	fmt.Printf("fields %+v\n", fields)

// 	// res := &Template{
// 	// 	ID:         string(args.ID),
// 	// 	ProviderID: providerID,
// 	// 	Content:    content,
// 	// }

// 	templateByIds, err := d.Model.Updater(ctx, tx, []*libdata.Filter{
// 		{Field: "id", Operator: "=", Operande: record.(libmodel.ModelInterface).GetID()},
// 	}, record, fields, with)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update template")
// 	}

// 	return templateByIds[record.(libmodel.ModelInterface).GetID()], nil
// }

// // GraphqlDelete todo
// // nolint: dupl, unparam
// func (d *Definition) Delete(
// 	ctx context.Context, cluster *libdata.Cluster, tenant string, id graphql.ID, with []string,
// ) (interface{}, error) {

// 	tx, err := cluster.BeginTransaction(ctx, tenant)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// 	}
// 	defer func() {
// 		err = tx.CommitTransaction(ctx, err)
// 	}()

// 	if d.Model.TranslamodelFields != nil {
// 		defaultLang, errT := libmodel.SettingGet(ctx, tx, "languageDefault")
// 		if errT != nil {
// 			return nil, errors.Wrap(errT, "Couldn't get language default")
// 		}
// 		ctx = context.WithValue(ctx, libmodel.LangKey, *defaultLang)
// 		fmt.Println(*defaultLang)
// 	}

// 	templateByIds, err := d.Model.Deleter(ctx, tx, []*libdata.Filter{{
// 		Field: "id", Operator: "=", Operande: string(id)}}, with)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't delete template")
// 	}

// 	return templateByIds[string(id)], nil
// }
