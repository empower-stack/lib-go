//nolint: dupl
package gengrpc

import (
	"context"
	"testing"
	"fmt"
	// "time"
	{{if .Event}}
	"encoding/json"
	{{end}}

	grpcMetadata "google.golang.org/grpc/metadata"

	grpcpb "{{.Repository}}/gen/pb"
	mpb "{{.GenDomain}}/pb"
	translator "{{.GenDomain}}"
)


{{- range .Service.Requests}}
{{$request := .Definition}}

/*{{$request.Title}} resolve the corresponding custom function, linked to {{$request.Name}} object.
 */
func (s *Server) {{$request.Title}}(
	ctx context.Context,
	r *mpb.{{$request.Title}}{{if $request.IsCommand}}Command{{else}}Query{{end}},
) (*mpb.{{$request.Title}}Results, error) {

	
	results, err := translator.{{$request.Title}}{{if $request.IsCommand}}Command{{else}}Query{{end}}(ctx, r)
	if err != nil {
		return nil, err
	}

	return results, nil
}
{{- end }}


{{- range .Service.Objects }}
{{$object := .Definition}}
{{if $object.IsAggregateRoot}}

/*ICheck{{$object.Title}}ConsistencyWithEvents resolve the corresponding custom function, linked to {{$object.Name}} object.
 */
func (s *Server) Check{{$object.Title}}ConsistencyWithEvents(
	ctx context.Context, query *grpcpb.CheckConsistencyWithEventsQuery,
) (*grpcpb.CheckConsistencyWithEventsResults, error) {

	err := translator.Check{{$object.Title}}ConsistencyWithEvents(ctx, {{if $object.UseTenants}}query.TenantID,{{end}} query.ID)
	if err != nil {
		return nil, err
	}

	return &grpcpb.CheckConsistencyWithEventsResults{}, nil
}

{{- end }}
{{- end }}



{{- range .Service.Requests }}
{{$request := .Definition}}

func (d *TestGRPCDriver) I{{$request.Title}}(
	ctx context.Context, token string,
	results *mpb.{{$request.Title}}Results, request *mpb.{{$request.Title}}{{if $request.IsCommand}}Command{{else}}Query{{end}},
) func(t *testing.T) {
	return func(t *testing.T) {

		ctx = grpcMetadata.AppendToOutgoingContext(ctx, "Authorization", "Bearer "+token)

		request.GetProperties = translator.GetAll{{$request.Title}}Properties()

		resultsGot, err := d.Client.{{$request.Title}}(
			ctx,
			request)
		if err != nil {
			t.Fatalf("SayHello failed: %v", err)
		}
		fmt.Printf("%+v\n", resultsGot)

		{{- range .GetResults}}
		{{- if eq .Definition.CollectionType "Slice"}}
		results.{{.Definition.Title}} = resultsGot.{{.Definition.Title}}
		{{- else if eq .Definition.CollectionType "OrderedMap"}}
		results.{{.Definition.Title}} = resultsGot.{{.Definition.Title}}
		{{- else}}
			{{- if eq .Definition.Type "EntityType"}}
			*results.{{.Definition.Title}} = *resultsGot.{{.Definition.Title}}
			{{- else if eq .Definition.Type "ValueObjectType"}}
			*results.{{.Definition.Title}} = *resultsGot.{{.Definition.Title}}
			{{- else}}
			results.{{.Definition.Title}} = resultsGot.{{.Definition.Title}}
			{{- end}}
		{{- end}}
		{{- end}}
	}
}

{{- end }}


{{- range .Service.Objects }}
{{$object := .Definition}}
{{if $object.IsAggregateRoot}}

func (d *TestGRPCDriver) ICheck{{$object.Title}}ConsistencyWithEvents(
	ctx context.Context, token string,
	{{if $object.UseTenants}}tenantID string,{{end}} {{$object.Name}} *mpb.{{$object.Title}},
) func(t *testing.T) {
	return func(t *testing.T) {

		ctx = grpcMetadata.AppendToOutgoingContext(ctx, "Authorization", "Bearer "+token)

		resultsGot, err := d.Client.Check{{$object.Title}}ConsistencyWithEvents(
			ctx, &grpcpb.CheckConsistencyWithEventsQuery{
				{{if $object.UseTenants}}TenantID: tenantID,{{end}}
				ID: {{$object.Name}}.ID,
			})
		if err != nil {
			t.Fatalf("SayHello failed: %v", err)
		}
		fmt.Printf("%+v\n", resultsGot)
	}
}

{{- end }}
{{- end }}

var _ grpcpb.GRPCServiceServer
