package libgrpc

import (
	"log"

	"gopkg.in/yaml.v2"
)

type SwaggerSpecData struct {
	Name  string
	Datas []*SchemaData
}

type SwaggerSpec struct {
	Swagger     string                            `yaml:"swagger"`
	BasePath    string                            `yaml:"basePath"`
	Info        *SwaggerInfo                      `yaml:"info"`
	Paths       map[string]map[string]interface{} `yaml:"paths"`
	Definitions map[string]*SwaggerDefinition     `yaml:"definitions"`
}

type SwaggerInfo struct {
	Description string `yaml:"description"`
	Title       string `yaml:"title"`
	Version     string `yaml:"version"`
}

type SwaggerRequest struct {
	OperationID string                      `yaml:"operationId"`
	Parameters  []*SwaggerParams            `yaml:"parameters"`
	Responses   map[string]*SwaggerResponse `yaml:"responses"`
}

type SwaggerParams struct {
	Name        string         `yaml:"name"`
	In          string         `yaml:"in"`
	Type        string         `yaml:"type"`
	Required    bool           `yaml:"required"`
	Description string         `yaml:"description"`
	Schema      *SwaggerSchema `yaml:"schema"`
}

type SwaggerSchema struct {
	Type  string         `yaml:"type"`
	Items *SwaggerSchema `yaml:"items"`
	Ref   string         `yaml:"$ref"`
}

type SwaggerResponse struct {
	Description string         `yaml:"description"`
	Schema      *SwaggerSchema `yaml:"schema"`
}

type SwaggerDefinition struct {
	Type       string                      `yaml:"type"`
	Required   []string                    `yaml:"required"`
	Properties map[string]*SwaggerProperty `yaml:"properties"`
}

type SwaggerProperty struct {
	Type     string `yaml:"type"`
	Format   string `yaml:"format"`
	Readonly bool   `yaml:"readOnly"`
}

func GenSwaggerSpec(specData *SwaggerSpecData) string {

	paths := map[string]map[string]interface{}{}
	for _, data := range specData.Datas {
		prefix := ""
		var parametersRoot []*SwaggerParams
		if data.Definitions.Tenant {
			prefix = "/{tenant_uuid}"
			parametersRoot = []*SwaggerParams{{
				Name:     "tenant_uuid",
				In:       "path",
				Type:     "string",
				Required: true,
			}}
		}
		paths[prefix+"/"+data.Name] = map[string]interface{}{
			"parameters": parametersRoot,
			"post": &SwaggerRequest{
				OperationID: "add" + data.Title,
				Parameters: []*SwaggerParams{
					{
						Name: "body",
						In:   "body",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/" + data.Name,
						},
					},
				},
				Responses: map[string]*SwaggerResponse{
					"201": &SwaggerResponse{
						Description: "Created",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/" + data.Name,
						},
					},
					"default": &SwaggerResponse{
						Description: "Error",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/error",
						},
					},
				},
			},
			"get": &SwaggerRequest{
				OperationID: "get" + data.Title,
				Responses: map[string]*SwaggerResponse{
					"200": &SwaggerResponse{
						Description: "OK",
						Schema: &SwaggerSchema{
							Type: "array",
							Items: &SwaggerSchema{
								Ref: "#/definitions/" + data.Name,
							},
						},
					},
					"default": &SwaggerResponse{
						Description: "Error",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/error",
						},
					},
				},
			},
		}
		parametersDetails := append(parametersRoot, &SwaggerParams{
			Name:     "uuid",
			In:       "path",
			Type:     "string",
			Required: true,
		})
		paths[prefix+"/"+data.Name+"/{uuid}"] = map[string]interface{}{
			"parameters": parametersDetails,
			"get": &SwaggerRequest{
				OperationID: "get" + data.Title,
				Responses: map[string]*SwaggerResponse{
					"200": &SwaggerResponse{
						Description: "OK",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/" + data.Name,
						},
					},
					"default": &SwaggerResponse{
						Description: "Error",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/error",
						},
					},
				},
			},
			"patch": &SwaggerRequest{
				OperationID: "update" + data.Title,
				Parameters: []*SwaggerParams{
					{
						Name: "body",
						In:   "body",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/" + data.Name,
						},
					},
				},
				Responses: map[string]*SwaggerResponse{
					"200": &SwaggerResponse{
						Description: "OK",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/" + data.Name,
						},
					},
					"default": &SwaggerResponse{
						Description: "Error",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/error",
						},
					},
				},
			},
			"delete": &SwaggerRequest{
				OperationID: "delete" + data.Title,
				Responses: map[string]*SwaggerResponse{
					"204": &SwaggerResponse{
						Description: "OK",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/" + data.Name,
						},
					},
					"default": &SwaggerResponse{
						Description: "Error",
						Schema: &SwaggerSchema{
							Ref: "#/definitions/error",
						},
					},
				},
			},
		}
	}

	definitions := map[string]*SwaggerDefinition{}
	for _, data := range specData.Datas {
		for _, definition := range data.Definitions.Slice() {

			var required []string
			properties := map[string]*SwaggerProperty{
				"id": &SwaggerProperty{
					Type:     "string",
					Readonly: true,
				},
			}
			for _, field := range definition.Application.Definition().Aggregate.Definition().AggregateRoot.Properties {
				properties[field.GetName()] = &SwaggerProperty{
					Type: field.GoType(),
				}

				if field.GetRequired() {
					required = append(required, field.GetName())
				}
			}

			definitions[definition.Application.Definition().Aggregate.Definition().AggregateRoot.Name] = &SwaggerDefinition{
				Type:       "object",
				Required:   required,
				Properties: properties,
			}
		}
	}
	definitions["error"] = &SwaggerDefinition{
		Type:     "object",
		Required: []string{"message"},
		Properties: map[string]*SwaggerProperty{
			"code": &SwaggerProperty{
				Type:   "integer",
				Format: "int64",
			},
			"message": &SwaggerProperty{
				Type: "string",
			},
		},
	}

	specs := &SwaggerSpec{
		Swagger:  "2.0",
		BasePath: "/api",
		Info: &SwaggerInfo{
			Description: "",
			Title:       specData.Name,
			Version:     "1.0.0",
		},
		Paths:       paths,
		Definitions: definitions,
	}

	result, err := yaml.Marshal(specs)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	print(result)

	return string(result)

}
