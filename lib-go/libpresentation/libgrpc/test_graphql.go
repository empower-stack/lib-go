package libgrpc

// import (
// 	"context"
// 	"fmt"
// 	"os"
// 	"reflect"
// 	"strconv"
// 	"testing"

// 	"github.com/mitchellh/mapstructure"
// 	"github.com/shurcooL/graphql"
// 	"gitlab.com/empowerlab/stack/lib-go/libdata"
// )

// // GraphqlResult todo
// type GraphqlResult interface {
// 	GetID() string
// }

// // GraphqlQuery todo
// type GraphqlQuery interface {
// 	GetResult() interface{}
// }

// // GraphqlSelectQuery todo
// type GraphqlSelectQuery interface {
// 	GetResult() []interface{}
// }

// const graphqlIDType = "*graphql.ID"
// const falseKey = "False"

// // CreateRecordGraphql todo
// // nolint: unparam
// func (d *Definition) CreateRecordGraphql(
// 	t *testing.T, cluster *libdata.Cluster, client *graphql.Client,
// 	tenant string, id string, fixedParams map[string]interface{}) GraphqlResult {

// 	t.Log("Creating record...")
// 	createMutation := d.NewCreateMutater()
// 	expectedResult := d.NewResulter()
// 	params, expectedParams := d.GetParams(expectedResult, false)

// 	if cluster.MultiTenant {
// 		params["tenant"] = graphql.String(tenant)
// 	}
// 	params["id"] = id
// 	expectedParams["id"] = id

// 	if !d.Model.DBInfo.CanAssignID {
// 		delete(params, "id")
// 	}

// 	fmt.Printf("fixedParams %s\n", fixedParams)
// 	// parentIsNotNull := false
// 	for k, v := range fixedParams {
// 		params[k] = v
// 		expectedParams[k] = v
// 		r := reflect.ValueOf(v)
// 		if r.Kind() == reflect.Ptr {
// 			if r.Type().String() == graphqlIDType {
// 				if r.IsNil() {
// 					expectedParams[k] = nil
// 				}
// 			}
// 		}
// 		// fmt.Println(k)
// 		// if k == "parentID" {
// 		// 	if v != nil {
// 		// 		parentIsNotNull = true
// 		// 	} else {
// 		// 		delete(params, k)
// 		// 	}
// 		// }
// 		// if v == nil {
// 		// 	delete(params, k)
// 		// }
// 	}
// 	// if d.Tree && !parentIsNotNull {
// 	// 	createMutation = d.NewCreateWithoutParentMutater()
// 	// }

// 	fmt.Printf("mutation %+v\n", createMutation)
// 	fmt.Printf("params %s\n", params)
// 	err := client.Mutate(context.Background(), createMutation, params)
// 	if err != nil {
// 		t.Errorf("Couldn't create record \n%s", err)
// 	}

// 	fmt.Printf("results %+v\n", createMutation.(GraphqlQuery).GetResult())

// 	if !d.Model.DBInfo.CanAssignID {
// 		expectedParams["id"] = createMutation.(GraphqlQuery).GetResult().(GraphqlResult).GetID()
// 	}
// 	fmt.Printf("expectedParams %+v\n", expectedParams)
// 	// Decode(t, expectedParams, expectedResult.(GraphqlResult))
// 	err = mapstructure.Decode(expectedParams, expectedResult)
// 	if err != nil {
// 		t.Errorf("Couldn't decode mapstructure \n%s", err)
// 	}

// 	if !reflect.DeepEqual(createMutation.(GraphqlQuery).GetResult(), expectedResult) {
// 		t.Errorf("Unexpected result\n%+v\n%+v",
// 			createMutation.(GraphqlQuery).GetResult(), expectedResult)
// 	} else {
// 		t.Log("OK")
// 	}

// 	return createMutation.(GraphqlQuery).GetResult().(GraphqlResult)

// }

// // PrepareRecordsGraphql todo
// func (d *Definition) PrepareRecordsGraphql(
// 	t *testing.T, cluster *libdata.Cluster, client *graphql.Client, tenant string,
// 	initFunc func(), fixedParams map[string]interface{},
// ) (GraphqlResult, GraphqlResult) {

// 	// fixedParams := map[string]interface{}{}
// 	if d.Model.TranslamodelFields != nil {
// 		fixedParams["lang"] = graphql.String("fr")
// 	}
// 	if d.Model.Tree {
// 		fixedParams["parentID"] = (*graphql.ID)(nil)
// 	}

// 	initFunc()
// 	record1 := d.CreateRecordGraphql(t, cluster, client, tenant, "test", fixedParams)
// 	record2 := d.CreateRecordGraphql(t, cluster, client, tenant, "test2", fixedParams)

// 	return record1, record2
// }

// // DeleteRecordGraphql todo
// // nolint: unparam
// func (d *Definition) DeleteRecordGraphql(
// 	t *testing.T, cluster *libdata.Cluster, client *graphql.Client, tenant string,
// 	id string, updatedValues bool, fixedParams map[string]interface{}) {

// 	t.Log("Deleting record...")
// 	deleteMutation := d.NewDeleteMutater()
// 	expectedResult := d.NewResulter()
// 	params, expectedParams := d.GetParams(expectedResult, updatedValues)
// 	if cluster.MultiTenant {
// 		params["tenant"] = graphql.String(tenant)
// 	}
// 	params["id"] = id
// 	expectedParams["id"] = id
// 	for k, v := range fixedParams {
// 		params[k] = v
// 		expectedParams[k] = v
// 		r := reflect.ValueOf(v)
// 		if r.Kind() == reflect.Ptr {
// 			if r.Type().String() == graphqlIDType {
// 				if r.IsNil() {
// 					expectedParams[k] = nil
// 				}
// 			}
// 		}
// 	}
// 	err := mapstructure.Decode(expectedParams, &expectedResult)
// 	if err != nil {
// 		t.Errorf("Couldn't decode mapstructure \n%s", err)
// 	}
// 	args := map[string]interface{}{
// 		"id": params["id"],
// 	}
// 	if cluster.MultiTenant {
// 		args["tenant"] = params["tenant"]
// 	}
// 	err = client.Mutate(context.Background(), deleteMutation, args)
// 	if err != nil {
// 		t.Errorf("Couldn't delete template\n%s", err)
// 	} else if !reflect.DeepEqual(deleteMutation.(GraphqlQuery).GetResult(), expectedResult) {
// 		t.Errorf("Unexpected result\n%+v\n%+v",
// 			deleteMutation.(GraphqlQuery).GetResult(), expectedResult)
// 	} else {
// 		t.Log("OK")
// 	}

// }

// // PurgeRecordsGraphql todo
// func (d *Definition) PurgeRecordsGraphql(
// 	t *testing.T, cluster *libdata.Cluster, client *graphql.Client,
// 	tenant string, records []GraphqlResult, fixedParams map[string]interface{}) {

// 	// fixedParams := map[string]interface{}{}
// 	if d.Model.Tree {
// 		fixedParams["parentID"] = (*graphql.ID)(nil)
// 	}

// 	for _, record := range records {
// 		d.DeleteRecordGraphql(t, cluster, client, tenant, record.GetID(), false, fixedParams)
// 	}
// }

// // RunnerTestCrudGraphql todo
// func (d *Definition) RunnerTestCrudGraphql(
// 	t *testing.T, cluster *libdata.Cluster, client *graphql.Client,
// 	tenant string, fixedParams map[string]interface{}) {

// 	fmt.Println("")
// 	fmt.Println("START CRUD TEST")

// 	if d.Model.TranslamodelFields != nil {
// 		fixedParams["lang"] = graphql.String("fr")
// 	}

// 	fmt.Printf("tenant %s\n", tenant)
// 	var parent GraphqlResult
// 	if d.Model.Tree {
// 		fixedParams["parentID"] = (*graphql.ID)(nil)
// 		parent = d.CreateRecordGraphql(t, cluster, client, tenant, "test parent", fixedParams)
// 		// parentID := graphql.ID(parent.GetID())
// 		fixedParams["parentID"] = parent.GetID()
// 	}

// 	record := d.CreateRecordGraphql(t, cluster, client, tenant, "test", fixedParams)

// 	t.Log("Requesting record...")
// 	query := d.NewGetQuerier()
// 	expectedResult := d.NewResulter()
// 	params, expectedParams := d.GetParams(expectedResult, false)
// 	if cluster.MultiTenant {
// 		params["tenant"] = tenant
// 	}
// 	params["id"] = record.GetID()
// 	expectedParams["id"] = record.GetID()
// 	for k, v := range fixedParams {
// 		params[k] = v
// 		expectedParams[k] = v
// 	}
// 	err := mapstructure.Decode(expectedParams, expectedResult)
// 	if err != nil {
// 		t.Errorf("Couldn't decode mapstructure \n%s", err)
// 	}
// 	args := map[string]interface{}{
// 		"id": params["id"],
// 	}
// 	if cluster.MultiTenant {
// 		args["tenant"] = graphql.String(params["tenant"].(string))
// 	}
// 	if d.Model.TranslamodelFields != nil {
// 		args["lang"] = fixedParams["lang"]
// 	}
// 	err = client.Query(context.Background(), query, args)
// 	if err != nil {
// 		t.Errorf("Couldn't get record \n%s", err)
// 	} else if !reflect.DeepEqual(query.(GraphqlQuery).GetResult(), expectedResult) {
// 		t.Errorf("Unexpected result\n%+v\n%+v",
// 			query.(GraphqlQuery).GetResult(), expectedResult)
// 	} else {
// 		t.Log("OK")
// 	}

// 	if d.Model.Tree {
// 		selectQuery := d.NewSelectQuerier()
// 		expectedResult = d.NewResulter()
// 		params, expectedParams = d.GetParams(expectedResult, false)
// 		if cluster.MultiTenant {
// 			params["tenant"] = tenant
// 		}
// 		expectedParams["id"] = record.GetID()
// 		for k, v := range fixedParams {
// 			expectedParams[k] = v
// 		}
// 		err = mapstructure.Decode(expectedParams, expectedResult)
// 		if err != nil {
// 			t.Errorf("Couldn't decode mapstructure \n%s", err)
// 		}
// 		args = map[string]interface{}{
// 			"filter":  d.NewParentSearchInput(parent.GetID()),
// 			"first":   (*graphql.Int)(nil),
// 			"after":   (*graphql.ID)(nil),
// 			"orderBy": (*graphql.String)(nil),
// 		}
// 		if cluster.MultiTenant {
// 			args["tenant"] = graphql.String(params["tenant"].(string))
// 		}
// 		if d.Model.TranslamodelFields != nil {
// 			args["lang"] = fixedParams["lang"]
// 		}
// 		err = client.Query(context.Background(), selectQuery, args)
// 		if err != nil {
// 			t.Errorf("Couldn't select children\n%s", err)
// 		} else if !reflect.DeepEqual(
// 			selectQuery.(GraphqlSelectQuery).GetResult()[0], expectedResult) {
// 			t.Errorf("Unexpected result\n%v\n%v",
// 				selectQuery.(GraphqlSelectQuery).GetResult()[0], expectedResult)
// 		} else {
// 			t.Log("OK")
// 		}
// 	}

// 	t.Log("Updating record...")
// 	updateMutation := d.NewUpdateMutater()
// 	expectedResult = d.NewResulter()
// 	params, expectedParams = d.GetParams(expectedResult, true)
// 	if cluster.MultiTenant {
// 		params["tenant"] = graphql.String(tenant)
// 	}
// 	params["id"] = record.GetID()
// 	expectedParams["id"] = record.GetID()
// 	for k, v := range fixedParams {
// 		params[k] = v
// 		expectedParams[k] = v
// 	}
// 	if d.Model.TranslamodelFields != nil {
// 		params["lang"] = graphql.String("en")
// 		for _, k := range d.Model.TranslamodelFields {
// 			params[k] = graphql.String(fmt.Sprintf("%s %s", params[k], "en"))
// 			expectedParams[k] = params[k]
// 			kt := fmt.Sprintf("%sDefault", k)
// 			expectedParams[kt] = params[k]
// 		}
// 	}
// 	fmt.Printf("params %+v\n", params)
// 	err = mapstructure.Decode(expectedParams, expectedResult)
// 	if err != nil {
// 		t.Errorf("Couldn't decode mapstructure \n%s", err)
// 	}
// 	err = client.Mutate(context.Background(), updateMutation, params)
// 	if err != nil {
// 		t.Errorf("Couldn't update record \n%s", err)
// 	} else if !reflect.DeepEqual(updateMutation.(GraphqlQuery).GetResult(), expectedResult) {
// 		t.Errorf("Unexpected result\n%+v\n%+v",
// 			updateMutation.(GraphqlQuery).GetResult(), expectedResult)
// 	} else {
// 		t.Log("OK")
// 	}

// 	if d.Model.TranslamodelFields != nil {
// 		t.Log("Updating record2...")
// 		updateMutation := d.NewUpdateMutater()
// 		expectedResult = d.NewResulter()
// 		params, expectedParams = d.GetParams(expectedResult, true)
// 		if cluster.MultiTenant {
// 			params["tenant"] = graphql.String(tenant)
// 		}
// 		params["id"] = record.GetID()
// 		expectedParams["id"] = record.GetID()
// 		for k, v := range fixedParams {
// 			params[k] = v
// 			expectedParams[k] = v
// 		}
// 		if d.Model.TranslamodelFields != nil {
// 			params["lang"] = graphql.String("fr")
// 			for _, k := range d.Model.TranslamodelFields {
// 				v := params[k]
// 				params[k] = graphql.String(fmt.Sprintf("%s %s", v, "fr"))
// 				expectedParams[k] = params[k]
// 				kt := fmt.Sprintf("%sDefault", k)
// 				expectedParams[kt] = graphql.String(fmt.Sprintf("%s %s", v, "en"))
// 			}
// 		}
// 		fmt.Printf("params %+v\n", params)
// 		err = mapstructure.Decode(expectedParams, expectedResult)
// 		if err != nil {
// 			t.Errorf("Couldn't decode mapstructure \n%s", err)
// 		}
// 		err = client.Mutate(context.Background(), updateMutation, params)
// 		if err != nil {
// 			t.Errorf("Couldn't update record \n%s", err)
// 		} else if !reflect.DeepEqual(updateMutation.(GraphqlQuery).GetResult(), expectedResult) {
// 			t.Errorf("Unexpected result\n%+v\n%+v",
// 				updateMutation.(GraphqlQuery).GetResult(), expectedResult)
// 		} else {
// 			t.Log("OK")
// 		}
// 	}

// 	params = map[string]interface{}{}
// 	for k, v := range fixedParams {
// 		params[k] = v
// 	}
// 	if d.Model.TranslamodelFields != nil {
// 		for _, k := range d.Model.TranslamodelFields {
// 			v := graphql.String("test2 en")
// 			params[k] = v
// 			kt := fmt.Sprintf("%sDefault", k)
// 			params[kt] = v
// 		}
// 	}
// 	d.DeleteRecordGraphql(t, cluster, client, tenant, record.GetID(), true, params)

// 	if d.Model.Tree {
// 		fixedParams["parentID"] = (*graphql.ID)(nil)
// 		d.DeleteRecordGraphql(t, cluster, client, tenant, parent.GetID(), false, fixedParams)
// 	}

// 	i := 1
// 	records := map[int]GraphqlResult{}
// 	for i < 30 {
// 		t.Log("Creating multiple records...")
// 		record := d.CreateRecordGraphql(
// 			t, cluster, client, tenant, fmt.Sprintf("test%s", strconv.Itoa(i)), fixedParams)
// 		// createMutation := d.NewCreateMutater()
// 		// expectedResult = d.NewResulter()
// 		// params = getParams(expectedResult, false)
// 		// params["id"] = fmt.Sprintf("test%s", strconv.Itoa(i))
// 		// err = mapstructure.Decode(params, expectedResult)
// 		// if err != nil {
// 		// 	t.Errorf("Couldn't decode mapstructure \n%s", err)
// 		// }
// 		// err = client.Mutate(context.Background(), createMutation, params)
// 		// if err != nil {
// 		// 	t.Errorf("Couldn't create record \n%s", err)
// 		// } else if !reflect.DeepEqual(createMutation.(GraphqlQuery).GetResult(), expectedResult) {
// 		// 	t.Errorf("Unexpected result\n%+v\n%+v",
// 		// 		createMutation.(GraphqlQuery).GetResult(), expectedResult)
// 		// } else {
// 		// 	t.Log("OK")
// 		// }

// 		records[i] = record
// 		i = i + 1
// 	}

// 	t.Log("Selecting records...")
// 	selectQuery := d.NewSelectQuerier()
// 	expectedResult = d.NewResulter()
// 	params, expectedParams = d.GetParams(expectedResult, false)
// 	if cluster.MultiTenant {
// 		params["tenant"] = tenant
// 	}
// 	args = map[string]interface{}{
// 		"filter":  d.NewParentSearchInput(""),
// 		"first":   graphql.Int(10),
// 		"after":   records[19].(GraphqlResult).GetID(),
// 		"orderBy": graphql.String("created_at_DESC"),
// 	}
// 	if cluster.MultiTenant {
// 		args["tenant"] = graphql.String(params["tenant"].(string))
// 	}
// 	if d.Model.TranslamodelFields != nil {
// 		args["lang"] = fixedParams["lang"]
// 	}
// 	err = client.Query(context.Background(), selectQuery, args)
// 	if err != nil {
// 		t.Errorf("Couldn't select templates\n%s", err)
// 	} else {
// 		t.Log("OK")
// 	}
// 	if os.Getenv("DATABASE_TYPE") != "cassandra" {
// 		i = 0
// 		results := selectQuery.(GraphqlSelectQuery).GetResult()
// 		for i < 5 {
// 			params["id"] = records[18-i].(GraphqlResult).GetID()
// 			expectedParams["id"] = params["id"]
// 			for k, v := range fixedParams {
// 				params[k] = v
// 				expectedParams[k] = v
// 				r := reflect.ValueOf(v)
// 				if r.Kind() == reflect.Ptr {
// 					if r.Type().String() == "*graphql.ID" {
// 						if r.IsNil() {
// 							expectedParams[k] = nil
// 						}
// 					}
// 				}
// 			}
// 			fmt.Printf("params %+v\n", params)
// 			expectedResult = d.NewResulter()
// 			err = mapstructure.Decode(expectedParams, &expectedResult)
// 			if err != nil {
// 				t.Errorf("Couldn't decode mapstructure \n%s", err)
// 			}
// 			if !reflect.DeepEqual(results[i], expectedResult) {
// 				t.Errorf("Unexpected result\n%v\n%v",
// 					results[i], expectedResult)
// 			}
// 			i = i + 1
// 		}
// 	}

// 	for _, record := range records {
// 		t.Log("Deleting record...")
// 		deleteMutation := d.NewDeleteMutater()
// 		expectedResult = d.NewResulter()
// 		params, expectedParams = d.GetParams(expectedResult, false)
// 		if cluster.MultiTenant {
// 			params["tenant"] = tenant
// 		}
// 		params["id"] = record.(GraphqlResult).GetID()
// 		expectedParams["id"] = params["id"]
// 		for k, v := range fixedParams {
// 			params[k] = v
// 			expectedParams[k] = v
// 			r := reflect.ValueOf(v)
// 			if r.Kind() == reflect.Ptr {
// 				if r.Type().String() == "*graphql.ID" {
// 					if r.IsNil() {
// 						expectedParams[k] = nil
// 					}
// 				}
// 			}
// 		}
// 		err = mapstructure.Decode(expectedParams, &expectedResult)
// 		if err != nil {
// 			t.Errorf("Couldn't decode mapstructure \n%s", err)
// 		}
// 		args = map[string]interface{}{
// 			"id": params["id"],
// 		}
// 		if cluster.MultiTenant {
// 			args["tenant"] = graphql.String(params["tenant"].(string))
// 		}
// 		err = client.Mutate(context.Background(), deleteMutation, args)
// 		if err != nil {
// 			t.Errorf("Couldn't delete record\n%s", err)
// 		} else if !reflect.DeepEqual(deleteMutation.(GraphqlQuery).GetResult(), expectedResult) {
// 			t.Errorf("Unexpected result\n%v\n%v",
// 				deleteMutation.(GraphqlQuery).GetResult(), expectedResult)
// 		} else {
// 			t.Log("OK")
// 		}
// 	}
// }

// // GetParams todo
// func (d *Definition) GetParams(result interface{}, updatedValues bool) (
// 	map[string]interface{}, map[string]interface{}) {
// 	params := map[string]interface{}{}
// 	expectedParams := map[string]interface{}{}
// 	s := reflect.ValueOf(result).Elem()
// 	typeOfResult := s.Type()
// 	for i := 0; i < s.NumField(); i++ {
// 		fs := s.Field(i)
// 		if fs.Kind() == reflect.Ptr {
// 			fs = fs.Elem()
// 		}
// 		name := typeOfResult.Field(i).Name
// 		fieldType, _ := s.Type().FieldByName(name)
// 		fd := s.FieldByName(name)
// 		var value interface{}
// 		s := "test"
// 		if fieldType.Tag.Get("value1") != "" {
// 			s = fieldType.Tag.Get("value1")
// 		}
// 		if updatedValues && name != "ID" {
// 			s = "test2"
// 			if fieldType.Tag.Get("value2") != "" {
// 				s = fieldType.Tag.Get("value2")
// 			}
// 		}
// 		fmt.Println(fd.Type().String())
// 		typeField := fd.Type().String()
// 		originTypeField := typeField
// 		if fieldType.Tag.Get("inputType") != "" {
// 			typeField = fieldType.Tag.Get("inputType")
// 		}
// 		switch typeField {
// 		case "graphql.ID":
// 			value = graphql.ID(s)
// 		case "graphql.String":
// 			value = graphql.String(s)
// 		case "graphql.Int":
// 			value = graphql.Int(1)
// 			if updatedValues {
// 				value = graphql.Int(2)
// 			}
// 		case "graphql.Boolean":
// 			value = graphql.Boolean(true)
// 			if updatedValues {
// 				value = graphql.Boolean(false)
// 			}
// 		case "[]graphql.ID":
// 			value = []graphql.ID{graphql.ID(s)}
// 		}

// 		if fieldType.Tag.Get("input") != falseKey && fieldType.Tag.Get("input") != "" {
// 			if d.GenerateParamer != nil {
// 				v := d.GenerateParamer(originTypeField, updatedValues, fieldType.Tag.Get("input"))
// 				if v != nil {
// 					value = v
// 				}
// 			}
// 		}

// 		if fieldType.Tag.Get("input") != falseKey {
// 			if fieldType.Tag.Get("input") != "" {
// 				params[fieldType.Tag.Get("input")] = value
// 			} else {
// 				params[fieldType.Tag.Get("mapstructure")] = value
// 			}
// 		}

// 		if d.GenerateParamer != nil {
// 			v := d.GenerateParamer(originTypeField, updatedValues, "")
// 			if v != nil {
// 				value = v
// 			}
// 		}

// 		if fieldType.Tag.Get("mapstructure") != falseKey {
// 			expectedParams[fieldType.Tag.Get("mapstructure")] = value
// 		}
// 	}
// 	return params, expectedParams
// }

// // TestExecution todo
// type TestExecution struct {
// 	T        *testing.T
// 	Name     string
// 	Func     func(context.Context, interface{}, map[string]interface{}) error
// 	Query    interface{}
// 	Params   map[string]interface{}
// 	Expected interface{}
// 	Select   bool
// }

// // Exec todo
// func (e *TestExecution) Exec() {
// 	e.T.Logf("%s...", e.Name)
// 	err := e.Func(context.Background(), e.Query, e.Params)
// 	if err != nil {
// 		e.T.Errorf("Couldn't add product\n%s", err)
// 	}
// }

// // Check todo
// func (e *TestExecution) Check() {

// 	if e.Select {
// 		expected := e.Expected.([]interface{})
// 		for i, result := range e.Query.(GraphqlSelectQuery).GetResult() {
// 			if !reflect.DeepEqual(result, expected[i]) {
// 				e.T.Errorf("Unexpected result\n%+v\n%+v", result, expected[i])
// 			} else {
// 				e.T.Log("OK")
// 			}
// 		}
// 	} else {
// 		if !reflect.DeepEqual(e.Query.(GraphqlQuery).GetResult(), e.Expected) {
// 			e.T.Errorf("Unexpected result\n%+v\n%+v", e.Query.(GraphqlQuery).GetResult(), e.Expected)
// 		} else {
// 			e.T.Log("OK")
// 		}
// 	}
// }

// // func Decode(t *testing.T, expectedParams
// map[string]interface{}, expectedResult GraphqlResult) {
// // 	err := mapstructure.Decode(expectedParams, expectedResult)
// // 	if err != nil {
// // 		t.Errorf("Couldn't decode mapstructure \n%s", err)
// // 	}

// // 	d := reflect.ValueOf(expectedResult).Elem()
// // 	typeOfResult := d.Type()
// // 	for i := 0; i < d.NumField(); i++ {
// // 		fd := d.Field(i)
// // 		name := typeOfResult.Field(i).Name
// // 		fieldType, _ := d.Type().FieldByName(name)
// // 		t.Log(name)
// // 		t.Log(fd.Kind().String())
// // 		if fd.Kind() == reflect.Ptr {
// // 			t.Log("ok")
// // 			if fieldType.Tag.Get("mapstructure") != "" {
// // 				t.Log(fieldType.Tag.Get("mapstructure"))
// // 				if _, ok := expectedParams[fieldType.Tag.Get("mapstructure")]; ok {
// // 					t.Log("ok2")
// // 					t.Log(expectedParams[fieldType.Tag.Get("mapstructure")])
// // 					t.Log(reflect.Zero(fd.Type()))
// // 					v := reflect.ValueOf(expectedParams[fieldType.Tag.Get("mapstructure")])
// // 					test := v.IsNil()
// // 					t.Log(test)
// // 					if reflect.ValueOf(expectedParams[fieldType.Tag.Get("mapstructure")]).IsNil() {
// // 						t.Log("ok3")
// // 						fd.Set(reflect.Zero(fd.Type()))
// // 					}
// // 				}
// // 			}
// // 		}
// // 	}
// // }
