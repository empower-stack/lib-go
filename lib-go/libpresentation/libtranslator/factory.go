package libtranslator

import (
	"context"
	"fmt"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	// "gitlab.com/empowerlab/stack/lib-go/libutils"
)

// Pool represent a Model within the current environnement, so we can easily make operation on it.
type Factory struct {
	Aggregate                AggregateInterface
	Workflow                 context.Context
	New                      func() DomainObjectInterface
	Scan                     func(interface{}, *Collection) ([]DomainObjectInterface, error)
	CheckExternalForeignKeys func([]DomainObjectInterface) error
	Select                   func(
		filters []FilterInterface, with []string,
		offset *uint, limit *uint, orderByArg *string,
	) ([]DomainObjectInterface, error)
	Create                  func(records interface{}) ([]DomainObjectInterface, error)
	PostCreate              func(records *Collection) error
	PostCreateSelectFilters func([]FilterInterface, []DomainObjectInterface) []FilterInterface
	Upsert                  func([]FilterInterface, interface{}, bool) (interface{}, bool, error)
}

// Init will initialize the pool.
func (d *Factory) Init() {
	d.Create = d.create
	d.PostCreate = d.postCreate
	d.Upsert = d.upsert
}

type FactoryInterface interface {
}

func (d *Factory) create(records interface{}) ([]DomainObjectInterface, error) {
	// var fields []string
	// for _, record := range records {
	// 	fmt.Printf("test %+v\n", record)
	// 	if len(fields) == 0 {
	// 		fields = libutils.GetDBFields(record)
	// 	}
	// 	if 1 == 0 {
	// 		err := Validate.Struct(record)
	// 		if err != nil {
	// 			return nil, errors.Wrap(err, "Couldn't validate template")
	// 		}
	// 	}
	// }

	// err := d.CheckExternalForeignKeys(records)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Check External Foreign keys fail")
	// }

	// if !d.Definition.Model.CanAssignID {
	// fields = libutils.RemoveStringsInSlice(fields, []string{"id"})
	// }
	// if d.Definition.TranslatableFields != nil {
	// 	fields = libutils.RemoveStringsInSlice(fields, d.Definition.TranslatableFields)
	// }
	// fields = libutils.RemoveStringsInSlice(fields, []string{"created_at", "updated_at"})
	// fmt.Printf("TEST %+v %v\n", fields, len(fields))

	// fmt.Println(records)
	// rows, errI := d.Aggregate.RepositoryInterface().Insert(d.Workflow, records)

	// fmt.Println("rows")
	// fmt.Printf("%+v\n", rows)
	// fmt.Printf(d.Aggregate.Definition().AggregateRoot.Name)
	// fmt.Printf("%v", d.Scan)

	// We scan before testing error in case some rows were still returned
	// records, err = d.Scan(rows, nil)
	// if err != nil {

	// 	fmt.Println("error")
	// 	fmt.Printf("%+v\n", err)

	// 	return nil, err
	// }

	// fmt.Println("records")
	// fmt.Printf("%+v\n", records)

	// if errI != nil {

	// 	fmt.Println("errorI")
	// 	fmt.Printf("%+v\n", errI)

	// 	res := &Collection{
	// 		Aggregate: d.Aggregate,
	// 		Workflow:  d.Workflow,
	// 	}
	// 	res.Init(rows)
	// 	return rows, errors.Wrap(errI, "Couldn't insert template")
	// }
	// fmt.Printf("rows %+v\n", rows)

	// // var recordIds []string
	// // nolint: dupl
	// switch tx.Type {
	// case libdata.CassandraType:
	// 	for i, id := range rows.([]string) {
	// 		records[i].(ObjectInterface).SetID(id)
	// 	}
	// 	// recordIds = rows.([]string)
	// case libdata.PostgresType:
	// i := 0
	// for rows.(*sqlx.Rows).Next() {
	// 	fmt.Println(i)
	// 	err = rows.(*sqlx.Rows).StructScan(records[i])
	// 	if err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't scan to tag")
	// 	}
	// 	// recordIds = append(recordIds, records[i].(modelInterface).GetID())
	// 	i = i + 1
	// 	fmt.Println(i)
	// }
	// records, err = d.Scan(rows, nil)
	// if err != nil {
	// 	return nil, err
	// }
	// fmt.Printf("rows %+v\n", rows)
	// default:
	// 	return nil, nil, &libdata.TypeError{}
	// }

	// fmt.Printf("records %+v\n", records[0])

	// if d.TranslamodelFields != nil {
	// 	defaultLang, errT := SettingGet(ctx, tx, "languageDefault")
	// 	if errT != nil {
	// 		return nil, nil, errors.Wrap(errT, "Couldn't get language default")
	// 	}

	// 	lang := ctx.Value(LangKey).(string)
	// 	langs := []string{*defaultLang}
	// 	if lang != *defaultLang {
	// 		langs = append(langs, lang)
	// 	}

	// 	toCreate := d.CreateTrer(records, langs)
	// 	// var requirementTrsToCreate []*RequirementTr
	// 	// for _, record := range records {
	// 	// 	requirement := record.(*Requirement)
	// 	// 	for _, l := range langs {
	// 	// 		requirementTrsToCreate = append(requirementTrsToCreate, &RequirementTr{
	// 	// 			ID:          requirement.ID,
	// 	// 			Lang:        l,
	// 	// 			Title:       requirement.Title,
	// 	// 			Description: requirement.Description,
	// 	// 		})
	// 	// 	}
	// 	// }

	// 	// fmt.Printf("requirementTrsToCreate %+v\n", requirementTrsToCreate)
	// 	_, _, err = d.TranslateModel.Creater(ctx, tx, toCreate)
	// 	if err != nil {
	// 		return nil, nil, errors.Wrap(err, "Couldn't create requirement translation")
	// 	}
	// }

	// for _, record := range records {
	// 	model := record.(libdomain.ObjectInterface)
	// 	errE := d.Env.Transaction.RegisterEvent(d.Definition.Model.Name, model.GetID(), "created", "{}")
	// 	if errE != nil {
	// 		return nil, errE
	// 	}
	// }

	fmt.Println("records")
	fmt.Printf("%+v\n", records)

	// recordIds := GetIdsFromRecordList(rows)
	// filters := []*libdata.Filter{{
	// 	Field: "id", Operator: "IN", Operande: recordIds,
	// }}
	// if d.PostCreateSelectFilters != nil {
	// 	filters = d.PostCreateSelectFilters(filters, rows)
	// }

	// fmt.Println("filters")
	// for _, filter := range filters {
	// 	fmt.Printf("%+v\n", filter)
	// }

	// results, err := d.Select(filters, []string{}, nil, nil, nil)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't get templates")
	// }

	// err = d.PostCreate(results)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't execute post creation")
	// }

	// results := &Collection{
	// 	Aggregate: d.Aggregate,
	// 	Workflow:  d.Workflow,
	// }
	// results.Init(rows)
	// return rows, nil

	return nil, nil
}

// nolint: unparam
func (d *Factory) upsert(
	filters []FilterInterface,
	record interface{},
	updateMask bool,
) (interface{}, bool, error) {

	// records, err := d.Select(where, []string{}, nil, nil, nil)
	// if err != nil {
	// 	return nil, false, errors.Wrap(err, "Couldn't get templates")
	// }

	// var wasCreated bool
	// if len(records) > 0 {
	// 	_, err = records.Update(record)
	// 	if err != nil {
	// 		return nil, false, errors.Wrap(err, "Couldn't get templates")
	// 	}
	// } else {
	// 	records, err = d.Create([]DomainObjectInterface{record.(DomainObjectInterface)})
	// 	if err != nil {
	// 		return nil, false, errors.Wrap(err, "Couldn't get templates")
	// 	}
	// 	wasCreated = true
	// }

	// for _, r := range records.Slice() {
	// 	record = r
	// }

	return record, true, nil
}

func (d *Factory) postCreate(records *Collection) error {
	return nil
}

// GetIdsFromRecordList will return the id from a list of records.
func GetIdsFromRecordList(records []DomainObjectInterface) []string {
	var recordIds []string
	for _, record := range records {
		recordIds = append(recordIds, record.(DomainObjectInterface).GetID())
	}
	return recordIds
}
