package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	// "gitlab.com/empowerlab/stack/lib-go/libdata/fields"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/arguments"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed domain-*.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Domains(defs *libtranslator.Definitions) {

	var err error
	fileInit, _ := fdomain.ReadFile("domain-init.go.tmpl")
	initTemplate := template.Must(initFuncs.Parse(string(fileInit)))

	fileExternal, _ := fdomain.ReadFile("domain-external.go.tmpl")
	externalTemplate := template.Must(externalFuncs.Parse(string(fileExternal)))

	file, _ := fdomain.ReadFile("domain-main.go.tmpl")
	objectsTemplate := template.Must(aggregateFuncs.Parse(string(file)))

	fileRequests, _ := fdomain.ReadFile("domain-requests.go.tmpl")
	requestsTemplate := template.Must(requestsFuncs.Parse(string(fileRequests)))

	// event := false
	// if defs.EventClient != nil {
	// 	event = true
	// }

	err = os.MkdirAll("./gen", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	importsMap := map[string]struct {
		DomainName string
		DomainPath string
		Aggregate  libtranslator.TableInterface
	}{}
	// var aggregates []*libtranslator.AggregateDefinition
	for _, definition := range defs.Slice() {

		// customFuncsPool, customFuncsCollection := definition.GetCustomFuncsData()

		// var aggregateRootField libdata.Field
		// var aggregateNestedField libdata.Field
		// if definition.Model.Aggregate != nil && definition.Model.Aggregate.Name != definition.Model.Name {
		// 	for _, field := range definition.Model.Fields {
		// 		if field.GetReference() != nil && (field.GetReference().Name == definition.Model.Aggregate.Name) {
		// 			aggregateRootField = field
		// 			for _, nestedField := range field.GetReference().Fields {
		// 				if nestedField.GetReferenceName() == definition.Model.Name && nestedField.GetInverseField() == field.Title() {
		// 					aggregateNestedField = nestedField
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		d := struct {
			Repository string
			// Event      bool
			Service *libtranslator.ServiceDefinition
			// AggregateRootField    libdata.Field
			// AggregateNestedField  libdata.Field
			// Model                 *libdata.ModelDefinition
			// CustomCommandsAggregate  []*libtranslator.CustomRequest
			// CustomCommandsCollection []*libtranslator.CustomRequest
			Import string
		}{
			Repository: defs.Repository,
			// Event:      event,
			Service: definition,
			// AggregateRootField:    aggregateRootField,
			// AggregateNestedField:  aggregateNestedField,
			// Model:                 definition.Model,
			// CustomFuncsPool:       definition.CustomFuncsPool,
			// CustomCommandsAggregate:  definition.GetCustomRequests(),
			// CustomCommandsCollection: definition.AggregateRoot.Commands.CustomCommands,
		}

		imports := map[string]string{}
		for _, object := range definition.Objects() {
			path := object.Definition.DomainPackage() + " \"" + object.Definition.DomainPath() + "\""
			imports[path] = path
			if object.Definition.IsEntity() {
				path = object.Definition.DomainPackage() + "identities \"" + object.Definition.DomainPath() + "/identities\""
				imports[path] = path
			}
			if object.Definition.GetType() != "Representation" {
				for _, property := range object.Properties {
					if !property.InInput() {
						continue
					}
					switch v := property.Definition().(type) {
					case *properties.IDDefinition, *properties.SliceDefinition:
						if v.GetReference().DomainPath() != "" {
							path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
							imports[path] = path
						}
					case *properties.OrderedMapDefinition:
						if v.GetReference().DomainPath() != "" {
							path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
							imports[path] = path
						}
					case *properties.SelectionDefinition:
						if v.GetReference().DomainPath() != "" {
							path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
							imports[path] = path
						}
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		// aggregates = append(aggregates, definition)

		buf := &bytes.Buffer{}
		err := objectsTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("./gen/%s-objects.gen.go", strcase.ToSnake(definition.Definition().Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		if len(definition.Requests()) > 0 {

			imports = map[string]string{}
			for _, request := range definition.Requests() {
				if !request.Definition.IsCommand() {
					path := definition.Definition().ApplicationPackage() + "queries \"" + definition.Definition().ApplicationPath() + "/queries\""
					imports[path] = path
				} else {
					path := definition.Definition().ApplicationPackage() + "commands \"" + definition.Definition().ApplicationPath() + "/commands\""
					imports[path] = path
				}
				for _, arg := range request.Args {
					switch v := arg.Definition().(type) {
					case *arguments.SliceDefinition:
						switch vv := v.PropertyDefinition.(type) {
						case *properties.IDDefinition:
							if vv.GetReference().DomainPath() != "" {
								path := vv.GetReference().DomainPackage() + "identities \"" + vv.GetReference().DomainPath() + "/identities\""
								imports[path] = path
							}
						case *properties.EntityDefinition, *properties.ValueObjectDefinition:
							if vv.GetReference().DomainPath() != "" {
								path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
								imports[path] = path
							}
						}
					case *arguments.IDDefinition:
						if v.GetReference().DomainPath() != "" {
							path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
							imports[path] = path
						}
					case *arguments.SelectionDefinition:
						path := v.EnumDefinition.Package + " \"" + v.EnumDefinition.Path + "\""
						imports[path] = path
					case *arguments.EntityDefinition, *arguments.ValueObjectDefinition:
						if v.GetReference().DomainPath() != "" && v.InputType() != "" {
							path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
							imports[path] = path
						}
					}
				}
				for _, _ = range request.ResultsQueries {
					path := definition.Definition().ApplicationPackage() + "queries \"" + definition.Definition().ApplicationPath() + "/queries\""
					imports[path] = path
					// path = definition.Definition().ApplicationPackage() + "data \"" + definition.Definition().ApplicationPath() + "/data\""
					// imports[path] = path
					// for _, result := range rq.Results {
					// 	switch v := result.Definition().(type) {
					// 	case *properties.Entity:
					// 		if v.GetReference().DomainPath() != "" {
					// 			path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
					// 			imports[path] = path
					// 		}
					// 	}
					// }
				}
			}
			d.Import = strings.Join(libutils.MapKeys(imports), "\n")

			buf = &bytes.Buffer{}
			err = requestsTemplate.Execute(buf, d)
			if err != nil {
				fmt.Println(buf)
				fmt.Println("execute ", err)
			}
			content, err = format.Source(buf.Bytes())
			if err != nil {
				fmt.Println("model ", err)
				content = buf.Bytes()
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("./gen/%s-requests.gen.go", strcase.ToSnake(definition.Definition().Name)),
				content, 0644)
			if err != nil {
				fmt.Println(err)
			}
		}

		// for _, table := range definition.GetTables() {
		// 	for _, field := range table.StoredProperties() {
		// 		if field.GetReference() != nil {
		// 			if field.GetReference().DomainPath() != defs.Repository {
		// 				importsMap[field.GetReference().GetName()] = struct {
		// 					DomainName string
		// 					DomainPath string
		// 					Aggregate  libtranslator.TableInterface
		// 				}{
		// 					DomainName: field.GetReference().GetName(),
		// 					DomainPath: field.GetReference().DomainPath(),
		// 					Aggregate:  field.GetReference(),
		// 				}
		// 			}
		// 		}
		// 	}
		// 	for _, request := range definition.GetCustomRequests() {
		// 		for _, result := range request.Results {
		// 			if result.GetReference() != nil {
		// 				if result.GetReference().DomainPath() != defs.Repository {
		// 					importsMap[result.GetReference().GetName()] = struct {
		// 						DomainName string
		// 						DomainPath string
		// 						Aggregate  libtranslator.TableInterface
		// 					}{
		// 						DomainName: result.GetReference().GetName(),
		// 						DomainPath: result.GetReference().DomainPath(),
		// 						Aggregate:  result.GetReference(),
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }

	}

	// d := struct {
	// 	Repository string
	// 	Event      bool
	// 	Timestamp  time.Time
	// 	Models     []*libdata.ModelDefinition
	// }{
	// 	Repository: defs.Repository,
	// 	Event:      event,
	// 	Timestamp:  time.Now(),
	// 	Models:     models,
	// }

	// buf := &bytes.Buffer{}
	// err = dataTemplate.Execute(buf, d)
	// if err != nil {
	// 	fmt.Println("execute ", err)
	// }
	// // content, err := format.Source(buf.Bytes())
	// // if err != nil {
	// // 	fmt.Println(buf)
	// // 	fmt.Println("data model ", err)
	// // }
	// err = ioutil.WriteFile(
	// 	"gen/orm/data.gen.go",
	// 	buf.Bytes(), 0644)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// buf := &bytes.Buffer{}
	// err = initTemplate.Execute(buf, d)
	// if err != nil {
	// 	fmt.Println("execute ", err)
	// }
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("init model ", err)
	// 	content = buf.Bytes()
	// }
	// err = ioutil.WriteFile(
	// 	"gen/orm/init.gen.go",
	// 	content, 0644)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	d := struct {
		Repository string
		Services   []*libtranslator.ServiceDefinition
	}{
		Repository: defs.Repository,
		Services:   defs.Slice(),
	}
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("./gen/init.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	externalAggregates := []struct {
		DomainName string
		DomainPath string
		Aggregate  libtranslator.TableInterface
	}{}
	for _, value := range importsMap {
		externalAggregates = append(externalAggregates, value)
	}
	externalData := struct {
		ExternalAggregates []struct {
			DomainName string
			DomainPath string
			Aggregate  libtranslator.TableInterface
		}
	}{
		ExternalAggregates: externalAggregates,
	}
	buf = &bytes.Buffer{}
	err = externalTemplate.Execute(buf, externalData)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("./gen/external.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

var initFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var externalFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

// nolint:lll
var aggregateFuncs = template.New("").Funcs(template.FuncMap{
	"getCustomCommandSingleton": func(aggregate *libtranslator.AggregateDefinition, table libtranslator.TableInterface, customCommand *libtranslator.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (t *{{.ModelTitle}}) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   collection := &{{.ModelTitle}}Collection{}
		   collection.Init([]libtranslator.DomainObjectInterface{t})
		   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(collection, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libtranslator.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandPrototype": func(customFunc *libtranslator.CustomRequest) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandCollection": func(aggregate *libtranslator.AggregateDefinition, table libtranslator.TableInterface, customCommand *libtranslator.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (c *{{.ModelTitle}}Collection) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(c, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libtranslator.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandAggregatePrototype": func(aggregate *libtranslator.AggregateDefinition, customFunc *libtranslator.CustomRequest, isAggregate bool) string {

		content := "{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}} func({{if not .OnFactory}}*{{.AggregateTitle}}Collection,{{else}}context.Context,{{end}} *{{.AggregateTitle}}InternalLibrary, *pb.{{.AggregateTitle}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) ({{if not .OnFactory}}map[string]{{end}}*pb.{{.AggregateTitle}}{{.Title}}Results, error)"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		// for _, result := range customFunc.Results {
		// 	goType := result.GoType()
		// 	if result.Type() == properties.Many2onePropertyType {
		// 		goType = "*" + strcase.ToCamel(result.GetReferenceName())
		// 	}
		// 	results = append(results, string(goType))
		// }
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsCommand      bool
			OnFactory      bool
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			IsCommand:      customFunc.IsCommand(),
			OnFactory:      customFunc.OnFactory,
			IsAggregate:    isAggregate,
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandRepositoryPrototype": func(aggregate *libtranslator.AggregateDefinition, customFunc *libtranslator.CustomRequest, isAggregate bool) string {

		content := "{{.Title}}({{if not .IsAggregate}}*{{.AggregateTitle}}Collection,{{else}}context.Context,{{end}} *{{.AggregateTitle}}InternalLibrary, {{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		// for _, result := range customFunc.Results {
		// 	goType := result.GoType()
		// 	if result.Type() == properties.Many2onePropertyType {
		// 		goType = "*" + strcase.ToCamel(result.GetReferenceName())
		// 	}
		// 	results = append(results, string(goType))
		// }
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			IsAggregate:    isAggregate,
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandAggregate": func(aggregate *libtranslator.AggregateDefinition, customCommand *libtranslator.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
		type {{.Aggregate.AggregateRoot.Title}}{{.Title}}Results struct{
			{{- range .Results}}
			{{.Title}} {{.GoType}}
			{{- end}}
		}
		{{if .OnFactory}}
	   	func (a *{{.Aggregate.AggregateRoot.Title}}Factory) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
	   
		   results, err := {{.Aggregate.AggregateRoot.Title}}Aggregate.{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}}(a.Workflow, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, command)
		   if err != nil {
			   return nil, err
		   }
		   return results, nil
	   
	   	}
		{{else}}
		func (a *{{.Aggregate.AggregateRoot.Title}}Collection) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (map[string]*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
	   
			results, err := {{.Aggregate.AggregateRoot.Title}}Aggregate.{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}}(a, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, command)
			if err != nil {
				return nil, err
			}

			if results == nil {
				results = map[string]*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results{}
			}
			for _, record := range a.Slice() {
				if _, ok := results[record.pb.ID]; !ok {
					results[record.pb.ID] = &pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results{}
				}
			}

			return results, nil
		
		}
		func (a *{{.Aggregate.AggregateRoot.Title}}) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
			
			/*
			collection := &{{.Aggregate.AggregateRoot.Title}}Collection{
				Collection: &libtranslator.Collection{
					Aggregate: {{.Aggregate.AggregateRoot.Title}}Aggregate,
					Workflow: a.Workflow,
				},
			}
			collection.Init([]libtranslator.DomainObjectInterface{a})
			*/

			collection := {{.Aggregate.AggregateRoot.Title}}Aggregate.NewFactory(a.Workflow).NewCollection([]*{{.Aggregate.AggregateRoot.Title}}{a})
	   
			results, err := collection.{{.Title}}(command)
			if err != nil {
				return nil, err
			}

			return results[a.pb.ID], nil
		
		}	
		{{end}}
		`

		var argsPrototype []string
		var args []string
		// for _, arg := range customCommand.Args {
		// 	goType := arg.GoType()
		// 	if arg.Type() == properties.Many2onePropertyType {
		// 		goType = "*" + strcase.ToCamel(arg.GetReferenceName())
		// 	}
		// 	argsPrototype = append(argsPrototype, arg.GetName()+" "+goType)
		// 	args = append(args, arg.GetName())
		// }

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			goType := result.GoType()
			// if result.Type() == properties.Many2onePropertyType {
			// 	goType = "*" + strcase.ToCamel(result.GetReferenceName())
			// }
			resultsPrototype = append(resultsPrototype, string(goType))
			results = append(results, result.GetName())
			returnKO = append(returnKO, result.GoNil())
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libtranslator.AggregateDefinition
			IsCommand        bool
			OnFactory        bool
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          []libtranslator.ArgsInterface
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			IsCommand:        customCommand.IsCommand(),
			OnFactory:        customCommand.OnFactory,
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          customCommand.Results,
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	// "getRecursiveOne2many": func(entity *libtranslator.OldEntityDefinition) []struct {
	// 	Table    libtranslator.TableInterface
	// 	// Property *properties.One2many
	// 	Path     string
	// } {

	// 	results := recursiveOne2ManySearch(entity, "", []struct {
	// 		Table    libtranslator.TableInterface
	// 		// Property *properties.One2many
	// 		Path     string
	// 	}{})

	// 	for i, j := 0, len(results)-1; i < j; i, j = i+1, j-1 {
	// 		results[i], results[j] = results[j], results[i]
	// 	}

	// 	return results

	// },
	"makeSlice": func(args ...string) []string {
		return args
	},
	"convertJson": func(field libtranslator.Property) string {
		// if field.Type() == properties.FloatPropertyType {
		// 	return fmt.Sprintf("%vField := data[\"%v\"].(float64)", field.GetName(), field.GetName())
		// }
		return fmt.Sprintf("%vField := data[\"%v\"].(string)", field.GetName(), field.GetName())
	},
	"getImportExternal": func(repository string, aggregate *libtranslator.AggregateDefinition) string {
		importsMap := map[string]string{}
		for _, table := range aggregate.GetTables() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != repository {
						importsMap[field.GetReference().GetName()] = field.GetReference().DomainPath()
					}
				}
			}
		}
		var imports []string
		for name, path := range importsMap {
			imports = append(imports, fmt.Sprintf("%s \"%s/gen\"", name, path))
		}
		return strings.Join(imports, "\n")
	},
	"getExternalEntities": func(repository string, aggregate *libtranslator.AggregateDefinition) string {
		importsMap := map[string]string{}
		for _, table := range aggregate.GetTables() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != repository {
						importsMap[field.GetReference().GetName()] = field.GetReference().Title()
					}
				}
			}
		}
		var imports []string
		for name, entity := range importsMap {
			imports = append(imports, fmt.Sprintf("type %s = %s.%s", entity, name, entity))
			imports = append(imports, fmt.Sprintf("var %sEntity = %s.%sEntity", entity, name, entity))
		}
		return strings.Join(imports, "\n")
	},
	// "getScanFunc": func(model *libdata.ModelDefinition) string {
	// 	if model.Client != nil {
	// 		return model.Client.GenerateScanFunc(model)
	// 	}
	// 	return fmt.Sprintf(`
	// 	func (d *%sPool) scan(
	// 		rows interface{}, collection *liborm.Collection,
	// 	) ([]libdata.ModelInterface, error) {
	// 		var records []libdata.ModelInterface
	// 		return records, nil
	// 	}`, model.Title())
	// },
	// "getImportName": func(field libdata.Field) string {
	// 	return field.GetReference().ExternalDriver.GetImportName()
	// },
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var requestsFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
	"getQueryGetProperties": func(queryResult libdomain.ArgumentDefinition, requestResults []*libtranslator.ArgumentDefinition) string {
		for _, result := range requestResults {
			if result.Definition().GetName() == queryResult.GetName() {
				return fmt.Sprintf("ConvertTo%sGetProperties(args.GetProperties.%s)", result.Definition().GetReference().Title(), result.Definition().Title())
			}
		}
		return "nil"
	},
})

// nolint:lll
var dataTemplate = template.Must(template.New("").Parse(`

`))

// nolint:lll
var initTemplate = template.Must(template.New("").Parse(`
`))

// func recursiveOne2ManySearch(
// 	table libtranslator.TableInterface,
// 	path string,
// 	results []struct {
// 		Table    libtranslator.TableInterface
// 		Property *properties.One2many
// 		Path     string
// 	},
// ) []struct {
// 	Table    libtranslator.TableInterface
// 	Property *properties.One2many
// 	Path     string
// } {

// 	for _, property := range table.NestedProperties() {
// 		if property.Type() == properties.One2manyPropertyType {
// 			results = append(results, struct {
// 				Table    libtranslator.TableInterface
// 				Property *properties.One2many
// 				Path     string
// 			}{
// 				Table:    table,
// 				Property: property.(*properties.One2many),
// 				Path:     path,
// 			})
// 			path = path + property.GetName() + "/"
// 			results = recursiveOne2ManySearch(property.GetReference(), path, results)
// 		}
// 	}

// 	return results
// }
