package libtranslator

type Event struct {
	ID          string
	Aggregate   AggregateInterface
	AggregateID string
	Name        string
	Payload     map[string]interface{}
}

type EventStoreClient interface {
	RegisterEvents(WorkflowInterface, AggregateInterface, []*Event) error
	MarkDispatchedEvent(WorkflowInterface, *Event) error
	GetAggregateEvents(WorkflowInterface, AggregateInterface, string) ([]*Event, error)
}

type EventDispatchClient interface {
	DispatchEvent(
		// wk *Workflow,
		event *Event) error
}
