package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// One2manyPropertyType contains the field type for One2many
const One2manyPropertyType = libtranslator.PropertyType("One2manyType")

/*One2many is the field type you can use in definition to declare a One2many field.
 */
type One2many struct {
	Name            string
	String          string
	Child           string
	ChildDefinition libtranslator.TableInterface
	InverseProperty string
	TitleName       string
	DBName          string
	LoadedPosition  int
	Position        int
}

// GetName return the name of the field.
func (f *One2many) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *One2many) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *One2many) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *One2many) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *One2many) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *One2many) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *One2many) Type() libtranslator.PropertyType {
	return One2manyPropertyType
}

// Type return the type of the field.
func (f *One2many) GoType() string {
	return libtranslator.STRING
}

// Type return the type of the field.
func (f *One2many) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *One2many) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *One2many) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *One2many) ProtoType() string {
	return f.GoType()
}

func (f *One2many) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *One2many) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *One2many) DBType() *libtranslator.DBType {
	return nil
}

// Type return the type of the field.
func (f *One2many) GraphqlType() string {
	return f.GetReference().Title()
}

// Type return the type of the field.
func (f *One2many) GraphqlSchemaType() string {
	return f.GetReference().Title()
}

// func (f *One2many) GoType() string {
// 	reference := ""
// 	referenceDefinition := f.GetReferenceDefinition()
// 	if referenceDefinition != nil {
// 		reference = referenceDefinition.GetTemplateData().Title
// 	}
// 	return reference
// }

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *One2many) GetReferenceName() string {
	return f.Child
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *One2many) GetReference() libtranslator.TableInterface {
	return f.ChildDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *One2many) SetReference(d libtranslator.TableInterface) {
	f.ChildDefinition = d
}

// GetInverseProperty return the inverse field, if applicable.
func (f *One2many) GetInverseProperty() string {
	return strcase.ToCamel(f.InverseProperty)
}

// GetInverseProperty return the inverse field, if applicable.
func (f *One2many) InversePropertySnake() string {
	return strcase.ToSnake(f.InverseProperty)
}

// GetInverseProperty return the inverse field, if applicable.
func (f *One2many) InversePropertyUpper() string {
	return strings.ToUpper(f.InversePropertySnake())
}

// GetRequired return if this field is required or not.
func (f *One2many) GetRequired() bool {
	return false
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *One2many) GetPrimaryKey() bool {
	return false
}

func (f *One2many) IsStored() bool {
	return false
}

func (f *One2many) IsNested() bool {
	return true
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *One2many) GetFieldData() *libtranslator.FieldData {

// 	// var reference *ReferenceData
// 	// referenceDefinition := f.GetReferenceDefinition()
// 	// if referenceDefinition != nil {
// 	// 	reference = &ReferenceData{}
// 	// 	if referenceDefinition != f.Model {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// 	// 			Title: referenceDefinition.GetTemplateData().Title,
// 	// 		}
// 	// 	} else {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  result.Name,
// 	// 			Title: result.Title,
// 	// 		}
// 	// 	}
// 	// }

// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:             One2manyPropertyType.GoType(),
// 		// GoType:           One2manyPropertyType.GoType(),
// 		// ProtoType:        One2manyPropertyType.ProtoType(),
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         false,
// 		Nested:           true,
// 		InverseProperty:     strings.Title(f.InverseProperty),
// 		// Definition:       f,
// 	}
// }

func (f *One2many) SetPosition(position int) {
	f.Position = position
}

func (f *One2many) GetPosition() int {
	return f.Position
}

func (r *One2many) IsRepeated() bool {
	return false
}

func (r *One2many) IsTranslatable() bool {
	return false
}

func (r *One2many) GetReturnDetailsInTests() bool {
	return false
}

func (r *One2many) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *One2many) GetLoadedPositionMany2one() int {
	return 0
}
func (r *One2many) GetPositionMany2one() int {
	return 0
}
