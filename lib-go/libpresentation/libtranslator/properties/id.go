package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// IDPropertyType contains the field type for ID
const IDPropertyType = libtranslator.PropertyType("IDType")

/*ID is the field type you can use in definition to declare an ID field.
 */
type ID struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *ID) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *ID) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *ID) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *ID) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *ID) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *ID) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *ID) Type() libtranslator.PropertyType {
	return IDPropertyType
}

// Type return the type of the field.
func (f *ID) GoType() string {
	return libtranslator.STRING
}

// Type return the type of the field.
func (f *ID) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *ID) GoNil() string {
	return "\"\""
}

// Type return the type of the field.
func (f *ID) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *ID) ProtoType() string {
	return f.GoType()
}

func (f *ID) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *ID) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *ID) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *ID) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *ID) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *ID) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *ID) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *ID) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *ID) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *ID) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *ID) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *ID) IsStored() bool {
	return true
}

func (f *ID) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *ID) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            IDPropertyType.GoType(),
// 		// GoType:          IDPropertyType.GoType(),
// 		// ProtoType:       IDPropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *ID) SetPosition(position int) {
	f.Position = position
}

func (f *ID) GetPosition() int {
	return f.Position
}

func (r *ID) IsRepeated() bool {
	return false
}

func (r *ID) IsTranslatable() bool {
	return false
}

func (r *ID) GetReturnDetailsInTests() bool {
	return false
}

func (r *ID) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *ID) GetLoadedPositionMany2one() int {
	return 0
}
func (r *ID) GetPositionMany2one() int {
	return 0
}
