package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// BooleanPropertyType contains the field type for Boolean
const BooleanPropertyType = libtranslator.PropertyType("BooleanType")

/*Boolean is the field type you can use in definition to declare a Boolean field.
 */
type Boolean struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *Boolean) GetName() string {
	return f.Name
}

// GetTitleName return the title of the field.
func (f *Boolean) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Boolean) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Boolean) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Boolean) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Boolean) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Boolean) Type() libtranslator.PropertyType {
	return BooleanPropertyType
}

// Type return the type of the field.
func (f *Boolean) GoType() string {
	return "bool"
}

// Type return the type of the field.
func (f *Boolean) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Boolean) GoNil() string {
	return "false"
}

// Type return the type of the field.
func (f *Boolean) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Boolean) ProtoType() string {
	return f.GoType()
}

func (f *Boolean) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Boolean) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Boolean) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullBool",
		Value: "Bool",
	}
}

// Type return the type of the field.
func (f *Boolean) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Boolean) GraphqlSchemaType() string {
	return "Boolean"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Boolean) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Boolean) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Boolean) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Boolean) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Boolean) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Boolean) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Boolean) IsStored() bool {
	return true
}

func (f *Boolean) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Boolean) GetFieldData() *libtranslator.FieldData {
// 	return &libdomain.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            BooleanPropertyType.GoType(),
// 		// GoType:          BooleanPropertyType.GoType(),
// 		// ProtoType:       BooleanPropertyType.ProtoType(),
// 		DBType: &libdomain.DBType{
// 			Type:  "sql.NullBool",
// 			Value: "Bool",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Boolean) SetPosition(position int) {
	f.Position = position
}

func (f *Boolean) GetPosition() int {
	return f.Position
}

func (r *Boolean) IsRepeated() bool {
	return false
}

func (r *Boolean) IsTranslatable() bool {
	return false
}

func (r *Boolean) GetReturnDetailsInTests() bool {
	return false
}

func(r *Boolean) GetLoadedPosition() int {
	return r.LoadedPosition
}
func(r *Boolean) GetLoadedPositionMany2one() int {
	return 0
}
func(r *Boolean) GetPositionMany2one() int {
	return 0
}
