package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// Many2manyPropertyType contains the field type for Many2many
const Many2manyPropertyType = libtranslator.PropertyType("Many2manyType")

/*Many2many is the field type you can use in definition to declare a Many2many field.
 */
type Many2many struct {
	Name                string
	String              string
	Reference           string
	ReferenceDefinition libtranslator.TableInterface
	Relation            string
	InverseProperty     string
	TargetProperty      string
	OnDelete            libtranslator.OnDeleteValue
	// ValueObject         *libtranslator.ValueObjectDefinition
	TitleName               string
	DBName                  string
	LoadedPosition          int
	Position                int
	LoadedPositionMany2many int
	PositionMany2many       int
}

// GetName return the name of the field.
func (f *Many2many) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Many2many) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Many2many) NameWithoutID() string {
	return strings.Replace(f.Name, "Id", "", -1)
}

// Title return the title of the field.
func (f *Many2many) TitleWithoutID() string {
	return strings.Replace(f.Title(), "Id", "", -1)
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Many2many) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Many2many) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Many2many) Type() libtranslator.PropertyType {
	return Many2manyPropertyType
}

// Type return the type of the field.
func (f *Many2many) GoType() string {
	return libtranslator.STRING
}

// Type return the type of the field.
func (f *Many2many) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Many2many) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *Many2many) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Many2many) ProtoType() string {
	return f.GoType()
}

func (f *Many2many) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Many2many) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Many2many) DBType() *libtranslator.DBType {
	return nil
}

// Type return the type of the field.
func (f *Many2many) GraphqlType() string {
	return f.GetReference().Title()
}

// Type return the type of the field.
func (f *Many2many) GraphqlSchemaType() string {
	return f.GetReference().Title()
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Many2many) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Many2many) GetReference() libtranslator.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Many2many) SetReference(d libtranslator.TableInterface) {
	f.ReferenceDefinition = d
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) RelationTitle() string {
	return strcase.ToCamel(f.Relation)
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) RelationUpper() string {
	return strings.ToUpper(strcase.ToSnake(f.Relation))
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) GetInverseProperty() string {
	return f.InverseProperty
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) InverseTitle() string {
	return strcase.ToCamel(f.InverseProperty)
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) InverseSnake() string {
	return strcase.ToSnake(f.InverseProperty)
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) InverseUpper() string {
	return strings.ToUpper(f.InverseSnake())
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) TargetTitle() string {
	return strcase.ToCamel(f.TargetProperty)
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2many) TargetSnake() string {
	return strcase.ToSnake(f.TargetProperty)
}

// GetRequired return if this field is required or not.
func (f *Many2many) GetRequired() bool {
	return false
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Many2many) GetPrimaryKey() bool {
	return false
}

func (f *Many2many) IsStored() bool {
	return false
}

func (f *Many2many) IsNested() bool {
	return true
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Many2many) GetFieldData() *libtranslator.FieldData {

// 	// var reference *ReferenceData
// 	// referenceDefinition := f.GetReferenceDefinition()
// 	// if referenceDefinition != nil {
// 	// 	reference = &ReferenceData{}
// 	// 	if referenceDefinition != f.Model {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// 	// 			Title: referenceDefinition.GetTemplateData().Title,
// 	// 		}
// 	// 	} else {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  result.Name,
// 	// 			Title: result.Title,
// 	// 		}
// 	// 	}
// 	// }

// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:             Many2manyPropertyType.GoType(),
// 		// GoType:           Many2manyPropertyType.GoType(),
// 		// ProtoType:        Many2manyPropertyType.ProtoType(),
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         false,
// 		Nested:           true,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Many2many) SetPosition(position int) {
	f.Position = position
}

func (f *Many2many) GetPosition() int {
	return f.Position
}

func (r *Many2many) IsRepeated() bool {
	return false
}

func (r *Many2many) IsTranslatable() bool {
	return false
}

func (r *Many2many) GetReturnDetailsInTests() bool {
	return false
}

func (r *Many2many) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Many2many) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Many2many) GetPositionMany2one() int {
	return 0
}
