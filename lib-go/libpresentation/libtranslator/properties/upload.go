package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// UploadPropertyType contains the field type for Upload
const UploadPropertyType = libtranslator.PropertyType("UploadType")

/*Upload is the field type you can use in definition to declare a Upload field.
 */
type Upload struct {
	Name                string
	String              string
	Required            bool
	Unique              bool
	PrimaryKey          bool
	Store               bool
	TitleName           string
	DBName              string
	LoadedPosition      int
	Position            int
}

// GetName return the name of the field.
func (f *Upload) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Upload) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Upload) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Upload) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Upload) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Upload) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Upload) Type() libtranslator.PropertyType {
	return UploadPropertyType
}

// Type return the type of the field.
func (f *Upload) GoType() string {
	return libtranslator.STRING
}

// Type return the type of the field.
func (f *Upload) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Upload) GoNil() string {
	return "\"\""
}

// Type return the type of the field.
func (f *Upload) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Upload) ProtoType() string {
	return "google.protobuf.File"
}

func (f *Upload) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Upload) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Upload) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *Upload) GraphqlType() string {
	return "graphql.Upload"
}

// Type return the type of the field.
func (f *Upload) GraphqlSchemaType() string {
	return "Upload"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Upload) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Upload) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Upload) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Upload) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Upload) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Upload) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Upload) IsStored() bool {
	return true
}

func (f *Upload) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Upload) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            UploadPropertyType.GoType(),
// 		// GoType:          UploadPropertyType.GoType(),
// 		// ProtoType:       UploadPropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Upload) SetPosition(position int) {
	f.Position = position
}

func (f *Upload) GetPosition() int {
	return f.Position
}

func (r *Upload) IsRepeated() bool {
	return false
}

func (r *Upload) IsTranslatable() bool {
	return false
}

func (r *Upload) GetReturnDetailsInTests() bool {
	return false
}

func (r *Upload) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Upload) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Upload) GetPositionMany2one() int {
	return 0
}
