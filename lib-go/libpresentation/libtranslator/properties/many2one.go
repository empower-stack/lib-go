package properties

import (
	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

func init() {
	libtranslator.SetAggregateIDProperty = func(reference string) libtranslator.Property {
		return &Many2one{
			Name: "aggregateID", Reference: reference,
			Required: true, OnDelete: libtranslator.OnDeleteCascade,
			LoadedPosition: 3, Position: 4, LoadedPositionMany2one: 5, PositionMany2one: 6}
	}
}

// Many2onePropertyType contains the field type for Many2one
const Many2onePropertyType = libtranslator.PropertyType("Many2oneType")

/*Many2one is the field type you can use in definition to declare a Many2one field.
 */
type Many2one struct {
	Name                   string
	String                 string
	Reference              string
	ReferenceDefinition    libtranslator.TableInterface
	ReferencePrefix        string
	ReferencePath          string
	Required               bool
	Unique                 bool
	OnDelete               libtranslator.OnDeleteValue
	PrimaryKey             bool
	Store                  bool
	TitleName              string
	DBName                 string
	ReturnDetailsInTests   bool
	LoadedPosition         int
	Position               int
	LoadedPositionMany2one int
	PositionMany2one       int
}

// GetName return the name of the field.
func (f *Many2one) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Many2one) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Many2one) NameWithoutID() string {
	return strings.Replace(f.Name, "ID", "", -1)
}

// Title return the title of the field.
func (f *Many2one) TitleWithoutID() string {
	return strings.Replace(f.Title(), "ID", "", -1)
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Many2one) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Many2one) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Many2one) Type() libtranslator.PropertyType {
	return Many2onePropertyType
}

// Type return the type of the field.
func (f *Many2one) GoType() string {
	return "*" + strcase.ToCamel(f.Reference)
	// return libtranslator.STRING
}

// Type return the type of the field.
func (f *Many2one) GoTypeID() string {
	return "string"
	// return libtranslator.STRING
}

// Type return the type of the field.
func (f *Many2one) GoTypeWithPath() string {
	prefix := ""
	if f.ReferencePrefix != "" {
		prefix = f.ReferencePrefix + "."
	}
	return prefix + strcase.ToCamel(f.Reference)
	// return libtranslator.STRING
}

// Type return the type of the field.
func (f *Many2one) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *Many2one) JSONType() string {
	return libtranslator.STRING
}

// ProtoType return the protobuf type for this field.
func (f *Many2one) ProtoType() string {
	return strcase.ToCamel(f.Reference)
}

func (f *Many2one) ProtoTypeArg() string {
	return "string"
}

// ProtoType return the protobuf type for this field.
func (f *Many2one) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Many2one) ProtoTypeOptionalArg() string {
	return "string"
}

// ProtoType return the protobuf type for this field.
func (f *Many2one) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *Many2one) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *Many2one) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Many2one) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Many2one) GetReference() libtranslator.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Many2one) SetReference(d libtranslator.TableInterface) {
	f.ReferenceDefinition = d
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Many2one) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Many2one) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Many2one) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Many2one) IsStored() bool {
	return true
}

func (f *Many2one) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Many2one) GetFieldData() *libtranslator.FieldData {

// 	// var reference *ReferenceData
// 	// referenceDefinition := f.GetReferenceDefinition()
// 	// if referenceDefinition != nil {
// 	// 	reference = &ReferenceData{}
// 	// 	if referenceDefinition != f.Model {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// 	// 			Title: referenceDefinition.GetTemplateData().Title,
// 	// 		}
// 	// 	} else {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  result.Name,
// 	// 			Title: result.Title,
// 	// 		}
// 	// 	}
// 	// }

// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: strings.Replace(f.Name, "ID", "", -1),
// 		Type:            "Many2one",
// 		// GoType:          Many2onePropertyType.GoType(),
// 		// ProtoType:       Many2onePropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: strings.Replace(f.Title(), "ID", "", -1),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Many2one) SetPosition(position int) {
	f.Position = position
}

func (f *Many2one) GetPosition() int {
	return f.Position
}

func (r *Many2one) IsRepeated() bool {
	return false
}

func (r *Many2one) IsTranslatable() bool {
	return false
}

func (r *Many2one) GetReturnDetailsInTests() bool {
	return r.ReturnDetailsInTests
}

func (r *Many2one) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Many2one) GetLoadedPositionMany2one() int {
	return r.LoadedPositionMany2one
}
func (r *Many2one) GetPositionMany2one() int {
	return r.PositionMany2one
}
