package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// DatePropertyType contains the field type for Date
const DatePropertyType = libtranslator.PropertyType("DateType")

/*Date is the field type you can use in definition to declare a Date field.
 */
type Date struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *Date) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Date) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Date) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Date) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Date) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Date) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Date) Type() libtranslator.PropertyType {
	return DatePropertyType
}

// Type return the type of the field.
func (f *Date) GoType() string {
	return "*ptypes.Timestamp"
}

// Type return the type of the field.
func (f *Date) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Date) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *Date) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Date) ProtoType() string {
	return "google.protobuf.Timestamp"
}

func (f *Date) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Date) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Date) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

// Type return the type of the field.
func (f *Date) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Date) GraphqlSchemaType() string {
	return "Time"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Date) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Date) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Date) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Date) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Date) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Date) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Date) IsStored() bool {
	return true
}

func (f *Date) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Date) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            DatePropertyType.GoType(),
// 		// GoType:          DatePropertyType.GoType(),
// 		// ProtoType:       DatePropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "ptypes.Timestamp",
// 			Value: "",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Date) SetPosition(position int) {
	f.Position = position
}

func (f *Date) GetPosition() int {
	return f.Position
}

func (r *Date) IsRepeated() bool {
	return false
}
func (r *Date) IsTranslatable() bool {
	return false
}

func (r *Date) GetReturnDetailsInTests() bool {
	return false
}
func (r *Date) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Date) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Date) GetPositionMany2one() int {
	return 0
}
