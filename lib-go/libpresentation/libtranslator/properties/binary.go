package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// JSONPropertyType contains the field type for JSON
const BinaryPropertyType = libtranslator.PropertyType("BinaryType")

/*JSON is the field type you can use in definition to declare a JSON field.
 */
type Binary struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *Binary) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Binary) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Binary) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Binary) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Binary) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Binary) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Binary) Type() libtranslator.PropertyType {
	return BinaryPropertyType
}

// Type return the type of the field.
func (f *Binary) GoType() string {
	return "[]byte"
}

// Type return the type of the field.
func (f *Binary) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Binary) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *Binary) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Binary) ProtoType() string {
	return "bytes"
}

func (f *Binary) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Binary) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Binary) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *Binary) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Binary) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Binary) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Binary) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Binary) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Binary) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Binary) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Binary) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Binary) IsStored() bool {
	return true
}

func (f *Binary) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *JSON) GetFieldData() *libdomain.FieldData {
// 	return &libdomain.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            JSONPropertyType.GoType(),
// 		// GoType:          JSONPropertyType.GoType(),
// 		// ProtoType:       JSONPropertyType.ProtoType(),
// 		DBType: &libdomain.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Binary) SetPosition(position int) {
	f.Position = position
}

func (f *Binary) GetPosition() int {
	return f.Position
}

func (r *Binary) IsRepeated() bool {
	return false
}

func (r *Binary) IsTranslatable() bool {
	return false
}

func (r *Binary) GetReturnDetailsInTests() bool {
	return false
}

func (r *Binary) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Binary) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Binary) GetPositionMany2one() int {
	return 0
}

