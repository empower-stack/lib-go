package libtranslator

import (
	"fmt"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator/generator/basepb"
)

type Definitions struct {
	Repository string
	// UseTenants bool
	slice []*ServiceDefinition
	byIds map[string]*ServiceDefinition
}

func (ds *Definitions) Register(a *ServiceDefinition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*ServiceDefinition{}
	}

	ds.slice = append(ds.slice, a)
	ds.byIds[a.definition.Name] = a

}

type ServiceDefinition struct {
	definition *libapplication.ServiceDefinition
	enums      []*EnumDefinition
	objects    []*ObjectDefinition
	queries    []*RequestDefinition
	commands   []*RequestDefinition
}

func NewServiceDefinition(definition *libapplication.ServiceDefinition) *ServiceDefinition {
	return &ServiceDefinition{
		definition: definition,
	}
}
func (s *ServiceDefinition) RegisterEnum(e *EnumDefinition) {
	s.enums = append(s.enums, e)
}
func (s *ServiceDefinition) RegisterObject(e *ObjectDefinition) {
	s.objects = append(s.objects, e)
}
func (s *ServiceDefinition) RegisterQuery(r *RequestDefinition) {
	s.queries = append(s.queries, r)
}
func (s *ServiceDefinition) RegisterCommand(r *RequestDefinition) {
	s.commands = append(s.commands, r)
}
func (s *ServiceDefinition) Definition() *libapplication.ServiceDefinition {
	return s.definition
}
func (s *ServiceDefinition) Enums() []*EnumDefinition {
	return s.enums
}
func (s *ServiceDefinition) Objects() []*ObjectDefinition {
	return s.objects
}
func (s *ServiceDefinition) Requests() []*RequestDefinition {
	return append(s.queries, s.commands...)
}
func (s *ServiceDefinition) Queries() []*RequestDefinition {
	return s.queries
}
func (s *ServiceDefinition) Commands() []*RequestDefinition {
	return s.commands
}

type ObjectDefinitionInterface interface {
	GetName() string
	GetType() string
	DomainPackage() string
	DomainPath() string
	IsEntity() bool
	UseTenants() bool
}

type ObjectDefinition struct {
	Definition ObjectDefinitionInterface
	Properties []*PropertyDefinition
}

type RequestDefinitionInterface interface {
	GetName() string
	Title() string
	IsCommand() bool
	UseTenants() bool
}

type RequestDefinition struct {
	Definition     RequestDefinitionInterface
	Args           []*ArgumentDefinition
	Results        []*ArgumentDefinition
	ResultsQueries []*RequestDefinition
}

func (r *RequestDefinition) GetResults() []*ArgumentDefinition {
	var results []*ArgumentDefinition
	for _, query := range r.ResultsQueries {
		results = append(results, query.Results...)
	}
	results = append(results, r.Results...)
	return results
}

type PropertyOptions struct {
	InputPartialUpdate bool
	InputRemove bool
	ObjectIgnoreInTest bool
}

type ArgumentDefinition struct {
	definition                 libdomain.ArgumentDefinition
	enumTitle                  string
	enumDomainDefinition       *libdomain.EnumDefinition
	position                   int
	positionLoaded             int
	positionTranslations       int
	positionTranslationsLoaded int
	inInput 				bool
	options                    *PropertyOptions
}

type PropertyDefinition struct {
	definition                 libdomain.PropertyDefinition
	enumTitle                  string
	enumDomainDefinition       *libdomain.EnumDefinition
	position                   int
	positionLoaded             int
	positionTranslations       int
	positionTranslationsLoaded int
	inInput 				bool
	options                    *PropertyOptions
}

func NewArgumentDefinition(argument libdomain.ArgumentDefinition, position int, positionLoaded int, options *PropertyOptions) *ArgumentDefinition {
	_ = NewPropertyDefinition(argument.GetProperty(), false,  position, positionLoaded, options)

	
	return &ArgumentDefinition{
		definition:     argument,
		position:       position,
		positionLoaded: positionLoaded,
		inInput:      false,
		options:        options,
	}
}
func NewPropertyDefinition(property libdomain.PropertyDefinition, inInput bool, position int, positionLoaded int, options *PropertyOptions) *PropertyDefinition {

	if property.Type() == properties.TextPropertyType && property.IsTranslatable() {
		panic(fmt.Sprintf("property %s from %s must use the translatablePropertyDefinition", property.GetName(), property.Owner().Title()))
	}
	if property.Type() == properties.TextPropertyType && property.GetTranslatable() == properties.TranslatableWYSIWYG {
		panic(fmt.Sprintf("property %s from %s must use the translatablePropertyDefinition", property.GetName(), property.Owner().Title()))
	}
	if property.Type() == properties.SelectionPropertyType {
		panic(fmt.Sprintf("property %s from %s must use the enumPropertyDefinition", property.GetName(), property.Owner().Title()))
	}

	return &PropertyDefinition{
		definition:     property,
		position:       position,
		positionLoaded: positionLoaded,
		inInput:      inInput,
		options:        options,
	}
}
func NewEnumArgumentDefinition(argument libdomain.ArgumentDefinition, enumTitle string, enumDomainDefinition *libdomain.EnumDefinition, position int, positionLoaded int, options *PropertyOptions) *ArgumentDefinition {
	_ = NewEnumPropertyDefinition(argument.GetProperty(), enumTitle, enumDomainDefinition, false, position, positionLoaded, options)

	return &ArgumentDefinition{
		definition:     argument,
		enumTitle:            enumTitle,
		enumDomainDefinition: enumDomainDefinition,
		position:       position,
		positionLoaded: positionLoaded,
		inInput:      false,
		options:        options,
	}
}
func NewEnumPropertyDefinition(property libdomain.PropertyDefinition, enumTitle string, enumDomainDefinition *libdomain.EnumDefinition, inInput bool,position int, positionLoaded int, options *PropertyOptions) *PropertyDefinition {

	if property.Type() != properties.SelectionPropertyType {
		panic(fmt.Sprintf("property %s from %s must be a selection property to use the enumPropertyDefinition", property.GetName(), property.Owner().Title()))
	}

	return &PropertyDefinition{
		definition:           property,
		enumTitle:            enumTitle,
		enumDomainDefinition: enumDomainDefinition,
		position:             position,
		positionLoaded:       positionLoaded,
		inInput:      inInput,
		options:              options,
	}
}
func NewTranslatableArgumentDefinition(argument libdomain.ArgumentDefinition, position int, positionLoaded int, positionTranslations int, positionTranslationsLoaded int, options *PropertyOptions) *ArgumentDefinition {
	_ = NewTranslatablePropertyDefinition(argument.GetProperty(), false, position, positionLoaded, positionTranslations, positionTranslationsLoaded, options)

	return &ArgumentDefinition{
		definition:                 argument,
		position:                   position,
		positionLoaded:             positionLoaded,
		positionTranslations:       positionTranslations,
		positionTranslationsLoaded: positionTranslationsLoaded,
		inInput:      false,
		options:                    options,
	}
}
func NewTranslatablePropertyDefinition(property libdomain.PropertyDefinition, inInput bool,position int, positionLoaded int, positionTranslations int, positionTranslationsLoaded int, options *PropertyOptions) *PropertyDefinition {

	if property.Type() != properties.TextPropertyType || property.GetTranslatable() == "" {
		panic(fmt.Sprintf("property %s from %s must be a text property and must be translatable to use the translatablePropertyDefinition", property.GetName(), property.Owner().Title()))
	}

	return &PropertyDefinition{
		definition:                 property,
		position:                   position,
		positionLoaded:             positionLoaded,
		positionTranslations:       positionTranslations,
		positionTranslationsLoaded: positionTranslationsLoaded,
		inInput:      inInput,
		options:                    options,
	}
}
func (p *PropertyDefinition) Definition() libdomain.PropertyDefinition {
	return p.definition
}
func (p *PropertyDefinition) EnumTitle() string {
	return strings.Title(p.enumTitle)
}
func (p *PropertyDefinition) EnumDomainDefinition() *libdomain.EnumDefinition {
	return p.enumDomainDefinition
}
func (p *PropertyDefinition) Position() int {
	return p.position
}
func (p *PropertyDefinition) PositionLoaded() int {
	return p.positionLoaded
}
func (p *PropertyDefinition) PositionTranslations() int {
	return p.positionTranslations
}
func (p *PropertyDefinition) PositionTranslationsLoaded() int {
	return p.positionTranslationsLoaded
}
func (p *PropertyDefinition) InInput() bool {
	return p.inInput
}
func (p *PropertyDefinition) Options() *PropertyOptions {
	if p.options == nil {
		return &PropertyOptions{}
	}
	return p.options
}
func (p *ArgumentDefinition) Definition() libdomain.ArgumentDefinition {
	return p.definition
}
func (p *ArgumentDefinition) EnumTitle() string {
	return strings.Title(p.enumTitle)
}
func (p *ArgumentDefinition) EnumDomainDefinition() *libdomain.EnumDefinition {
	return p.enumDomainDefinition
}
func (p *ArgumentDefinition) Position() int {
	return p.position
}
func (p *ArgumentDefinition) PositionLoaded() int {
	return p.positionLoaded
}
func (p *ArgumentDefinition) PositionTranslations() int {
	return p.positionTranslations
}
func (p *ArgumentDefinition) PositionTranslationsLoaded() int {
	return p.positionTranslationsLoaded
}
func (p *ArgumentDefinition) InInput() bool {
	return p.inInput
}
func (p *ArgumentDefinition) Options() *PropertyOptions {
	if p.options == nil {
		return &PropertyOptions{}
	}
	return p.options
}

// Register is used to register a new definition into the service.
// func (ds *Definitions) OldRegister(a *AggregateDefinition) {

// 	if ds.byIds == nil {
// 		// ds.byIds = map[string]*AggregateDefinition{}
// 	}

// 	a.AggregateRoot.isAggregateRoot = true

// 	EntitiesInAggregate := a.GetEntities()
// 	a.entitiesByName = map[string]*OldEntityDefinition{}
// 	a.valueObjectsByName = map[string]*ValueObjectDefinition{}
// 	a.commandsByName = map[string]*CustomRequest{}

// 	for _, e := range EntitiesInAggregate {

// 		e.aggregateDefinition = a
// 		a.entitiesByName[e.Name] = e

// 		e.domainPath = ds.Repository

// 		if e.Queries == nil {
// 			e.Queries = &EntityQueriesDefinition{}
// 		}
// 		if e.Commands == nil {
// 			e.Commands = &EntityCommandsDefinition{}
// 		}

// 		for _, f := range e.Keys {
// 			for _, old := range EntitiesInAggregate {
// 				if f.GetReferenceName() == old.Name {
// 					f.SetReference(old)
// 				}
// 			}
// 			for _, old := range ds.Slice() {
// 				if f.GetReferenceName() == old.AggregateRoot.Name {
// 					f.SetReference(old.AggregateRoot)
// 				}
// 			}
// 			if f.IsTranslatable() {
// 				e.hasTranslatable = true
// 			}
// 		}
// 		for _, f := range e.Properties {
// 			for _, old := range EntitiesInAggregate {
// 				if f.GetReferenceName() == old.Name {
// 					f.SetReference(old)
// 				}
// 			}
// 			for _, old := range ds.Slice() {
// 				if f.GetReferenceName() == old.AggregateRoot.Name {
// 					f.SetReference(old.AggregateRoot)
// 				}
// 			}
// 			if f.IsTranslatable() {
// 				e.hasTranslatable = true
// 			}
// 		}

// 		// e.setUseTenants(e.AggregateDefinition.UseTenants)
// 	}

// 	for _, v := range a.ValueObjects {
// 		v.aggregateDefinition = a
// 		v.aggregateIDProperty = SetAggregateIDProperty(a.AggregateRoot.Name)
// 		a.valueObjectsByName[v.Name] = v

// 		v.domainPath = ds.Repository

// 		for _, f := range v.GetKeys() {
// 			for _, old := range EntitiesInAggregate {
// 				if f.GetReferenceName() == old.Name {
// 					f.SetReference(old)
// 				}
// 			}
// 			for _, old := range ds.Slice() {
// 				if f.GetReferenceName() == old.AggregateRoot.Name {
// 					f.SetReference(old.AggregateRoot)
// 				}
// 			}
// 		}
// 		for _, f := range v.Properties {
// 			for _, old := range EntitiesInAggregate {
// 				if f.GetReferenceName() == old.Name {
// 					f.SetReference(old)
// 				}
// 			}
// 			for _, old := range ds.Slice() {
// 				if f.GetReferenceName() == old.AggregateRoot.Name {
// 					f.SetReference(old.AggregateRoot)
// 				}
// 			}
// 		}
// 	}

// 	for _, c := range a.Queries {
// 		a.commandsByName[c.Name] = c
// 	}
// 	for _, c := range a.Commands {
// 		c.isCommand = true
// 		a.commandsByName[c.Name] = c
// 	}

// 	// ds.slice = append(ds.slice, a)
// 	// ds.byIds[a.AggregateRoot.Name] = a

// 	for _, old := range ds.Slice() {
// 		// fmt.Println("old ", old.AggregateRoot.Name)
// 		for _, e := range old.GetEntities() {
// 			for _, f := range e.Keys {
// 				if f.GetReferenceName() == a.AggregateRoot.Name {
// 					f.SetReference(a.AggregateRoot)
// 				}
// 			}
// 			for _, f := range e.Properties {
// 				if f.GetReferenceName() == a.AggregateRoot.Name {
// 					f.SetReference(a.AggregateRoot)
// 				}
// 			}
// 			for _, f := range e.Keys {
// 				for _, compare := range ds.Slice() {
// 					for _, compareEntity := range compare.GetEntities() {
// 						if f.GetReferenceName() == compareEntity.Name {
// 							f.SetReference(compareEntity)
// 						}
// 					}
// 					for _, compareValueObject := range compare.ValueObjects {
// 						if f.GetReferenceName() == compareValueObject.Name {
// 							f.SetReference(compareValueObject)
// 						}
// 					}
// 				}
// 			}
// 			for _, f := range e.Properties {
// 				// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
// 				for _, compare := range ds.Slice() {
// 					for _, compareEntity := range compare.GetEntities() {
// 						if f.GetReferenceName() == compareEntity.Name {
// 							f.SetReference(compareEntity)
// 						}
// 					}
// 					for _, compareValueObject := range compare.ValueObjects {
// 						if f.GetReferenceName() == compareValueObject.Name {
// 							f.SetReference(compareValueObject)
// 						}
// 					}
// 				}
// 			}
// 		}
// 		for _, v := range old.ValueObjects {
// 			// fmt.Println("\nvalueObject ", v.Name)
// 			for _, f := range v.GetKeys() {
// 				for _, compare := range ds.Slice() {
// 					for _, compareEntity := range compare.GetEntities() {
// 						if f.GetReferenceName() == compareEntity.Name {
// 							f.SetReference(compareEntity)
// 						}
// 					}
// 					for _, compareValueObject := range compare.ValueObjects {
// 						if f.GetReferenceName() == compareValueObject.Name {
// 							f.SetReference(compareValueObject)
// 						}
// 					}
// 				}
// 			}
// 			for _, f := range v.Properties {
// 				// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
// 				for _, compare := range ds.Slice() {
// 					for _, compareEntity := range compare.GetEntities() {
// 						if f.GetReferenceName() == compareEntity.Name {
// 							f.SetReference(compareEntity)
// 						}
// 					}
// 					for _, compareValueObject := range compare.ValueObjects {
// 						if f.GetReferenceName() == compareValueObject.Name {
// 							f.SetReference(compareValueObject)
// 						}
// 					}
// 				}
// 			}
// 		}

// 		for _, c := range old.GetCustomRequests() {
// 			for _, a = range ds.slice {
// 				for _, e := range a.GetEntities() {
// 					for _, f := range c.Args {
// 						// fmt.Printf("%s %s\n", f.GetReferenceName(), e.GetName())
// 						if f.GetReferenceName() == e.GetName() {
// 							// fmt.Println("ok")
// 							f.SetReference(e)
// 						}
// 					}
// 					for _, f := range c.Results {
// 						// fmt.Printf("%s %s\n", f.GetReferenceName(), e.GetName())
// 						if f.GetReferenceName() == e.GetName() {
// 							// fmt.Println("ok")
// 							f.SetReference(e)
// 						}
// 					}
// 				}
// 				for _, v := range a.ValueObjects {
// 					for _, f := range c.Args {
// 						// fmt.Printf("%s %s\n", f.GetReferenceName(), v.GetName())
// 						if f.GetReferenceName() == v.GetName() {
// 							// fmt.Println("ok")
// 							f.SetReference(v)
// 						}
// 					}
// 					for _, f := range c.Results {
// 						// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
// 						if f.GetReferenceName() == v.GetName() {
// 							// fmt.Println("ok")
// 							f.SetReference(v)
// 						}
// 					}
// 				}
// 			}
// 		}
// 	}

// }

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*ServiceDefinition {
	result := ds.slice

	return result
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *ServiceDefinition {
	d := ds.byIds[id]

	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

// Definition is used to declare the information of a model, so it can generate its code.
type AggregateDefinition struct {
	AggregateRoot       *OldEntityDefinition
	Children            []*OldEntityDefinition
	UseTenants          bool
	entitiesByName      map[string]*OldEntityDefinition
	ValueObjects        []*ValueObjectDefinition
	valueObjectsByName  map[string]*ValueObjectDefinition
	Enums               []*EnumDefinition
	Queries             []*CustomRequest
	Commands            []*CustomRequest
	commandsByName      map[string]*CustomRequest
	EventStoreClient    EventStoreClient
	EventDispatchClient EventDispatchClient
	ExternalData        []*ExternalData
}

func (a *AggregateDefinition) GetEntities() []*OldEntityDefinition {
	EntitiesInAggregate := []*OldEntityDefinition{
		a.AggregateRoot,
	}
	for _, c := range a.Children {
		EntitiesInAggregate = append(EntitiesInAggregate, c)
	}
	return EntitiesInAggregate
}
func (a *AggregateDefinition) GetEntityByName(name string) *OldEntityDefinition {
	return a.entitiesByName[name]
}
func (a *AggregateDefinition) GetValueObjectByName(name string) *ValueObjectDefinition {
	return a.valueObjectsByName[name]
}
func (a *AggregateDefinition) GetCommandByName(name string) *CustomRequest {
	return a.commandsByName[name]
}
func (a *AggregateDefinition) GetTables() []TableInterface {
	var tables []TableInterface
	for _, c := range a.GetEntities() {
		tables = append(tables, c)
	}
	for _, c := range a.ValueObjects {
		tables = append(tables, c)
	}
	return tables
}
func (a *AggregateDefinition) GetCustomRequests() []*CustomRequest {
	var requests []*CustomRequest
	for _, c := range a.Queries {
		requests = append(requests, c)
	}
	for _, c := range a.Commands {
		requests = append(requests, c)
	}
	return requests
}

// Definition is used to declare the information of a model, so it can generate its code.
type AggregateInterface interface {
	Definition() *AggregateDefinition
	RepositoryInterface() RepositoryInterface
	// SetRepository(RepositoryInterface)
	AggregateRootInterface() AggregateRootInterface
}

type OldEntityDefinition struct {
	Aggregate           AggregateInterface
	aggregateDefinition *AggregateDefinition
	Name                string
	Keys                []Property
	Properties          []Property
	DisableID           bool
	DisableDatetime     bool
	Queries             *EntityQueriesDefinition
	Commands            *EntityCommandsDefinition
	UseOneOf            bool
	// useTenants bool
	DisableDatabaseStore bool
	SafeDelete           bool
	Abstract             bool
	NoCache              bool
	isAggregateRoot      bool
	hasTranslatable      bool
	domainPath           string
	useTenants           bool
	// Select               func(
	// 	WorkflowInterface, *basepb.ListQuery,
	// ) (*Collection, error)
}

func (t *OldEntityDefinition) AggregateDefinition() *AggregateDefinition {
	return t.aggregateDefinition
}
func (t *OldEntityDefinition) GetName() string {
	return t.Name
}
func (t *OldEntityDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *OldEntityDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *OldEntityDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *OldEntityDefinition) GetType() string {
	return "Entity"
}
func (t *OldEntityDefinition) IsEntity() bool {
	return true
}
func (t *OldEntityDefinition) UseTenants() bool {
	return t.AggregateDefinition().UseTenants
}
func (t *OldEntityDefinition) GetDisableID() bool {
	return t.DisableID
}
func (t *OldEntityDefinition) GetDisableDatetime() bool {
	return t.DisableDatetime
}
func (t *OldEntityDefinition) GetDisableDatabaseStore() bool {
	return t.DisableDatabaseStore
}
func (t *OldEntityDefinition) GetUseOneOf() bool {
	return t.UseOneOf
}
func (t *OldEntityDefinition) IsAggregateRoot() bool {
	return t.isAggregateRoot
}
func (t *OldEntityDefinition) HasTranslatable() bool {
	return t.hasTranslatable
}
func (t *OldEntityDefinition) GetAbstract() bool {
	return t.Abstract
}
func (t *OldEntityDefinition) GetNoCache() bool {
	return t.NoCache
}

//	func (t *EntityDefinition) setUseTenants(useTenants bool) {
//		t.useTenants = useTenants
//	}
func (t *OldEntityDefinition) StoredProperties() []Property {
	var storedProperties []Property
	for _, property := range t.Keys {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *OldEntityDefinition) NestedProperties() []Property {
	var nestedProperties []Property
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *OldEntityDefinition) GetCommands() *EntityCommandsDefinition {
	return t.Commands
}
func (t *OldEntityDefinition) GetKeys() []Property {
	return t.Keys
}
func (t *OldEntityDefinition) DomainPath() string {
	return t.domainPath
}

type RequestConfig struct {
	Groups *[]*basepb.GetByExternalIDQuery
}

type EntityQueriesDefinition struct {
	Get            *RequestConfig
	List           *RequestConfig
	GetFromEvents  *RequestConfig
	CustomCommands []*CustomRequest
}

type EntityCommandsDefinition struct {
	Create         *RequestConfig
	Update         *RequestConfig
	Delete         *RequestConfig
	CustomCommands []*CustomRequest
}

type CustomRequest struct {
	Name       string
	Event      string
	OnFactory  bool
	Repository bool
	Args       []ArgsInterface
	Results    []ArgsInterface
	isCommand  bool
}

func (f *CustomRequest) Title() string {
	return strings.Title(f.Name)
}
func (f *CustomRequest) IsCommand() bool {
	return f.isCommand
}

type ArgsInterface interface {
	GetName() string
	Title() string
	ProtoType() string
	GoType() string
	GoNil() string
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() TableInterface
	SetReference(e TableInterface)
	Type() PropertyType
	IsRepeated() bool
}

const RepeatedPropertyType = PropertyType("RepeatedPropertyType")

type RepeatedProperty struct {
	Property Property
}

func (r *RepeatedProperty) GetName() string {
	return r.Property.GetName()
}
func (r *RepeatedProperty) Title() string {
	return r.Property.Title()
}
func (r *RepeatedProperty) Upper() string {
	return r.Property.Upper()
}
func (r *RepeatedProperty) ProtoType() string {
	prototype := r.Property.ProtoType()
	if r.Property.Type() == "Many2oneType" {
		prototype = strcase.ToCamel(r.Property.GetReference().Title())
	}
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) ProtoTypeArg() string {
	prototype := r.Property.ProtoTypeArg()
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) GoType() string {
	return fmt.Sprintf("[]%s", r.Property.GoType())
}
func (r *RepeatedProperty) GoTypeID() string {
	return fmt.Sprintf("[]%s", r.Property.GoTypeID())
}
func (r *RepeatedProperty) GoNil() string {
	return "nil"
}
func (r *RepeatedProperty) GetReferenceName() string {
	return r.Property.GetReferenceName()
}
func (r *RepeatedProperty) Type() PropertyType {
	return r.Property.Type()
}
func (r *RepeatedProperty) DBType() *DBType {
	return r.Property.DBType()
}
func (r *RepeatedProperty) GetInverseProperty() string {
	return ""
}
func (r *RepeatedProperty) GetPrimaryKey() bool {
	return false
}
func (r *RepeatedProperty) GetReference() TableInterface {
	return r.Property.GetReference()
}
func (r *RepeatedProperty) GetRequired() bool {
	return false
}
func (r *RepeatedProperty) GraphqlSchemaType() string {
	return "[" + r.Property.GraphqlSchemaType() + "!]"
}
func (r *RepeatedProperty) GraphqlType() string {
	return ""
}
func (r *RepeatedProperty) IsNested() bool {
	return false
}
func (r *RepeatedProperty) IsStored() bool {
	return true
}
func (r *RepeatedProperty) JSONType() string {
	return ""
}
func (r *RepeatedProperty) NameWithoutID() string {
	return r.Property.NameWithoutID()
}
func (r *RepeatedProperty) ProtoTypeOptional() string {
	return r.ProtoType()
}
func (r *RepeatedProperty) SetPosition(position int) {
	// r.Position = position
}
func (r *RepeatedProperty) GetPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) SetReference(e TableInterface) {
	r.Property.SetReference(e)
}
func (r *RepeatedProperty) Snake() string {
	return ""
}
func (r *RepeatedProperty) TitleWithoutID() string {
	return r.Property.TitleWithoutID()
}
func (r *RepeatedProperty) IsRepeated() bool {
	return true
}
func (r *RepeatedProperty) IsTranslatable() bool {
	return false
}
func (r *RepeatedProperty) GetReturnDetailsInTests() bool {
	return r.Property.GetReturnDetailsInTests()
}
func (r *RepeatedProperty) LoadedPosition() int {
	return r.Property.GetLoadedPosition()
}
func (r *RepeatedProperty) Position() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) LoadedPositionMany2one() int {
	return r.Property.GetLoadedPositionMany2one()
}
func (r *RepeatedProperty) PositionMany2one() int {
	return r.Property.GetPositionMany2one()
}
func (r *RepeatedProperty) GetLoadedPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) GetLoadedPositionMany2one() int {
	return r.Property.GetLoadedPositionMany2one()
}
func (r *RepeatedProperty) GetPositionMany2one() int {
	return r.Property.GetPositionMany2one()
}

// STRING is a contains reprensenting the widely used string type.
const STRING = "string"

// FieldType is the generic type for a data field.
type PropertyType string

type DBType struct {
	Type  string
	Value string
}

// Field is an interface to get the data from the field.
type Property interface {
	GetName() string
	NameWithoutID() string
	Title() string
	TitleWithoutID() string
	Snake() string
	Upper() string
	Type() PropertyType
	GoType() string
	GoTypeID() string
	GoNil() string
	JSONType() string
	ProtoType() string
	ProtoTypeArg() string
	ProtoTypeOptional() string
	DBType() *DBType
	GraphqlType() string
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() TableInterface
	// GetReferenceDefinition() *ModelDefinition
	SetReference(TableInterface)
	GetInverseProperty() string
	GetRequired() bool
	GetPrimaryKey() bool
	// GetFieldData() *FieldData
	IsStored() bool
	IsNested() bool
	SetPosition(int)
	GetPosition() int
	IsRepeated() bool
	IsTranslatable() bool
	GetReturnDetailsInTests() bool
	GetLoadedPosition() int
	GetLoadedPositionMany2one() int
	GetPositionMany2one() int
}

type ValueObjectDefinition struct {
	Aggregate           AggregateInterface
	aggregateDefinition *AggregateDefinition
	Name                string
	Keys                []Property
	Properties          []Property
	Abstract            bool
	aggregateIDProperty Property
	domainPath          string
	// Select              func(
	// 	WorkflowInterface, *basepb.ListQuery,
	// ) (*Collection, error)
}

func (t *ValueObjectDefinition) AggregateDefinition() *AggregateDefinition {
	return t.aggregateDefinition
}
func (t *ValueObjectDefinition) GetName() string {
	return t.Name
}
func (t *ValueObjectDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *ValueObjectDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *ValueObjectDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *ValueObjectDefinition) GetType() string {
	return "ValueObject"
}
func (t *ValueObjectDefinition) IsEntity() bool {
	return false
}
func (t *ValueObjectDefinition) UseTenants() bool {
	return t.aggregateDefinition.UseTenants
}
func (t *ValueObjectDefinition) GetDisableID() bool {
	return true
}
func (t *ValueObjectDefinition) GetDisableDatetime() bool {
	return true
}
func (t *ValueObjectDefinition) GetDisableDatabaseStore() bool {
	return false
}
func (t *ValueObjectDefinition) GetUseOneOf() bool {
	return false
}
func (t *ValueObjectDefinition) StoredProperties() []Property {
	var storedProperties []Property
	for _, property := range t.GetKeys() {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *ValueObjectDefinition) NestedProperties() []Property {
	var nestedProperties []Property
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *ValueObjectDefinition) GetCommands() *EntityCommandsDefinition {
	return &EntityCommandsDefinition{}
}

var SetAggregateIDProperty func(string) Property

func (t *ValueObjectDefinition) GetKeys() []Property {
	keys := []Property{
		t.aggregateIDProperty,
	}
	for _, key := range t.Keys {
		keys = append(keys, key)
	}
	return keys
}
func (t *ValueObjectDefinition) IsAggregateRoot() bool {
	return false
}
func (t *ValueObjectDefinition) HasTranslatable() bool {
	return false
}
func (t *ValueObjectDefinition) GetAbstract() bool {
	return t.Abstract
}
func (t *ValueObjectDefinition) DomainPath() string {
	return t.domainPath
}
func (t *ValueObjectDefinition) GetNoCache() bool {
	return false
}

type TableInterface interface {
	GetName() string
	Title() string
	Snake() string
	Upper() string
	GetType() string
	IsEntity() bool
	UseTenants() bool
	GetDisableID() bool
	GetDisableDatetime() bool
	GetDisableDatabaseStore() bool
	GetUseOneOf() bool
	StoredProperties() []Property
	NestedProperties() []Property
	GetCommands() *EntityCommandsDefinition
	GetKeys() []Property
	IsAggregateRoot() bool
	HasTranslatable() bool
	GetAbstract() bool
	GetNoCache() bool
	DomainPath() string
}

type OnDeleteValue string

const OnDeleteCascade = OnDeleteValue("CASCADE")
const OnDeleteProtect = OnDeleteValue("PROTECT")
const OnDeleteSetNull = OnDeleteValue("SET NULL")

type JWTClaims struct {
	*jwt.StandardClaims
	TenantID  string
	UserID    string
	UserEmail string
	Groups    []*basepb.GetByExternalIDQuery
}

type ExternalData struct {
	Aggregate AggregateInterface
	TenantID  string
	Module    string
	Data      []map[string]string
}

type EnumDefinition struct {
	Name   string
	Values []*EnumValue
}

func (e *EnumDefinition) Title() string {
	return strings.Title(e.Name)
}

type EnumValue struct {
	Value    string
	Position int
}

func (e *EnumValue) Title() string {
	return strings.Title(e.Value)
}
func (e *EnumValue) Upper() string {
	return strings.ToUpper(strcase.ToSnake(e.Value))
}

var IDProperty = &basepb.BaseProperty{Snake: "id"}
var TenantIDProperty = &basepb.BaseProperty{Snake: "tenant_id"}
var ExternalModuleProperty = &basepb.BaseProperty{Snake: "external_module"}
var ExternalIDProperty = &basepb.BaseProperty{Snake: "external_id"}
