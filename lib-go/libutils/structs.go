package libutils

import (
	"fmt"
	"reflect"
	// "github.com/pkg/errors"
)

// ConvertStruct todo
func ConvertStruct(src interface{}, dest interface{}, fields []string) ([]string, error) {

	// var fields []string
	fmt.Printf("%+v %+v\n", src, dest)
	s := reflect.ValueOf(src).Elem()
	d := reflect.ValueOf(dest).Elem()
	fmt.Printf("d %s\n", d.Kind())
	typeOfSrc := s.Type()
	for i := 0; i < s.NumField(); i++ {
		fs := s.Field(i)
		if fs.Kind() == reflect.Ptr {
			fs = fs.Elem()
		}
		name := typeOfSrc.Field(i).Name
		fmt.Printf("name %s\n", name)
		fd := d.FieldByName(name)
		fdType, _ := d.Type().FieldByName(name)
		if fd.IsValid() {
			fmt.Printf("ok\n")
			// A Value can be changed only if it is
			// addressable and was not obtained by
			// the use of unexported struct fields.
			if fd.CanSet() {
				fmt.Printf("ok2\n")
				fmt.Printf("kind %s\n", fd.Kind())
				// change value of N
				switch fd.Kind() {
				case reflect.Int32:
					fd.SetInt(fs.Int())
				case reflect.String:
					fd.SetString(fs.String())
				case reflect.Bool:
					fd.SetBool(fs.Bool())
					// case graphql.ID:
					// 	fd.SetString(string(fs.Interface().(graphql.ID)))
				case reflect.Slice:
					fmt.Printf("type %s\n", fd.Type())
					if fs.IsValid() && fd.Type().String() == "[]string" {

						fmt.Printf("IS VALID \n")
						slice := reflect.MakeSlice(reflect.TypeOf([]string{}), 0, 0)
						for si := 0; si < fs.Len(); si++ {
							v := fs.Index(si)
							if v.Kind() == reflect.Ptr {
								v = v.Elem()
							}
							slice = reflect.Append(slice, reflect.ValueOf(v.String()))
						}
						fd.Set(slice)
					}
				case reflect.Ptr:
					v := fs.String()
					if v == "" {
						return nil, fmt.Errorf(
							"the field %s can't have an empty string as value, it must be null or have value", name)
					}
					if fs.IsValid() {
						fd.Set(reflect.ValueOf(&v))
					} else {
						fd.Set(reflect.Zero(fd.Type()))
					}
				}

				if fdType.Tag.Get("db") != "" {
					fields = append(fields, fdType.Tag.Get("db"))
				}
			}
		}
	}
	fmt.Printf("%+v %+v\n", src, dest)
	return fields, nil
}

// GetDBFields todo
func GetDBFields(src interface{}) []string {
	var fields []string
	s := reflect.ValueOf(src).Elem()
	typeOfSrc := s.Type()
	for i := 0; i < s.NumField(); i++ {
		name := typeOfSrc.Field(i).Name
		fieldType, _ := s.Type().FieldByName(name)
		if fieldType.Tag.Get("db") != "" {
			fields = append(fields, fieldType.Tag.Get("db"))
		}
	}
	return fields
}
