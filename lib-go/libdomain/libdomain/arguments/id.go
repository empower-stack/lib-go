package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const IDArgumentType = libdomain.ArgumentType("IDType")


type IDDefinition struct {
	*properties.IDDefinition
}

func ID(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.IDOptions) *IDDefinition {
	return &IDDefinition{
		IDDefinition: properties.ID(name, required, referenceName, referenceDefinition, libdomain.OnDeleteSetNull, 0, 0, options),
	}
}
func IDWithEntity(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.IDOptions) *IDDefinition {
	return &IDDefinition{
		IDDefinition: properties.IDWithEntity(name, required, referenceName, referenceDefinition, libdomain.OnDeleteCascade, 0, 0, 0,0,options),
	}
}

// Type return the type of the field.
func (f *IDDefinition) Type() libdomain.ArgumentType {
	return IDArgumentType
}

func (f *IDDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.IDDefinition
}