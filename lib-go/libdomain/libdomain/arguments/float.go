package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const FloatArgumentType = libdomain.ArgumentType("FloatType")


type FloatDefinition struct {
	*properties.FloatDefinition
}

func Float(name string, required bool, options *properties.FloatOptions) *FloatDefinition {
	return &FloatDefinition{
		FloatDefinition: properties.Float(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *FloatDefinition) Type() libdomain.ArgumentType {
	return FloatArgumentType
}

func (f *FloatDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.FloatDefinition
}