package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const EntityArgumentType = libdomain.ArgumentType("EntityType")

const EntityCreateInputType libdomain.ObjectInputType = "CreateInput"
const EntityUpdateInputType libdomain.ObjectInputType = "UpdateInput"


type EntityDefinition struct {
	*properties.EntityDefinition
	inputType libdomain.ObjectInputType
}

func Entity(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.EntityOptions) *EntityDefinition {
	return &EntityDefinition{
		EntityDefinition: properties.Entity(name, required, referenceName, referenceDefinition, 0, 0, options),
	}
}
func EntityCreateInput(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.EntityOptions) *EntityDefinition {
	return &EntityDefinition{
		EntityDefinition: properties.Entity(name, required, referenceName, referenceDefinition, 0, 0, options),
		inputType: EntityCreateInputType,
	}
}
func EntityUpdateInput(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.EntityOptions) *EntityDefinition {
	return &EntityDefinition{
		EntityDefinition: properties.Entity(name, required, referenceName, referenceDefinition, 0, 0, options),
		inputType: EntityUpdateInputType,
	}	
}

// Type return the type of the field.
func (f *EntityDefinition) Type() libdomain.ArgumentType {
	return EntityArgumentType
}

func (f *EntityDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.EntityDefinition
}

func (f *EntityDefinition) InputType() libdomain.ObjectInputType {
	return f.inputType
}
