package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const OrderedMapArgumentType = libdomain.ArgumentType("OrderedMapType")


type OrderedMapDefinition struct {
	*properties.OrderedMapDefinition
	Argument libdomain.ArgumentDefinition
}

func OrderedMap(arg libdomain.ArgumentDefinition) *OrderedMapDefinition {
	return &OrderedMapDefinition{
		OrderedMapDefinition: properties.OrderedMap(arg.GetProperty(), ""),
		Argument: arg,
	}
}

// Type return the type of the field.
func (f *OrderedMapDefinition) Type() libdomain.ArgumentType {
	return f.Argument.Type()
}

func (f *OrderedMapDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.OrderedMapDefinition
}

func (r *OrderedMapDefinition) InputType() libdomain.ObjectInputType {
	return r.Argument.InputType()
}