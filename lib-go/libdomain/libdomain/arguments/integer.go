package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const IntegerArgumentType = libdomain.ArgumentType("IntegerType")


type IntegerDefinition struct {
	*properties.IntegerDefinition
}

func Integer(name string, required bool, options *properties.IntegerOptions) *IntegerDefinition {
	return &IntegerDefinition{
		IntegerDefinition: properties.Integer(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *IntegerDefinition) Type() libdomain.ArgumentType {
	return IntegerArgumentType
}

func (f *IntegerDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.IntegerDefinition
}