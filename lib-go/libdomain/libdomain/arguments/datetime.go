package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const DatetimeArgumentType = libdomain.ArgumentType("DatetimeType")


type DatetimeDefinition struct {
	*properties.DatetimeDefinition
}

func Datetime(name string, required bool, options *properties.DatetimeOptions) *DatetimeDefinition {
	return &DatetimeDefinition{
		DatetimeDefinition: properties.Datetime(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *DatetimeDefinition) Type() libdomain.ArgumentType {
	return DatetimeArgumentType
}

func (f *DatetimeDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.DatetimeDefinition
}