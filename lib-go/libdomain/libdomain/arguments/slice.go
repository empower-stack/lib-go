package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const SliceArgumentType = libdomain.ArgumentType("SliceType")


type SliceDefinition struct {
	Argument libdomain.ArgumentDefinition
	*properties.SliceDefinition
}

func Slice(arg libdomain.ArgumentDefinition) *SliceDefinition {
	return &SliceDefinition{
		SliceDefinition: properties.Slice(arg.GetProperty(), ""),
		Argument: arg,
	}
}

// Type return the type of the field.
func (f *SliceDefinition) Type() libdomain.ArgumentType {
	return f.Argument.Type()
}

// Type return the type of the field.
func (f *SliceDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.SliceDefinition
}

func (r *SliceDefinition) InputType() libdomain.ObjectInputType {
	return r.Argument.InputType()
}