package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const MapArgumentType = libdomain.ArgumentType("MapType")


type MapDefinition struct {
	*properties.MapDefinition
}

func Map(name string, required bool, options *properties.MapOptions) *MapDefinition {
	return &MapDefinition{
		MapDefinition: properties.Map(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *MapDefinition) Type() libdomain.ArgumentType {
	return MapArgumentType
}

func (f *MapDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.MapDefinition
}