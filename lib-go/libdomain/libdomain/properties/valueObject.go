package properties

import (
	"fmt"
	"strings"
	"unicode"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// EntityPropertyType contains the field type for Entity
const ValueObjectPropertyType = libdomain.PropertyType("ValueObjectType")

/*Entity is the field type you can use in definition to declare a Entity field.
 */
type ValueObjectDefinition struct {
	Common	
	Reference              string
	ReferenceDefinition    libdomain.TableInterface
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	Input 				 bool
	ReturnDetailsInTests   bool
	FromID bool
}
type ValueObjectOptions struct {
	Unique bool
	PrimaryKey bool	
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	ReturnDetailsInTests   bool
	FromID bool
}

func ValueObject(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *ValueObjectOptions) *ValueObjectDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	if referenceDefinition != nil {
		if referenceName != "" {
			panic(fmt.Sprintf("You can't set a reference name and a reference definition: %s", name))
		}
		referenceName = referenceDefinition.GetName()
	}

	result := &ValueObjectDefinition{
		Common: Common{
			Name:           name,
			Required:       required,
		},
		Reference: referenceName,
		ReferenceDefinition: referenceDefinition,
	}
	if options != nil {
		result.Common.Unique = options.Unique
		result.Common.PrimaryKey = options.PrimaryKey
		result.ReferencePrefix = options.ReferencePrefix
		result.ReferencePath = options.ReferencePath
		result.OnDelete = options.OnDelete
		result.ReturnDetailsInTests = options.ReturnDetailsInTests
		result.FromID = options.FromID
	}

	return result
}
// func ValueObjectInput(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *ValueObjectOptions) *ValueObjectDefinition {
// 	result := ValueObject(name, required, referenceName, referenceDefinition, options)
// 	result.Input = true
// 	return result
// }

// Title return the title of the field.
func (f *ValueObjectDefinition) NameWithoutID() string {
	return strings.Replace(f.Name, "ID", "", -1)
}

// Title return the title of the field.
func (f *ValueObjectDefinition) TitleWithoutID() string {
	return strings.Replace(f.Title(), "ID", "", -1)
}

// Type return the type of the field.
func (f *ValueObjectDefinition) Type() libdomain.PropertyType {
	return ValueObjectPropertyType
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GoType() string {
	return "*" + strcase.ToCamel(f.Reference)
	// return libdomain.STRING
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GoTypeWithPackage() string {
	return "*" + f.ReferenceDefinition.DomainPackage() + "." + strcase.ToCamel(f.Reference)
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GoTypeID() string {
	return "string"
	// return libdomain.STRING
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GoTypeWithPath() string {
	prefix := ""
	if f.ReferencePrefix != "" {
		prefix = f.ReferencePrefix + "."
	}
	return prefix + strcase.ToCamel(f.Reference)
	// return libdomain.STRING
}

func (f *ValueObjectDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *ValueObjectDefinition) JSONType() string {
	return libdomain.STRING
}

// ProtoType return the protobuf type for this field.
func (f *ValueObjectDefinition) ProtoType() string {
	return strcase.ToCamel(f.Reference)
}

func (f *ValueObjectDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *ValueObjectDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *ValueObjectDefinition) ProtoTypeOptionalArg() string {
	return "string"
}

// ProtoType return the protobuf type for this field.
func (f *ValueObjectDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *ValueObjectDefinition) DiffType(pack string) string {
	return f.GoTypeWithPackage() + "Diff"
}

// Type return the type of the field.
func (f *ValueObjectDefinition) DiffTypeNew(pack string) string {
	if f.ReferenceDefinition.DomainPackage() != pack {
		return f.ReferenceDefinition.DomainPackage() + ".New" + f.Title() + "Diff"
	}
	return "New" + f.Title() + "Diff"
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *ValueObjectDefinition) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *ValueObjectDefinition) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *ValueObjectDefinition) GetReference() libdomain.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *ValueObjectDefinition) SetReference(d libdomain.TableInterface) {
	f.ReferenceDefinition = d
}

func (r *ValueObjectDefinition) GetInput() bool {
	return r.Input
}

func (r *ValueObjectDefinition) GetReturnDetailsInTests() bool {
	return r.ReturnDetailsInTests
}
