package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// DatetimePropertyType contains the field type for Datetime
const DatetimePropertyType = libdomain.PropertyType("DatetimeType")

/*Datetime is the field type you can use in definition to declare a Datetime field.
 */
type DatetimeDefinition struct {
	Common
}
type DatetimeOptions struct {
	Unique bool
	PrimaryKey bool
}

func Datetime(name string, required bool, position int, loadedPosition int, options *DatetimeOptions) *DatetimeDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &DatetimeDefinition{
		Common: common,
	}
}

// Type return the type of the field.
func (f *DatetimeDefinition) Type() libdomain.PropertyType {
	return DatetimePropertyType
}

// Type return the type of the field.
func (f *DatetimeDefinition) GoType() string {
	return "time.Time"
}

// Type return the type of the field.
func (f *DatetimeDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

func (f *DatetimeDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *DatetimeDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *DatetimeDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *DatetimeDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *DatetimeDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *DatetimeDefinition) ProtoType() string {
	return "google.protobuf.Timestamp"
}

func (f *DatetimeDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *DatetimeDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *DatetimeDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "time.Time",
		Value: "",
	}
}

// Type return the type of the field.
func (f *DatetimeDefinition) DiffType(pack string) string {
	return "*libdomain.TimeDiff"
}

// Type return the type of the field.
func (f *DatetimeDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTimeDiff"
}

// Type return the type of the field.
func (f *DatetimeDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *DatetimeDefinition) GraphqlSchemaType() string {
	return "Time"
}
