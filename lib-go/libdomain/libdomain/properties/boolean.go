package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// BooleanPropertyType contains the field type for Boolean
const BooleanPropertyType = libdomain.PropertyType("BooleanType")

/*Boolean is the field type you can use in definition to declare a Boolean field.
 */
 type BooleanDefinition struct {
	Common
}
type BooleanOptions struct {
	Unique bool
	PrimaryKey bool
}

func Boolean(name string, required bool, position int, loadedPosition int, options *BooleanOptions) *BooleanDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &BooleanDefinition{
		Common: common,
	}
}

// Type return the type of the field.
func (f *BooleanDefinition) Type() libdomain.PropertyType {
	return BooleanPropertyType
}

// Type return the type of the field.
func (f *BooleanDefinition) GoType() string {
	return "bool"
}

// Type return the type of the field.
func (f *BooleanDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

func (f *BooleanDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *BooleanDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *BooleanDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *BooleanDefinition) GoNil() string {
	return "false"
}

// Type return the type of the field.
func (f *BooleanDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *BooleanDefinition) ProtoType() string {
	return f.GoType()
}

func (f *BooleanDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *BooleanDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *BooleanDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullBool",
		Value: "Bool",
	}
}

// Type return the type of the field.
func (f *BooleanDefinition) DiffType(pack string) string {
	return "*libdomain.BooleanDiff"
}

// Type return the type of the field.
func (f *BooleanDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewBooleanDiff"
}

// Type return the type of the field.
func (f *BooleanDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *BooleanDefinition) GraphqlSchemaType() string {
	return "Boolean"
}
