package properties

import (
	// "strings"

	"fmt"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

const SlicePropertyType = libdomain.PropertyType("Slice")

type SliceDefinition struct {
	libdomain.PropertyDefinition
	TargetProperty string
}

func Slice(property libdomain.PropertyDefinition, targetProperty string) *SliceDefinition {
	return &SliceDefinition{
		PropertyDefinition:       property,
		TargetProperty: targetProperty,
	}
}

func (r *SliceDefinition) Name() string {
	return r.PropertyDefinition.GetName()
}
// func (r *Slice) GetName() string {
// 	return r.Property.GetName()
// }
// func (r *Slice) Title() string {
// 	return r.Property.Title()
// }
// func (r *Slice) Upper() string {
// 	return r.Property.Upper()
// }
func (r *SliceDefinition) ProtoType() string {
	prototype := r.PropertyDefinition.ProtoType()
	if r.PropertyDefinition.Type() == "Many2oneType" {
		prototype = strcase.ToCamel(r.PropertyDefinition.GetReference().Title())
	}
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *SliceDefinition) ProtoTypeArg() string {
	prototype := r.PropertyDefinition.ProtoTypeArg()
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *SliceDefinition) GoType() string {
	return fmt.Sprintf("[]%s", r.PropertyDefinition.GoType())
}
func (f *SliceDefinition) GoTypeWithPackage() string {
	return fmt.Sprintf("[]%s", f.PropertyDefinition.GoTypeWithPackage())
}
func (r *SliceDefinition) PropertyGoType() string {
	return r.PropertyDefinition.GoType()
}
func (r *SliceDefinition) GoTypeID() string {
	return fmt.Sprintf("[]%s", r.PropertyDefinition.GoTypeID())
}
func (f *SliceDefinition) GoTypeIDWithPackage() string {
	return fmt.Sprintf("[]%s", f.PropertyDefinition.GoTypeWithPackage())
}
func (f *SliceDefinition) GoTypeBase() string {
	return fmt.Sprintf("[]%s", f.PropertyDefinition.GoTypeBase())
}
func (r *SliceDefinition) GoNil() string {
	return "nil"
}
// func (r *Slice) GetReferenceName() string {
// 	return r.Property.GetReferenceName()
// }
// func (r *Slice) Type() libdomain.PropertyType {
// 	return r.Property.Type()
// }
// func (r *Slice) DBType() *libdomain.DBType {
// 	return r.Property.DBType()
// }
func (f *SliceDefinition) DiffType(pack string) string {
	if (f.PropertyDefinition.Type() == TextPropertyType) {
		return "*libdomain.SliceDiff"
	}
	if f.PropertyDefinition.GetReference() == nil {
		return f.PropertyDefinition.DiffType(pack)
	}
	return "*" + f.PropertyDefinition.GetReference().DomainPackage() + "identities.Slice" + f.PropertyDefinition.GetReference().Title() + "IDDiff"
}
func (f *SliceDefinition) DiffTypeNew(pack string) string {
	if (f.PropertyDefinition.Type() == TextPropertyType) {
		return "libdomain.NewSliceDiff"
	}
	if f.PropertyDefinition.GetReference() == nil {
		return f.PropertyDefinition.DiffTypeNew(pack)
	}
	return f.PropertyDefinition.GetReference().DomainPackage() + "identities.NewSlice" + f.PropertyDefinition.GetReference().Title() + "IDDiff"
}
// func (r *Slice) GetInverseProperty() string {
// 	return ""
// }
// func (r *Slice) GetPrimaryKey() bool {
// 	return false
// }
// func (r *Slice) GetReference() libdomain.TableInterface {
// 	return r.Property.GetReference()
// }
// func (r *Slice) GetRequired() bool {
// 	return false
// }
// func (f *Slice) GetUnique() bool {
// 	return f.Property.GetUnique()
// }
func (r *SliceDefinition) GraphqlSchemaType() string {
	return "[" + r.PropertyDefinition.GraphqlSchemaType() + "!]"
}
// func (r *Slice) GraphqlType() string {
// 	return ""
// }
// func (r *Slice) IsNested() bool {
// 	return false
// }
// func (r *Slice) IsStored() bool {
// 	return true
// }
// func (f *Slice) GetWithEntity() bool {
// 	return f.Property.GetWithEntity()
// }
// func (r *Slice) JSONType() string {
// 	return ""
// }
// func (r *Slice) NameWithoutID() string {
// 	return r.Property.NameWithoutID()
// }
func (r *SliceDefinition) ProtoTypeOptional() string {
	return r.ProtoType()
}
// func (r *Slice) SetPosition(position int) {
// 	// r.Position = position
// }
// func (r *Slice) GetPosition() int {
// 	return r.Property.GetPosition()
// }
// func (r *Slice) SetReference(e libdomain.TableInterface) {
// 	r.Property.SetReference(e)
// }
// func (r *Slice) Snake() string {
// 	return ""
// }
// func (r *Slice) TitleWithoutID() string {
// 	return r.Property.TitleWithoutID()
// }
func (r *SliceDefinition) IsSlice() bool {
	return true
}
// func (r *Slice) IsTranslatable() bool {
// 	return false
// }
// func (r *Slice) GetTranslatable() libdomain.TranslatableType {
// 	return ""
// }
// func (r *Slice) GetData() bool {
// 	return r.Property.GetData()
// }
// func (r *Slice) GetReturnDetailsInTests() bool {
// 	return r.Property.GetReturnDetailsInTests()
// }
// func (r *Slice) LoadedPosition() int {
// 	return r.Property.GetLoadedPosition()
// }
// func (r *Slice) Position() int {
// 	return r.Property.GetPosition()
// }
// func (r *Slice) LoadedPositionMany2one() int {
// 	return r.Property.GetLoadedPositionMany2one()
// }
// func (r *Slice) PositionMany2one() int {
// 	return r.Property.GetPositionMany2one()
// }
// func (r *Slice) GetLoadedPosition() int {
// 	return r.Property.GetPosition()
// }
// func (r *Slice) GetLoadedPositionMany2one() int {
// 	return r.Property.GetLoadedPositionMany2one()
// }
// func (r *Slice) GetPositionMany2one() int {
// 	return r.Property.GetPositionMany2one()
// }
// func (r *Slice) IsRepeated() bool {
// 	return false
// }
func (r *SliceDefinition) CollectionType() libdomain.PropertyType {
	return SlicePropertyType
}
// func (r *Slice) FromDomain() bool {
// 	return r.Property.FromDomain()
// }
// func (r *Slice) SetFromDomain(d bool) {
// 	r.Property.SetFromDomain(d)
// }
// func (r *Slice) Owner() libdomain.OwnerInterface {
// 	return r.Property.Owner()
// }
// func (r *Slice) SetOwner(d libdomain.OwnerInterface) {
// 	r.Property.SetOwner(d)
// }
// func (r *Slice) Patch() bool {
// 	return false
// }
// func (r *Slice) Translatable() libdomain.TranslatableType {
// 	return ""
// }
func (r *SliceDefinition) InputType() libdomain.ObjectInputType {
	return r.PropertyDefinition.InputType()
}