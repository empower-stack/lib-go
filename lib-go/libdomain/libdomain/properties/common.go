package properties

import (
	"strings"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)


type Common struct {	
	Name                string
	Required            bool
	Unique              bool
	PrimaryKey          bool
	Store               bool
	TitleName           string
	DBName              string
	Position            int
	LoadedPosition      int	
	owner               libdomain.OwnerInterface
	fromDomain          bool
}



// GetName return the name of the field.
func (f *Common) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Common) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Common) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Common) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Common) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Common) Upper() string {
	return strings.ToUpper(f.Snake())
}

// // Type return the type of the field.
// func (f *Common) GoType() string {
// 	return libdomain.STRING
// }

// // Type return the type of the field.
// func (f *Common) GoTypeWithPackage() string {
// 	return f.GoType()
// }

// // Type return the type of the field.
// func (f *Common) GoTypeID() string {
// 	return f.GoType()
// }

// // Type return the type of the field.
// func (f *Common) GoTypeIDWithPackage() string {
// 	return f.GoTypeID()
// }

// func (f *Common) GoTypeBase() string {
// 	return f.GoType()
// }

// // Type return the type of the field.
// func (f *Common) GoNil() string {
// 	return "\"\""
// }

// // Type return the type of the field.
// func (f *Common) JSONType() string {
// 	return f.GoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Common) ProtoType() string {
// 	return "string"
// }

// func (f *Common) ProtoTypeArg() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Common) ProtoTypeOptional() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Common) DBType() *libdomain.DBType {
// 	return &libdomain.DBType{
// 		Type:  "sql.NullString",
// 		Value: "String",
// 	}
// }

// // Type return the type of the field.
// func (f *Common) DiffType(pack string) string {
// 	return "*libdomain.TextDiff"
// }

// // Type return the type of the field.
// func (f *Common) DiffTypeNew(pack string) string {
// 	return "libdomain.NewTextDiff"
// }

func (f *Common) DiffTypeInterface() string {
	return "*libdomain.TextDiff"
}

// // Type return the type of the field.
// func (f *Common) GraphqlType() string {
// 	return f.GoType()
// }

// // Type return the type of the field.
// func (f *Common) GraphqlSchemaType() string {
// 	return "String"
// }

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Common) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Common) GetReference() libdomain.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Common) SetReference(libdomain.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Common) GetInverseProperty() string {
	return ""
}

func (f *Common) InverseTitle() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Common) GetRequired() bool {
	return f.Required
}

// GetRequired return if this field is required or not.
func (f *Common) GetUnique() bool {
	return f.Unique
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Common) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Common) IsStored() bool {
	return true
}

func (f *Common) IsNested() bool {
	return false
}

func (f *Common) GetWithEntity() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Text) GetFieldData() *libdomain.FieldData {
// 	return &libdomain.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            TextPropertyType.GoType(),
// 		// GoType:          TextPropertyType.GoType(),
// 		// ProtoType:       TextPropertyType.ProtoType(),
// 		DBType: &libdomain.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Common) SetPosition(position int) {
	f.Position = position
}

func (f *Common) GetPosition() int {
	return f.Position
}

func (r *Common) IsRepeated() bool {
	return false
}

func (r *Common) IsTranslatable() bool {
	return false
}

func (r *Common) GetTranslatable() libdomain.TranslatableType {
	return ""
}

func (r *Common) GetPatch() bool {
	return false
}

func (r *Common) GetInput() bool {
	return false
}

func (r *Common) GetReturnDetailsInTests() bool {
	return false
}

func (r *Common) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Common) GetLoadedPositionEntity() int {
	return 0
}
func (r *Common) GetPositionEntity() int {
	return 0
}
func (r *Common) CollectionType() libdomain.PropertyType {
	return ""
}
func (r *Common) FromDomain() bool {
	return r.fromDomain
}
func (r *Common) SetFromDomain(d bool) {
	r.fromDomain = d
}
func (r *Common) Owner() libdomain.OwnerInterface {
	return r.owner
}
func (r *Common) SetOwner(d libdomain.OwnerInterface) {
	r.owner = d
}
func (r *Common) InputType() libdomain.ObjectInputType {
	return ""
}
func (r *Common) GetOptions() interface{} {
	return nil
}