package properties

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

func init() {
	libdomain.PrepareEntity = func(entity *libdomain.EntityDefinition) {
		if !entity.IsAggregateRoot() {
			entity.Properties = append([]libdomain.PropertyDefinition{IDWithEntity(
				"aggregateID", true, entity.AggregateDefinition().AggregateRoot.Name, nil,
				libdomain.OnDeleteCascade, 3, 4, 5, 6, &IDOptions{NotInInput: true})}, entity.Properties...)
		}
	}
}
