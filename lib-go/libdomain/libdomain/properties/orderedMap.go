package properties

import (
	// "strings"

	"fmt"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

const OrderedMapPropertyType = libdomain.PropertyType("OrderedMap")

type OrderedMapDefinition struct {
	libdomain.PropertyDefinition
	InverseProperty string
}

func OrderedMap(property libdomain.PropertyDefinition, inverseProperty string) *OrderedMapDefinition {
	return &OrderedMapDefinition{
		PropertyDefinition: property,
		InverseProperty: inverseProperty,
	}
}

func (r *OrderedMapDefinition) Name() string {
	return r.PropertyDefinition.GetName()
}
// func (r *OrderedMapDefinition) GetName() string {
// 	return r.Property.GetName()
// }
// func (r *OrderedMapDefinition) Title() string {
// 	return r.Property.Title()
// }
// func (r *OrderedMapDefinition) Upper() string {
// 	return r.Property.Upper()
// }
func (r *OrderedMapDefinition) ProtoType() string {
	prototype := r.PropertyDefinition.ProtoType()
	if r.PropertyDefinition.Type() == "Many2oneType" {
		prototype = strcase.ToCamel(r.PropertyDefinition.GetReference().Title())
	}
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *OrderedMapDefinition) ProtoTypeArg() string {
	prototype := r.PropertyDefinition.ProtoTypeArg()
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *OrderedMapDefinition) GoType() string {
	return fmt.Sprintf("*orderedmap.OrderedMap[%s, %s]", r.PropertyDefinition.GoTypeID(), r.PropertyDefinition.GoType())
}
func (r *OrderedMapDefinition) GoTypeWithPackage() string {
	return fmt.Sprintf("*orderedmap.OrderedMap[%s, %s]", r.PropertyDefinition.GoTypeID(), r.PropertyDefinition.GoTypeWithPackage())
}
func (r *OrderedMapDefinition) GoTypeID() string {
	return fmt.Sprintf("[]%s", r.PropertyDefinition.GoTypeID())
}
func (f *OrderedMapDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}
func (f *OrderedMapDefinition) GoTypeBase() string {
	return  fmt.Sprintf("[]%s", f.PropertyDefinition.GoType())
}
func (r *OrderedMapDefinition) GoNil() string {
	return "nil"
}
func (r *OrderedMapDefinition) GetReferenceName() string {
	return r.PropertyDefinition.GetReferenceName()
}
// func (r *OrderedMapDefinition) Type() libdomain.PropertyType {
// 	return r.Property.Type()
// }
// func (r *OrderedMap) DBType() *libdomain.DBType {
// 	return r.Property.DBType()
// }
func (f *OrderedMapDefinition) DiffType(pack string) string {
	return fmt.Sprintf("map[%s]%sDiff", f.PropertyDefinition.GoTypeIDWithPackage(), f.PropertyDefinition.GoTypeWithPackage())
}
func (f *OrderedMapDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTextDiff"
}
func (r *OrderedMapDefinition) GetInverseProperty() string {
	return r.InverseProperty
}
func (r *OrderedMapDefinition) InverseTitle() string {
	return strcase.ToCamel(r.InverseProperty)
}
// func (r *OrderedMap) GetPrimaryKey() bool {
// 	return false
// }
// func (r *OrderedMap) GetReference() libdomain.TableInterface {
// 	return r.Property.GetReference()
// }
// func (r *OrderedMap) GetRequired() bool {
// 	return false
// }
// func (f *OrderedMap) GetUnique() bool {
// 	return f.Property.GetUnique()
// }
func (r *OrderedMapDefinition) GraphqlSchemaType() string {
	return "[" + r.PropertyDefinition.GraphqlSchemaType() + "!]"
}
// func (r *OrderedMap) GraphqlType() string {
// 	return ""
// }
// func (r *OrderedMap) IsNested() bool {
// 	return false
// }
// func (r *OrderedMap) IsStored() bool {
// 	return true
// }
// func (f *OrderedMap) GetWithEntity() bool {
// 	return false
// }
// func (r *OrderedMap) JSONType() string {
// 	return ""
// }
// func (r *OrderedMap) NameWithoutID() string {
// 	return r.Property.NameWithoutID()
// }
func (r *OrderedMapDefinition) ProtoTypeOptional() string {
	return r.ProtoType()
}
// func (r *OrderedMap) SetPosition(position int) {
// 	// r.Position = position
// }
// func (r *OrderedMap) GetPosition() int {
// 	return r.Property.GetPosition()
// }
// func (r *OrderedMap) SetReference(e libdomain.TableInterface) {
// 	r.Property.SetReference(e)
// }
// func (r *OrderedMap) Snake() string {
// 	return ""
// }
// func (r *OrderedMap) TitleWithoutID() string {
// 	return r.Property.TitleWithoutID()
// }
func (r *OrderedMapDefinition) IsOrderedMap() bool {
	return true
}
// func (r *OrderedMap) IsTranslatable() bool {
// 	return false
// }
// func (r *OrderedMap) GetTranslatable() libdomain.TranslatableType {
// 	return ""
// }
// func (r *OrderedMap) GetData() bool {
// 	return false
// }
// func (r *OrderedMap) GetReturnDetailsInTests() bool {
// 	return r.Property.GetReturnDetailsInTests()
// }
// func (r *OrderedMap) LoadedPosition() int {
// 	return r.Property.GetLoadedPosition()
// }
// func (r *OrderedMap) Position() int {
// 	return r.Property.GetPosition()
// }
// func (r *OrderedMap) LoadedPositionMany2one() int {
// 	return r.Property.GetLoadedPositionMany2one()
// }
// func (r *OrderedMap) PositionMany2one() int {
// 	return r.Property.GetPositionMany2one()
// }
// func (r *OrderedMap) GetLoadedPosition() int {
// 	return r.Property.GetPosition()
// }
// func (r *OrderedMap) GetLoadedPositionMany2one() int {
// 	return r.Property.GetLoadedPositionMany2one()
// }
// func (r *OrderedMap) GetPositionMany2one() int {
// 	return r.Property.GetPositionMany2one()
// }
// func (r *OrderedMap) IsRepeated() bool {
// 	return false
// }
func (r *OrderedMapDefinition) CollectionType() libdomain.PropertyType {
	return OrderedMapPropertyType
}
// func (r *OrderedMap) FromDomain() bool {
// 	return r.Property.FromDomain()
// }
// func (r *OrderedMap) SetFromDomain(d bool) {
// 	r.Property.SetFromDomain(d)
// }
// func (r *OrderedMap) Owner() libdomain.OwnerInterface {
// 	return r.Property.Owner()
// }
// func (r *OrderedMap) SetOwner(d libdomain.OwnerInterface) {
// 	r.Property.SetOwner(d)
// }
func (r *OrderedMapDefinition) InputType() libdomain.ObjectInputType {
	return r.PropertyDefinition.InputType()
}