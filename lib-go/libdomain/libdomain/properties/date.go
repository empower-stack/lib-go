package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// DatePropertyType contains the field type for Date
const DatePropertyType = libdomain.PropertyType("DateType")

/*Date is the field type you can use in definition to declare a Date field.
 */
type DateDefinition struct {
	Common
}
type DateOptions struct {
	Unique bool
	PrimaryKey bool
}

func Date(name string, required bool, position int, loadedPosition int, options *DateOptions) *DateDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &DateDefinition{
		Common: common,
	}
}


// Type return the type of the field.
func (f *DateDefinition) Type() libdomain.PropertyType {
	return DatePropertyType
}

// Type return the type of the field.
func (f *DateDefinition) GoType() string {
	return "time.Time"
}

// Type return the type of the field.
func (f *DateDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

func (f *DateDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *DateDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *DateDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *DateDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *DateDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *DateDefinition) ProtoType() string {
	return "google.protobuf.Timestamp"
}

func (f *DateDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *DateDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *DateDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

// Type return the type of the field.
func (f *DateDefinition) DiffType(pack string) string {
	return "*libdomain.TimeDiff"
}

// Type return the type of the field.
func (f *DateDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTimeDiff"
}

// Type return the type of the field.
func (f *DateDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *DateDefinition) GraphqlSchemaType() string {
	return "Time"
}
