package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// IntegerPropertyType contains the field type for Interger
const IntegerPropertyType = libdomain.PropertyType("IntegerType")

/*Integer is the field type you can use in definition to declare an Integer field.
 */
type IntegerDefinition struct {
	Common
}
type IntegerOptions struct {
	Unique bool
	PrimaryKey bool
}

func Integer(name string, required bool, position int, loadedPosition int, options *IntegerOptions) *IntegerDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &IntegerDefinition{
		Common: common,
	}
}

// Type return the type of the field.
func (f *IntegerDefinition) Type() libdomain.PropertyType {
	return IntegerPropertyType
}

// Type return the type of the field.
func (f *IntegerDefinition) GoType() string {
	return "int"
}

// Type return the type of the field.
func (f *IntegerDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *IntegerDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *IntegerDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

func (f *IntegerDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *IntegerDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *IntegerDefinition) JSONType() string {
	return "float64"
}

// ProtoType return the protobuf type for this field.
func (f *IntegerDefinition) ProtoType() string {
	return "int64"
}

func (f *IntegerDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *IntegerDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *IntegerDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullInt64",
		Value: "Int64",
	}
}

// Type return the type of the field.
func (f *IntegerDefinition) DiffType(pack string) string {
	return "*libdomain.IntegerDiff"
}

// Type return the type of the field.
func (f *IntegerDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewIntegerDiff"
}

// Type return the type of the field.
func (f *IntegerDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *IntegerDefinition) GraphqlSchemaType() string {
	return "Int"
}
