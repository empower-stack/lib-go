package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/arguments"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed domain-*.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Domains(defs *libdomain.Definitions) {

	defs.ControlReferences()

	var err error
	fileInit, _ := fdomain.ReadFile("domain-init.go.tmpl")
	initTemplate := template.Must(initFuncs.Parse(string(fileInit)))

	file, _ := fdomain.ReadFile("domain-main.go.tmpl")
	domainTemplate := template.Must(aggregateFuncs.Parse(string(file)))

	eventsFile, _ := fdomain.ReadFile("domain-events.go.tmpl")
	eventsTemplate := template.Must(eventsFuncs.Parse(string(eventsFile)))

	identitiesFile, _ := fdomain.ReadFile("domain-identities.go.tmpl")
	identitiesTemplate := template.Must(identitiesFuncs.Parse(string(identitiesFile)))

	importsFile, _ := fdomain.ReadFile("domain-imports.go.tmpl")
	importsTemplate := template.Must(importsFuncs.Parse(string(importsFile)))

	fileExternal, _ := fdomain.ReadFile("domain-external.go.tmpl")
	externalTemplate := template.Must(externalFuncs.Parse(string(fileExternal)))

	event := false
	// if defs.EventClient != nil {
	// 	event = true
	// }

	err = os.MkdirAll("gen/identities", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll("gen/events", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll("gen/imports", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	importsMap := map[string]struct {
		DomainName string
		DomainPath string
		Aggregate  libdomain.TableInterface
	}{}
	var aggregates []*libdomain.AggregateDefinition
	for _, definition := range defs.Slice() {

		d := struct {
			Package    string
			Repository string
			Event      bool
			Aggregate  *libdomain.AggregateDefinition
			CustomCommandsAggregate  []*libdomain.CustomRequest
			CustomCommandsCollection []*libdomain.CustomRequest
			Import                   string
		}{
			Package:    defs.Package,
			Repository: defs.Repository,
			Event:      event,
			Aggregate:  definition,
			CustomCommandsAggregate:  definition.GetCustomRequests(),
			// CustomCommandsCollection: definition.AggregateRoot.Commands.CustomCommands,
		}

		imports := map[string]string{}
		for _, e := range definition.GetEntities() {
			for _, property := range e.Properties {
				switch v := property.(type) {
				case *properties.IDDefinition:
					if v.ReferenceDefinition.DomainPath() != "" {
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
						if v.WithEntity {
							if v.ReferenceDefinition.DomainPath() != "" && v.ReferenceDefinition.DomainPath() != defs.RepositoryGen() {
								path = v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					}
				case *properties.EntityDefinition:
					if v.ReferenceDefinition.DomainPath() != "" {
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
						if !v.ReferenceDefinition.(*libdomain.EntityDefinition).HasUpdateInputArguments() {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
						}
					}
				case *properties.ValueObjectDefinition:
					if v.ReferenceDefinition.DomainPath() != "" && v.ReferenceDefinition.DomainPath() != defs.RepositoryGen() {
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					}
				case *properties.OrderedMapDefinition:
					switch v := v.PropertyDefinition.(type) {
					case *properties.EntityDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
							if v.ReferenceDefinition.DomainPackage() != d.Package {
								path = v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					}
				case *properties.SliceDefinition:
					switch v := v.PropertyDefinition.(type) {
					case *properties.IDDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
							if v.ReferenceDefinition.DomainPackage() != d.Package {
								path = v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					}
				}
			}
			for _, method := range e.Methods {
				for _, property := range method.Args {
					switch v := property.(type) {
					case *arguments.SliceDefinition:
						switch vv := v.PropertyDefinition.(type) {
						case *properties.EntityDefinition:
							if vv.ReferenceDefinition.DomainPackage() != d.Package {
								path := vv.ReferenceDefinition.DomainPackage() + " \"" + vv.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					case *arguments.OrderedMapDefinition:
						switch v := v.PropertyDefinition.(type) {
						case *properties.EntityDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
								imports[path] = path
							}
						}
					case *arguments.IDDefinition:
						if v.ReferenceDefinition.DomainPackage() != d.Package {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
						}
					case *arguments.EntityDefinition:
						if v.ReferenceDefinition.DomainPackage() != d.Package {
							path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
							imports[path] = path
						}
					}
				}
			}

		}
		for _, e := range definition.ValueObjects {
			for _, property := range e.Properties {
				// fmt.Printf("%s %T\n", property.GetName(), property)
				switch v := property.(type) {
				case *properties.IDDefinition:
					if v.ReferenceDefinition.DomainPath() != "" {
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
					}
					if v.ReferenceDefinition.DomainPath() != "" && v.ReferenceDefinition.DomainPath() != defs.RepositoryGen() && v.WithEntity {
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					}
				case *properties.OrderedMapDefinition:
					switch v := v.PropertyDefinition.(type) {
					case *properties.EntityDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
						}
					}
				case *properties.SliceDefinition:
					switch v := v.PropertyDefinition.(type) {
					case *properties.IDDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
						}
					}
				}
			}
		}
		for _, e := range definition.RepositoryFunctions {
			for _, property := range e.Args {
				// fmt.Printf("%s %T\n", property.GetName(), property)
				switch v := property.(type) {
				case *arguments.IDDefinition:
					if v.ReferenceDefinition.DomainPath() != "" {
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		aggregates = append(aggregates, definition)

		buf := &bytes.Buffer{}
		err := domainTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/%s.gen.go", strcase.ToSnake(definition.AggregateRoot.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		buf = &bytes.Buffer{}
		err = identitiesTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/identities/%s.gen.go", strcase.ToSnake(definition.AggregateRoot.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		imports = map[string]string{}
		path := defs.Package + "identities \"" + defs.Repository + "/gen/identities\""
		imports[path] = path
		for _, event := range definition.Events {
			for _, version := range event.Versions {
				for _, property := range version.Properties {
					switch v := property.(type) {
					case *arguments.IDDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
						}
					case *arguments.SelectionDefinition:
						if v.GetReference().DomainPath() != "" {
							path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
							imports[path] = path
						}
					case *arguments.EntityDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
							path = v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
							imports[path] = path
						}
					case *arguments.ValueObjectDefinition:
						if v.ReferenceDefinition.DomainPath() != "" {
							// path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/gen/identities\""
							// imports[path] = path
							path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
							imports[path] = path
						}
					case *arguments.OrderedMapDefinition:
						switch v := v.PropertyDefinition.(type) {
						case *properties.EntityDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
								imports[path] = path
								path = v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					case *arguments.SliceDefinition:
						switch v := v.PropertyDefinition.(type) {
						case *properties.IDDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
								imports[path] = path
							}
						case *properties.EntityDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								// path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/gen/identities\""
								// imports[path] = path
								path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		if len(definition.Events) > 0 {
			buf = &bytes.Buffer{}
			err = eventsTemplate.Execute(buf, d)
			if err != nil {
				fmt.Println(buf)
				fmt.Println("execute ", err)
			}
			content, err = format.Source(buf.Bytes())
			if err != nil {
				fmt.Println("model ", err)
				content = buf.Bytes()
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("gen/events/%s.gen.go", strcase.ToSnake(definition.AggregateRoot.Name)),
				content, 0644)
			if err != nil {
				fmt.Println(err)
			}
		}

		imports = map[string]string{}
		for _, entityWithIds := range definition.EntitiesWithIDFields() {
			path := entityWithIds.Entity.DomainPackage() + "identities \"" + entityWithIds.Entity.DomainPath() + "/identities\""
			imports[path] = path
			path = entityWithIds.Entity.DomainPackage() + " \"" + entityWithIds.Entity.DomainPath() + "\""
			imports[path] = path
		}
		for _, entity := range definition.GetEntities() {
			for _, property := range entity.GetProperties() {
				switch v := property.(type) {
				case *properties.OrderedMapDefinition:
					switch vv := v.PropertyDefinition.(type) {
					case *properties.EntityDefinition:
						if vv.ReferenceDefinition.DomainPackage() != d.Package {
							path := vv.ReferenceDefinition.DomainPackage() + "imports \"" + vv.ReferenceDefinition.DomainPath() + "/imports\""
							imports[path] = path
						}
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		buf = &bytes.Buffer{}
		err = importsTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/imports/%s.gen.go", strcase.ToSnake(definition.AggregateRoot.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		for _, table := range definition.GetObjects() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil && field.Type() != properties.SelectionPropertyType{
					if field.GetReference().DomainPath() != defs.RepositoryGen() {
						importsMap[field.GetReference().GetName()] = struct {
							DomainName string
							DomainPath string
							Aggregate  libdomain.TableInterface
						}{
							DomainName: field.GetReference().GetName(),
							DomainPath: field.GetReference().DomainPath(),
							Aggregate:  field.GetReference(),
						}
					}
				}
			}
			for _, request := range definition.AggregateRoot.Methods {
				for _, result := range request.Args {
					if result.GetReference() != nil {
						if result.GetReference().DomainPath() != defs.RepositoryGen() {
							importsMap[result.GetReference().GetName()] = struct {
								DomainName string
								DomainPath string
								Aggregate  libdomain.TableInterface
							}{
								DomainName: result.GetReference().GetName(),
								DomainPath: result.GetReference().DomainPath(),
								Aggregate:  result.GetReference(),
							}
						}
					}
				}
			}
		}

	}

	d := struct {
		Package    string
		Repository string
		Aggregates []*libdomain.AggregateDefinition
	}{
		Package:    defs.Package,
		Repository: defs.Repository,
		Aggregates: aggregates,
	}
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("gen/init.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	externalAggregates := []struct {
		DomainName string
		DomainPath string
		Aggregate  libdomain.TableInterface
	}{}
	for _, value := range importsMap {
		externalAggregates = append(externalAggregates, value)
	}
	externalData := struct {
		Package            string
		ExternalAggregates []struct {
			DomainName string
			DomainPath string
			Aggregate  libdomain.TableInterface
		}
	}{
		Package:            defs.Package,
		ExternalAggregates: externalAggregates,
	}
	buf = &bytes.Buffer{}
	err = externalTemplate.Execute(buf, externalData)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("gen/external.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

var initFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var identitiesFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var eventsFuncs = template.New("").Funcs(template.FuncMap{
	"generateDeletedEvent": func(aggregate *libdomain.AggregateDefinition, entity *libdomain.EntityDefinition) bool {
		for _, event := range aggregate.Events {
			if event.Name == entity.Title()+"Deleted" {
				return false
			}
		}
		return true
	},
	"computeImports": func(aggregate *libdomain.AggregateDefinition) string {
		imports := map[libdomain.TableInterface]libdomain.TableInterface{}
		for _, event := range aggregate.Events {
			for _, version := range event.Versions {
				for _, property := range version.Properties {
					switch v := property.(type) {
					case *arguments.IDDefinition:
						imports[v.ReferenceDefinition] = v.ReferenceDefinition
					}
				}
			}
		}

		var results []string
		for _, value := range imports {
			results = append(results, fmt.Sprintf(`type %sID = %s.%sID`, value.Title(), value.DomainPackage(), value.Title()))
		}

		return strings.Join(results, "\n")
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var externalFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

func generatePropertyFunction(packageName string, entityTitle string, valueObjectsPath []string, property libdomain.PropertyDefinition) string {

	var functionDeclaration string
	var after string

	if property.CollectionType() == properties.OrderedMapPropertyType {
		functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.GoType}} {`
	} else if property.CollectionType() == properties.SlicePropertyType {
		functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}String() []string {
				var result []string
				for _, v := range t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{.Property.Title}}() {
					result = append(result, string(v))
				}
				return result
			}
			func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.GoType}} {`
		after = `{{- if and (eq .Property.Type "IDType") .Property.GetWithEntity}}
			func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() []*{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}} {
				if !t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.TitleWithoutID}}IsLoaded(){{else}}{{.Property.NameWithoutID}}IsLoaded{{end}} {
					panic(errors.New("{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}} is not loaded"))
				}
				return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.TitleWithoutID}}(){{else}}{{.Property.NameWithoutID}}{{end}}
			}
			func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}IsLoaded() bool {
				return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.TitleWithoutID}}IsLoaded(){{else}}{{.Property.NameWithoutID}}IsLoaded{{end}}
			}
			{{- end}}
			`
	} else {
		if property.Type() == properties.EntityPropertyType {
			functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}} {`
		} else if property.Type() == properties.ValueObjectPropertyType {
			functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}} {`
			after = ""
			for _, p := range property.GetReference().GetProperties() {
				after += generatePropertyFunction(packageName, entityTitle, append(valueObjectsPath, property.Title()), p)
			}
		} else if property.Type() == properties.IDPropertyType {
			functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}String() string {
					return string(t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{.Property.Title}}())
				}
				func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.GoType}} {`
			after = `{{- if and (eq .Property.Type "IDType") .Property.GetWithEntity}}
				func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}} {
					if !t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.TitleWithoutID}}IsLoaded(){{else}}{{.Property.NameWithoutID}}IsLoaded{{end}} {
						panic(errors.New("{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}} is not loaded"))
					}
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.TitleWithoutID}}(){{else}}{{.Property.NameWithoutID}}{{end}}
				}
				func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}IsLoaded() bool {
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.TitleWithoutID}}IsLoaded(){{else}}{{.Property.NameWithoutID}}IsLoaded{{end}}
				}
				{{- end}}
				`
		} else if property.Type() == properties.SelectionPropertyType {
			functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}String() string {
					return string(t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{.Property.Title}}())
				}
				func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}}Type {`
		} else {
			functionDeclaration = `func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.GoType}} {`
			after = `{{- if and (eq .Property.Type "TextType") (eq .Property.GetTranslatable "TranslatableWYSIWYG")}}
			func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}WYSIWYG() *libdomain.WYSIWYG {
				if !t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}WYSIWYGIsLoaded(){{else}}{{.Property.Name}}WYSIWYGIsLoaded{{end}} {
					panic(errors.New("{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}WYSIWYG is not loaded"))
				}
				return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}WYSIWYG(){{else}}{{.Property.Name}}WYSIWYG{{end}}
			}
			func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}WYSIWYGIsLoaded() bool {
				return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}WYSIWYGIsLoaded(){{else}}{{.Property.Name}}WYSIWYGIsLoaded{{end}}
			}
			{{- end}}
			`
		}
	}

	content := `{{.FunctionDeclarationContent}}
			if !t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}IsLoaded(){{else}}{{.Property.Name}}IsLoaded{{end}} {
				panic(errors.New("{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}} is not loaded"))
			}
			{{- if eq .Property.Type "ValueObjectType"}}
			if t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}} == nil {
				{{.Property.GetReference.Name}} := &{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}}{}
				{{.Property.GetReference.Name}}.AllPropertiesLoaded()
				return {{.Property.GetReference.Name}}
			}
			return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
			{{- else }}
			return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
			{{- end }}
		}
		func (t *{{.EntityTitle}}) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}IsLoaded() bool {
			return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}IsLoaded(){{else}}{{.Property.Name}}IsLoaded{{end}}
		}
		{{.AfterContent}}
		`

	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(functionDeclaration)).Execute(buf, struct {
		Package 	string
		EntityTitle		string
		ValueObjectsPath []string
		Property 		libdomain.PropertyDefinition
	}{
		Package: packageName,
		EntityTitle: entityTitle,
		ValueObjectsPath: valueObjectsPath,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	functionDeclarationContent := buf.String()

	buf = &bytes.Buffer{}
	err = template.Must(template.New("").Parse(after)).Execute(buf, struct {
		Package 	string
		EntityTitle		string
		ValueObjectsPath []string
		Property 		libdomain.PropertyDefinition
	}{
		Package: packageName,
		EntityTitle: entityTitle,
		ValueObjectsPath: valueObjectsPath,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	afterContent := buf.String()

	buf = &bytes.Buffer{}
	err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
		Package 	string
		EntityTitle		string
		ValueObjectsPath []string
		FunctionDeclarationContent string
		AfterContent string
		Property 		libdomain.PropertyDefinition
	}{
		Package: packageName,
		EntityTitle: entityTitle,
		ValueObjectsPath: valueObjectsPath,
		FunctionDeclarationContent: functionDeclarationContent,
		AfterContent: afterContent,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()


}

func generateGetPropertyFunction(packageName string, entityTitle string, valueObjectsPath []string, property libdomain.PropertyDefinition) string {

	var content string

	if property.CollectionType() == properties.OrderedMapPropertyType {
		content = `func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}Get{{.Property.GetReference.Title}}Properties {
				return t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.Title}}
			}
			`

	} else {
		if property.Type() == properties.EntityPropertyType {
			content = `func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}Get{{.Property.GetReference.Title}}Properties {
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{.Property.Title}}
				}
				`
		} else if property.Type() == properties.ValueObjectPropertyType {
			content = `func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}Get{{.Property.GetReference.Title}}Properties {	
					if t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.Title}} == nil {
						return &{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}Get{{.Property.GetReference.Title}}Properties{}
					}
					return t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.Title}}
				}
				`
			for _, p := range property.GetReference().GetProperties() {
				content += generateGetPropertyFunction(packageName, entityTitle, append(valueObjectsPath, property.Title()), p)
			}
		} else {
			content = `func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() bool {
					return t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.Title}}
				}
				{{- if and (eq .Property.Type "IDType") .Property.GetWithEntity}}
				func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() interface{} {
					if  t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.TitleWithoutID}} != nil {
						return t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.TitleWithoutID}}
					}
					return nil
				}
				{{- end}}
				{{- if and (eq .Property.Type "TextType") (eq .Property.GetTranslatable "TranslatableSimple")}}
				func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}Translations() bool {
					return t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.Title}}Translations
				}
				{{- end}}
				{{- if and (eq .Property.Type "TextType") (eq .Property.GetTranslatable "TranslatableWYSIWYG")}}
				func (t *Get{{.EntityTitle}}Properties) Get{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}WYSIWYG() *libdomain.GetWYSIWYGProperties {
					return t.{{range .ValueObjectsPath}}Get{{.}}().{{end}}{{.Property.Title}}WYSIWYG
				}
				{{- end}}	
				`
		}
	}


	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		Package 	string
		EntityTitle		string
		ValueObjectsPath []string
		Property 		libdomain.PropertyDefinition
	}{
		Package: packageName,
		EntityTitle: entityTitle,
		ValueObjectsPath: valueObjectsPath,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()


}

func generatePropertyDiffFunction(packageName string, entityTitle string, valueObjectsPath []string, property libdomain.PropertyDefinition) string {

	var content string

	if property.CollectionType() == properties.OrderedMapPropertyType {
		content = `func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() map[{{.Property.GetReference.DomainPackage}}identities.{{.Property.GetReference.Title}}ID]*{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}}Diff {
				return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
			}
			`
	} else if property.CollectionType() == properties.SlicePropertyType {
		content = `func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}String() *libdomain.SliceDiff {
				if t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}} != nil {
					var olds []string
					for _, v := range t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}.Old() {
						olds = append(olds, string(v))
					}
					var news []string
					for _, v := range t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}.New() {
						news = append(news, string(v))
					}
					return libdomain.NewSliceDiff(
						olds,
						news,
					)
				}
				return nil
			}
			func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.DiffType $.Package}} {
				return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
			}
			`		
	} else {
		if property.Type() == properties.EntityPropertyType {
			content = ""
		} else if property.Type() == properties.ValueObjectPropertyType {
			content = `func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}}Diff {
					if t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}} == nil {
						return &{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}}Diff{}
					}
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
				}
				`
			for _, p := range property.GetReference().GetProperties() {
				content += generatePropertyDiffFunction(packageName, entityTitle, append(valueObjectsPath, property.Title()), p)
			}
		} else if property.Type() == properties.IDPropertyType {
			content = `func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}String() *libdomain.TextDiff {
					if t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}} != nil {
						return libdomain.NewTextDiff(
							string(t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}.Old()),
							string(t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}.New()),
						)
					}
					return nil
				}
				func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.DiffType $.Package}} {
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
				}
				`
		} else if property.Type() == properties.SelectionPropertyType {
			content = `func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}String() *libdomain.TextDiff {
					if t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}} != nil {
						return libdomain.NewTextDiff(
							string(t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}.Old()),
							string(t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}.New()),
						)
					}
					return nil
				}
				func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() *{{if ne .Property.GetReference.DomainPackage $.Package}}{{.Property.GetReference.DomainPackage}}.{{end}}{{.Property.GetReference.Title}}TypeDiff {
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
				}
				`
		} else {
			content = `func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}() {{.Property.DiffType $.Package}} {
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}(){{else}}{{.Property.Name}}{{end}}
				}
				{{- if and (eq .Property.Type "TextType") (eq .Property.GetTranslatable "TranslatableWYSIWYG")}}
				func (t *{{.EntityTitle}}Diff) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}WYSIWYG() *libdomain.WYSIWYGDiff {
					return t.{{range .ValueObjectsPath}}{{.}}().{{end}}{{if .ValueObjectsPath}}{{.Property.Title}}WYSIWYG(){{else}}{{.Property.Name}}WYSIWYG{{end}}
				}
				{{- end}}
				`
		}
	}


	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		Package 	string
		EntityTitle		string
		ValueObjectsPath []string
		Property 		libdomain.PropertyDefinition
	}{
		Package: packageName,
		EntityTitle: entityTitle,
		ValueObjectsPath: valueObjectsPath,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()

}

// nolint:lll
var aggregateFuncs = template.New("").Funcs(template.FuncMap{
	"generateDeleteMethod": func(entity *libdomain.EntityDefinition) bool {
		for _, event := range entity.Methods {
			if event.Name == "delete" {
				return false
			}
		}
		return true
	},
	"makeSlice": func(args ...string) []string {
		return args
	},
	"hasInputArguments": func(entity *libdomain.EntityDefinition) bool {
		for _, property := range entity.Properties {
			if property.CollectionType() == properties.OrderedMapPropertyType {
				return true
			}
			if property.CollectionType() == properties.SlicePropertyType {
				if property.Type() == properties.EntityPropertyType {
					return true
				}
			}
			if property.Type() == properties.EntityPropertyType {
				return true
			}
		}
		return false
	},
	"hasUpdateInputArguments": func(entity *libdomain.EntityDefinition) bool {
		return entity.HasUpdateInputArguments()
	},
	"generatePropertyFunction": func(packageName string, entityTitle string, property libdomain.PropertyDefinition) string {
		return generatePropertyFunction(packageName, entityTitle, []string{}, property)
	},
	"generateGetPropertyFunction": func(packageName string, entityTitle string, property libdomain.PropertyDefinition) string {
		return generateGetPropertyFunction(packageName, entityTitle, []string{}, property)
	},
	"generatePropertyDiffFunction": func(packageName string, entityTitle string, property libdomain.PropertyDefinition) string {
		return generatePropertyDiffFunction(packageName, entityTitle, []string{}, property)
	},
	"getCustomCommandSingleton": func(aggregate *libdomain.AggregateDefinition, table libdomain.TableInterface, customCommand *libdomain.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (t *{{.ModelTitle}}) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   collection := &{{.ModelTitle}}Collection{}
		   collection.Init([]libdomain.DomainObjectInterface{t})
		   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(collection, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libdomain.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandPrototype": func(customFunc *libdomain.CustomRequest) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandCollection": func(aggregate *libdomain.AggregateDefinition, table libdomain.TableInterface, customCommand *libdomain.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (c *{{.ModelTitle}}Collection) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(c, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libdomain.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandAggregatePrototype": func(aggregate *libdomain.AggregateDefinition, customFunc *libdomain.CustomRequest, isAggregate bool) string {

		content := "{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}} func({{if not .OnFactory}}*{{.AggregateTitle}}Collection,{{else}}libdomain.ApplicationContext,{{end}} *{{.AggregateTitle}}InternalLibrary, *pb.{{.AggregateTitle}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) ({{if not .OnFactory}}map[string]{{end}}*pb.{{.AggregateTitle}}{{.Title}}Results, error)"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			goType := result.GoType()
			if result.Type() == arguments.EntityArgumentType {
				goType = "*" + strcase.ToCamel(result.GetReferenceName())
			}
			results = append(results, string(goType))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsCommand      bool
			OnFactory      bool
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			IsCommand:      customFunc.IsCommand(),
			OnFactory:      customFunc.OnFactory,
			IsAggregate:    isAggregate,
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandRepositoryPrototype": func(aggregate *libdomain.AggregateDefinition, customFunc *libdomain.CustomRequest, isAggregate bool) string {

		content := "{{.Title}}({{if not .IsAggregate}}*{{.AggregateTitle}}Collection,{{else}}libdomain.ApplicationContext,{{end}} *{{.AggregateTitle}}InternalLibrary, {{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			goType := result.GoType()
			if result.Type() == arguments.EntityArgumentType {
				goType = "*" + strcase.ToCamel(result.GetReferenceName())
			}
			results = append(results, string(goType))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			IsAggregate:    isAggregate,
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandAggregate": func(aggregate *libdomain.AggregateDefinition, customCommand *libdomain.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
		type {{.Aggregate.AggregateRoot.Title}}{{.Title}}Results struct{
			{{- range .Results}}
			{{.Title}} {{.GoType}}
			{{- end}}
		}
		{{if .OnFactory}}
	   	func (a *{{.Aggregate.AggregateRoot.Title}}Factory) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
	   
		   results, err := {{.Aggregate.AggregateRoot.Title}}Aggregate.{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}}(a.Workflow, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, command)
		   if err != nil {
			   return nil, err
		   }
		   return results, nil
	   
	   	}
		{{else}}
		func (a *{{.Aggregate.AggregateRoot.Title}}Collection) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (map[string]*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
	   
			results, err := {{.Aggregate.AggregateRoot.Title}}Aggregate.{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}}(a, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, command)
			if err != nil {
				return nil, err
			}

			if results == nil {
				results = map[string]*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results{}
			}
			for _, record := range a.Slice() {
				if _, ok := results[record.pb.ID]; !ok {
					results[record.pb.ID] = &pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results{}
				}
			}

			return results, nil
		
		}
		func (a *{{.Aggregate.AggregateRoot.Title}}) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
			
			/*
			collection := &{{.Aggregate.AggregateRoot.Title}}Collection{
				Collection: &libdomain.Collection{
					Aggregate: {{.Aggregate.AggregateRoot.Title}}Aggregate,
					Workflow: a.Workflow,
				},
			}
			collection.Init([]libdomain.DomainObjectInterface{a})
			*/

			collection := {{.Aggregate.AggregateRoot.Title}}Aggregate.NewFactory(a.Workflow).NewCollection([]*{{.Aggregate.AggregateRoot.Title}}{a})
	   
			results, err := collection.{{.Title}}(command)
			if err != nil {
				return nil, err
			}

			return results[a.pb.ID], nil
		
		}	
		{{end}}
		`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			goType := arg.GoType()
			if arg.Type() == arguments.EntityArgumentType {
				goType = "*" + strcase.ToCamel(arg.GetReferenceName())
			}
			argsPrototype = append(argsPrototype, arg.GetName()+" "+goType)
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			goType := result.GoType()
			if result.Type() == arguments.EntityArgumentType {
				goType = "*" + strcase.ToCamel(result.GetReferenceName())
			}
			resultsPrototype = append(resultsPrototype, string(goType))
			results = append(results, result.GetName())
			returnKO = append(returnKO, result.GoNil())
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libdomain.AggregateDefinition
			IsCommand        bool
			OnFactory        bool
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          []libdomain.ArgumentDefinition
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			IsCommand:        customCommand.IsCommand(),
			OnFactory:        customCommand.OnFactory,
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          customCommand.Results,
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getRecursiveOne2many": func(entity *libdomain.EntityDefinition) []*one2manyData {

		results := recursiveOne2ManySearch(entity, nil, "", []*one2manyData{}, nil, []*one2manyData{})

		for i, j := 0, len(results)-1; i < j; i, j = i+1, j-1 {
			results[i], results[j] = results[j], results[i]
		}

		return results

		// return []struct {
		// 	Table libdomain.TableInterface
		// 	// Property *properties.One2many
		// 	Path string
		// }{}

	},
	"convertJson": func(field libdomain.PropertyDefinition) string {
		if field.Type() == properties.FloatPropertyType {
			return fmt.Sprintf("%vField := data[\"%v\"].(float64)", field.GetName(), field.GetName())
		}
		return fmt.Sprintf("%vField := data[\"%v\"].(string)", field.GetName(), field.GetName())
	},
	"getImportExternal": func(repository string, aggregate *libdomain.AggregateDefinition) string {
		importsMap := map[string]string{}
		for _, table := range aggregate.GetObjects() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != repository {
						importsMap[field.GetReference().GetName()] = field.GetReference().DomainPath()
					}
				}
			}
		}
		var imports []string
		for name, path := range importsMap {
			imports = append(imports, fmt.Sprintf("%s \"%s/gen\"", name, path))
		}
		return strings.Join(imports, "\n")
	},
	"getExternalEntities": func(repository string, aggregate *libdomain.AggregateDefinition) string {
		importsMap := map[string]string{}
		for _, table := range aggregate.GetObjects() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != repository {
						importsMap[field.GetReference().GetName()] = field.GetReference().Title()
					}
				}
			}
		}
		var imports []string
		for name, entity := range importsMap {
			imports = append(imports, fmt.Sprintf("type %s = %s.%s", entity, name, entity))
			imports = append(imports, fmt.Sprintf("var %sEntity = %s.%sEntity", entity, name, entity))
		}
		return strings.Join(imports, "\n")
	},
	"title": func(s string) string {
		return strings.Title(s)
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

// nolint:lll
var importsFuncs = template.New("").Funcs(template.FuncMap{
	"getRecursiveOne2many": func(entity *libdomain.EntityDefinition) []*one2manyData {

		results := recursiveOne2ManySearch(entity, nil, "", []*one2manyData{}, nil, []*one2manyData{})
		for i, j := 0, len(results)-1; i < j; i, j = i+1, j-1 {
			results[i], results[j] = results[j], results[i]
		}

		return results

		// return []struct {
		// 	Table libdomain.TableInterface
		// 	// Property *properties.One2many
		// 	Path string
		// }{}

	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

type one2manyData struct {
	Table         libdomain.TableInterface
	Property      *properties.OrderedMapDefinition
	Path          string
	PreviousDatas []*one2manyData
	ParentTable   libdomain.TableInterface
}

func recursiveOne2ManySearch(
	table libdomain.TableInterface,
	property *properties.OrderedMapDefinition,
	path string,
	previousDatas []*one2manyData,
	parentTable libdomain.TableInterface,
	results []*one2manyData,
) []*one2manyData {

	previousDatas = append(previousDatas, &one2manyData{
		Table:       table,
		Property:    property,
		Path:        path,
		ParentTable: parentTable,
	})

	for _, property := range table.StoredProperties() {
		if property.CollectionType() == "OrderedMap" {

			data := &one2manyData{
				Table:         table,
				Property:      property.(*properties.OrderedMapDefinition),
				Path:          path,
				PreviousDatas: previousDatas,
				ParentTable:   parentTable,
			}
			results = append(results, data)

			newPath := path + property.GetName() + "/"

			results = recursiveOne2ManySearch(property.GetReference(), property.(*properties.OrderedMapDefinition), newPath, previousDatas, table, results)
		}
	}

	return results
}
