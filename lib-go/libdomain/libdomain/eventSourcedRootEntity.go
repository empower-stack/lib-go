package libdomain

type EventSourcedRootEntity struct {
	unmutatedVersion int
	when             func(DomainEvent)
	mutatingEvents   []DomainEvent
}

func NewEventSourcedRootEntity(version int, when func(DomainEvent)) *EventSourcedRootEntity {
	return &EventSourcedRootEntity{
		unmutatedVersion: version,
		when:             when,
	}
}

func (e *EventSourcedRootEntity) Apply(event DomainEvent) {

	e.mutatingEvents = append(e.mutatingEvents, event)

	e.when(event)
}

func (e *EventSourcedRootEntity) UnmutatedVersion() int {
	return e.unmutatedVersion
}

func (e *EventSourcedRootEntity) MutatingEvents() []DomainEvent {
	return e.mutatingEvents
}

func (e *EventSourcedRootEntity) Version() int {
	return e.unmutatedVersion + len(e.mutatingEvents)
}

func (e *EventSourcedRootEntity) Mutated() {
	e.unmutatedVersion += len(e.mutatingEvents)
	e.mutatingEvents = []DomainEvent{}
}
