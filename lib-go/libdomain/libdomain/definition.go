package libdomain

import (
	"context"
	"fmt"
	"strings"
	"time"

	"golang.org/x/exp/slices"

	"github.com/iancoleman/strcase"
	"github.com/sergi/go-diff/diffmatchpatch"
)

type Definitions struct {
	Package    string
	Repository string
	// UseTenants bool
	slice []*AggregateDefinition
	byIds map[string]*AggregateDefinition

	enumByIds map[string]*EnumDefinition

	objectsByName map[string]TableInterface
}

func (ds *Definitions) RepositoryGen() string {
	return ds.Repository + "/gen"
}

var PrepareEntity func(*EntityDefinition)

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(a *AggregateDefinition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*AggregateDefinition{}
	}

	if ds.objectsByName == nil {
		ds.objectsByName = map[string]TableInterface{}
	}

	a.AggregateRoot.isAggregateRoot = true

	EntitiesInAggregate := a.GetEntities()
	a.entitiesByName = map[string]*EntityDefinition{}
	a.valueObjectsByName = map[string]*ValueObjectDefinition{}
	a.commandsByName = map[string]*CustomRequest{}

	for _, e := range EntitiesInAggregate {

		e.aggregateDefinition = a
		a.entitiesByName[e.Name] = e

		e.domainPackage = ds.Package
		e.domainPath = ds.RepositoryGen()

		// if e.Queries == nil {
		// 	e.Queries = &EntityQueriesDefinition{}
		// }
		// if e.Commands == nil {
		// 	e.Commands = &EntityCommandsDefinition{}
		// }

		ds.objectsByName[e.Name] = e

		PrepareEntity(e)

		for _, f := range e.Keys {
			f.SetFromDomain(true)
			f.SetOwner(e)
			// for _, old := range EntitiesInAggregate {
			// 	if f.GetReferenceName() == old.Name {
			// 		f.SetReference(old)
			// 	}
			// }
			// for _, old := range ds.Slice() {
			// 	if f.GetReferenceName() == old.AggregateRoot.Name {
			// 		f.SetReference(old.AggregateRoot)
			// 	}
			// }
			if f.IsTranslatable() {
				e.hasTranslatable = true
			}
			if f.GetName() == e.UniqueProperty {
				e.uniqueProperty = f
			}
		}
		for _, f := range e.Properties {
			f.SetFromDomain(true)
			f.SetOwner(e)
			// for _, old := range EntitiesInAggregate {
			// 	if f.GetReferenceName() == old.Name {
			// 		f.SetReference(old)
			// 	}
			// }
			// for _, old := range ds.Slice() {
			// 	if f.GetReferenceName() == old.AggregateRoot.Name {
			// 		f.SetReference(old.AggregateRoot)
			// 	}
			// }
			if f.IsTranslatable() {
				e.hasTranslatable = true
			}
			if f.GetName() == e.UniqueProperty {
				e.uniqueProperty = f
			}
		}
		for _, f := range e.Methods {
			f.tableDefinition = e
		}

		if e.UniqueProperty != "" && e.uniqueProperty == nil {
			panic(fmt.Sprintf("The unique property %s doesn't exist in the entity %s", e.UniqueProperty, e.Name))
		}

		// e.setUseTenants(e.AggregateDefinition.UseTenants)
	}

	for _, e := range a.Enums {

		if e.Name == "" {
			panic(fmt.Sprintf("The enum name can't be empty"))
		}

		e.Package = ds.Package
		e.Path = ds.RepositoryGen()

		ds.objectsByName[e.Name] = e
	}

	for _, v := range a.ValueObjects {
		v.aggregateDefinition = a
		// v.aggregateIDProperty = SetAggregateIDProperty(a.AggregateRoot.Name)
		a.valueObjectsByName[v.Name] = v

		v.domainPackage = ds.Package
		v.domainPath = ds.RepositoryGen()

		ds.objectsByName[v.Name] = v

		for _, f := range v.GetKeys() {
			f.SetFromDomain(true)
			f.SetOwner(v)
			// for _, old := range EntitiesInAggregate {
			// 	if f.GetReferenceName() == old.Name {
			// 		f.SetReference(old)
			// 	}
			// }
			// for _, old := range ds.Slice() {
			// 	if f.GetReferenceName() == old.AggregateRoot.Name {
			// 		f.SetReference(old.AggregateRoot)
			// 	}
			// }
		}
		for _, f := range v.Properties {
			f.SetFromDomain(true)
			f.SetOwner(v)
			// for _, old := range EntitiesInAggregate {
			// 	if f.GetReferenceName() == old.Name {
			// 		f.SetReference(old)
			// 	}
			// }
			// for _, old := range ds.Slice() {
			// 	if f.GetReferenceName() == old.AggregateRoot.Name {
			// 		f.SetReference(old.AggregateRoot)
			// 	}
			// }
		}
	}

	for _, c := range a.Queries {
		a.commandsByName[c.Name] = c
	}
	for _, c := range a.Commands {
		c.isCommand = true
		a.commandsByName[c.Name] = c
	}
	for _, e := range a.Events {
		if len(e.Versions) > 0 {
			e.Versions[len(e.Versions)-1].isLast = true
		}

		for _, v := range e.Versions {
			for _, f := range v.Properties {
				switch f.Type() {
				case "EntityType", "ValueObjectType":
					if f.InputType() == "" {
						panic(fmt.Sprintf("Event object properties must be input: %s %s", e.Name, f.GetName()))
					}
				}
			}
		}
	}

	ds.slice = append(ds.slice, a)
	ds.byIds[a.AggregateRoot.Name] = a

	for _, old := range ds.Slice() {
		// fmt.Println("old ", old.AggregateRoot.Name)
		for _, e := range old.GetObjects() {
			for _, f := range e.GetKeys() {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.objectsByName[f.GetReferenceName()])
					}
				}
			}
			for _, f := range e.GetProperties() {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.objectsByName[f.GetReferenceName()])
					}
				}
			}

			if e.IsEntity() {
				for _, c := range e.(*EntityDefinition).Methods {
					for _, f := range c.Args {
						if f.GetReferenceName() != "" && f.GetReference() == nil {
							if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
								f.SetReference(ds.objectsByName[f.GetReferenceName()])
							}
						}
					}

					for _, f := range c.Results {
						if f.GetReferenceName() != "" && f.GetReference() == nil {
							if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
								f.SetReference(ds.objectsByName[f.GetReferenceName()])
							}
						}
					}
				}
			}

		}

		for _, c := range old.GetCustomRequests() {
			for _, f := range c.Args {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.objectsByName[f.GetReferenceName()])
					}
				}
			}
			for _, f := range c.Results {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.objectsByName[f.GetReferenceName()])
					}
				}
			}

		}

		for _, c := range old.Events {
			for _, v := range c.Versions {
				for _, f := range v.Properties {
					if f.GetReferenceName() != "" && f.GetReference() == nil {
						if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
							f.SetReference(ds.objectsByName[f.GetReferenceName()])
						}
					}
				}
			}
		}

		for _, c := range old.RepositoryFunctions {
			for _, f := range c.Args {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.objectsByName[f.GetReferenceName()])
					}
				}
			}
			for _, f := range c.Results {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					if _, ok := ds.objectsByName[f.GetReferenceName()]; ok {
						f.SetReference(ds.objectsByName[f.GetReferenceName()])
					}
				}
			}
		}
	}

}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*AggregateDefinition {
	result := ds.slice

	return result
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *AggregateDefinition {
	d := ds.byIds[id]

	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

func (ds *Definitions) ControlReferences() {
	for _, old := range ds.Slice() {
		// fmt.Println("old ", old.AggregateRoot.Name)
		for _, e := range old.GetObjects() {
			for _, f := range e.GetKeys() {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), e.GetName(), f.GetReferenceName()))
				}
			}
			for _, f := range e.GetProperties() {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), e.GetName(), f.GetReferenceName()))
				}
			}

			if e.IsEntity() {
				for _, c := range e.(*EntityDefinition).Methods {
					for _, f := range c.Args {
						if f.GetReferenceName() != "" && f.GetReference() == nil {
							panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
						}
					}

					for _, f := range c.Results {
						if f.GetReferenceName() != "" && f.GetReference() == nil {
							panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
						}
					}
				}
			}

		}

		for _, c := range old.GetCustomRequests() {
			for _, f := range c.Args {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
				}
			}
			for _, f := range c.Results {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
				}
			}

		}

		for _, c := range old.Events {
			for _, v := range c.Versions {
				for _, f := range v.Properties {
					if f.GetReferenceName() != "" && f.GetReference() == nil {
						panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
					}
				}
			}
		}

		for _, c := range old.RepositoryFunctions {
			for _, f := range c.Args {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
				}
			}
			for _, f := range c.Results {
				if f.GetReferenceName() != "" && f.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", f.GetName(), c.Name, f.GetReferenceName()))
				}
			}
		}
	}
}

// Definition is used to declare the information of a model, so it can generate its code.
type AggregateDefinition struct {
	AggregateRoot       *EntityDefinition
	Children            []*EntityDefinition
	Enums               []*EnumDefinition
	Events              []*EventDefinition
	RepositoryFunctions []*CustomRequest
	UseTenants          bool
	entitiesByName      map[string]*EntityDefinition
	ValueObjects        []*ValueObjectDefinition
	valueObjectsByName  map[string]*ValueObjectDefinition
	Queries             []*CustomRequest
	Commands            []*CustomRequest
	commandsByName      map[string]*CustomRequest
	ExternalData        []*ExternalData
	// EventDispatcher     EventDispatcher
}

func (a *AggregateDefinition) GetEntities() []*EntityDefinition {
	EntitiesInAggregate := []*EntityDefinition{
		a.AggregateRoot,
	}
	for _, c := range a.Children {
		EntitiesInAggregate = append(EntitiesInAggregate, c)
	}
	return EntitiesInAggregate
}
func (a *AggregateDefinition) GetEntityByName(name string) *EntityDefinition {
	return a.entitiesByName[name]
}
func (a *AggregateDefinition) GetValueObjectByName(name string) *ValueObjectDefinition {
	return a.valueObjectsByName[name]
}
func (a *AggregateDefinition) GetCommandByName(name string) *CustomRequest {
	return a.commandsByName[name]
}
func (a *AggregateDefinition) GetObjects() []TableInterface {
	var tables []TableInterface
	for _, c := range a.GetEntities() {
		tables = append(tables, c)
	}
	for _, c := range a.ValueObjects {
		tables = append(tables, c)
	}
	return tables
}
func (a *AggregateDefinition) GetCustomRequests() []*CustomRequest {
	var requests []*CustomRequest
	for _, c := range a.Queries {
		requests = append(requests, c)
	}
	for _, c := range a.Commands {
		requests = append(requests, c)
	}
	return requests
}
func (a *AggregateDefinition) EntitiesWithIDFields() []*EntityWithIDFields {
	entitiesWithIds := map[string]*EntityWithIDFields{
		// a.AggregateRoot.GetName(): {
		// 	Entity: a.AggregateRoot,
		// 	Fields: []string{""},
		// 	Many: false,
		// },
	}

	order := recursiveOne2ManySearch(a.AggregateRoot, "", entitiesWithIds, []string{})

	results := []*EntityWithIDFields{}
	for _, v := range order {
		results = append(results, entitiesWithIds[v])
	}
	// for i, j := 0, len(results)-1; i < j; i, j = i+1, j-1 {
	// 	results[i], results[j] = results[j], results[i]
	// }

	return results
}

type EntityWithIDFields struct {
	Entity TableInterface
	Fields []string
	Many bool
}

func recursiveOne2ManySearch(
	entity TableInterface,
	path string,
	results map[string]*EntityWithIDFields,
	order []string,
) []string {

	for _, property := range entity.StoredProperties() {

		if property.CollectionType() == "" {
			if string(property.Type()) == "ValueObjectType" {
				for _, property2 := range property.GetReference().StoredProperties() {
					if string(property2.Type()) == "IDType" {
						if _, ok := results[property2.GetReference().GetName()]; !ok {
							results[property2.GetReference().GetName()] = &EntityWithIDFields{
								Entity: property2.GetReference(),
								Fields: []string{},
							}
						}
						results[property2.GetReference().GetName()].Fields = append(
							results[property2.GetReference().GetName()].Fields, path+property2.GetName()+"/")
						if !slices.Contains(order, property2.GetReference().GetName()) {
							order = append(order, property2.GetReference().GetName())
						}
					}
				}
			}
			if string(property.Type()) == "IDType" {
				if _, ok := results[property.GetReference().GetName()]; !ok {
					results[property.GetReference().GetName()] = &EntityWithIDFields{
						Entity: property.GetReference(),
						Fields: []string{},
					}
				}
				results[property.GetReference().GetName()].Fields = append(
					results[property.GetReference().GetName()].Fields, path+property.GetName()+"/")
				if !slices.Contains(order, property.GetReference().GetName()) {
					order = append(order, property.GetReference().GetName())
				}
			}
		}

		if property.CollectionType() == "Slice" {
			if string(property.Type()) == "ValueObjectType" {
				for _, property2 := range property.GetReference().StoredProperties() {
					if string(property2.Type()) == "IDType" {
						if _, ok := results[property2.GetReference().GetName()]; !ok {
							results[property2.GetReference().GetName()] = &EntityWithIDFields{
								Entity: property2.GetReference(),
								Fields: []string{},
								Many: true,
							}
						}
						results[property2.GetReference().GetName()].Fields = append(
							results[property2.GetReference().GetName()].Fields, path+property2.GetName()+"/")
						if !slices.Contains(order, property2.GetReference().GetName()) {
							order = append(order, property2.GetReference().GetName())
						}
					}
				}
			}
			if string(property.Type()) == "IDType" {
				if _, ok := results[property.GetReference().GetName()]; !ok {
					results[property.GetReference().GetName()] = &EntityWithIDFields{
						Entity: property.GetReference(),
						Fields: []string{},
						Many: true,
					}
				}
				results[property.GetReference().GetName()].Fields = append(
					results[property.GetReference().GetName()].Fields, path+property.GetName()+"/")
				if !slices.Contains(order, property.GetReference().GetName()) {
					order = append(order, property.GetReference().GetName())
				}
			}
		}
	}

	if _, ok := results[entity.GetName()]; !ok {
		results[entity.GetName()] = &EntityWithIDFields{
			Entity: entity,
			Fields: []string{},
			Many: true,
		}
	}
	results[entity.GetName()].Fields = append(
		results[entity.GetName()].Fields, path)
	if !slices.Contains(order, entity.GetName()) {
		order = append(order, entity.GetName())
	}


	for _, property := range entity.StoredProperties() {
		if property.CollectionType() == "OrderedMap" {

			new_path := path + property.GetName() + "/"
			order = recursiveOne2ManySearch(property.GetReference(), new_path, results, order)

		}
	}

	return order
}

// Definition is used to declare the information of a model, so it can generate its code.
type AggregateInterface interface {
	Definition() *AggregateDefinition
}

type EntityDefinition struct {
	Aggregate           AggregateInterface
	aggregateDefinition *AggregateDefinition
	Name                string
	Keys                []PropertyDefinition
	Properties          []PropertyDefinition
	UniqueProperty      string
	DisableID           bool
	DisableDatetime     bool
	Methods             []*CustomRequest
	// Queries             *EntityQueriesDefinition
	// Commands            *EntityCommandsDefinition
	UseOneOf            bool
	// useTenants bool
	DisableDatabaseStore bool
	SafeDelete           bool
	Abstract             bool
	NoCache              bool
	isAggregateRoot      bool
	uniqueProperty       PropertyDefinition
	hasTranslatable      bool
	domainPackage        string
	domainPath           string
	// Select               func(
	// 	WorkflowInterface, *baserepositorypb.ListQuery,
	// ) (*Collection, error)
}

func (t *EntityDefinition) AggregateDefinition() *AggregateDefinition {
	return t.aggregateDefinition
}
func (t *EntityDefinition) GetName() string {
	return t.Name
}
func (t *EntityDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *EntityDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *EntityDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *EntityDefinition) GetType() string {
	return "Entity"
}
func (t *EntityDefinition) IsEntity() bool {
	return true
}
func (t *EntityDefinition) UseTenants() bool {
	return t.AggregateDefinition().UseTenants
}
func (t *EntityDefinition) GetDisableID() bool {
	return t.DisableID
}
func (t *EntityDefinition) GetDisableDatetime() bool {
	return t.DisableDatetime
}
func (t *EntityDefinition) GetDisableDatabaseStore() bool {
	return t.DisableDatabaseStore
}
func (t *EntityDefinition) GetUseOneOf() bool {
	return t.UseOneOf
}
func (t *EntityDefinition) IsAggregateRoot() bool {
	return t.isAggregateRoot
}
func (t *EntityDefinition) HasTranslatable() bool {
	return t.hasTranslatable
}
func (t *EntityDefinition) UniquePropertyName() string {
	title := "id"
	if t.uniqueProperty != nil {
		title = t.uniqueProperty.GetName()
		if t.uniqueProperty.Type() == "IDType" {
			if t.uniqueProperty.GetReference().UniquePropertyName() != "id" {
				title = t.uniqueProperty.GetName() + "/" + t.uniqueProperty.GetReference().UniquePropertyName()
			}
		}
	}
	return title
}
func (t *EntityDefinition) UniquePropertyTitle() string {
	title := "Identity().ID"
	if t.uniqueProperty != nil {
		title = t.uniqueProperty.Title()
	}
	return title
}
func (t *EntityDefinition) UniquePropertyID() TableInterface {
	if t.uniqueProperty != nil {
		if t.uniqueProperty.Type() == "IDType" {
			if t.uniqueProperty.GetReference().UniquePropertyName() != "id" {
				return t.uniqueProperty.GetReference()
			}
		}
	}
	return nil
}
func (t *EntityDefinition) GetAbstract() bool {
	return t.Abstract
}
func (t *EntityDefinition) GetNoCache() bool {
	return t.NoCache
}

//	func (t *EntityDefinition) setUseTenants(useTenants bool) {
//		t.useTenants = useTenants
//	}
func (t *EntityDefinition) StoredProperties() []PropertyDefinition {
	var storedProperties []PropertyDefinition
	for _, property := range t.Keys {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *EntityDefinition) NestedProperties() []PropertyDefinition {
	var nestedProperties []PropertyDefinition
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *EntityDefinition) GetProperties() []PropertyDefinition {
	var storedProperties []PropertyDefinition
	for _, property := range t.StoredProperties() {
		storedProperties = append(storedProperties, property)
	}
	for _, property := range t.NestedProperties() {
		storedProperties = append(storedProperties, property)
	}
	return storedProperties
}
// func (t *EntityDefinition) GetCommands() *EntityCommandsDefinition {
// 	return t.Commands
// }
func (t *EntityDefinition) GetKeys() []PropertyDefinition {
	return t.Keys
}
func (t *EntityDefinition) DomainPackage() string {
	return t.domainPackage
}
func (t *EntityDefinition) DomainPath() string {
	return t.domainPath
}

func (t *EntityDefinition) HasUpdateInputArguments() bool {
	for _, property := range t.Properties {
		if string(property.CollectionType()) == "OrderedMap" {
			return true
		}
		if string(property.CollectionType()) == "Slice" {
			if string(property.Type()) == "EntityType" {
				return true
			}
		}
		if string(property.Type()) == "EntityType" {
			return true
		}
	}
	return false
}

type RequestConfig struct {
	// Groups *[]*baserepositorypb.GetByExternalIDQuery
}

type EntityQueriesDefinition struct {
	Get            *RequestConfig
	List           *RequestConfig
	GetFromEvents  *RequestConfig
	CustomCommands []*CustomRequest
}

type EntityCommandsDefinition struct {
	Create         *RequestConfig
	Update         *RequestConfig
	Delete         *RequestConfig
	CustomCommands []*CustomRequest
}

type CustomRequest struct {
	Name            string
	Event           string
	OnFactory       bool
	Repository      bool
	Args            []ArgumentDefinition
	Results         []ArgumentDefinition
	create          bool
	isCommand       bool
	tableDefinition TableInterface
}

func (f *CustomRequest) Title() string {
	return strings.Title(f.Name)
}
func (f *CustomRequest) IsCommand() bool {
	return f.isCommand
}
func (f *CustomRequest) Type() string {
	if f.create {
		return "create"
	}
	return "method"
}
func (f *CustomRequest) TableDefinition() TableInterface {
	return f.tableDefinition
}
func (f *CustomRequest) SetTableDefinition(t TableInterface) {
	f.tableDefinition = t
}
func (f *CustomRequest) SetCreate() {
	f.create = true
}

type ArgumentDefinition interface {
	GetName() string
	Title() string
	ProtoType() string
	GoType() string
	GoTypeBase() string
	GoNil() string
	GetRequired() bool
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() TableInterface
	SetReference(e TableInterface)
	Type() ArgumentType
	CollectionType() PropertyType
	IsRepeated() bool
	IsStored() bool
	IsTranslatable() bool
	GetTranslatable() TranslatableType
	GetPatch() bool
	GetInput() bool
	FromDomain() bool
	SetFromDomain(bool)
	Owner() OwnerInterface
	SetOwner(OwnerInterface)
	GetProperty() PropertyDefinition
	InputType() ObjectInputType
}

type ObjectInputType string

const RepeatedPropertyType = PropertyType("RepeatedPropertyType")

type RepeatedProperty struct {
	Property PropertyDefinition
}

func (r *RepeatedProperty) GetName() string {
	return r.Property.GetName()
}
func (r *RepeatedProperty) Title() string {
	return r.Property.Title()
}
func (r *RepeatedProperty) Upper() string {
	return r.Property.Upper()
}
func (r *RepeatedProperty) ProtoType() string {
	prototype := r.Property.ProtoType()
	if r.Property.Type() == "Many2oneType" {
		prototype = strcase.ToCamel(r.Property.GetReference().Title())
	}
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) ProtoTypeArg() string {
	prototype := r.Property.ProtoTypeArg()
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) GoType() string {
	return fmt.Sprintf("[]%s", r.Property.GoType())
}
func (r *RepeatedProperty) GoTypeID() string {
	return fmt.Sprintf("[]%s", r.Property.GoTypeID())
}
func (r *RepeatedProperty) GoTypeBase() string {
	return fmt.Sprintf("[]%s", r.Property.GoTypeBase())
}
func (r *RepeatedProperty) GoNil() string {
	return "nil"
}
func (r *RepeatedProperty) GetReferenceName() string {
	return r.Property.GetReferenceName()
}
func (r *RepeatedProperty) Type() PropertyType {
	return r.Property.Type()
}
func (r *RepeatedProperty) DBType() *DBType {
	return r.Property.DBType()
}
func (r *RepeatedProperty) DiffType(pack string) string {
	return r.Property.DiffType(pack)
}
func (r *RepeatedProperty) DiffTypeNew(pack string) string {
	return r.Property.DiffTypeNew(pack)
}
func (r *RepeatedProperty) GetInverseProperty() string {
	return ""
}
func (r *RepeatedProperty) GetPrimaryKey() bool {
	return false
}
func (r *RepeatedProperty) GetReference() TableInterface {
	return r.Property.GetReference()
}
func (r *RepeatedProperty) GetRequired() bool {
	return false
}
func (r *RepeatedProperty) GraphqlSchemaType() string {
	return "[" + r.Property.GraphqlSchemaType() + "!]"
}
func (r *RepeatedProperty) GraphqlType() string {
	return ""
}
func (r *RepeatedProperty) IsNested() bool {
	return false
}
func (r *RepeatedProperty) IsStored() bool {
	return true
}
func (r *RepeatedProperty) JSONType() string {
	return ""
}
func (r *RepeatedProperty) NameWithoutID() string {
	return r.Property.NameWithoutID()
}
func (r *RepeatedProperty) ProtoTypeOptional() string {
	return r.ProtoType()
}
func (r *RepeatedProperty) SetPosition(position int) {
	// r.Position = position
}
func (r *RepeatedProperty) GetPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) SetReference(e TableInterface) {
	r.Property.SetReference(e)
}
func (r *RepeatedProperty) Snake() string {
	return ""
}
func (r *RepeatedProperty) TitleWithoutID() string {
	return r.Property.TitleWithoutID()
}
func (r *RepeatedProperty) IsRepeated() bool {
	return true
}
func (r *RepeatedProperty) IsTranslatable() bool {
	return false
}
func (r *RepeatedProperty) GetTranslatable() TranslatableType {
	return ""
}
func (r *RepeatedProperty) GetData() bool {
	return false
}
func (r *RepeatedProperty) GetReturnDetailsInTests() bool {
	return r.Property.GetReturnDetailsInTests()
}
func (r *RepeatedProperty) LoadedPosition() int {
	return r.Property.GetLoadedPosition()
}
func (r *RepeatedProperty) Position() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) LoadedPositionMany2one() int {
	return r.Property.GetLoadedPositionEntity()
}
func (r *RepeatedProperty) PositionMany2one() int {
	return r.Property.GetPositionEntity()
}
func (r *RepeatedProperty) GetLoadedPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) GetLoadedPositionMany2one() int {
	return r.Property.GetLoadedPositionEntity()
}
func (r *RepeatedProperty) GetPositionMany2one() int {
	return r.Property.GetPositionEntity()
}
func (r *RepeatedProperty) CollectionType() PropertyType {
	return r.Property.CollectionType()
}
func (r *RepeatedProperty) FromDomain() bool {
	return r.Property.FromDomain()
}
func (r *RepeatedProperty) SetFromDomain(d bool) {
	r.Property.SetFromDomain(d)
}
func (r *RepeatedProperty) Owner() OwnerInterface {
	return nil
}
func (r *RepeatedProperty) SetOwner(d OwnerInterface) {
	// r.owner = d
}

// STRING is a contains reprensenting the widely used string type.
const STRING = "string"

// FieldType is the generic type for a data field.
type PropertyType string

type ArgumentType string

type IDType string

type DBType struct {
	Type  string
	Value string
}

// Field is an interface to get the data from the field.
type PropertyDefinition interface {
	GetName() string
	NameWithoutID() string
	Title() string
	TitleWithoutID() string
	Snake() string
	Upper() string
	Type() PropertyType
	GoType() string
	GoTypeWithPackage() string
	GoTypeID() string
	GoTypeIDWithPackage() string
	GoTypeBase() string
	GoNil() string
	JSONType() string
	ProtoType() string
	ProtoTypeArg() string
	ProtoTypeOptional() string
	DBType() *DBType
	DiffType(string) string
	DiffTypeNew(string) string
	DiffTypeInterface() string
	GraphqlType() string
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() TableInterface
	// GetReferenceDefinition() *ModelDefinition
	SetReference(TableInterface)
	GetInverseProperty() string
	InverseTitle() string
	GetRequired() bool
	GetUnique() bool
	GetPrimaryKey() bool
	// GetFieldData() *FieldData
	IsStored() bool
	IsNested() bool
	GetWithEntity() bool
	SetPosition(int)
	GetPosition() int
	IsRepeated() bool
	IsTranslatable() bool
	GetTranslatable() TranslatableType
	GetPatch() bool
	GetInput() bool
	GetReturnDetailsInTests() bool
	GetLoadedPosition() int
	GetLoadedPositionEntity() int
	GetPositionEntity() int
	CollectionType() PropertyType
	FromDomain() bool
	SetFromDomain(bool)
	Owner() OwnerInterface
	SetOwner(OwnerInterface)
	InputType() ObjectInputType
	GetOptions() interface{}
}

type TranslatableType string

type ValueObjectDefinition struct {
	Aggregate           AggregateInterface
	aggregateDefinition *AggregateDefinition
	Name                string
	Keys                []PropertyDefinition
	Properties          []PropertyDefinition
	Abstract            bool
	// aggregateIDProperty PropertyDefinition
	domainPackage       string
	domainPath          string
	// Select              func(
	// 	WorkflowInterface, *baserepositorypb.ListQuery,
	// ) (*Collection, error)
}

func (t *ValueObjectDefinition) AggregateDefinition() *AggregateDefinition {
	return t.aggregateDefinition
}
func (t *ValueObjectDefinition) GetName() string {
	return t.Name
}
func (t *ValueObjectDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *ValueObjectDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *ValueObjectDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *ValueObjectDefinition) GetType() string {
	return "ValueObject"
}
func (t *ValueObjectDefinition) IsEntity() bool {
	return false
}
func (t *ValueObjectDefinition) UseTenants() bool {
	return t.aggregateDefinition.UseTenants
}
func (t *ValueObjectDefinition) GetDisableID() bool {
	return true
}
func (t *ValueObjectDefinition) GetDisableDatetime() bool {
	return true
}
func (t *ValueObjectDefinition) GetDisableDatabaseStore() bool {
	return false
}
func (t *ValueObjectDefinition) GetUseOneOf() bool {
	return false
}
func (t *ValueObjectDefinition) StoredProperties() []PropertyDefinition {
	var storedProperties []PropertyDefinition
	for _, property := range t.GetKeys() {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *ValueObjectDefinition) NestedProperties() []PropertyDefinition {
	var nestedProperties []PropertyDefinition
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *ValueObjectDefinition) GetProperties() []PropertyDefinition {
	var storedProperties []PropertyDefinition
	for _, property := range t.StoredProperties() {
		storedProperties = append(storedProperties, property)
	}
	for _, property := range t.NestedProperties() {
		storedProperties = append(storedProperties, property)
	}
	return storedProperties
}
func (t *ValueObjectDefinition) GetCommands() *EntityCommandsDefinition {
	return &EntityCommandsDefinition{}
}

var SetAggregateIDProperty func(string) PropertyDefinition

func (t *ValueObjectDefinition) GetKeys() []PropertyDefinition {
	keys := []PropertyDefinition{
		// t.aggregateIDProperty,
	}
	for _, key := range t.Keys {
		keys = append(keys, key)
	}
	return keys
}
func (t *ValueObjectDefinition) IsAggregateRoot() bool {
	return false
}
func (t *ValueObjectDefinition) HasTranslatable() bool {
	return false
}
func (t *ValueObjectDefinition) GetAbstract() bool {
	return t.Abstract
}
func (t *ValueObjectDefinition) UniquePropertyName() string {
	return "id"
}
func (t *ValueObjectDefinition) UniquePropertyTitle() string {
	return ""
}
func (t *ValueObjectDefinition) DomainPackage() string {
	return t.domainPackage
}
func (t *ValueObjectDefinition) DomainPath() string {
	return t.domainPath
}
func (t *ValueObjectDefinition) GetNoCache() bool {
	return false
}

type TableInterface interface {
	GetName() string
	Title() string
	Snake() string
	Upper() string
	GetType() string
	IsEntity() bool
	UseTenants() bool
	GetDisableID() bool
	GetDisableDatetime() bool
	GetDisableDatabaseStore() bool
	GetUseOneOf() bool
	StoredProperties() []PropertyDefinition
	NestedProperties() []PropertyDefinition
	// GetCommands() *EntityCommandsDefinition
	GetKeys() []PropertyDefinition
	GetProperties() []PropertyDefinition
	AggregateDefinition() *AggregateDefinition
	IsAggregateRoot() bool
	HasTranslatable() bool
	GetAbstract() bool
	GetNoCache() bool
	UniquePropertyName() string
	UniquePropertyTitle() string
	DomainPackage() string
	DomainPath() string
}

type OnDeleteValue string

const OnDeleteCascade = OnDeleteValue("OnDeleteCascade")
const OnDeleteProtect = OnDeleteValue("OnDeleteProtect")
const OnDeleteSetNull = OnDeleteValue("OnDeleteSetNull")

// type JWTClaims struct {
// 	*jwt.StandardClaims
// 	TenantID  string
// 	UserID    string
// 	UserEmail string
// 	Groups    []*baserepositorypb.GetByExternalIDQuery
// }

type ExternalData struct {
	Aggregate AggregateInterface
	TenantID  string
	Module    string
	Data      []map[string]string
}

// var IDProperty = &baserepositorypb.BaseProperty{Snake: "id"}
// var TenantIDProperty = &baserepositorypb.BaseProperty{Snake: "tenant_id"}
// var ExternalModuleProperty = &baserepositorypb.BaseProperty{Snake: "external_module"}
// var ExternalIDProperty = &baserepositorypb.BaseProperty{Snake: "external_id"}

type RepositoryDefinition struct {
	Package             string
	Path                string
	Name                string
	AggregateName       string
	AggregateDefinition *AggregateDefinition
}

func (r *RepositoryDefinition) Title() string {
	return strings.Title(r.Name)
}

func (r *RepositoryDefinition) AggregateTitle() string {
	return strings.Title(r.AggregateName)
}

type EventDefinition struct {
	Name     string
	Versions []*EventVersionDefinition
}

func (r *EventDefinition) Title() string {
	return strings.Title(r.Name)
}

type EventVersionDefinition struct {
	Version    int
	Properties []ArgumentDefinition
	isLast     bool
}

func (r *EventVersionDefinition) IsLast() bool {
	return r.isLast
}

type EventDispatcher interface {
	DispatchEvent(ctx context.Context, aggregate string, payload []byte) error
	Subscribe(ctx context.Context, consumer string, stream string, subject string, function func([]byte) error) (interface{}, error)
	Publish(string, []byte) error
	NewInbox() string
	SubscribeSync(string) (EventDispatcherSubscription, error)
}

type EventDispatcherSubscription interface {
	NextMsg(time.Duration) ([]byte, error)
}

type Aggregate interface {
	Definition() *AggregateDefinition
	IdentityInterface() AggregateIdentity
	UnmutatedVersion() int
	MutatingEvents() []DomainEvent
	Mutated()
	Map() map[string]interface{}
}

type EnumDefinition struct {
	Package string
	Path    string
	Name    string
	Values  []string
}

func (t *EnumDefinition) AggregateDefinition() *AggregateDefinition {
	return nil
}
func (t *EnumDefinition) GetName() string {
	return t.Name
}
func (t *EnumDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *EnumDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *EnumDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *EnumDefinition) GetType() string {
	return "Enum"
}
func (t *EnumDefinition) IsEntity() bool {
	return false
}
func (t *EnumDefinition) UseTenants() bool {
	return false
}
func (t *EnumDefinition) GetDisableID() bool {
	return false
}
func (t *EnumDefinition) GetDisableDatetime() bool {
	return false
}
func (t *EnumDefinition) GetDisableDatabaseStore() bool {
	return false
}
func (t *EnumDefinition) GetUseOneOf() bool {
	return false
}
func (t *EnumDefinition) IsAggregateRoot() bool {
	return false
}
func (t *EnumDefinition) HasTranslatable() bool {
	return false
}
func (t *EnumDefinition) UniquePropertyName() string {
	return ""
}
func (t *EnumDefinition) UniquePropertyTitle() string {
	return ""
}
func (t *EnumDefinition) UniquePropertyID() TableInterface {
	return nil
}
func (t *EnumDefinition) GetAbstract() bool {
	return false
}
func (t *EnumDefinition) GetNoCache() bool {
	return false
}

//	func (t *EntityDefinition) setUseTenants(useTenants bool) {
//		t.useTenants = useTenants
//	}
func (t *EnumDefinition) StoredProperties() []PropertyDefinition {
	return []PropertyDefinition{}
}
func (t *EnumDefinition) NestedProperties() []PropertyDefinition {
	return []PropertyDefinition{}
}
func (t *EnumDefinition) GetProperties() []PropertyDefinition {
	return []PropertyDefinition{}
}
func (t *EnumDefinition) GetCommands() *EntityCommandsDefinition {
	return nil
}
func (t *EnumDefinition) GetKeys() []PropertyDefinition {
	return []PropertyDefinition{}
}
func (t *EnumDefinition) DomainPackage() string {
	return t.Package
}
func (t *EnumDefinition) DomainPath() string {
	return t.Path
}

type OwnerInterface interface {
	Title() string
}

type BooleanDiff struct {
	old bool
	new bool
}

func NewBooleanDiff(old, new bool) *BooleanDiff {
	return &BooleanDiff{
		old: old,
		new: new,
	}
}
func (t *BooleanDiff) Old() bool {
	return t.old
}
func (t *BooleanDiff) New() bool {
	return t.new
}

type TextDiff struct {
	old string
	new string
}

func NewTextDiff(old, new string) *TextDiff {
	return &TextDiff{
		old: old,
		new: new,
	}
}
func (t *TextDiff) Old() string {
	return t.old
}
func (t *TextDiff) New() string {
	return t.new
}

type TextPatchDiff struct {
	old    string
	patch  []diffmatchpatch.Patch
	pretty string
	new    string
}

func NewTextPatchDiff(old string, patch []diffmatchpatch.Patch, pretty string, new string) *TextPatchDiff {
	return &TextPatchDiff{
		old:    old,
		patch:  patch,
		pretty: pretty,
		new:    new,
	}
}
func (t *TextPatchDiff) Old() string {
	return t.old
}
func (t *TextPatchDiff) Patch() string {
	dmp := diffmatchpatch.New()
	return dmp.PatchToText(t.patch)
}
func (t *TextPatchDiff) Pretty() string {
	return t.pretty
}
func (t *TextPatchDiff) New() string {
	return t.new
}

type WYSIWYGDiffInside struct {
	translatedTerms map[string]map[string]*TextDiff
	assetsAdded     []*WYSIWYGAssetInput
	assetIdsRemoved []string
}

func NewWYSIWYGDiffInside(translatedTerms map[string]map[string]*TextDiff, assetsAdded []*WYSIWYGAssetInput, assetIdsRemoved []string) *WYSIWYGDiffInside {
	return &WYSIWYGDiffInside{
		translatedTerms: translatedTerms,
		assetsAdded:     assetsAdded,
		assetIdsRemoved: assetIdsRemoved,
	}
}
func (t *WYSIWYGDiffInside) TranslatedTerms() map[string]map[string]*TextDiff {
	return t.translatedTerms
}
func (t *WYSIWYGDiffInside) AssetsAdded() []*WYSIWYGAssetInput {
	return t.assetsAdded
}
func (t *WYSIWYGDiffInside) AssetIdsRemoved() []string {
	return t.assetIdsRemoved
}

type WYSIWYGDiff struct {
	old  *WYSIWYG
	diff *WYSIWYGDiffInside
	new  *WYSIWYG
}

func NewWYSIWYGDiff(old *WYSIWYG, diff *WYSIWYGDiffInside, new *WYSIWYG) *WYSIWYGDiff {
	return &WYSIWYGDiff{
		old:  old,
		diff: diff,
		new:  new,
	}
}
func (t *WYSIWYGDiff) Old() *WYSIWYG {
	return t.old
}
func (t *WYSIWYGDiff) Diff() *WYSIWYGDiffInside {
	return t.diff
}
func (t *WYSIWYGDiff) New() *WYSIWYG {
	return t.new
}

type IntegerDiff struct {
	old int
	new int
}

func NewIntegerDiff(old, new int) *IntegerDiff {
	return &IntegerDiff{
		old: old,
		new: new,
	}
}
func (t *IntegerDiff) Old() int {
	return t.old
}
func (t *IntegerDiff) New() int {
	return t.new
}

type FloatDiff struct {
	old float64
	new float64
}

func NewFloatDiff(old, new float64) *FloatDiff {
	return &FloatDiff{
		old: old,
		new: new,
	}
}
func (t *FloatDiff) Old() float64 {
	return t.old
}
func (t *FloatDiff) New() float64 {
	return t.new
}

type TimeDiff struct {
	old time.Time
	new time.Time
}

func NewTimeDiff(old, new time.Time) *TimeDiff {
	return &TimeDiff{
		old: old,
		new: new,
	}
}
func (t *TimeDiff) Old() time.Time {
	return t.old
}
func (t *TimeDiff) New() time.Time {
	return t.new
}

type SliceDiff struct {
	old []string
	new []string
}

func NewSliceDiff(old, new []string) *SliceDiff {
	return &SliceDiff{
		old: old,
		new: new,
	}
}
func (t *SliceDiff) Old() []string {
	return t.old
}
func (t *SliceDiff) New() []string {
	return t.new
}

type PackageLocation struct {
	Package string
	Path    string
}

type EventHistory struct {
	ID           string
	Version      int
	Event        string
	EventVersion int
	Diff         string
	Payload      string
	OccurredAt   time.Time
	OccurredBy   string
}

type WYSIWYGAssetInput struct {
	ID          string `json:"id"`
	Filename    string `json:"filename"`
	ContentType string `json:"contentType"`
	Content     []byte `json:"content"`
	Size        int    `json:"size"`
	URL         string `json:"url"`
}

type WYSIWYGInput struct {
	TranslatedTerms  map[string]map[string]string `json:"translatedTerms"`
	AssetsToAdd      []*WYSIWYGAssetInput         `json:"assetsToAdd"`
	AssetIdsToRemove []string                     `json:"assetIdsToRemove"`
}

type WYSIWYGAsset struct {
	id          string
	filename    string
	contentType string
	size        int
	url         string
}

func NewWYSIWYGAsset(id string, filename string, contentType string, size int, url string) *WYSIWYGAsset {
	return &WYSIWYGAsset{
		id:          id,
		filename:    filename,
		contentType: contentType,
		size:        size,
		url:         url,
	}
}
func (t *WYSIWYGAsset) ID() string {
	return t.id
}
func (t *WYSIWYGAsset) Filename() string {
	return t.filename
}
func (t *WYSIWYGAsset) ContentType() string {
	return t.contentType
}
func (t *WYSIWYGAsset) Size() int {
	return t.size
}
func (t *WYSIWYGAsset) URL() string {
	return t.url
}

type WYSIWYG struct {
	translatedTerms       map[string]map[string]string
	translatedTermsLoaded bool
	assets                []*WYSIWYGAsset
	assetsLoaded          bool
}

func NewWYSIWYG(translatedTerms map[string]map[string]string, translatedTermsLoaded bool, assets []*WYSIWYGAsset, assetsLoaded bool) *WYSIWYG {
	return &WYSIWYG{
		translatedTerms:       translatedTerms,
		translatedTermsLoaded: translatedTermsLoaded,
		assets:                assets,
		assetsLoaded:          assetsLoaded,
	}
}

func (t *WYSIWYG) TranslatedTerms() map[string]map[string]string {
	if !t.translatedTermsLoaded {
		panic("TranslatedTerms not loaded")
	}
	return t.translatedTerms
}
func (t *WYSIWYG) TranslatedTermsLoaded() bool {
	return t.translatedTermsLoaded
}
func (t *WYSIWYG) Assets() []*WYSIWYGAsset {
	if !t.assetsLoaded {
		panic("Assets not loaded")
	}
	return t.assets
}
func (t *WYSIWYG) AssetsLoaded() bool {
	return t.assetsLoaded
}
func (t *WYSIWYG) AllPropertiesLoaded() {
	t.translatedTermsLoaded = true
	t.assetsLoaded = true
}

type GetWYSIWYGProperties struct {
	TranslatedTerms bool
	Assets          bool
}

func PatchFromText(data string) []diffmatchpatch.Patch {
	dmp := diffmatchpatch.New()
	return dmp.PatchMake("", dmp.DiffMain("", data, false))
}

type File struct {
	Filename	 string
	Content []byte
	ContentType string
	Size int
}

type AccessGroup struct {
	code string
	name string
}

func NewAccessGroup(code string, name string) *AccessGroup {
	return &AccessGroup{
		code: code,
		name: name,
	}
}
func (t *AccessGroup) Code() string {
	return t.code
}
func (t *AccessGroup) Name() string {
	return t.name
}
