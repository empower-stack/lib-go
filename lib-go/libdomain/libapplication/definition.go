package libapplication

import (
	"bytes"
	"fmt"
	"html/template"
	"strings"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// type representationAssignation struct {
// 	Definition *RepresentationDefinition
// 	Properties []libdomain.ArgsInterface
// }

type Definitions struct {
	Package    string
	Repository string
	// UseTenants bool
	slice []*ServiceDefinition
	byIds map[string]*ServiceDefinition

	representationsByName map[string]*RepresentationDefinition
}

func (ds *Definitions) RepositoryGen() string {
	return ds.Repository + "/gen"
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(a *ServiceDefinition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*ServiceDefinition{}
	}
	if ds.representationsByName == nil {
		ds.representationsByName = map[string]*RepresentationDefinition{}
	}

	// a.AggregateRoot.isAggregateRoot = true

	// EntitiesInAggregate := a.GetEntities()
	// a.entitiesByName = map[string]*EntityDefinition{}
	// a.valueObjectsByName = map[string]*ValueObjectDefinition{}
	// a.commandsByName = map[string]*CustomRequest{}

	a.applicationPackage = ds.Package
	a.applicationPath = ds.RepositoryGen()

	for _, e := range a.Commands {
		e.isCommand = true
	}

	for _, e := range a.Representations {

		// 	e.aggregateDefinition = a
		// 	a.entitiesByName[e.Name] = e

		e.domainPackage = ds.Package + "data"
		e.domainPath = ds.RepositoryGen() + "/data"

		// if _, ok := ds.representationsAssignation[e.Name]; !ok {
		// 	ds.representationsAssignation[e.Name] = &representationAssignation{}
		// }
		ds.representationsByName[e.Name] = e
		// for _, property := range ds.representationsAssignation[e.Name].Properties {
		// 	property.SetReference(e)
		// }
	}

	for _, f := range a.Representations {
		for _, p := range f.Properties {
			p.SetFromDomain(true)
			p.SetOwner(f)
			// 	if p.GetReferenceName() != "" && p.GetReference() == nil {
			// 		if _, ok := ds.representationsAssignation[p.GetReferenceName()]; !ok {
			// 			ds.representationsAssignation[p.GetReferenceName()] = &representationAssignation{}
			// 		}
			// 		ds.representationsAssignation[p.GetReferenceName()].Properties = append(ds.representationsAssignation[p.GetReferenceName()].Properties, p)
			// 		if ds.representationsAssignation[p.GetReferenceName()].Definition != nil {
			// 			p.SetReference(ds.representationsAssignation[p.GetReferenceName()].Definition)
			// 		}
			// 	}
		}
	}

	for _, q := range a.Queries {
		q.SetFromDomain(true)
		for _, p := range q.Args {
			p.SetFromDomain(true)
			p.SetOwner(q)
		}
		for _, p := range q.Results {
			p.SetFromDomain(true)
			p.SetOwner(q)
		}
		// for _, p := range q.Args {
		// 	p.SetFromDomain(true)
		// 	if p.GetReferenceName() != "" && p.GetReference() == nil {
		// 		if _, ok := ds.representationsAssignation[p.GetReferenceName()]; !ok {
		// 			ds.representationsAssignation[p.GetReferenceName()] = &representationAssignation{}
		// 		}
		// 		ds.representationsAssignation[p.GetReferenceName()].Properties = append(ds.representationsAssignation[p.GetReferenceName()].Properties, p)
		// 		if ds.representationsAssignation[p.GetReferenceName()].Definition != nil {
		// 			p.SetReference(ds.representationsAssignation[p.GetReferenceName()].Definition)
		// 		}
		// 	}
		// }
		// for _, p := range q.Results {
		// 	p.SetFromDomain(true)
		// 	if p.GetReferenceName() != "" && p.GetReference() == nil {
		// 		if _, ok := ds.representationsAssignation[p.GetReferenceName()]; !ok {
		// 			ds.representationsAssignation[p.GetReferenceName()] = &representationAssignation{}
		// 		}
		// 		ds.representationsAssignation[p.GetReferenceName()].Properties = append(ds.representationsAssignation[p.GetReferenceName()].Properties, p)
		// 		if ds.representationsAssignation[p.GetReferenceName()].Definition != nil {
		// 			p.SetReference(ds.representationsAssignation[p.GetReferenceName()].Definition)
		// 		}
		// 	}
		// }
	}

	for _, q := range a.Commands {
		q.SetFromDomain(true)
		for _, p := range q.Args {
			p.SetFromDomain(true)
			p.SetOwner(q)
		}
		for _, p := range q.Results {
			p.SetFromDomain(true)
			p.SetOwner(q)
		}
		// for _, p := range q.Args {
		// 	p.SetFromDomain(true)
		// 	if p.GetReferenceName() != "" && p.GetReference() == nil {
		// 		if _, ok := ds.representationsAssignation[p.GetReferenceName()]; !ok {
		// 			ds.representationsAssignation[p.GetReferenceName()] = &representationAssignation{}
		// 		}
		// 		ds.representationsAssignation[p.GetReferenceName()].Properties = append(ds.representationsAssignation[p.GetReferenceName()].Properties, p)
		// 		if ds.representationsAssignation[p.GetReferenceName()].Definition != nil {
		// 			p.SetReference(ds.representationsAssignation[p.GetReferenceName()].Definition)
		// 		}
		// 	}
		// }
		// for _, p := range q.Results {
		// 	p.SetFromDomain(true)
		// 	if p.GetReferenceName() != "" && p.GetReference() == nil {
		// 		if _, ok := ds.representationsAssignation[p.GetReferenceName()]; !ok {
		// 			ds.representationsAssignation[p.GetReferenceName()] = &representationAssignation{}
		// 		}
		// 		ds.representationsAssignation[p.GetReferenceName()].Properties = append(ds.representationsAssignation[p.GetReferenceName()].Properties, p)
		// 		if ds.representationsAssignation[p.GetReferenceName()].Definition != nil {
		// 			p.SetReference(ds.representationsAssignation[p.GetReferenceName()].Definition)
		// 		}
		// 	}
		// }
	}

	// 	if e.Queries == nil {
	// 		e.Queries = &EntityQueriesDefinition{}
	// 	}
	// 	if e.Commands == nil {
	// 		e.Commands = &EntityCommandsDefinition{}
	// 	}

	// 	for _, f := range e.Keys {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 		if f.IsTranslatable() {
	// 			e.hasTranslatable = true
	// 		}
	// 	}
	// 	for _, f := range e.Properties {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 		if f.IsTranslatable() {
	// 			e.hasTranslatable = true
	// 		}
	// 	}

	// 	// e.setUseTenants(e.AggregateDefinition.UseTenants)
	// }

	// for _, v := range a.ValueObjects {
	// 	v.aggregateDefinition = a
	// 	v.aggregateIDProperty = SetAggregateIDProperty(a.AggregateRoot.Name)
	// 	a.valueObjectsByName[v.Name] = v

	// 	v.domainPath = ds.Repository

	// 	for _, f := range v.GetKeys() {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 	}
	// 	for _, f := range v.Properties {
	// 		for _, old := range EntitiesInAggregate {
	// 			if f.GetReferenceName() == old.Name {
	// 				f.SetReference(old)
	// 			}
	// 		}
	// 		for _, old := range ds.Slice() {
	// 			if f.GetReferenceName() == old.AggregateRoot.Name {
	// 				f.SetReference(old.AggregateRoot)
	// 			}
	// 		}
	// 	}
	// }

	// for _, c := range a.Queries {
	// 	a.commandsByName[c.Name] = c
	// }
	// for _, c := range a.Commands {
	// 	c.isCommand = true
	// 	a.commandsByName[c.Name] = c
	// }

	ds.slice = append(ds.slice, a)
	ds.byIds[a.Name] = a

	// for _, old := range ds.Slice() {
	// 	for _, f := range old.Representations {
	// 		for _, p := range f.Properties {

	// 			for _, r := range a.Representations {
	// 				if p.GetReferenceName() == r.Name {
	// 					p.SetReference(r)
	// 				}

	// 				for _, pr := range r.Properties {
	// 					if pr.GetReferenceName() == f.Name {
	// 						pr.SetReference(f)
	// 					}
	// 				}
	// 			}
	// 		}

	// 	}

	// 	for _, q := range old.Queries {
	// 		for _, p := range q.Args {
	// 			for _, r := range a.Representations {
	// 				if p.GetReferenceName() == r.Name {
	// 					p.SetReference(r)
	// 				}
	// 			}
	// 		}
	// 		for _, p := range q.Results {
	// 			for _, r := range a.Representations {
	// 				if p.GetReferenceName() == r.Name {
	// 					p.SetReference(r)
	// 				}
	// 			}
	// 		}
	// 	}

	// 	for _, c := range old.Commands {
	// 		for _, p := range c.Results {
	// 			for _, r := range a.Representations {
	// 				if p.GetReferenceName() == r.Name {
	// 					p.SetReference(r)
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	// for _, old := range ds.Slice() {
	// 	// fmt.Println("old ", old.AggregateRoot.Name)
	// 	for _, e := range old.GetEntities() {
	// 		for _, f := range e.Keys {
	// 			if f.GetReferenceName() == a.AggregateRoot.Name {
	// 				f.SetReference(a.AggregateRoot)
	// 			}
	// 		}
	// 		for _, f := range e.Properties {
	// 			if f.GetReferenceName() == a.AggregateRoot.Name {
	// 				f.SetReference(a.AggregateRoot)
	// 			}
	// 		}
	// 		for _, f := range e.Keys {
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 		for _, f := range e.Properties {
	// 			// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	for _, v := range old.ValueObjects {
	// 		// fmt.Println("\nvalueObject ", v.Name)
	// 		for _, f := range v.GetKeys() {
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 		for _, f := range v.Properties {
	// 			// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
	// 			for _, compare := range ds.Slice() {
	// 				for _, compareEntity := range compare.GetEntities() {
	// 					if f.GetReferenceName() == compareEntity.Name {
	// 						f.SetReference(compareEntity)
	// 					}
	// 				}
	// 				for _, compareValueObject := range compare.ValueObjects {
	// 					if f.GetReferenceName() == compareValueObject.Name {
	// 						f.SetReference(compareValueObject)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}

	// 	for _, c := range old.GetCustomRequests() {
	// 		for _, a = range ds.slice {
	// 			for _, e := range a.GetEntities() {
	// 				for _, f := range c.Args {
	// 					// fmt.Printf("%s %s\n", f.GetReferenceName(), e.GetName())
	// 					if f.GetReferenceName() == e.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(e)
	// 					}
	// 				}
	// 				for _, f := range c.Results {
	// 					// fmt.Printf("%s %s\n", f.GetReferenceName(), e.GetName())
	// 					if f.GetReferenceName() == e.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(e)
	// 					}
	// 				}
	// 			}
	// 			for _, v := range a.ValueObjects {
	// 				for _, f := range c.Args {
	// 					// fmt.Printf("%s %s\n", f.GetReferenceName(), v.GetName())
	// 					if f.GetReferenceName() == v.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(v)
	// 					}
	// 				}
	// 				for _, f := range c.Results {
	// 					// fmt.Printf("%s %s %s\n", f.GetName(), f.GetReferenceName(), v.GetName())
	// 					if f.GetReferenceName() == v.GetName() {
	// 						// fmt.Println("ok")
	// 						f.SetReference(v)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	for _, s := range ds.slice {
		for _, f := range s.Representations {
			for _, p := range f.Properties {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					if _, ok := ds.representationsByName[p.GetReferenceName()]; ok {
						p.SetReference(ds.representationsByName[p.GetReferenceName()])
					}
				}
			}
		}

		for _, q := range s.Queries {
			q.SetFromDomain(true)
			for _, p := range q.Args {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					if _, ok := ds.representationsByName[p.GetReferenceName()]; ok {
						p.SetReference(ds.representationsByName[p.GetReferenceName()])
					}
				}
			}
			for _, p := range q.Results {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					if _, ok := ds.representationsByName[p.GetReferenceName()]; ok {
						p.SetReference(ds.representationsByName[p.GetReferenceName()])
					}
				}
			}
		}

		for _, q := range s.Commands {
			q.SetFromDomain(true)
			for _, p := range q.Args {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					if _, ok := ds.representationsByName[p.GetReferenceName()]; ok {
						p.SetReference(ds.representationsByName[p.GetReferenceName()])
					}
				}
			}
			for _, p := range q.Results {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					if _, ok := ds.representationsByName[p.GetReferenceName()]; ok {
						p.SetReference(ds.representationsByName[p.GetReferenceName()])
					}
				}
			}
		}
	}

}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*ServiceDefinition {
	result := ds.slice

	return result
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *ServiceDefinition {
	d := ds.byIds[id]

	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

func (ds *Definitions) ControlReferences() {
	for _, s := range ds.slice {
		for _, f := range s.Representations {
			for _, p := range f.Properties {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", p.GetName(), f.Name, p.GetReferenceName()))
				}
			}
		}

		for _, q := range s.Queries {
			q.SetFromDomain(true)
			for _, p := range q.Args {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", p.GetName(), q.Name, p.GetReferenceName()))
				}
			}
			for _, p := range q.Results {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", p.GetName(), q.Name, p.GetReferenceName()))
				}
			}
		}

		for _, q := range s.Commands {
			q.SetFromDomain(true)
			for _, p := range q.Args {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", p.GetName(), q.Name, p.GetReferenceName()))
				}
			}
			for _, p := range q.Results {
				if p.GetReferenceName() != "" && p.GetReference() == nil {
					panic(fmt.Sprintf("The property %s in %s can't find his reference %s", p.GetName(), q.Name, p.GetReferenceName()))
				}
			}
		}
	}
}

// Definition is used to declare the information of a model, so it can generate its code.
type ServiceDefinition struct {
	Name string
	// AggregateRoot      *EntityDefinition
	// Children           []*EntityDefinition
	// UseTenants         bool
	// entitiesByName     map[string]*EntityDefinition
	// ValueObjects       []*ValueObjectDefinition
	// valueObjectsByName map[string]*ValueObjectDefinition
	ReferenceDefinition *ServiceDefinition
	Queries             []*CustomRequest
	Commands            []*CustomRequest
	Representations     []*RepresentationDefinition
	Services            []*ServiceDefinition
	Repositories        []*libdomain.RepositoryDefinition
	// commandsByName     map[string]*CustomRequest
	// ExternalData       []*ExternalData
	applicationPackage string
	applicationPath    string
}

func (a *ServiceDefinition) Title() string {
	return strings.Title(a.Name)
}
func (a *ServiceDefinition) TitleWithPackage() string {
	return a.applicationPackage + "." + a.Title()
}
func (a *ServiceDefinition) Snake() string {
	return strcase.ToSnake(a.Name)
}
func (a *ServiceDefinition) ApplicationPackage() string {
	return a.applicationPackage
}
func (a *ServiceDefinition) ApplicationPath() string {
	return a.applicationPath
}

//	func (a *AggregateDefinition) GetEntities() []*EntityDefinition {
//		EntitiesInAggregate := []*EntityDefinition{
//			a.AggregateRoot,
//		}
//		for _, c := range a.Children {
//			EntitiesInAggregate = append(EntitiesInAggregate, c)
//		}
//		return EntitiesInAggregate
//	}
//
//	func (a *AggregateDefinition) GetEntityByName(name string) *EntityDefinition {
//		return a.entitiesByName[name]
//	}
//
//	func (a *AggregateDefinition) GetValueObjectByName(name string) *ValueObjectDefinition {
//		return a.valueObjectsByName[name]
//	}
//
//	func (a *AggregateDefinition) GetCommandByName(name string) *CustomRequest {
//		return a.commandsByName[name]
//	}
//
//	func (a *AggregateDefinition) GetTables() []TableInterface {
//		var tables []TableInterface
//		for _, c := range a.GetEntities() {
//			tables = append(tables, c)
//		}
//		for _, c := range a.ValueObjects {
//			tables = append(tables, c)
//		}
//		return tables
//	}
func (a *ServiceDefinition) GetCustomRequests() []*CustomRequest {
	var requests []*CustomRequest
	for _, c := range a.Queries {
		requests = append(requests, c)
	}
	for _, c := range a.Commands {
		requests = append(requests, c)
	}
	return requests
}

// Definition is used to declare the information of a model, so it can generate its code.
// type AggregateInterface interface {
// 	Definition() *AggregateDefinition
// }

// type EntityDefinition struct {
// 	Aggregate           AggregateInterface
// 	aggregateDefinition *AggregateDefinition
// 	Name                string
// 	Keys                []Property
// 	Properties          []Property
// 	DisableID           bool
// 	DisableDatetime     bool
// 	Methods             []*CustomRequest
// 	Queries             *EntityQueriesDefinition
// 	Commands            *EntityCommandsDefinition
// 	UseOneOf            bool
// 	// useTenants bool
// 	DisableDatabaseStore bool
// 	SafeDelete           bool
// 	Abstract             bool
// 	NoCache              bool
// 	isAggregateRoot      bool
// 	hasTranslatable      bool
// 	domainPath           string
// 	// Select               func(
// 	// 	WorkflowInterface, *baserepositorypb.ListQuery,
// 	// ) (*Collection, error)
// }

// func (t *EntityDefinition) AggregateDefinition() *AggregateDefinition {
// 	return t.aggregateDefinition
// }
// func (t *EntityDefinition) GetName() string {
// 	return t.Name
// }
// func (t *EntityDefinition) Title() string {
// 	return strings.Title(t.Name)
// }
// func (t *EntityDefinition) Snake() string {
// 	return strcase.ToSnake(t.Name)
// }
// func (f *EntityDefinition) Upper() string {
// 	return strings.ToUpper(f.Snake())
// }
// func (t *EntityDefinition) GetType() string {
// 	return "Entity"
// }
// func (t *EntityDefinition) IsEntity() bool {
// 	return true
// }
// func (t *EntityDefinition) UseTenants() bool {
// 	return t.AggregateDefinition().UseTenants
// }
// func (t *EntityDefinition) GetDisableID() bool {
// 	return t.DisableID
// }
// func (t *EntityDefinition) GetDisableDatetime() bool {
// 	return t.DisableDatetime
// }
// func (t *EntityDefinition) GetDisableDatabaseStore() bool {
// 	return t.DisableDatabaseStore
// }
// func (t *EntityDefinition) GetUseOneOf() bool {
// 	return t.UseOneOf
// }
// func (t *EntityDefinition) IsAggregateRoot() bool {
// 	return t.isAggregateRoot
// }
// func (t *EntityDefinition) HasTranslatable() bool {
// 	return t.hasTranslatable
// }
// func (t *EntityDefinition) GetAbstract() bool {
// 	return t.Abstract
// }
// func (t *EntityDefinition) GetNoCache() bool {
// 	return t.NoCache
// }

// //	func (t *EntityDefinition) setUseTenants(useTenants bool) {
// //		t.useTenants = useTenants
// //	}
// func (t *EntityDefinition) StoredProperties() []Property {
// 	var storedProperties []Property
// 	for _, property := range t.Keys {
// 		if property.IsStored() {
// 			storedProperties = append(storedProperties, property)
// 		}
// 	}
// 	for _, property := range t.Properties {
// 		if property.IsStored() {
// 			storedProperties = append(storedProperties, property)
// 		}
// 	}
// 	return storedProperties
// }
// func (t *EntityDefinition) NestedProperties() []Property {
// 	var nestedProperties []Property
// 	for _, property := range t.Properties {
// 		if !property.IsStored() {
// 			// if string(property.Type()) == "One2manyType" {
// 			nestedProperties = append(nestedProperties, property)
// 		}
// 	}
// 	return nestedProperties
// }
// func (t *EntityDefinition) GetCommands() *EntityCommandsDefinition {
// 	return t.Commands
// }
// func (t *EntityDefinition) GetKeys() []Property {
// 	return t.Keys
// }
// func (t *EntityDefinition) DomainPath() string {
// 	return t.domainPath
// }

// type RequestConfig struct {
// 	// Groups *[]*baserepositorypb.GetByExternalIDQuery
// }

// type EntityQueriesDefinition struct {
// 	Get            *RequestConfig
// 	List           *RequestConfig
// 	GetFromEvents  *RequestConfig
// 	CustomCommands []*CustomRequest
// }

// type EntityCommandsDefinition struct {
// 	Create         *RequestConfig
// 	Update         *RequestConfig
// 	Delete         *RequestConfig
// 	CustomCommands []*CustomRequest
// }

type RepresentationDefinition struct {
	Name          string
	Properties    []libdomain.PropertyDefinition
	domainPackage string
	domainPath    string
}

func (t *RepresentationDefinition) AggregateDefinition() *libdomain.AggregateDefinition {
	return nil
}
func (t *RepresentationDefinition) GetName() string {
	return t.Name
}
func (t *RepresentationDefinition) Title() string {
	return strings.Title(t.Name)
}
func (t *RepresentationDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}
func (f *RepresentationDefinition) Upper() string {
	return strings.ToUpper(f.Snake())
}
func (t *RepresentationDefinition) GetType() string {
	return "Representation"
}
func (t *RepresentationDefinition) IsEntity() bool {
	return false
}
func (t *RepresentationDefinition) UseTenants() bool {
	return true
}
func (t *RepresentationDefinition) GetDisableID() bool {
	return true
}
func (t *RepresentationDefinition) GetDisableDatetime() bool {
	return true
}
func (t *RepresentationDefinition) GetDisableDatabaseStore() bool {
	return false
}
func (t *RepresentationDefinition) GetUseOneOf() bool {
	return false
}
func (t *RepresentationDefinition) StoredProperties() []libdomain.PropertyDefinition {
	var storedProperties []libdomain.PropertyDefinition
	for _, property := range t.GetKeys() {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	for _, property := range t.Properties {
		if property.IsStored() {
			storedProperties = append(storedProperties, property)
		}
	}
	return storedProperties
}
func (t *RepresentationDefinition) NestedProperties() []libdomain.PropertyDefinition {
	var nestedProperties []libdomain.PropertyDefinition
	for _, property := range t.Properties {
		if !property.IsStored() {
			// if string(property.Type()) == "One2manyType" {
			nestedProperties = append(nestedProperties, property)
		}
	}
	return nestedProperties
}
func (t *RepresentationDefinition) GetProperties() []libdomain.PropertyDefinition {
	var storedProperties []libdomain.PropertyDefinition
	for _, property := range t.StoredProperties() {
		storedProperties = append(storedProperties, property)
	}
	for _, property := range t.NestedProperties() {
		storedProperties = append(storedProperties, property)
	}
	return storedProperties
}
func (t *RepresentationDefinition) GetCommands() *libdomain.EntityCommandsDefinition {
	return &libdomain.EntityCommandsDefinition{}
}

var SetAggregateIDProperty func(string) libdomain.PropertyDefinition

func (t *RepresentationDefinition) GetKeys() []libdomain.PropertyDefinition {
	keys := []libdomain.PropertyDefinition{
		// t.aggregateIDProperty,
	}
	// for _, key := range t.Keys {
	// 	keys = append(keys, key)
	// }
	return keys
}
func (t *RepresentationDefinition) IsAggregateRoot() bool {
	return false
}
func (t *RepresentationDefinition) HasTranslatable() bool {
	return false
}
func (t *RepresentationDefinition) GetAbstract() bool {
	return false
}
func (t *RepresentationDefinition) UniquePropertyName() string {
	return "id"
}
func (t *RepresentationDefinition) UniquePropertyTitle() string {
	return ""
}
func (t *RepresentationDefinition) DomainPackage() string {
	return t.domainPackage
}
func (t *RepresentationDefinition) DomainPath() string {
	return t.domainPath
}
func (t *RepresentationDefinition) GetNoCache() bool {
	return false
}

type CustomRequest struct {
	Name          string
	Event         string
	OnFactory     bool
	Repository    bool
	Args          []libdomain.ArgumentDefinition
	Results       []libdomain.ArgumentDefinition
	TenantRequest bool
	// ResultRepresentation string
	// ResultReference      *libdomain.EntityDefinition
	// ResultIsSlice        bool
	isCommand  bool
	fromDomain bool
	Generator  interface {
		Generate() string
		Imports() []*libdomain.PackageLocation
	}
}

func (f *CustomRequest) GetName() string {
	return f.Name
}
func (f *CustomRequest) Title() string {
	return strings.Title(f.Name)
}
func (f *CustomRequest) IsCommand() bool {
	return f.isCommand
}

//	func (f *CustomRequest) ResultTitle() string {
//		return strings.Title(f.ResultRepresentation)
//	}
func (f *CustomRequest) UseTenants() bool {
	return f.TenantRequest
}
func (f *CustomRequest) FromDomain() bool {
	return f.fromDomain
}
func (f *CustomRequest) SetFromDomain(fromDomain bool) {
	f.fromDomain = fromDomain
}

// type ArgsInterface interface {
// 	GetName() string
// 	Title() string
// 	ProtoType() string
// 	GoType() string
// 	GoNil() string
// 	GraphqlSchemaType() string
// 	GetReferenceName() string
// 	GetReference() TableInterface
// 	SetReference(e TableInterface)
// 	Type() PropertyType
// 	IsRepeated() bool
// }

const RepeatedPropertyType = libdomain.PropertyType("RepeatedPropertyType")

type RepeatedProperty struct {
	Property libdomain.PropertyDefinition
}

func (r *RepeatedProperty) GetName() string {
	return r.Property.GetName()
}
func (r *RepeatedProperty) Title() string {
	return r.Property.Title()
}
func (r *RepeatedProperty) Upper() string {
	return r.Property.Upper()
}

//	func (r *RepeatedProperty) ProtoType() string {
//		prototype := r.Property.ProtoType()
//		if r.Property.Type() == "Many2oneType" {
//			prototype = strcase.ToCamel(r.Property.GetReference().Title())
//		}
//		return fmt.Sprintf("repeated %s", prototype)
//	}
func (r *RepeatedProperty) ProtoTypeArg() string {
	prototype := r.Property.ProtoTypeArg()
	return fmt.Sprintf("repeated %s", prototype)
}
func (r *RepeatedProperty) GoType() string {
	return fmt.Sprintf("[]%s", r.Property.GoType())
}
func (r *RepeatedProperty) GoTypeID() string {
	return fmt.Sprintf("[]%s", r.Property.GoTypeID())
}
func (r *RepeatedProperty) GoNil() string {
	return "nil"
}
func (r *RepeatedProperty) GetReferenceName() string {
	return r.Property.GetReferenceName()
}
func (r *RepeatedProperty) Type() libdomain.PropertyType {
	return r.Property.Type()
}
func (r *RepeatedProperty) DBType() *libdomain.DBType {
	return r.Property.DBType()
}
func (r *RepeatedProperty) GetInverseProperty() string {
	return ""
}
func (r *RepeatedProperty) GetPrimaryKey() bool {
	return false
}

//	func (r *RepeatedProperty) GetReference() TableInterface {
//		return r.Property.GetReference()
//	}
func (r *RepeatedProperty) GetRequired() bool {
	return false
}
func (r *RepeatedProperty) GraphqlSchemaType() string {
	return "[" + r.Property.GraphqlSchemaType() + "!]"
}
func (r *RepeatedProperty) GraphqlType() string {
	return ""
}
func (r *RepeatedProperty) IsNested() bool {
	return false
}
func (r *RepeatedProperty) IsStored() bool {
	return true
}
func (r *RepeatedProperty) JSONType() string {
	return ""
}
func (r *RepeatedProperty) NameWithoutID() string {
	return r.Property.NameWithoutID()
}

//	func (r *RepeatedProperty) ProtoTypeOptional() string {
//		return r.ProtoType()
//	}
func (r *RepeatedProperty) SetPosition(position int) {
	// r.Position = position
}
func (r *RepeatedProperty) GetPosition() int {
	return r.Property.GetPosition()
}

//	func (r *RepeatedProperty) SetReference(e TableInterface) {
//		r.Property.SetReference(e)
//	}
func (r *RepeatedProperty) Snake() string {
	return ""
}
func (r *RepeatedProperty) TitleWithoutID() string {
	return r.Property.TitleWithoutID()
}
func (r *RepeatedProperty) IsRepeated() bool {
	return true
}
func (r *RepeatedProperty) IsTranslatable() bool {
	return false
}
func (r *RepeatedProperty) GetReturnDetailsInTests() bool {
	return r.Property.GetReturnDetailsInTests()
}
func (r *RepeatedProperty) LoadedPosition() int {
	return r.Property.GetLoadedPosition()
}
func (r *RepeatedProperty) Position() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) LoadedPositionEntity() int {
	return r.Property.GetLoadedPositionEntity()
}
func (r *RepeatedProperty) PositionEntity() int {
	return r.Property.GetPositionEntity()
}
func (r *RepeatedProperty) GetLoadedPosition() int {
	return r.Property.GetPosition()
}
func (r *RepeatedProperty) GetLoadedPositionEntity() int {
	return r.Property.GetLoadedPositionEntity()
}
func (r *RepeatedProperty) GetPositionEntity() int {
	return r.Property.GetPositionEntity()
}

// STRING is a contains reprensenting the widely used string type.
const STRING = "string"

// FieldType is the generic type for a data field.
// type PropertyType string

// type DBType struct {
// 	Type  string
// 	Value string
// }

// type ValueObjectDefinition struct {
// 	Aggregate           AggregateInterface
// 	aggregateDefinition *AggregateDefinition
// 	Name                string
// 	Keys                []Property
// 	Properties          []Property
// 	Abstract            bool
// 	aggregateIDProperty Property
// 	domainPath          string
// 	// Select              func(
// 	// 	WorkflowInterface, *baserepositorypb.ListQuery,
// 	// ) (*Collection, error)
// }

// func (t *ValueObjectDefinition) AggregateDefinition() *AggregateDefinition {
// 	return t.aggregateDefinition
// }
// func (t *ValueObjectDefinition) GetName() string {
// 	return t.Name
// }
// func (t *ValueObjectDefinition) Title() string {
// 	return strings.Title(t.Name)
// }
// func (t *ValueObjectDefinition) Snake() string {
// 	return strcase.ToSnake(t.Name)
// }
// func (f *ValueObjectDefinition) Upper() string {
// 	return strings.ToUpper(f.Snake())
// }
// func (t *ValueObjectDefinition) GetType() string {
// 	return "ValueObject"
// }
// func (t *ValueObjectDefinition) IsEntity() bool {
// 	return false
// }
// func (t *ValueObjectDefinition) UseTenants() bool {
// 	return t.aggregateDefinition.UseTenants
// }
// func (t *ValueObjectDefinition) GetDisableID() bool {
// 	return true
// }
// func (t *ValueObjectDefinition) GetDisableDatetime() bool {
// 	return true
// }
// func (t *ValueObjectDefinition) GetDisableDatabaseStore() bool {
// 	return false
// }
// func (t *ValueObjectDefinition) GetUseOneOf() bool {
// 	return false
// }
// func (t *ValueObjectDefinition) StoredProperties() []Property {
// 	var storedProperties []Property
// 	for _, property := range t.GetKeys() {
// 		if property.IsStored() {
// 			storedProperties = append(storedProperties, property)
// 		}
// 	}
// 	for _, property := range t.Properties {
// 		if property.IsStored() {
// 			storedProperties = append(storedProperties, property)
// 		}
// 	}
// 	return storedProperties
// }
// func (t *ValueObjectDefinition) NestedProperties() []Property {
// 	var nestedProperties []Property
// 	for _, property := range t.Properties {
// 		if !property.IsStored() {
// 			// if string(property.Type()) == "One2manyType" {
// 			nestedProperties = append(nestedProperties, property)
// 		}
// 	}
// 	return nestedProperties
// }
// func (t *ValueObjectDefinition) GetCommands() *EntityCommandsDefinition {
// 	return &EntityCommandsDefinition{}
// }

// var SetAggregateIDProperty func(string) Property

// func (t *ValueObjectDefinition) GetKeys() []Property {
// 	keys := []Property{
// 		t.aggregateIDProperty,
// 	}
// 	for _, key := range t.Keys {
// 		keys = append(keys, key)
// 	}
// 	return keys
// }
// func (t *ValueObjectDefinition) IsAggregateRoot() bool {
// 	return false
// }
// func (t *ValueObjectDefinition) HasTranslatable() bool {
// 	return false
// }
// func (t *ValueObjectDefinition) GetAbstract() bool {
// 	return t.Abstract
// }
// func (t *ValueObjectDefinition) DomainPath() string {
// 	return t.domainPath
// }
// func (t *ValueObjectDefinition) GetNoCache() bool {
// 	return false
// }

type TableInterface interface {
	GetName() string
	Title() string
	Snake() string
	Upper() string
	GetType() string
	IsEntity() bool
	UseTenants() bool
	GetDisableID() bool
	GetDisableDatetime() bool
	GetDisableDatabaseStore() bool
	GetUseOneOf() bool
	StoredProperties() []libdomain.PropertyDefinition
	NestedProperties() []libdomain.PropertyDefinition
	// GetCommands() *EntityCommandsDefinition
	GetKeys() []libdomain.PropertyDefinition
	IsAggregateRoot() bool
	HasTranslatable() bool
	GetAbstract() bool
	GetNoCache() bool
	DomainPath() string
}

type OnDeleteValue string

const OnDeleteCascade = OnDeleteValue("CASCADE")
const OnDeleteProtect = OnDeleteValue("PROTECT")
const OnDeleteSetNull = OnDeleteValue("SET NULL")

// type JWTClaims struct {
// 	*jwt.StandardClaims
// 	TenantID  string
// 	UserID    string
// 	UserEmail string
// 	Groups    []*baserepositorypb.GetByExternalIDQuery
// }

// type ExternalData struct {
// 	Aggregate AggregateInterface
// 	TenantID  string
// 	Module    string
// 	Data      []map[string]string
// }

// var IDProperty = &baserepositorypb.BaseProperty{Snake: "id"}
// var TenantIDProperty = &baserepositorypb.BaseProperty{Snake: "tenant_id"}
// var ExternalModuleProperty = &baserepositorypb.BaseProperty{Snake: "external_module"}
// var ExternalIDProperty = &baserepositorypb.BaseProperty{Snake: "external_id"}

type ImportSuccessInterface interface {
	GetCode() string
	GetEvents() []libdomain.DomainEvent
}
type ImportErrorInterface interface {
	GetCode() string
	GetError() string
}

type CommandFromDomainCreate struct {
	Aggregate   *libdomain.AggregateDefinition
	NewPackage  *libdomain.PackageLocation
	NewFunction string
	Args        []string
}

func (c *CommandFromDomainCreate) Type() string {
	return "CommandFromDomainMethod"
}
func (c *CommandFromDomainCreate) Generate() string {

	var args []string
	for _, arg := range c.Args {
		args = append(args, "command."+strings.Title(arg)+"()")
	}

	content := `
		new{{.Aggregate.AggregateRoot.Name}}ID, err := s.{{.Aggregate.AggregateRoot.Title}}Repository().Next{{.Aggregate.AggregateRoot.Title}}Identity(ctx)
		if err != nil {
			return nil, err
		}

		{{.Aggregate.AggregateRoot.Name}}, err := {{.NewPackage}}.{{.NewFunction}}(ctx, command.TenantID(), new{{.Aggregate.AggregateRoot.Name}}ID, {{.Args}})
		if err != nil {
			return nil, err
		}

		err = s.{{.Aggregate.AggregateRoot.Title}}Repository().Add(ctx, {{.Aggregate.AggregateRoot.Name}})
		if err != nil {
			return nil, err
		}

		return {{.Aggregate.AggregateRoot.Name}}, nil
   	`

	var newFunction string
	if newFunction == "" {
		newFunction = "New" + c.Aggregate.AggregateRoot.Title()
	} else {
		newFunction = strings.Title(c.NewFunction)
	}

	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		Aggregate   *libdomain.AggregateDefinition
		NewPackage  string
		NewFunction string
		Args        string
	}{
		Aggregate:   c.Aggregate,
		NewPackage:  c.NewPackage.Package,
		NewFunction: newFunction,
		Args:        strings.Join(args, ","),
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()
}
func (c *CommandFromDomainCreate) Imports() []*libdomain.PackageLocation {
	return []*libdomain.PackageLocation{c.NewPackage}
}

type CommandFromDomainMethod struct {
	Method      *libdomain.CustomRequest
	AggregateID string
	ArgsMapping interface {
		Generate() string
	}
}

func (c *CommandFromDomainMethod) Type() string {
	return "CommandFromDomainMethod"
}
func (c *CommandFromDomainMethod) Generate() string {

	content := `
		{{.Aggregate.AggregateRoot.Name}}, err := s.Existing{{.Aggregate.AggregateRoot.Title}}(ctx, command.{{.AggregateID}}(), {{.Aggregate.AggregateRoot.DomainPackage}}.GetAll{{.Aggregate.AggregateRoot.Title}}Properties())
		if err != nil {
			return nil, err
		}

		err = {{.Aggregate.AggregateRoot.Name}}.{{.Method.Title}}(ctx, {{.ArgsMapping.Generate}})
		if err != nil {
			return nil, err
		}

		err = s.{{.Aggregate.AggregateRoot.Title}}Repository().Save(ctx, {{.Aggregate.AggregateRoot.Name}})
		if err != nil {
			return nil, err
		}

		return {{.Aggregate.AggregateRoot.Name}}, nil
   	`

	var aggregateID string
	if aggregateID == "" {
		aggregateID = c.Method.TableDefinition().AggregateDefinition().AggregateRoot.Title() + "ID"
	} else {
		aggregateID = strings.Title(c.AggregateID)
	}

	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		Aggregate   *libdomain.AggregateDefinition
		Method      *libdomain.CustomRequest
		AggregateID string
		ArgsMapping interface{}
	}{
		Aggregate:   c.Method.TableDefinition().AggregateDefinition(),
		Method:      c.Method,
		AggregateID: aggregateID,
		ArgsMapping: c.ArgsMapping,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()
}
func (c *CommandFromDomainMethod) Imports() []*libdomain.PackageLocation {
	return []*libdomain.PackageLocation{}
}