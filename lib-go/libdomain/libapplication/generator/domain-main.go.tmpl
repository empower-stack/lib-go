// Code generated by go generate; DO NOT EDIT.
// This file was generated by robots at
// {{ timestamp }}

package {{.Package}}

import (
	"fmt"
	"context"
	"errors"
	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libutils"

	"{{.Repository}}/definitions"
	{{if .Service.Commands}}"{{.Repository}}/gen/commands"{{end}}
	{{- range .Service.Repositories}}
	{{.AggregateDefinition.AggregateRoot.Name}}imports "{{.AggregateDefinition.AggregateRoot.DomainPath}}/imports"
	{{- end}}
	"{{.Repository}}/gen/data"

	{{.Import}}
)

var {{.Service.Title}}ServiceDefinition *libapplication.ServiceDefinition
func init() {
	{{.Service.Title}}ServiceDefinition = definitions.Definitions.GetByID("{{.Service.Name}}")
}

type {{.Service.Title}}ApplicationService struct {
	transactionClient      libapplication.TransactionClient
	cacheClient      libapplication.CacheClient
	domainEventPublisher   *libdomain.DomainEventPublisher
	{{- range .Service.Services}}
	{{.Name}}         *{{if .ReferenceDefinition}}{{.ReferenceDefinition.ApplicationPackage}}.{{end}}{{.Title}}
	{{- end}}
	{{- range .Service.Repositories}}
	{{.Name}}         {{.Package}}.{{.Title}}
	{{- end}}
}

func (s *{{.Service.Title}}ApplicationService) TransactionClient() libapplication.TransactionClient {
	return s.transactionClient
}
func (s *{{.Service.Title}}ApplicationService) CacheClient() libapplication.CacheClient {
	return s.cacheClient
}
func (s *{{.Service.Title}}ApplicationService) DomainEventPublisher() *libdomain.DomainEventPublisher {
	return s.domainEventPublisher
}
{{- range .Service.Services}}
func (s *{{$.Service.Title}}ApplicationService) {{.Title}}() *{{if .ReferenceDefinition}}{{.ReferenceDefinition.ApplicationPackage}}.{{end}}{{.Title}} {
	return s.{{.Name}}
}
{{- end}}
{{- range .Service.Repositories}}
func (s *{{$.Service.Title}}ApplicationService) {{.Title}}() {{.Package}}.{{.Title}} {
	return s.{{.Name}}
}
{{- end}}

var {{.Service.Title}}ApplicationServiceInitialization func(*{{.Service.Title}}ApplicationService, *libdomain.DomainEventPublisher)
func init() {
	{{.Service.Title}}ApplicationServiceInitialization = func(*{{.Service.Title}}ApplicationService, *libdomain.DomainEventPublisher) {
		return
	}
}

func New{{.Service.Title}}ApplicationService(
	transactionClient libapplication.TransactionClient,
	cacheClient libapplication.CacheClient,
	domainEventPublisher *libdomain.DomainEventPublisher,
	{{- range .Service.Services}}
	{{.Name}}         *{{if .ReferenceDefinition}}{{.ReferenceDefinition.ApplicationPackage}}.{{end}}{{.Title}},
	{{- end}}
	{{- range .Service.Repositories}}
	{{.Name}}         {{.Package}}.{{.Title}},
	{{- end}}
) *{{.Service.Title}}ApplicationService {


	 {{.Service.Name}}ApplicationService := &{{.Service.Title}}ApplicationService{
		transactionClient:      transactionClient,
		cacheClient:      cacheClient,
		domainEventPublisher:   domainEventPublisher,
		{{- range .Service.Services}}
		{{.Name}}:         {{.Name}},
		{{- end}}
		{{- range .Service.Repositories}}
		{{.Name}}: {{.Name}},
		{{- end}}
	}

	{{.Service.Title}}ApplicationServiceInitialization({{.Service.Name}}ApplicationService, domainEventPublisher)

	return {{.Service.Name}}ApplicationService
}

{{- range .Service.Commands}}
type {{.Name}}CommandType struct {
	Definition *libapplication.CustomRequest
	Args struct {
		{{- range .Args}}
		{{.Title}} libdomain.ArgumentDefinition
		{{- end}}
	}
	Results struct {
		{{- range .Results}}
		{{.Title}} libdomain.ArgumentDefinition
		{{- end}}
	}
}
var {{.Title}}Command *{{.Name}}CommandType

var {{$.Service.Title}}{{.Title}}Implementation func(*libapplication.ApplicationContext, *{{$.Service.Title}}ApplicationService, *commands.{{.Title}}Command) ({{- range .Results}}{{.GoTypeWithPackage}}, {{- end}}error)
func (s *{{$.Service.Title}}ApplicationService) {{.Title}}(ctx *libapplication.ApplicationContext, aCommand *commands.{{.Title}}Command) ({{- range .Results}}{{.GoTypeWithPackage}}, {{- end}} error) {

	/*ctx := libapplication.NewApplicationContext(c, s.TransactionClient(), s.CacheClient(), s.DomainEventPublisher(), true)
	err := s.TransactionClient().BeginTransaction(ctx, true)
	if err != nil {
		return {{- range .Results}} {{.GoNil}}, {{- end}} err
	}
	defer func() {
		ctx.Commit()
	}()
	ctx.Workflow().TenantID = aCommand.TenantID()
	err = ctx.Authenticate(s)
	if err != nil {
		return {{- range .Results}} {{.GoNil}}, {{- end}} err
	}*/

	return {{$.Service.Title}}{{.Title}}Implementation(ctx, s, aCommand)
}
func init() {
	{{$.Service.Title}}{{.Title}}Implementation = func(ctx *libapplication.ApplicationContext, s *{{$.Service.Title}}ApplicationService, command *commands.{{.Title}}Command) ({{- range .Results}}{{.GoTypeWithPackage}}, {{- end}}error) {
		{{- if .Generator}}
			{{.Generator.Generate}}
		{{- else}}
			panic("{{$.Service.Title}}{{.Title}}Implementation not implemented")
		{{- end}}
	}
}

{{- end}}


func init() {
	for _, command := range {{.Service.Title}}ServiceDefinition.Commands {
		switch command.Name {
		{{- range .Service.Commands}}
		{{$command := .}}
		case "{{.Name}}":
			{{.Title}}Command = &{{.Name}}CommandType{
				Definition: command,
			}
			for _, arg := range {{.Title}}Command.Definition.Args {
				switch arg.GetName() {
				{{- range .Args}}
				case "{{.GetName}}":
					{{$command.Title}}Command.Args.{{.Title}} = arg
				{{- end}}
				}
			}
			for _, result := range {{.Title}}Command.Definition.Results {
				switch result.GetName() {
				{{- range .Results}}
				case "{{.GetName}}":
					{{$command.Title}}Command.Results.{{.Title}} = result
				{{- end}}
				}
			}
		{{- end}}
		}
	}
}


{{- range getTargetRepositories .Service }}
var {{$.Service.Title}}ApplicationService{{.Title}}Repository func(*{{$.Service.Title}}ApplicationService) {{.DomainPackage}}.{{.Title}}Repository
{{end}}

func init() {
	{{- range getTargetRepositories .Service }}
	{{$.Service.Title}}ApplicationService{{.Title}}Repository = func(s *{{$.Service.Title}}ApplicationService) {{.DomainPackage}}.{{.Title}}Repository {
		panic("{{.Title}} repository is not set")
	}
	{{end}}
}

{{- range .Service.Repositories}}
{{$repository := .}}
type {{.AggregateTitle}} = {{.Package}}.{{.AggregateTitle}}
func (s *{{$.Service.Title}}ApplicationService) {{.AggregateTitle}}(ctx *libapplication.ApplicationContext, a{{.AggregateTitle}}Id {{.Package}}identities.{{.AggregateTitle}}ID, getProperties *{{.Package}}.Get{{.AggregateTitle}}Properties) (*{{.Package}}.{{.AggregateTitle}}, error) {
	return s.{{.AggregateName}}Repository.{{.AggregateTitle}}ByID(ctx, a{{.AggregateTitle}}Id, getProperties)
}
func (s *{{$.Service.Title}}ApplicationService) {{.AggregateTitle}}ByCode(ctx *libapplication.ApplicationContext, code string, getProperties *{{.Package}}.Get{{.AggregateTitle}}Properties) (*{{.Package}}.{{.AggregateTitle}}, error) {
	_, tenantByCodes, err := s.{{.AggregateName}}Repository.{{.AggregateTitle}}sByCodes(ctx, "", []string{code}, nil, getProperties)
	if err != nil {
		return nil, err
	}

	if v, ok := tenantByCodes[code]; ok {
		return v, nil
	} else {
		return nil, nil
	}
}
func (s *{{$.Service.Title}}ApplicationService) {{.AggregateTitle}}WithExternalID(ctx *libapplication.ApplicationContext, aModuleName string, anExternal{{.AggregateTitle}}Id string, getProperties *{{.Package}}.Get{{.AggregateTitle}}Properties) (*{{.Package}}.{{.AggregateTitle}}, error) {
	return s.{{.AggregateName}}Repository.{{.AggregateTitle}}OfExternalId(ctx, aModuleName, anExternal{{.AggregateTitle}}Id, getProperties)
}
func (s *{{$.Service.Title}}ApplicationService) Existing{{.AggregateTitle}}(ctx *libapplication.ApplicationContext, a{{.AggregateTitle}}Id {{.Package}}identities.{{.AggregateTitle}}ID, getProperties *{{.Package}}.Get{{.AggregateTitle}}Properties) (*{{.Package}}.{{.AggregateTitle}}, error) {
	{{.AggregateName}}, err := s.{{.AggregateTitle}}(ctx, a{{.AggregateTitle}}Id, getProperties)
	if err != nil {
		return nil, err
	}

	if {{.AggregateName}} == nil {
		return nil, errors.New("{{.AggregateTitle}} does not exist.")
	}

	return {{.AggregateName}}, nil
}
func (s *{{$.Service.Title}}ApplicationService) Get{{.AggregateTitle}}History(ctx *libapplication.ApplicationContext, tenantID libdomain.TenantID, a{{.AggregateTitle}}Id {{.Package}}identities.{{.AggregateTitle}}ID) ([]*libdomain.EventHistory, error) {
	
	{{.AggregateName}}Events, err := s.{{.AggregateName}}Repository.Get{{.AggregateTitle}}History(ctx, a{{.AggregateTitle}}Id)
	if err != nil {
		return nil, err
	}

	return {{.AggregateName}}Events, nil
}
func (s *{{$.Service.Title}}ApplicationService) Check{{.AggregateTitle}}ConsistencyWithEvents(c context.Context, tenantID libdomain.TenantID, a{{.AggregateTitle}}Id {{.Package}}identities.{{.AggregateTitle}}ID) error {
	
	ctx := libapplication.NewApplicationContext(c, s.TransactionClient(), s.CacheClient(), nil, false)
	err := s.TransactionClient().BeginTransaction(ctx, true)
	if err != nil {
		return err
	}
	defer func() {
		if r := recover(); r != nil {
			ctx.TransactionClient.RollbackTransaction(ctx)
			panic("Panic. Error:\n"+string(fmt.Sprintf("%s", r)))
		}
		ctx.Commit(err)
	}()
	ctx.Workflow().TenantID = tenantID
	err = ctx.Authenticate(s)
	if err != nil {
		return err
	}

	{{.AggregateName}}, err := s.{{.AggregateTitle}}(ctx, a{{.AggregateTitle}}Id,  {{.Package}}.GetAll{{.AggregateTitle}}Properties())
	if err != nil {
		return err
	}

	{{.AggregateName}}Events, err := s.{{.AggregateName}}Repository.Get{{.AggregateTitle}}Events(ctx, a{{.AggregateTitle}}Id)
	if err != nil {
		return err
	}


	return {{.AggregateName}}.CheckConsistencyWithEvents({{.AggregateName}}Events)
}

var PrepareLoad{{.AggregateTitle}}ExistingDatas func(ctx *libapplication.ApplicationContext, s*{{$.Service.Title}}ApplicationService, module string, data []map[string]string, existingDatas map[string]interface{}) error
func init () {
	PrepareLoad{{.AggregateTitle}}ExistingDatas = func(ctx *libapplication.ApplicationContext, s*{{$.Service.Title}}ApplicationService, module string, data []map[string]string,  existingDatas map[string]interface{}) error {
		return nil
	}
}

var Prepare{{.AggregateTitle}}RootExists func(ctx *libapplication.ApplicationContext, s*{{$.Service.Title}}ApplicationService, module string, data []map[string]string, existingDatas map[string]interface{}, initialRootExists map[string]*{{.Package}}.{{.AggregateTitle}}) (interface{}, error)
func init () {
	Prepare{{.AggregateTitle}}RootExists = func(ctx *libapplication.ApplicationContext, s*{{$.Service.Title}}ApplicationService, module string, data []map[string]string, existingDatas map[string]interface{}, initialRootExists map[string]*{{.Package}}.{{.AggregateTitle}}) (interface{}, error) {
		return initialRootExists, nil
	}
}

func (s *{{$.Service.Title}}ApplicationService) Load{{.AggregateTitle}}ExternalData(
	ctx *libapplication.ApplicationContext, module string, data []map[string]string,
) ([]libapplication.ImportSuccessInterface, []libapplication.ImportErrorInterface, error) {

	// existingDatas := {{.AggregateName}}imports.NewLoad{{.AggregateTitle}}ExistingDatas()
	existingDatas := map[string]interface{}{}

	err := PrepareLoad{{.AggregateTitle}}ExistingDatas(ctx, s, module, data, existingDatas)
	if err != nil {
		return nil, nil, err
	}

	{{- range .AggregateDefinition.EntitiesWithIDFields }}
	{{$entity := .Entity}}
	{{$many := .Many}}

	if _, ok := existingDatas["{{.Entity.Name}}"]; !ok {
		existingDatas["{{.Entity.Name}}"] = {{.Entity.DomainPackage}}.NewLoad{{.Entity.Title}}ExistingDatas()
	}
	{{.Entity.Name}}Ids := map[{{.Entity.DomainPackage}}identities.{{.Entity.Title}}ID]bool{}
	{{.Entity.Name}}Codes := map[string]bool{}
	for _, row := range data {
		{{- range .Fields}}
		if v, ok := row["{{.}}id"]; ok && v != "" {
			{{- if $many}}
			for _, id := range strings.Split(v, ",") {
				{{$entity.Name}}Ids[{{$entity.DomainPackage}}identities.{{$entity.Title}}ID(id)] = true
			}
			{{- else}}
			{{$entity.Name}}Ids[{{$entity.DomainPackage}}identities.{{$entity.Title}}ID(v)] = true
			{{- end}}
		}
		{{- if eq $repository.AggregateName $entity.Name}}
		if code, err := {{$repository.AggregateName}}imports.Compute{{$entity.Title}}Code("{{.}}", row); err != nil || code != "" {
			if err != nil {
				return nil, nil, err
			}
			if code == "" {
				continue
			}
		{{- else}}
		if code, ok := row["{{.}}{{$entity.UniquePropertyName}}"]; ok && code != "" {
		{{- end}}
			{{- if $many}}
			for _, c := range strings.Split(code, ",") {
				{{$entity.Name}}Codes[c] = true
			}
			{{- else}}
			{{$entity.Name}}Codes[code] = true
			{{- end}}
		}
		{{- end}}
	}

	if len({{.Entity.Name}}Ids) > 0 {
		var {{.Entity.Name}}IdsSlice []{{.Entity.DomainPackage}}identities.{{.Entity.Title}}ID
		for k := range {{.Entity.Name}}Ids {
			{{.Entity.Name}}IdsSlice = append({{.Entity.Name}}IdsSlice, k)
		}
		{{.Entity.Name}}ByIds, {{.Entity.Name}}ByCodes, err := {{if isInThisService $.Service .Entity}}s.{{.Entity.AggregateDefinition.AggregateRoot.Title}}Repository(){{else}}{{$.Service.Title}}ApplicationService{{.Entity.AggregateDefinition.AggregateRoot.Title}}Repository(s){{end}}.{{.Entity.Title}}ByIds(
			ctx, {{.Entity.Name}}IdsSlice, 
			{{if eq .Entity.AggregateDefinition $repository.AggregateDefinition}}
			{{.Entity.DomainPackage}}.GetAll{{.Entity.Title}}Properties())
			{{else}}
			&{{.Entity.DomainPackage}}.Get{{.Entity.Title}}Properties{
				{{if .Entity.UniqueProperty}}
				{{.Entity.UniquePropertyTitle}}: true,
				{{end}}
			})
			{{end}}
		if err != nil {
			return nil, nil, err
		}
		{{.Entity.Name}}ExistingDatas := existingDatas["{{.Entity.Name}}"].(*{{.Entity.DomainPackage}}.Load{{.Entity.Title}}ExistingDatas)
		for {{.Entity.Name}}ID, {{.Entity.Name}} := range {{.Entity.Name}}ByIds {
			{{.Entity.Name}}ExistingDatas.ByIds[{{.Entity.Name}}ID] = {{.Entity.Name}}
		}
		for {{.Entity.Name}}Code, {{.Entity.Name}} := range {{.Entity.Name}}ByCodes {
			{{.Entity.Name}}ExistingDatas.ByCodes[{{.Entity.Name}}Code] = {{.Entity.Name}}
		}
	}
	{{if ne .Entity.Name $repository.AggregateName}}if len({{.Entity.Name}}Codes) > 0 { {{end}}
		{{.Entity.Name}}ByIds, {{.Entity.Name}}ByCodes, err := {{if isInThisService $.Service .Entity}}s.{{.Entity.AggregateDefinition.AggregateRoot.Title}}Repository(){{else}}{{$.Service.Title}}ApplicationService{{.Entity.AggregateDefinition.AggregateRoot.Title}}Repository(s){{end}}.{{.Entity.Title}}sByCodes(
			ctx, module, libutils.MapKeys({{.Entity.Name}}Codes), existingDatas,
			{{if eq .Entity.AggregateDefinition $repository.AggregateDefinition}}
			{{.Entity.DomainPackage}}.GetAll{{.Entity.Title}}Properties())
			{{else}}
			&{{.Entity.DomainPackage}}.Get{{.Entity.Title}}Properties{
				{{if .Entity.UniqueProperty}}
				{{.Entity.UniquePropertyTitle}}: true,
				{{end}}
			})
			{{end}}
		if err != nil {
			return nil, nil, err
		}
		{{.Entity.Name}}ExistingDatas := existingDatas["{{.Entity.Name}}"].(*{{.Entity.DomainPackage}}.Load{{.Entity.Title}}ExistingDatas)
		for {{.Entity.Name}}ID, {{.Entity.Name}} := range {{.Entity.Name}}ByIds {
			{{.Entity.Name}}ExistingDatas.ByIds[{{.Entity.Name}}ID] = {{.Entity.Name}}
		}
		for {{.Entity.Name}}Code, {{.Entity.Name}} := range {{.Entity.Name}}ByCodes {
			{{.Entity.Name}}ExistingDatas.ByCodes[{{.Entity.Name}}Code] = {{.Entity.Name}}
		}
	{{if ne .Entity.Name $repository.AggregateName}} } {{end}}


	{{- end}}

	rootExists, err := Prepare{{.AggregateTitle}}RootExists(ctx, s, module, data, existingDatas, {{.AggregateName}}ExistingDatas.ByCodes)
	if err != nil {
		return nil, nil, err
	}

	{{.AggregateName}}sToCreate, {{.AggregateName}}sToSave, errors := {{.AggregateName}}imports.Load{{.AggregateTitle}}ExternalData(
		ctx, module, data, rootExists, existingDatas,
	)

	// initialCtx := ctx

	var sucesses []libapplication.ImportSuccessInterface
	for _, {{.AggregateName}}ToCreate := range {{.AggregateName}}sToCreate {

		// ctx, err := libapplication.NewApplicationContextFromService(initialCtx.Workflow().Context, string(initialCtx.Workflow().TenantID), s)
		// if err != nil {
			// return nil, nil, err
		// }

		var events []libdomain.DomainEvent
		for _, event := range {{.AggregateName}}ToCreate.MutatingEvents() {
			events = append(events, event)
		}
		err = s.{{.AggregateTitle}}Repository().Add(ctx, {{.AggregateName}}ToCreate)
		if err != nil {
			return nil, nil, err
		} else {
			sucesses = append(sucesses, &{{.AggregateName}}imports.{{.AggregateTitle}}SuccessfullyImported{
				{{.AggregateTitle}}: {{.AggregateName}}ToCreate,
				Events: events,
			})
		}

		// ctx.Commit(err)
	}
	for _, {{.AggregateName}}ToSave := range {{.AggregateName}}sToSave {

		// ctx, err := libapplication.NewApplicationContextFromService(initialCtx.Workflow().Context, string(initialCtx.Workflow().TenantID), s)
		// if err != nil {
			// return nil, nil, err
		// }

		var events []libdomain.DomainEvent
		for _, event := range {{.AggregateName}}ToSave.MutatingEvents() {
			events = append(events, event)
		}
		err = s.{{.AggregateTitle}}Repository().Save(ctx, {{.AggregateName}}ToSave)
		if err != nil {
			errors = append(errors, &{{.AggregateName}}imports.{{.AggregateTitle}}ImportationError{
				// {{.AggregateTitle}}: {{.AggregateName}}ToSave,
				Error: err,
			})
		} else {
			sucesses = append(sucesses, &{{.AggregateName}}imports.{{.AggregateTitle}}SuccessfullyImported{
				{{.AggregateTitle}}: {{.AggregateName}}ToSave,
				Events: events,
			})
		}

		// ctx.Commit(err)
	}

	var importErrors []libapplication.ImportErrorInterface
	for _, importError := range errors {
		importErrors = append(importErrors, importError)
	}

	// ctx = initialCtx

	return sucesses, importErrors, nil
}


{{- end}}

var {{$.Service.Title}}AuthenticateImplementation func(*libapplication.ApplicationContext, *{{$.Service.Title}}ApplicationService, string) (string, string, map[string]bool, error)
func (s *{{.Service.Title}}ApplicationService) Authenticate(ctx *libapplication.ApplicationContext, token string) (string, string, map[string]bool, error) {

	return {{.Service.Title}}AuthenticateImplementation(ctx, s, token)
}
func init() {
	{{.Service.Title}}AuthenticateImplementation = func(*libapplication.ApplicationContext, *{{$.Service.Title}}ApplicationService, string) (string, string, map[string]bool, error) {
		panic("{{.Service.Title}}AuthenticateImplementation not implemented")
	}
}

var _ libutils.DummyType
var _ data.Dummy{{.Service.Title}}
var _ strings.Builder