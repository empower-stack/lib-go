package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/arguments"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed domain-*.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Domains(defs *libapplication.Definitions) {

	defs.ControlReferences()

	var err error
	fileInit, _ := fdomain.ReadFile("domain-init.go.tmpl")
	initTemplate := template.Must(initFuncs.Parse(string(fileInit)))

	file, _ := fdomain.ReadFile("domain-main.go.tmpl")
	domainTemplate := template.Must(aggregateFuncs.Parse(string(file)))

	fileQuery, _ := fdomain.ReadFile("domain-query.go.tmpl")
	queryTemplate := template.Must(queryFuncs.Parse(string(fileQuery)))

	fileCommands, _ := fdomain.ReadFile("domain-commands.go.tmpl")
	commandsTemplate := template.Must(commandsFuncs.Parse(string(fileCommands)))

	fileQueries, _ := fdomain.ReadFile("domain-queries.go.tmpl")
	queriesTemplate := template.Must(queriesFuncs.Parse(string(fileQueries)))

	fileRepresentations, _ := fdomain.ReadFile("domain-representations.go.tmpl")
	representationsTemplate := template.Must(representationsFuncs.Parse(string(fileRepresentations)))

	event := false
	// if defs.EventClient != nil {
	// 	event = true
	// }

	err = os.MkdirAll("gen/commands", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll("gen/queries", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll("gen/data", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	var services []*libapplication.ServiceDefinition
	for _, definition := range defs.Slice() {

		repositories := map[*libdomain.RepositoryDefinition]*libdomain.RepositoryDefinition{}
		for _, repository := range definition.Repositories {
			repositories[repository] = repository
		}

		d := struct {
			Package    string
			Repository string
			Event      bool
			Service    *libapplication.ServiceDefinition
			CustomCommandsAggregate  []*libapplication.CustomRequest
			CustomCommandsCollection []*libapplication.CustomRequest
			Import                   string
			ImportRepositories       map[*libdomain.RepositoryDefinition]*libdomain.RepositoryDefinition
		}{
			Package:    defs.Package,
			Repository: defs.Repository,
			Event:      event,
			Service:    definition,
			ImportRepositories: repositories,
		}

		imports := map[string]string{}
		for _, service := range definition.Services {
			if service.ReferenceDefinition != nil {
				path := service.ReferenceDefinition.ApplicationPackage() + " \"" + service.ReferenceDefinition.ApplicationPath() + "\""
				imports[path] = path
			}
		}
		for _, repository := range definition.Repositories {
			path := repository.Package + " \"" + repository.Path + "\""
			imports[path] = path
			path = repository.Package + "identities \"" + repository.Path + "/identities\""
			imports[path] = path
			for _, entityWithIds := range repository.AggregateDefinition.EntitiesWithIDFields() {
				path = entityWithIds.Entity.DomainPackage() + "identities \"" + entityWithIds.Entity.DomainPath() + "/identities\""
				imports[path] = path
				path = entityWithIds.Entity.DomainPackage() + " \"" + entityWithIds.Entity.DomainPath() + "\""
				imports[path] = path
			}
		}
		for _, command := range definition.Commands {
			for _, property := range command.Args {
				switch v := property.(type) {
				case *arguments.OrderedMapDefinition, *arguments.EntityDefinition, *arguments.ValueObjectDefinition:
					if v.GetReference() == nil {
						panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
					}
				case *arguments.SliceDefinition:
					switch vv := v.Argument.(type) {
					case *arguments.EntityDefinition:
						if vv.GetReference() == nil {
							panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
						}
					}
				case *arguments.GetPropertiesDefinition:
					path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
					imports[path] = path
					// case *properties.ID:
					// 	path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/gen/identities\""
					// 	imports[path] = path
				}
			}
			for _, property := range command.Results {
				switch v := property.(type) {
				case *arguments.IDDefinition:
					path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
					imports[path] = path
				case *arguments.EntityDefinition:
					if v.GetReference() == nil {
						panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
					}
					path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
					imports[path] = path
				case *arguments.OrderedMapDefinition, *arguments.ValueObjectDefinition:
					if v.GetReference() == nil {
						panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
					}
				case *arguments.SliceDefinition:
					switch vv := v.Argument.(type) {
					case *arguments.EntityDefinition:
						if v.GetReference() == nil {
							panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
						}
						path := vv.ReferenceDefinition.DomainPackage() + " \"" + vv.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					}
				}
			}
			if command.Generator != nil {
				for _, i := range command.Generator.Imports() {
					path := i.Package + " \"" + i.Path + "\""
					imports[path] = path
				}
			}

		}
		for _, query := range definition.Queries {
			for _, property := range query.Args {
				switch v := property.(type) {
				case *arguments.OrderedMapDefinition, *arguments.EntityDefinition, *arguments.ValueObjectDefinition:
					if v.GetReference() == nil {
						panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
					}
				case *arguments.SliceDefinition:
					switch vv := v.Argument.(type) {
					case *arguments.EntityDefinition, *arguments.ValueObjectDefinition:
						if vv.GetReference() == nil {
							panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
						}
					}
				}
			}
			for _, property := range query.Results {
				switch v := property.(type) {
				case *arguments.OrderedMapDefinition, *arguments.EntityDefinition, *arguments.ValueObjectDefinition:
					if v.GetReference() == nil {
						panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
					}
				case *arguments.SliceDefinition:
					switch vv := v.Argument.(type) {
					case *arguments.EntityDefinition:
						if vv.GetReference() == nil {
							panic(fmt.Sprintf("property %s from %s can't find his reference", property.GetName(), property.Owner().Title()))
						}
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		services = append(services, definition)

		buf := &bytes.Buffer{}
		err := domainTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/%s.gen.go", strcase.ToSnake(definition.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		imports = map[string]string{}
		for _, query := range definition.Queries {
			for _, results := range query.Results {
				if results.GetReference() != nil {
					path := results.GetReference().DomainPackage() + " \"" + results.GetReference().DomainPath() + "\""
					imports[path] = path
				}
				switch v := results.(type) {
				case *arguments.SliceDefinition:
					switch vv := v.Argument.(type) {
					case *arguments.IDDefinition:
						path := vv.ReferenceDefinition.DomainPackage() + "identities \"" + vv.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path	
					}
				}
			}
		}
		for _, repository := range definition.Repositories {
			path := repository.Package + " \"" + repository.Path + "\""
			imports[path] = path
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		buf = &bytes.Buffer{}
		err = queryTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/%sQuery.gen.go", strcase.ToSnake(definition.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		if len(definition.Commands) > 0 {
			imports = map[string]string{}
			for _, command := range definition.Commands {
				for _, property := range command.Args {
					switch v := property.(type) {
					case *arguments.GetPropertiesDefinition:
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					case *arguments.IDDefinition:
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
					case *arguments.EntityDefinition:
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					case *arguments.ValueObjectDefinition:
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					case *arguments.SelectionDefinition:
						path := v.EnumDefinition.DomainPackage() + " \"" + v.EnumDefinition.DomainPath() + "\""
						imports[path] = path
					case *arguments.SliceDefinition:
						switch v := v.Argument.(type) {
						case *arguments.EntityDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						case *arguments.ValueObjectDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						case *arguments.IDDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
								imports[path] = path
							}
						}
					case *arguments.OrderedMapDefinition:
						switch v := v.Argument.(type) {
						case *arguments.EntityDefinition:
							if v.ReferenceDefinition.DomainPath() != "" {
								path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
								imports[path] = path
								path = v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
								imports[path] = path
							}
						}
					}
				}
				for _, property := range command.Results {
					switch v := property.(type) {
					case *arguments.SliceDefinition:
						switch vv := v.Argument.(type) {
						case *arguments.EntityDefinition:
							path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
							imports[path] = path
						}
					case *arguments.IDDefinition:
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
					case *arguments.EntityDefinition:
						path := property.GetReference().DomainPackage() + " \"" + property.GetReference().DomainPath() + "\""
						imports[path] = path
					}
				}

			}
			d.Import = strings.Join(libutils.MapKeys(imports), "\n")

			buf = &bytes.Buffer{}
			err = commandsTemplate.Execute(buf, d)
			if err != nil {
				fmt.Println(buf)
				fmt.Println("execute ", err)
			}
			content, err = format.Source(buf.Bytes())
			if err != nil {
				fmt.Println("model ", err)
				content = buf.Bytes()
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("gen/commands/%s.gen.go", strcase.ToSnake(definition.Name)),
				content, 0644)
			if err != nil {
				fmt.Println(err)
			}
		}

		imports = map[string]string{}
		for _, query := range definition.Queries {
			for _, property := range query.Args {
				switch v := property.(type) {
				case *arguments.GetPropertiesDefinition:
					path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
					imports[path] = path
				case *arguments.IDDefinition:
					path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
					imports[path] = path
				case *arguments.EntityDefinition:
					path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
					imports[path] = path
				case *arguments.SliceDefinition:
					switch v := v.Argument.(type) {
					case *arguments.IDDefinition:
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
					case *arguments.EntityDefinition:
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					case *arguments.ValueObjectDefinition:
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					}
				}
			}
		}
		for _, query := range definition.Queries {
			for _, results := range query.Results {
				if results.GetReference() != nil {
					path := results.GetReference().DomainPackage() + " \"" + results.GetReference().DomainPath() + "\""
					imports[path] = path
				}
			}
		}
		for _, repository := range definition.Repositories {
			path := repository.Package + " \"" + repository.Path + "\""
			imports[path] = path
			path = repository.Package + "identities \"" + repository.Path + "/identities\""
			imports[path] = path
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		buf = &bytes.Buffer{}
		err = queriesTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/queries/%s.gen.go", strcase.ToSnake(definition.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		imports = map[string]string{}
		for _, representation := range definition.Representations {
			for _, property := range representation.Properties {
				switch v := property.(type) {
				case *properties.EntityDefinition:
					if v.ReferenceDefinition.DomainPath() != representation.DomainPath() {
						path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
						imports[path] = path
					}
				case *properties.IDDefinition:
					if v.ReferenceDefinition.DomainPath() != representation.DomainPath() {
						path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
						imports[path] = path
					}
				case *properties.SliceDefinition:
					switch v := v.PropertyDefinition.(type) {
					case *properties.IDDefinition:
						if v.ReferenceDefinition.DomainPath() != representation.DomainPath() {
							path := v.ReferenceDefinition.DomainPackage() + "identities \"" + v.ReferenceDefinition.DomainPath() + "/identities\""
							imports[path] = path
						}
					case *properties.EntityDefinition:
						if v.ReferenceDefinition.DomainPath() != representation.DomainPath() {
							path := v.ReferenceDefinition.DomainPackage() + " \"" + v.ReferenceDefinition.DomainPath() + "\""
							imports[path] = path
						}
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		d.Repository = d.Repository + "/gen/data"
		buf = &bytes.Buffer{}
		err = representationsTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/data/%s.gen.go", strcase.ToSnake(definition.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

	}

	d := struct {
		Package    string
		Repository string
		Services   []*libapplication.ServiceDefinition
	}{
		Package:    defs.Package,
		Repository: defs.Repository,
		Services:   services,
	}
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("gen/init.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

}

var initFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var queryFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var commandsFuncs = template.New("").Funcs(template.FuncMap{
	"computeImports": func(service *libapplication.ServiceDefinition) string {
		imports := map[libdomain.TableInterface]libdomain.TableInterface{}
		for _, command := range service.Commands {
			for _, property := range command.Args {
				switch v := property.(type) {
				case *arguments.GetPropertiesDefinition:
					imports[v.ReferenceDefinition] = v.ReferenceDefinition
				}
			}
		}

		var results []string
		for _, value := range imports {
			results = append(results, fmt.Sprintf(`type Get%sProperties = %s.Get%sProperties`, value.Title(), value.DomainPackage(), value.Title()))
		}

		return strings.Join(results, "\n")
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var queriesFuncs = template.New("").Funcs(template.FuncMap{
	"computeImports": func(service *libapplication.ServiceDefinition) string {
		imports := map[libdomain.TableInterface]libdomain.TableInterface{}
		for _, command := range service.Commands {
			for _, property := range command.Args {
				switch v := property.(type) {
				case *arguments.GetPropertiesDefinition:
					imports[v.ReferenceDefinition] = v.ReferenceDefinition
				}
			}
		}

		var results []string
		for _, value := range imports {
			results = append(results, fmt.Sprintf(`type Get%sProperties = %s.Get%sProperties`, value.Title(), value.DomainPackage(), value.Title()))
		}

		return strings.Join(results, "\n")
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var representationsFuncs = template.New("").Funcs(template.FuncMap{
	"computeImports": func(service *libapplication.ServiceDefinition) string {
		imports := map[libdomain.TableInterface]libdomain.TableInterface{}
		for _, command := range service.Commands {
			for _, property := range command.Args {
				switch v := property.(type) {
				case *arguments.GetPropertiesDefinition:
					imports[v.ReferenceDefinition] = v.ReferenceDefinition
				}
			}
		}

		var results []string
		for _, value := range imports {
			results = append(results, fmt.Sprintf(`type Get%sProperties = %s.Get%sProperties`, value.Title(), value.DomainPackage(), value.Title()))
		}

		return strings.Join(results, "\n")
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

// nolint:lll
var aggregateFuncs = template.New("").Funcs(template.FuncMap{
	"getTargetRepositories": func(service *libapplication.ServiceDefinition) map[string]libdomain.TableInterface {
		results := map[string]libdomain.TableInterface{}
		invalid := map[string]bool{}
		for _, repository := range service.Repositories {
			for _, entity := range repository.AggregateDefinition.GetEntities() {
				invalid[entity.Name] = true
			}
		}
		for _, repository := range service.Repositories {
			for _, entityWithIds := range repository.AggregateDefinition.EntitiesWithIDFields() {
				if _, ok := invalid[entityWithIds.Entity.GetName()]; !ok && entityWithIds.Entity.IsAggregateRoot() {
					results[entityWithIds.Entity.GetName()] = entityWithIds.Entity
				}
			}
		}

		return results
	},
	"isInThisService": func(service *libapplication.ServiceDefinition, entitySource *libdomain.EntityDefinition) bool {
		for _, repository := range service.Repositories {
			for _, entity := range repository.AggregateDefinition.GetEntities() {
				if entity.Name == entitySource.Name {
					return true
				}
			}
		}
		return false
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

