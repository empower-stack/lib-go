package ptypes

import (
	// "fmt"
	// "time"

	"github.com/gogo/protobuf/types"
	// "github.com/golang/protobuf/ptypes"
)

// FieldMask is the main type you can use to represent timestamp
type FieldMask struct {
	*types.FieldMask
}

// Unmarshal will unmarshal bytes into FieldMask object
func (m *FieldMask) Unmarshal(b []byte) error {
	m.FieldMask = &types.FieldMask{}
	return m.FieldMask.XXX_Unmarshal(b)
}

// func (m *Timestamp) Unmarshal(b []byte) error {
// 	fmt.Println("unmarshal", b)
// 	m.Timestamp = &types.Timestamp{}
// 	return m.Timestamp.XXX_Unmarshal(b)
// }

// func (m *Timestamp) Marshal(b []byte, deterministic bool) ([]byte, error) {
// 	fmt.Println("marchasl", b, deterministic)
// 	res, err := m.Timestamp.XXX_Marshal(b, deterministic)
// 	fmt.Println("marchasl", res, err)
// 	return res, err
// }
// func (m *Timestamp) MarshalTo(dAtA []byte) (int, error) {
// 	test, err := m.Timestamp.XXX_Marshal(dAtA, false)
// 	fmt.Println("marchasl", test, err, m.Size())
// 	dAtA, err = m.Timestamp.XXX_Marshal(dAtA, false)
// 	return m.Size(), err

// 	fmt.Println("marchaslTO", dAtA)
// 	var i int
// 	_ = i
// 	var l int
// 	_ = l
// 	if m.Seconds != 0 {
// 		dAtA[i] = 0x40
// 		i++
// 		i = encodeVarintMain(dAtA, i, uint64(m.Seconds))
// 	}
// 	if m.Nanos != 0 {
// 		dAtA[i] = 0x40
// 		i++
// 		i = encodeVarintMain(dAtA, i, uint64(m.Nanos))
// 	}
// 	fmt.Println("marchaslTO end", dAtA)
// 	return i, nil
// }
// func (m *Timestamp) Size() int {
// 	return m.XXX_Size()
// }

// func (m *Timestamp) Scan(src interface{}) error {
// 	tspb, err := types.TimestampProto(src.(time.Time))
// 	if err != nil {
// 		return err
// 	}
// 	m.Timestamp = tspb
// 	return nil
// }

// func (m *Timestamp) Set(src interface{}) error {
// 	m.Nanos = src.(int32)
// 	return nil
// }

// func (m *Timestamp) Get() interface{} {
// 	return m.Seconds
// }

// func encodeVarintMain(dAtA []byte, offset int, v uint64) int {
// 	for v >= 1<<7 {
// 		dAtA[offset] = uint8(v&0x7f | 0x80)
// 		v >>= 7
// 		offset++
// 	}
// 	dAtA[offset] = uint8(v)
// 	return offset + 1
// }
