package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	// "gitlab.com/empowerlab/stack/lib-go/libdata/fields"

	"gitlab.com/empowerlab/stack/lib-go/libanemic"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed *.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Services(defs *libanemic.Definitions) {

	var err error

	fileDomain, _ := fdomain.ReadFile("domain.go.tmpl")
	domainTemplate := template.Must(domainFuncs.Parse(string(fileDomain)))

	fileDomainImpls, _ := fdomain.ReadFile("domain-impls.go.tmpl")
	domainImplsTemplate := template.Must(domainImplsFuncs.Parse(string(fileDomainImpls)))

	fileApplicationGenerator, _ := fdomain.ReadFile("application-generator.go.tmpl")
	applicationGeneratorTemplate := template.Must(applicationGeneratorFuncs.Parse(string(fileApplicationGenerator)))

	fileApplication, _ := fdomain.ReadFile("application.go.tmpl")
	applicationTemplate := template.Must(applicationFuncs.Parse(string(fileApplication)))

	fileApplicationImpls, _ := fdomain.ReadFile("application-impls.go.tmpl")
	applicationImplsTemplate := template.Must(applicationImplsFuncs.Parse(string(fileApplicationImpls)))

	fileDomainGenerator, _ := fdomain.ReadFile("domain-generator.go.tmpl")
	domainGeneratorTemplate := template.Must(domainGeneratorFuncs.Parse(string(fileDomainGenerator)))

	fileRepository, _ := fdomain.ReadFile("repository.go.tmpl")
	repositoryTemplate := template.Must(repositoryFuncs.Parse(string(fileRepository)))

	fileRepositoryImpls, _ := fdomain.ReadFile("repository-impls.go.tmpl")
	repositoryImplsTemplate := template.Must(repositoryImplsFuncs.Parse(string(fileRepositoryImpls)))

	fileTranslator, _ := fdomain.ReadFile("presentation-translator.go.tmpl")
	translatorTemplate := template.Must(translatorFuncs.Parse(string(fileTranslator)))

	fileTranslatorGenerator, _ := fdomain.ReadFile("presentation-generator.go.tmpl")
	translatorGeneratorTemplate := template.Must(translatorGeneratorFuncs.Parse(string(fileTranslatorGenerator)))

	fileGraphql, err := fdomain.ReadFile("presentation-graphql.go.tmpl")
	graphqlTemplate := template.Must(graphqlFuncs.Parse(string(fileGraphql)))

	err = os.MkdirAll("../gen", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	var services []*libanemic.ServiceDefinition
	for _, definition := range defs.Slice() {

		d := struct {
			Path    string
			Service *libanemic.ServiceDefinition
			// AggregateRootField    libdata.Field
			// AggregateNestedField  libdata.Field
			// Model                 *libdata.ModelDefinition
			// CustomCommandsAggregate  []*libpersistence.CustomRequest
			// CustomCommandsCollection []*libpersistence.CustomRequest
			Import string
		}{
			Path:    defs.Path,
			Service: definition,
		}

		err = os.MkdirAll(
			fmt.Sprintf("../gen/domains/domains/%s/definitions", strcase.ToSnake(definition.Name)),
			os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}

		err = os.MkdirAll(
			fmt.Sprintf("../gen/domains/domains/%s/implementations", strcase.ToSnake(definition.Name)),
			os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}

		imports := map[string]string{}
		// for _, function := range definition.Aggregate.RepositoryFunctions {
		// 	for _, property := range function.Args {
		// 		switch v := property.(type) {
		// 		case *properties.Entity, *properties.GetProperties:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
		// 				imports[path] = path
		// 			}
		// 		case *properties.ID:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
		// 				imports[path] = path
		// 			}
		// 		}
		// 	}
		// }
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		buf := &bytes.Buffer{}
		err := domainTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("../gen/domains/domains/%s/definitions/definitions.gen.go", strcase.ToSnake(definition.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		buf = &bytes.Buffer{}
		err = domainGeneratorTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("../gen/domains/domains/%s/generator.gen.go", strcase.ToSnake(definition.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		err = os.MkdirAll(
			fmt.Sprintf("../gen/repository/implementations"),
			os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}

		for _, aggregate := range definition.Aggregates {

			dd := struct {
				Path            string
				PersistencePath string
				Service         *libanemic.ServiceDefinition
				Aggregate       *libpersistence.AggregateDefinition
				// AggregateRootField    libdata.Field
				// AggregateNestedField  libdata.Field
				// Model                 *libdata.ModelDefinition
				// CustomCommandsAggregate  []*libpersistence.CustomRequest
				// CustomCommandsCollection []*libpersistence.CustomRequest
				Import string
			}{
				Path:            defs.Path,
				PersistencePath: defs.PersistencePath,
				Service:         definition,
				Aggregate:       aggregate,
			}

			buf = &bytes.Buffer{}
			err = domainImplsTemplate.Execute(buf, dd)
			if err != nil {
				fmt.Println(buf)
				fmt.Println("execute ", err)
			}
			content, err = format.Source(buf.Bytes())
			if err != nil {
				fmt.Println("model ", err)
				content = buf.Bytes()
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("../gen/domains/domains/%s/implementations/%s.gen.go", strcase.ToSnake(definition.Name), aggregate.AggregateRoot.Snake()),
				content, 0644)
			if err != nil {
				fmt.Println(err)
			}

			buf = &bytes.Buffer{}
			err = repositoryImplsTemplate.Execute(buf, dd)
			if err != nil {
				fmt.Println(buf)
				fmt.Println("execute ", err)
			}
			content, err = format.Source(buf.Bytes())
			if err != nil {
				fmt.Println("model ", err)
				content = buf.Bytes()
			}
			// err = ioutil.WriteFile(
			// 	fmt.Sprintf("../gen/repository/implementations/%s.gen.go", strcase.ToSnake(aggregate.AggregateRoot.Name)),
			// 	content, 0644)
			// if err != nil {
			// 	fmt.Println(err)
			// }

		}

		services = append(services, definition)

	}

	err = os.MkdirAll(
		fmt.Sprintf("../gen/domains/application/definitions"),
		os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll(
		fmt.Sprintf("../gen/domains/application/implementations"),
		os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	d := struct {
		Path     string
		Services []*libanemic.ServiceDefinition
	}{
		Path:     defs.Path,
		Services: services,
	}
	buf := &bytes.Buffer{}
	err = applicationTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/domains/application/definitions/definitions.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	for _, service := range services {

		d := struct {
			Path    string
			Service *libanemic.ServiceDefinition
		}{
			Path:    defs.Path,
			Service: service,
		}

		buf := &bytes.Buffer{}
		err = applicationImplsTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("../gen/domains/application/implementations/%s.gen.go", strcase.ToSnake(service.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}
	}

	buf = &bytes.Buffer{}
	err = applicationGeneratorTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/domains/application/generator.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll(
		fmt.Sprintf("../gen/repository/definitions"),
		os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf = &bytes.Buffer{}
	err = repositoryTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/repository/definitions/definitions.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll(
		fmt.Sprintf("../gen/presentation/translator/definitions"),
		os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf = &bytes.Buffer{}
	err = translatorTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/presentation/translator/definitions/definitions.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	buf = &bytes.Buffer{}
	err = translatorGeneratorTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/presentation/translator/generator.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	err = os.MkdirAll(
		fmt.Sprintf("../gen/presentation/graphql/definitions"),
		os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf = &bytes.Buffer{}
	err = graphqlTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/presentation/graphql/definitions/definitions.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

}

var checkReservedProperty = func(property libpersistence.Property) bool {
	if property.GetName() == "archived" {
		return false
	}
	if property.GetName() == "createdAt" {
		return false
	}
	if property.GetName() == "createdBy" {
		return false
	}
	if property.GetName() == "updatedAt" {
		return false
	}
	if property.GetName() == "updatedBy" {
		return false
	}
	if property.GetName() == "archivedAt" {
		return false
	}
	if property.GetName() == "archivedBy" {
		return false
	}
	if property.GetName() == "version" {
		return false
	}
	return true
}

var domainFuncs = template.New("").Funcs(template.FuncMap{
	"checkReservedProperty": checkReservedProperty,
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var domainImplsFuncs = template.New("").Funcs(template.FuncMap{
	"checkReservedProperty": checkReservedProperty,
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var applicationGeneratorFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var applicationFuncs = template.New("").Funcs(template.FuncMap{
	"checkReservedProperty": checkReservedProperty,
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var applicationImplsFuncs = template.New("").Funcs(template.FuncMap{
	"checkReservedProperty": checkReservedProperty,
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var domainGeneratorFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var repositoryFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var repositoryImplsFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var translatorFuncs = template.New("").Funcs(template.FuncMap{
	"checkReservedProperty": checkReservedProperty,
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var translatorGeneratorFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var graphqlFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})
