package nats

import (
	"context"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

type ClientInfo struct {
	URL string
}

func NewClient(info *ClientInfo) (*Client, error) {

	nc, err := nats.Connect(info.URL)
	if err != nil {
		return nil, err
	}

	// Create JetStream Context
	// js, _ := nc.JetStream(nats.PublishAsyncMaxPending(256))
	js, err := jetstream.New(nc)
	if err != nil {
		return nil, err
	}

	return &Client{
		nc: nc,
		js: js,
	}, nil
}

// Driver contains all the function needed to be recognized as a libdata driver
type Client struct {
	nc *nats.Conn
	js jetstream.JetStream
}

func (c *Client) Connection() *nats.Conn {
	return c.nc
}

func (c *Client) DispatchEvent(ctx context.Context, aggregate string, payload []byte) error {

	err := c.Publish(aggregate, payload)
	// fmt.Printf("ack = %+v\n", ack)
	if err != nil {
		return err
	}

	return nil

}

func (c *Client) Subscribe(ctx context.Context, consumer string, stream string, subject string, function func([]byte) error) (interface{}, error) {

	// create a consumer (this is an idempotent operation)
	cons, err := c.js.CreateOrUpdateConsumer(ctx, stream, jetstream.ConsumerConfig{
		Durable:       consumer,
		FilterSubject: subject,
		AckPolicy:     jetstream.AckExplicitPolicy,
	})
	if err != nil {
		return nil, err
	}
	fmt.Printf("Consumer %s %s created: %+v\n", consumer, subject, cons)

	sub, err := cons.Consume(func(msg jetstream.Msg) {
		fmt.Printf("Received a JetStream message: %s\n", string(msg.Data()))

		err := function(msg.Data())
		if err != nil {
			fmt.Println("Error computing message : ", err)
		} else {
			msg.Ack()
		}

	// }, jetstream.PullMaxMessages(10))
	}, jetstream.PullMaxBytes(65536), jetstream.ConsumeErrHandler(func(consumeCtx jetstream.ConsumeContext, err error) {
		fmt.Println("Error in consumer: ", err)
	}))
	if err != nil {
		return nil, err
	}
	fmt.Printf("Consumer %s %s created: %+v\n", consumer, subject, sub)

	// fetchResult, err := cons.Fetch(2, jetstream.FetchMaxWait(2*time.Second))
	// if err != nil {
	// 	return nil, err
	// }
	// for msg := range fetchResult.Messages() {
	// 	fmt.Printf("received %q\n", msg.Data())
		
	// 	err := function(msg.Data())
	// 	if err != nil {
	// 		fmt.Println("Error computing message : ", err)
	// 	} else {
	// 		msg.Ack()
	// 	}
	// }

	return sub, nil

	// c.js.DeleteStream(stream)

	// _, err := c.js.AddStream(&nats.StreamConfig{
	// 	Name:     stream,
	// 	Subjects: []string{aggregate},
	// })
	// if err != nil {
	// 	return nil, err
	// }

	// sub, err := c.js.Subscribe(aggregate, func(m *nats.Msg) {
	// 	fmt.Printf("Received a JetStream message: %s\n", string(m.Data))

	// 	function(m.Data)
	// })
	// if err != nil {
	// 	return nil, err
	// }

	// return sub, nil
}

func (c *Client) NewInbox() string {
	return c.nc.NewInbox()
}

func (c *Client) SubscribeSync(stream string) (libdomain.EventDispatcherSubscription, error) {

	sub, err := c.nc.SubscribeSync(stream)
	if err != nil {
		fmt.Println("yup")
		return nil, err
	}

	return &Subscription{
		sub: sub,
	}, nil
}

func (c *Client) Publish(subject string, message []byte) error {

	ackF, err := c.js.PublishMsgAsync(&nats.Msg{
		Data:    message,
		Subject: subject,
	})
	if err != nil {
		return err
	}

	// block and wait for ack
	select {
	case ack := <-ackF.Ok():
		fmt.Printf("Published msg with sequence number %d on stream %q", ack.Sequence, ack.Stream)
	case err := <-ackF.Err():
		fmt.Println(err)
	}

	// err := c.nc.Publish(uniqueReplyTo, message)
	// if err != nil {
	// 	return err
	// }

	return nil
}

type Subscription struct {
	sub *nats.Subscription
}

func (s *Subscription) NextMsg(timeout time.Duration) ([]byte, error) {

	msg, err := s.sub.NextMsg(timeout)
	if err != nil {
		return []byte{}, err
	}

	return msg.Data, nil
}
