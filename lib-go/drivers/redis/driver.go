package redis

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

type ClientInfo struct {
	Addr     string
	Password string
	DB       int
}

func NewClient(info *ClientInfo) (*Client, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     info.Addr,
		Password: info.Password,
		DB:       info.DB,
	})

	return &Client{
		connection: rdb,
	}, nil
}

// Driver contains all the function needed to be recognized as a libdata driver
type Client struct {
	connection *redis.Client
}

func (c *Client) Set(ctx context.Context, key string, value []byte, expiration time.Duration) error {

	err := c.connection.Set(ctx, key, value, 0).Err()
	if err != nil {
		return err
	}

	return nil

}

func (c *Client) Get(ctx context.Context, key string) ([]byte, error) {

	// fmt.Println(c.connection)
	// fmt.Println(ctx)

	val, err := c.connection.Get(ctx, key).Result()
	if err != nil {
		return nil, err
	}

	return []byte(val), nil

}

func (c *Client) Invalidate(ctx context.Context, key string) error {

	// iter := c.connection.Scan(ctx, 0, key ,0).Iterator()
	// fmt.Printf("After Scan %s %s\n", key, time.Now())

	// for iter.Next(ctx) {
	// 	key := iter.Val()

	// 	fmt.Printf("Before del %s %s\n", key, time.Now())
	// 	err := c.connection.Del(ctx, key).Err()
	// 	if err != nil {
	// 		return err
	// 	}
	// }

	err := c.connection.Del(ctx, key).Err()
	if err != nil {
		panic(err)
	}

	return nil

}
