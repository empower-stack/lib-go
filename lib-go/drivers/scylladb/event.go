package scylladb

import (

	// libuuid "github.com/google/uuid"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// RegisterEvents([]*Event) (interface{}, error)
// 	MarkDispatchedEvent(*Event) (interface{}, error)

var EventDefinition = &libpersistence.TableDefinition{
	Name: "event",
	Properties: []libpersistence.Property{
		// &properties.ID{Name: "tenantID", LoadedPosition: 3, Position: 4},
		// &properties.Text{Name: "aggregate", Required: true, LoadedPosition: 3, Position: 4},
		// &properties.ID{Name: "aggregateID", Required: true, LoadedPosition: 3, Position: 4},
		// &properties.Integer{Name: "aggregateVersion", Required: true, LoadedPosition: 3, Position: 4},
		// &properties.Text{Name: "event", Required: true, LoadedPosition: 3, Position: 4},
		// &properties.Integer{Name: "eventVersion", Required: true, LoadedPosition: 3, Position: 4},
		// &properties.Text{Name: "payload", Required: true, LoadedPosition: 3, Position: 4},
		// &properties.Boolean{Name: "dispatched", Required: true, LoadedPosition: 3, Position: 4},
	},
}

// RegisterEvent will register the event in the event model.
func (d *Client) RegisterEvents(
	wk *libapplication.ApplicationContext,
	a interface{},
	events []*libapplication.Event) error {
	// e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
	// 	aggregate, aggregateID, name, payload)
	// fmt.Printf("event %+v", e)

	// for _, event := range events {
	// 	aggregate := event.Aggregate
	// 	data := event.Data
	// 	payloadJson, err := json.Marshal(data.Payload)
	// 	if err != nil {
	// 		return err
	// 	}

	// 	// tx := &Tx{
	// 	// 	Sqlx: d.connection.MustBegin()}

	// 	var tenantID interface{}
	// 	if data.TenantID != "" {
	// 		tenantID = data.TenantID
	// 	}

	// 	q := "INSERT INTO event (id, tenant_id, aggregate, aggregate_id, event, event_version, payload, aggregate_version, dispatched) VALUES (:id, :tenant_id, :aggregate, :aggregate_id, :event, :event_version, :payload, :aggregate_version, :dispatched)"
	// 	args := map[string]interface{}{
	// 		"id":                data.ID,
	// 		"tenant_id":         tenantID,
	// 		"aggregate":         aggregate.AggregateRoot.Name,
	// 		"aggregate_id":      data.AggregateID,
	// 		"event":             data.Event,
	// 		"event_version":     data.EventVersion,
	// 		"payload":           payloadJson,
	// 		"aggregate_version": data.AggregateVersion,
	// 		"dispatched":        false,
	// 	}
	// 	// if a.Definition().UseTenants {
	// 	// 	q = "INSERT INTO event (tenant_id, aggregate, aggregate_id, action, payload) VALUES (:tenant_id, :aggregate, :aggregate_id, :action, :payload) RETURNING id"
	// 	// 	args["tenant_id"] = wk.Workflow().TenantID
	// 	// }
	// 	fmt.Println(q)
	// 	rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// 	// event := &libdata.Event{
	// 	// 	Aggregate: aggregate,
	// 	// }
	// 	for rows.Next() {
	// 		err = rows.Scan(
	// 		// &event.ID,
	// 		// &event.AggregateUUID,
	// 		// &event.Name,
	// 		// &payloadJson,
	// 		)
	// 		if err != nil {
	// 			// tx.Sqlx.Rollback()
	// 			return errors.Wrap(err, "Couldn't scan")
	// 		}
	// 	}
	// 	if err != nil {
	// 		// tx.Sqlx.Rollback()
	// 		return errors.Wrap(err, "Couldn't insert")
	// 	}

	// 	// err = json.Unmarshal(payloadJson, &event.Payload)
	// 	// if err != nil {
	// 	// 	// tx.Sqlx.Rollback()
	// 	// 	return nil, errors.Wrap(err, "Couldn't insert")
	// 	// }

	// 	// tx.Sqlx.Commit()
	// }

	return nil
	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
}

func (d *Client) MarkDispatchedEvent(
	wk *libapplication.ApplicationContext,
	event *libapplication.Event,
) error {

	// tx := &Tx{
	// 	Sqlx: d.connection.MustBegin()}

	// q := "UPDATE event set dispatched = True WHERE id = :id"
	// args := map[string]interface{}{
	// 	"id": event.Data.ID,
	// 	// "id":           libuuid.New().String(),
	// 	// "aggregate":    event.Aggregate.Definition().AggregateRoot.Name,
	// 	// "tenant_id":    wk.Workflow().TenantID,
	// 	// "aggregate_id": event.AggregateID,
	// }
	// rows, err := tx.QueryRows(q, args)
	// for rows.Next() {
	// }
	// if err != nil {
	// 	tx.Sqlx.Rollback()
	// 	return err
	// }
	// tx.Sqlx.Commit()

	return nil
}

func (d *Client) GetAggregateHistory(
	wk *libapplication.ApplicationContext,
	a *libdomain.AggregateDefinition, id string,
) ([]*libdomain.EventHistory, error) {

	return nil, nil
}

func (d *Client) GetAggregateEvents(
	wk *libapplication.ApplicationContext,
	a *libdomain.AggregateDefinition, id string,
) ([]*libapplication.Event, error) {

	// return nil, nil

	// // tx := &Tx{
	// // 	Sqlx: d.connection.MustBegin()}

	// q := "SELECT id, tenant_id, aggregate_id, event, payload FROM event WHERE aggregate = :aggregate AND tenant_id = :tenant_id AND aggregate_id = :id"
	// args := map[string]interface{}{
	// 	"aggregate": a.AggregateRoot.Name,
	// 	"id":        id,
	// 	"tenant_id": wk.Workflow().TenantID,
	// }
	// // if a.Definition().UseTenants {
	// // 	q = q + " AND tenant_id = :tenant_id"
	// // 	args["tenant_id"] = wk.Workflow().TenantID
	// // }
	// fmt.Println(q)
	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// // event := &libdata.Event{
	// // 	Aggregate: aggregate,
	// // }
	// var events []*libapplication.Event
	// for rows.Next() {
	// 	data := &libapplication.EventData{}
	// 	var payload string
	// 	err = rows.Scan(
	// 		&data.ID,
	// 		&data.TenantID,
	// 		&data.AggregateID,
	// 		&data.Event,
	// 		&payload,
	// 		// &event.AggregateUUID,
	// 		// &event.Name,
	// 		// &payloadJson,
	// 	)
	// 	if err != nil {
	// 		// tx.Sqlx.Rollback()
	// 		return nil, errors.Wrap(err, "Couldn't scan")
	// 	}

	// 	err = json.Unmarshal([]byte(payload), &data.Payload)
	// 	if err != nil {
	// 		// tx.Sqlx.Rollback()
	// 		return nil, err
	// 	}

	// 	event := &libapplication.Event{
	// 		Aggregate: a,
	// 		Data:      data,
	// 	}

	// 	events = append(events, event)
	// }
	// if err != nil {
	// 	// tx.Sqlx.Rollback()
	// 	return nil, errors.Wrap(err, "Couldn't insert")
	// }

	// return events, nil

	return []*libapplication.Event{}, nil
}

// func (d *Driver) EventsOK(
// 	wk *libdata.Workflow, aggregate string, aggregateID string, name string, payload []byte,
// ) (libdata.EventInterface, error) {
// 	// e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
// 	// 	aggregate, aggregateID, name, payload)
// 	// fmt.Printf("event %+v", e)
// 	return nil, nil
// 	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
// }

// func (d *Driver) EventsKO(
// 	wk *libdata.Workflow, aggregate string, aggregateID string, name string, payload []byte,
// ) (libdata.EventInterface, error) {
// 	// e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
// 	// 	aggregate, aggregateID, name, payload)
// 	// fmt.Printf("event %+v", e)
// 	return nil, nil
// 	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
// }
