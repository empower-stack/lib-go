package postgres

import (
	// "context"
	// "encoding/json"
	"fmt"

	// "gitlab.com/empowerlab/stack/lib-go/libdomain"

	// libid "github.com/google/id"
	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	// "gitlab.com/empowerlab/stack/lib-go/libapplication"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

// var WorkflowFunctions map[string]map[string]func(workflow.Context, interface{}) (interface{}, error)

// var ActivityFunctions map[string]func(context.Context, ...interface{}) (interface{}, error)

// type Worker struct{}

// func (w *Worker) RegisterWorkflow(model string, functionName string, wf func(workflow.Context, interface{}) (interface{}, error)) error {

// 	return nil
// }
// func (w *Worker) RegisterActivity(name string, a func(context.Context, ...interface{}) (interface{}, error)) error {

// 	return nil
// }
// func (w *Worker) Run(i <-chan interface{}) error {
// 	return nil
// }

func (c *Driver) Close() {
	return
}
// func (c *Driver) GetWorker(taskQueue interface{}, options interface{}) libdata.WorkerInterface {
// 	return nil
// }

func (c *Driver) GetOrchestratorTransaction() interface{} {
	return &Tx{Sqlx: c.connection.MustBegin()}
}

func (c *Driver) RollbackOrchestratorTransaction(tx interface{}) {
	tx.(*Tx).Sqlx.Rollback()
}

func (c *Driver) CommitOrchestratorTransaction(tx interface{}) {
	tx.(*Tx).Sqlx.Commit()
}

func (c *Driver) SearchWorkflow(wk *libapplication.ApplicationContext, idempotencyKey string) (string, error) {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	query := "SELECT id FROM \"orchestrator_workflow\" WHERE idempotency_key = :idempotency_key"
	rows, err := tx.QueryRows(wk, query, map[string]interface{}{
		"idempotency_key": idempotencyKey,
	})
	if err != nil {
		return "", errors.Wrap(err, "Couldn't get record")
	}

	var id string
	for rows.Next() {
		err = rows.Scan(
			&id,
		)
		if err != nil {
			return "", errors.Wrap(err, "Couldn't scan")
		}
	}
	return id, nil
}

func (c *Driver) NewWorkflow(
	wk *libapplication.ApplicationContext,
	name string, idempotencyKey string, serializedArgs []byte,
) (string, error) {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	var id string
	q := "INSERT INTO orchestrator_workflow (name, idempotency_key, args_binary, state) VALUES (:name, :idempotency_key, :args_binary, :state) RETURNING id"
	fmt.Println(q)
	rows, err := tx.QueryRows(wk, q, map[string]interface{}{
		// "id":            libid.New().String(),
		"name":            name,
		"idempotency_key": idempotencyKey,
		"args_binary":     serializedArgs,
		"state":           "new",
	})
	if err != nil {
		return "", err
	}
	for rows.Next() {
		err = rows.Scan(
			&id,
		)
		if err != nil {
			return "", errors.Wrap(err, "Couldn't scan")
		}
	}
	return id, nil
}

func (c *Driver) GetWorkflow(wk *libapplication.ApplicationContext, id string) (
	string, string, []byte, []byte, error,
) {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	query := "SELECT id, name, args_binary, result_binary FROM \"orchestrator_workflow\" WHERE id = :id"
	rows, err := tx.QueryRows(wk, query, map[string]interface{}{
		"id": id,
	})
	if err != nil {
		return "", "", nil, nil, errors.Wrap(err, "Couldn't get record")
	}

	var name string
	var argsBinary []byte
	var resultFromDatabase []byte
	for rows.Next() {
		fmt.Println("test")
		err = rows.Scan(
			&id,
			&name,
			&argsBinary,
			&resultFromDatabase,
		)
		if err != nil {
			return "", "", nil, nil, errors.Wrap(err, "Couldn't scan")
		}
	}
	if name == "" {
		return "", "", nil, nil, errors.New("Can't find workflow in database")
	}
	return id, name, argsBinary, resultFromDatabase, nil
}

func (c *Driver) OnWorkflowError(wk *libapplication.ApplicationContext, name string) error {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	q := fmt.Sprintf(
		"UPDATE \"orchestrator_workflow\" SET state='failed' WHERE name=:name RETURNING *")
	rows, err := tx.QueryRows(wk, q, map[string]interface{}{
		"name": name,
	})
	if err != nil {
		return errors.Wrap(err, "Couldn't update workflow")
	}
	for rows.Next() {
	}

	// q = fmt.Sprintf(
	// 	"UPDATE \"orchestrator_activity\" SET state='compensated' WHERE workflow_id=:name RETURNING *")
	// rows, err = tx.QueryRows(q, map[string]interface{}{
	// 	"name": name,
	// })
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't update workflow")
	// }
	// for rows.Next() {
	// }

	return nil
}

func (c *Driver) OnWorkflowSuccess(wk *libapplication.ApplicationContext, name string, serializedResult []byte) error {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	q := fmt.Sprintf(
		"UPDATE \"orchestrator_workflow\" SET result_binary=:result, state='done' WHERE name=:name RETURNING *")
	rows, err := tx.QueryRows(wk, q, map[string]interface{}{
		"result": serializedResult,
		"name":   name,
	})
	if err != nil {
		return errors.Wrap(err, "Couldn't update workflow")
	}
	for rows.Next() {
	}

	return nil
}

func (c *Driver) SearchActivity(
	wk *libapplication.ApplicationContext,
	workflowUUID string, step int,
) ([]byte, error) {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	query := "SELECT result_binary FROM \"orchestrator_activity\" WHERE orchestrator_workflow_id = :workflow_id AND step = :step"
	rows, err := tx.QueryRows(wk, query, map[string]interface{}{
		"workflow_id": workflowUUID,
		"step":          step,
	})
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get record")
	}

	var resultBinary []byte
	for rows.Next() {
		err = rows.Scan(
			&resultBinary,
		)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't scan")
		}
	}

	return resultBinary, nil
}

// func (c *Driver) NewActivity(
// 	workflowUUID string, step int64, serializedArgs []byte,
// ) (string, error) {

// 	var err error
// 	tx := &Tx{Sqlx: c.connection.MustBegin()}
// 	defer func() {
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 		} else {
// 			tx.Sqlx.Commit()
// 		}
// 	}()

// 	q := "INSERT INTO orchestrator_activity (id, orchestrator_workflow_id, step, transaction, args_binary, state) VALUES (:id, :workflow_id, :step, True, :args, :state) RETURNING id"
// 	fmt.Println(q)
// 	rows, err := tx.QueryRows(q, map[string]interface{}{
// 		"id":        libid.New().String(),
// 		"workflow_id": workflowUUID,
// 		"step":        step,
// 		"args":        serializedArgs,
// 		"state":       "new",
// 	})
// 	var id string
// 	for rows.Next() {
// 		err = rows.Scan(
// 			&id,
// 		)
// 		if err != nil {
// 			return "", errors.Wrap(err, "Couldn't scan")
// 		}
// 	}
// 	if err != nil {
// 		return "", errors.Wrap(err, "Couldn't insert")
// 	}
// 	return id, nil
// }

// func (c *Driver) GetActivity(id string) (
// 	string, string, int, []byte, []byte, error,
// ) {

// 	var err error
// 	tx := &Tx{Sqlx: c.connection.MustBegin()}
// 	defer func() {
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 		} else {
// 			tx.Sqlx.Commit()
// 		}
// 	}()

// 	query := "SELECT id, orchestrator_workflow_id, step, args_binary, result_binary FROM \"orchestrator_activity\" WHERE id = :id"
// 	rows, err := tx.QueryRows(query, map[string]interface{}{
// 		"id": id,
// 	})
// 	if err != nil {
// 		return "", "", 0, nil, nil, errors.Wrap(err, "Couldn't get record")
// 	}

// 	var workflowUUID string
// 	var step int
// 	var argsBinary []byte
// 	var resultFromDatabase []byte
// 	for rows.Next() {
// 		err = rows.Scan(
// 			&id,
// 			&workflowUUID,
// 			&step,
// 			&argsBinary,
// 			&resultFromDatabase,
// 		)
// 		if err != nil {
// 			return "", "", 0, nil, nil, errors.Wrap(err, "Couldn't scan")
// 		}
// 	}
// 	if id == "" {
// 		return "", "", 0, nil, nil, errors.New("Can't find activity in database")
// 	}
// 	return id, workflowUUID, step, argsBinary, resultFromDatabase, nil
// }

func (c *Driver) OnActivitySuccess(wk *libapplication.ApplicationContext, workflowUUID string, step int, serializedResult []byte) error {

	var err error
	tx := &Tx{Sqlx: c.connection.MustBegin()}
	defer func() {
		if err != nil {
			tx.Sqlx.Rollback()
		} else {
			tx.Sqlx.Commit()
		}
	}()

	q := "INSERT INTO orchestrator_activity (orchestrator_workflow_id, step, transaction, result_binary, state) VALUES (:workflow_id, :step, True, :result_binary, :state) RETURNING id"
	fmt.Println(q)
	rows, err := tx.QueryRows(wk, q, map[string]interface{}{
		// "id":          libid.New().String(),
		"workflow_id":   workflowUUID,
		"step":          step,
		"result_binary": serializedResult,
		"state":         "done",
	})
	// q := fmt.Sprintf(
	// 	"UPDATE \"orchestrator_activity\" SET result_binary=:result, state='done' WHERE id=:id RETURNING *")
	// rows, err := tx.QueryRows(q, map[string]interface{}{
	// 	"id":   id,
	// 	"result": serializedResult,
	// })
	if err != nil {
		return errors.Wrap(err, "Couldn't update activity")
	}
	for rows.Next() {
	}
	return nil
}

// type WorkflowRun struct {
// 	client   *Driver
// 	context  *libapplication.Workflow
// 	workflow libapplication.WorkflowInterface
// }

// func (r *WorkflowRun) Get(ctx context.Context) (interface{}, error) {

// 	tx := &Tx{
// 		Sqlx: r.client.connection.MustBegin()}

// 	query := "SELECT id, name, args_binary, result_binary FROM \"orchestrator_workflow\" WHERE id = :id"
// 	rows, err := tx.QueryRows(query, map[string]interface{}{
// 		"id": r.context.WorkflowUUID,
// 	})
// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, errors.Wrap(err, "Couldn't get record")
// 	}

// 	var id string
// 	var name string
// 	var argsBinary []byte
// 	var resultFromDatabase []byte
// 	for rows.Next() {
// 		fmt.Println("test")
// 		err = rows.Scan(
// 			&id,
// 			&name,
// 			&argsBinary,
// 			&resultFromDatabase,
// 		)
// 		if err != nil {
// 			return nil, errors.Wrap(err, "Couldn't scan")
// 		}
// 	}
// 	if id == "" {
// 		return nil, errors.New("Can't find workflow in database")
// 	}

// 	tx.Sqlx.Commit()

// 	if len(resultFromDatabase) != 0 {
// 		var result interface{}
// 		err = r.workflow.UnmarshalResult([]byte(resultFromDatabase), result)
// 		if err != nil {
// 			return nil, err
// 		}
// 		return result, nil
// 	}

// 	err = r.workflow.UnmarshalArgs([]byte(argsBinary))
// 	if err != nil {
// 		return nil, err
// 	}

// 	err = r.context.TransactionClient.BeginTransaction(r.context, false)
// 	if err != nil {
// 		return nil, err
// 	}

// 	if r.context.TenantClient != nil {
// 		if r.context.Workflow.TenantUUID == "" {
// 			return nil, errors.New("The tenant UUID must be specified")
// 		}

// 		err := r.context.TenantClient.NewFactoryInterface(r.context.Workflow).CheckUUIDExist(r.context.Workflow.TenantUUID)
// 		if err != nil {
// 			return nil, err
// 		}
// 	}

// 	result, err := r.workflow.Run(r.context)
// 	r.context.TransactionClient.CommitTransaction(r.context)
// 	if err != nil {

// 		if !r.context.TransactionAlwaysCommit {

// 			tx = &Tx{
// 				Sqlx: r.client.connection.MustBegin()}

// 			q := fmt.Sprintf(
// 				"UPDATE \"orchestrator_workflow\" SET state=\"failed\" WHERE name=:name RETURNING *")
// 			rows, err = tx.QueryRows(q, map[string]interface{}{
// 				"name": name,
// 			})
// 			if err != nil {
// 				tx.Sqlx.Rollback()
// 				return nil, errors.Wrap(err, "Couldn't update workflow")
// 			}
// 			for rows.Next() {
// 			}

// 			q = fmt.Sprintf(
// 				"UPDATE \"orchestrator_activity\" SET state=\"compensated\" WHERE workflow_id=:name RETURNING *")
// 			rows, err = tx.QueryRows(q, map[string]interface{}{
// 				"name": name,
// 			})
// 			if err != nil {
// 				tx.Sqlx.Rollback()
// 				return nil, errors.Wrap(err, "Couldn't update workflow")
// 			}
// 			for rows.Next() {
// 			}

// 			tx.Sqlx.Commit()
// 		}
// 		return nil, err
// 	}

// 	serializedResult, err := r.workflow.MarshalResult(result)
// 	if err != nil {
// 		return nil, err
// 	}

// 	for _, event := range r.context.Workflow.GetEvents() {

// 		applicationAggregate := libapplication.MappingAggregate[event.Aggregate.Definition()]

// 		if applicationAggregate.EventStoreClient() != nil {

// 			activityRun, err := r.context.OrchestratorClient.ExecuteActivity(
// 				r.context, &RegisterEventActivity{
// 					Workflow:         r.context,
// 					EventStoreClient: applicationAggregate.EventStoreClient(),
// 					Aggregate:        event.Aggregate,
// 					AggregateUUID:    event.AggregateUUID,
// 					Event:            event.Name,
// 					Payload:          event.Payload,
// 				})
// 			if err != nil {
// 				return nil, err
// 			}
// 			eventResult, err := activityRun.Get(r.context, nil)
// 			if err != nil {
// 				return nil, err
// 			}

// 			event = eventResult.(*libdomain.Event)

// 			if applicationAggregate.EventDispatchClient() != nil {
// 				activityRun, err := r.context.OrchestratorClient.ExecuteActivity(
// 					r.context, &DispatchEventActivity{
// 						Workflow:            r.context,
// 						EventDispatchClient: applicationAggregate.EventDispatchClient(),
// 						Event:               event,
// 					})
// 				if err != nil {
// 					return nil, err
// 				}
// 				_, err = activityRun.Get(r.context, nil)
// 				if err != nil {
// 					return nil, err
// 				}
// 			}

// 		}
// 	}

// 	tx = &Tx{
// 		Sqlx: r.client.connection.MustBegin()}

// 	q := fmt.Sprintf(
// 		"UPDATE \"orchestrator_workflow\" SET result_binary=:result, state='done' WHERE name=:name RETURNING *")
// 	rows, err = tx.QueryRows(q, map[string]interface{}{
// 		"result": serializedResult,
// 		"name":   name,
// 	})
// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, errors.Wrap(err, "Couldn't update workflow")
// 	}
// 	for rows.Next() {
// 	}

// 	tx.Sqlx.Commit()

// 	// if r.context.EventClient != nil {
// 	// 	err = r.context.EventClient.RegisterEvent(
// 	// 		ctx, nil, "{{.Model.Name}}",
// 	// 		r.{{.Model.Title}}.UUID, "{{.Model.Name}}CreateRequestCompleted",
// 	// 		r.workflow.MarshalEvent(), idempotencyKey)
// 	// 	if err != nil {
// 	// 		return err
// 	// 	}

// 	// }

// 	return result, nil
// }

// func (d *Driver) ExecuteWorkflow(
// 	ctx context.Context, m *libdomain.AggregateDefinition, tenantUUID string, idempotencyKey string,
// 	workflow libapplication.WorkflowInterface, options libapplication.StartWorkflowOptions,
// 	transactionClient libapplication.TransactionClient, tenantAggregate libdomain.AggregateInterface,
// ) (libapplication.WorkflowRunInterface, error) {

// 	serializedArgs, err := workflow.MarshalArgs()
// 	if err != nil {
// 		return nil, err
// 	}

// 	tx := &Tx{
// 		Sqlx: d.connection.MustBegin()}

// 	query := "SELECT id FROM \"orchestrator_workflow\" WHERE idempotency_key = :idempotency_key"
// 	rows, err := tx.QueryRows(query, map[string]interface{}{
// 		"idempotency_key": idempotencyKey,
// 	})
// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, errors.Wrap(err, "Couldn't get record")
// 	}

// 	var id string
// 	for rows.Next() {
// 		err = rows.Scan(
// 			&id,
// 		)
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 			return nil, errors.Wrap(err, "Couldn't scan")
// 		}
// 	}
// 	if id == "" {

// 		q := "INSERT INTO orchestrator_workflow (id, name, idempotency_key, args_binary, state) VALUES (:id, :name, :idempotency_key, :args_binary, :state) RETURNING id"
// 		fmt.Println(q)
// 		rows, err := tx.QueryRows(q, map[string]interface{}{
// 			"id":            libid.New().String(),
// 			"name":            workflow.GetName(),
// 			"idempotency_key": idempotencyKey,
// 			"args_binary":     serializedArgs,
// 			"state":           "new",
// 		})
// 		for rows.Next() {
// 			err = rows.Scan(
// 				&id,
// 			)
// 			if err != nil {
// 				return nil, errors.Wrap(err, "Couldn't scan")
// 			}
// 		}
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 			return nil, errors.Wrap(err, "Couldn't insert")
// 		}
// 	}

// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, errors.Wrap(err, "Couldn't insert record")
// 	}

// 	tx.Sqlx.Commit()

// 	return &WorkflowRun{
// 		client: d,
// 		context: &libapplication.Workflow{
// 			Workflow: &libdomain.Workflow{
// 				Context:    ctx,
// 				TenantUUID: tenantUUID,
// 			},
// 			WorkflowUUID:       id,
// 			WorkflowStep:       1,
// 			TenantClient:       tenantAggregate,
// 			TransactionClient:  transactionClient,
// 			OrchestratorClient: d,
// 		},
// 		workflow: workflow}, nil

// }

// type ActivityRun struct {
// 	client   *Driver
// 	id     string
// 	activity libapplication.ActivityInterface
// }

// func (r *ActivityRun) Get(wk *libapplication.Workflow, activityClient libapplication.TransactionClient) (interface{}, error) {

// 	var err error
// 	defer func() {

// 		if err != nil {
// 			wk.TransactionClient.RollbackTransaction(wk)
// 			return
// 		}

// 		if activityClient != wk.TransactionClient {
// 			wk.TransactionAlwaysCommit = true
// 		}

// 		if wk.TransactionAlwaysCommit {
// 			wk.TransactionClient.CommitTransaction(nil)
// 			wk.TransactionClient.BeginTransaction(wk, false)
// 		}
// 	}()

// 	tx := &Tx{
// 		Sqlx: r.client.connection.MustBegin()}
// 	defer func() {
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 		} else {
// 			tx.Sqlx.Commit()
// 		}
// 	}()

// 	query := "SELECT id, orchestrator_workflow_id, step, args_binary, result_binary FROM \"orchestrator_activity\" WHERE id = :id"
// 	rows, err := tx.QueryRows(query, map[string]interface{}{
// 		"id": r.id,
// 	})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get record")
// 	}

// 	var id string
// 	var workflowUUID string
// 	var step string
// 	var argsBinary []byte
// 	var resultFromDatabase []byte
// 	for rows.Next() {
// 		err = rows.Scan(
// 			&id,
// 			&workflowUUID,
// 			&step,
// 			&argsBinary,
// 			&resultFromDatabase,
// 		)
// 		if err != nil {
// 			return nil, errors.Wrap(err, "Couldn't scan")
// 		}
// 	}
// 	if id == "" {
// 		return nil, errors.New("Can't find activity in database")
// 	}

// 	if len(resultFromDatabase) > 0 {
// 		var result interface{}
// 		err = r.activity.UnmarshalResult([]byte(resultFromDatabase), result)
// 		if err != nil {
// 			return nil, err
// 		}
// 		return result, nil
// 	}

// 	err = r.activity.UnmarshalArgs(argsBinary)
// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, err
// 	}

// 	result, err := r.activity.Run(wk.Workflow.Context)
// 	if err != nil {
// 		return nil, err
// 	}

// 	serializedResult, err := r.activity.MarshalResult(result)
// 	if err != nil {
// 		return nil, err
// 	}

// 	q := fmt.Sprintf(
// 		"UPDATE \"orchestrator_activity\" SET result_binary=:result, state='done' WHERE id=:id RETURNING *")
// 	rows, err = tx.QueryRows(q, map[string]interface{}{
// 		"id":   r.id,
// 		"result": string(serializedResult),
// 	})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update activity")
// 	}
// 	for rows.Next() {
// 	}

// 	return result, nil
// }

// func (d *Driver) ExecuteActivity(
// 	wk *libapplication.Workflow, activity libapplication.ActivityInterface,
// ) (libapplication.ActivityRunInterface, error) {

// 	serializedArgs, err := activity.MarshalArgs()
// 	if err != nil {
// 		return nil, err
// 	}

// 	tx := &Tx{
// 		Sqlx: d.connection.MustBegin()}

// 	query := "SELECT id FROM \"orchestrator_activity\" WHERE orchestrator_workflow_id = :workflow_id AND step = :step"
// 	rows, err := tx.QueryRows(query, map[string]interface{}{
// 		"workflow_id": wk.WorkflowUUID,
// 		"step":          wk.WorkflowStep,
// 	})
// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, errors.Wrap(err, "Couldn't get record")
// 	}

// 	var id string
// 	for rows.Next() {
// 		err = rows.Scan(
// 			&id,
// 		)
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 			return nil, errors.Wrap(err, "Couldn't scan")
// 		}
// 	}
// 	if id == "" {

// 		q := "INSERT INTO orchestrator_activity (id, orchestrator_workflow_id, step, transaction, args_binary, state) VALUES (:id, :workflow_id, :step, True, :args, :state) RETURNING id"
// 		fmt.Println(q)
// 		rows, err := tx.QueryRows(q, map[string]interface{}{
// 			"id":        libid.New().String(),
// 			"workflow_id": wk.WorkflowUUID,
// 			"step":        wk.WorkflowStep,
// 			"args":        serializedArgs,
// 			"state":       "new",
// 		})
// 		for rows.Next() {
// 			err = rows.Scan(
// 				&id,
// 			)
// 			if err != nil {
// 				tx.Sqlx.Rollback()
// 				return nil, errors.Wrap(err, "Couldn't scan")
// 			}
// 		}
// 		if err != nil {
// 			tx.Sqlx.Rollback()
// 			return nil, errors.Wrap(err, "Couldn't insert")
// 		}
// 	}

// 	if err != nil {
// 		tx.Sqlx.Rollback()
// 		return nil, errors.Wrap(err, "Couldn't insert record")
// 	} else {
// 		tx.Sqlx.Commit()
// 	}

// 	wk.WorkflowStep = wk.WorkflowStep + 1

// 	return &ActivityRun{client: d, id: id, activity: activity}, nil

// }

// func (d *Driver) InitOrchestratorRepository() bool {
// 	return true
// }

// type RegisterEventActivity struct {
// 	Workflow         *libapplication.Workflow
// 	EventStoreClient libapplication.EventStoreClient
// 	Aggregate        libdomain.AggregateInterface
// 	AggregateUUID    string
// 	Event            string
// 	Payload          map[string]interface{}
// }

// func (a *RegisterEventActivity) MarshalArgs() ([]byte, error) {

// 	args := map[string]interface{}{
// 		"aggregate":     a.Aggregate,
// 		"aggregateUUID": a.AggregateUUID,
// 		"event":         a.Event,
// 		"payload":       a.Payload,
// 	}

// 	serializedArgs, err := json.Marshal(args)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return serializedArgs, nil
// }
// func (a *RegisterEventActivity) UnmarshalArgs(data []byte) error {

// 	var args map[string]interface{}
// 	err := json.Unmarshal([]byte(data), &args)
// 	if err != nil {
// 		return err
// 	}

// 	// a.Aggregate = args["aggregate"].(string)
// 	a.AggregateUUID = args["aggregateUUID"].(string)
// 	a.Event = args["event"].(string)
// 	// a.Payload = args["payload"].(string)

// 	return nil
// }
// func (a *RegisterEventActivity) MarshalResult(result interface{}) ([]byte, error) {

// 	serializedResult, err := json.Marshal(result.(*libdata.Event))
// 	if err != nil {
// 		return nil, err
// 	}

// 	return serializedResult, nil
// }
// func (a *RegisterEventActivity) UnmarshalResult(data []byte, resultInterface interface{}) error {

// 	var result *libdata.Event
// 	err := json.Unmarshal([]byte(data), result)
// 	if err != nil {
// 		return err
// 	}

// 	resultInterface = result

// 	return nil
// }
// func (a *RegisterEventActivity) Run(
// 	ctx context.Context,
// ) (interface{}, error) {

// 	event, err := a.EventStoreClient.RegisterEvent(
// 		a.Workflow,
// 		a.Aggregate,
// 		a.AggregateUUID,
// 		a.Event,
// 		a.Payload,
// 	)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return event, nil
// }

// type DispatchEventActivity struct {
// 	Workflow            *libapplication.Workflow
// 	EventDispatchClient libapplication.EventDispatchClient
// 	Event               *libdomain.Event
// }

// func (a *DispatchEventActivity) MarshalArgs() ([]byte, error) {

// 	// args := map[string]interface{}{}

// 	// serializedArgs, err := json.Marshal(args)
// 	// if err != nil {
// 	// 	return nil, err
// 	// }

// 	return []byte{}, nil
// }
// func (a *DispatchEventActivity) UnmarshalArgs(data []byte) error {

// 	// var args map[string]interface{}
// 	// err := json.Unmarshal([]byte(data), &args)
// 	// if err != nil {
// 	// 	return err
// 	// }

// 	return nil
// }
// func (a *DispatchEventActivity) MarshalResult(result interface{}) ([]byte, error) {

// 	// serializedArgs, err := json.Marshal(result.(map[string]interface{}))
// 	// if err != nil {
// 	// 	return nil, err
// 	// }

// 	return []byte{}, nil
// }
// func (a *DispatchEventActivity) UnmarshalResult(data []byte, resultInterface interface{}) error {

// 	// var result map[string]interface{}
// 	// err := json.Unmarshal([]byte(data), &result)
// 	// if err != nil {
// 	// 	return err
// 	// }

// 	// resultInterface = result

// 	return nil
// }
// func (a *DispatchEventActivity) Run(
// 	ctx context.Context,
// ) (interface{}, error) {

// 	err := a.EventDispatchClient.DispatchEvent(
// 		a.Workflow,
// 		a.Event,
// 	)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return nil, nil
// }
