package postgres

import (
	"fmt"
	"log"
	"strings"

	// "strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.opentelemetry.io/otel/attribute"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	// "gitlab.com/empowerlab/stack/lib-go/libdomain/generator/baserepositorypb"
)

// Tx contains all the function needed to be recognized as a libdata Tx
type Tx struct {
	Sqlx *sqlx.Tx
}

// Exec will execute the specified query in this transaction.
func (tx *Tx) Exec(wk *libapplication.ApplicationContext, query string, args interface{}) error {
	
	_, span := wk.Workflow().Tracer.Start(wk.Workflow().Context, "Postgres-Execute")
	span.SetAttributes(attribute.String("query", query))
	defer span.End()

	txStmt, err := tx.Sqlx.PrepareNamed(query)
	if err != nil {
		panic(err)
		return errors.Wrap(err, (&DBRequestError{Q: query, R: fmt.Sprintf("%s", args)}).Error())
	}
	_, err = txStmt.Exec(args)
	if err != nil {
		panic(err)
		return errors.Wrap(err, (&DBRequestError{Q: query, R: fmt.Sprintf("%s", args)}).Error())
	}
	log.Printf("%s\n%+v", query, args)
	return nil
}

// QueryRows will execute the specified query in this transaction and return the records.
func (tx *Tx) QueryRows(wk *libapplication.ApplicationContext, q string, args interface{}) (*sqlx.Rows, error) {

	_, span := wk.Workflow().Tracer.Start(wk.Workflow().Context, "Postgres-QueryRows")
	span.SetAttributes(attribute.String("query", q))
	defer span.End()

	// logger := otelslog.NewLogger("test")
	// logger.InfoContext(wk.Workflow().Context, "QueryRows", attribute.String("query", q))

	log.Printf("%s\n%+v", q, args)
	txStmt, err := tx.Sqlx.PrepareNamed(q)
	if err != nil {
		panic(err)
		log.Println(err)
		return nil, errors.Wrap(err, (&DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}
	rows, err := txStmt.Queryx(args)
	if err != nil {
		panic(err)
		log.Println(err)
		return nil, errors.Wrap(err, (&DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}

	return rows, nil
}

func convertFilter(w *libpersistence.Filter, query string, args map[string]interface{}) (string, error) {
	
	switch f := w.Operator.(type) {
	case *libpersistence.IsEqualTo:
		query = fmt.Sprintf("%s%s = :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.IsDifferentTo:
		query = fmt.Sprintf("%s%s != :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.Contains:
		field := w.Field
		if w.JSONKey != "" {
			field = fmt.Sprintf("%s->>'%s'", w.Field, w.JSONKey)
		}
		query = fmt.Sprintf("%s%s ILIKE :%s", query, field, w.Field)
		args[w.Field] = "%" + f.Value + "%"
	case *libpersistence.IsIn:
		query = fmt.Sprintf("%s%s = ANY (:%s)", query, w.Field, w.Field)
		args[w.Field] = "{" + strings.Join(f.Values, ",") + "}"
	case *libpersistence.IsNull:
		query = fmt.Sprintf("%s%s IS NULL", query, w.Field)
	case *libpersistence.IsNotNull:
		query = fmt.Sprintf("%s%s IS NOT NULL", query, w.Field)
	case *libpersistence.IsFalse:
		query = fmt.Sprintf("%s%s IS NOT TRUE", query, w.Field)
	case *libpersistence.IsTrue:
		query = fmt.Sprintf("%s%s IS TRUE", query, w.Field)
	case *libpersistence.IsBefore:
		query = fmt.Sprintf("%s%s < :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.IsAfter:
		query = fmt.Sprintf("%s%s > :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.IsGreaterThan:
		query = fmt.Sprintf("%s%s > :%s", query, w.Field, w.Field)
		args[w.Field] = int(f.Value)
	case *libpersistence.IsGreaterThanOrEqualTo:
		query = fmt.Sprintf("%s%s >= :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.IsLessThan:
		query = fmt.Sprintf("%s%s < :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.IsLessThanOrEqualTo:
		query = fmt.Sprintf("%s%s <= :%s", query, w.Field, w.Field)
		args[w.Field] = f.Value
	case *libpersistence.NativeOR:
		subqueries := []string{}
		for _, filter := range f.Filters {
			subquery, err := convertFilter(filter, "", args)
			if err != nil {
				return "", err
			}
			subqueries = append(subqueries, subquery)
		}
		query = fmt.Sprintf("%s(%s)", query, strings.Join(subqueries, " OR "))
	default:
		return "", &DBUnrecognizedOperatorError{O: fmt.Sprintln(w.Operator)}
	}

	return query, nil
}

func buildFilters(
	wk *libapplication.ApplicationContext, model libpersistence.TableInterface,
	filters []*libpersistence.Filter, options *libpersistence.OptionsListQuery,
) (string, map[string]interface{}, error) {

	query := ""
	args := map[string]interface{}{}
	// // fmt.Printf("tx %+v\n", tx)
	// if model.UseTenants() { //model.Cluster.MultiTenant && model != libdata.TenantModel {
	// 	skip := false
	// 	if options != nil {
	// 		if options.GlobalIgnoreTenants {
	// 			skip = true
	// 		}
	// 	}
	// 	if !skip {
	// 		filters = append(filters, &libpersistence.Filter{
	// 			Field:    "tenant_id",
	// 			Operator: &libpersistence.IsEqualTo{Value: string(wk.Workflow().TenantID)},
	// 		})
	// 	}
	// }
	for _, w := range filters {
		if query != "" {
			query = fmt.Sprintf("%s AND ", query)
		}

		var errConvert error
		query, errConvert = convertFilter(w, query, args)
		if errConvert != nil {
			return "", nil, errConvert
		}

	}

	if query != "" {
		// nolint: gas
		query = fmt.Sprintf("WHERE %s", query)
	}
	return query, args, nil
}
