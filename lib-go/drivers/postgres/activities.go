package postgres

import (
	"encoding/json"

	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
)

type InsertActivity struct {
	Workflow *libapplication.ApplicationContext
	Query    string
	Args     map[string]interface{}
}

func (a *InsertActivity) MarshalArgs() ([]byte, error) {

	args := map[string]interface{}{
		"query": a.Query,
		"args":  a.Args,
	}

	serializedArgs, err := json.Marshal(args)
	if err != nil {
		return nil, err
	}

	return serializedArgs, nil
}
func (a *InsertActivity) UnmarshalArgs(data []byte) error {

	var args map[string]interface{}
	err := json.Unmarshal([]byte(data), &args)
	if err != nil {
		return err
	}

	a.Query = args["aggregateUUID"].(string)
	a.Args = args["args"].(map[string]interface{})

	return nil
}
func (a *InsertActivity) MarshalResult(result interface{}) ([]byte, error) {

	// serializedResult, err := json.Marshal(result.(*libdata.Event))
	// if err != nil {
	// 	return nil, err
	// }

	return nil, nil
}
func (a *InsertActivity) UnmarshalResult(data []byte, resultInterface interface{}) error {

	// var result *libdata.Event
	// err := json.Unmarshal([]byte(data), result)
	// if err != nil {
	// 	return err
	// }

	// resultInterface = result

	return nil
}
func (a *InsertActivity) Run(
	wk *libapplication.ApplicationContext,
) (interface{}, error) {

	rows, err := a.Workflow.Transaction.(*Tx).QueryRows(wk, a.Query, a.Args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't insert record")
	}

	return rows, nil
}
