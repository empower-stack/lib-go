---
id: local
title: Installing your local/development environment
sidebar_label: Your local environment
---

## The docker-compose file

In the [example repository](https://gitlab.com/empowerlab/example), a corresponding docker-compose.yml file is provided so you can easily set up your local development environment.

[Install Docker Compose](https://docs.docker.com/compose/install), download the file and execute the following command:

```
sudo docker-compose -f docker-compose.yml up
```

Docker Compose will download the images from the gitlab repository, deploy them, link them together and initialize the databases and authentification, so you have an immediate working environment.

The web applications will be accessible at the following urls:
-React-admin: http://localhost:5000
-Gateway GraphiQL: http://localhost:5001
-Hydra Oauth2: http://localhost:5002
-Login service: http://localhost:5003
-Jaeger: http://localhost:5004

Of course, if you fork the example repository and create your own services you'll need to adapt the docker-compose file to the new architecture you are creating. Having an up-to-date docker-compose.yml file is important, so any new contributor to your project can easily create his local environment.


## Installing your development environment

By using the docker-compose.yml file you should have immediately a working environment, but you can't easily update the applications inside your containers.

To do this, and set up an effective development environment, you need to map the volumes inside the containers to your local folders containing the source code of your applications.

Make sure to choose a good location for your $GOPATH folder (which will contain all the code related to Golang).
To build your Go binaries, you can't really install Golang on your computer, because the images are built with alpine and your computer will build incompatible binaries. You have no other choice but to set up another docker image that will be used as a builder.
```
sudo docker run -d --name golang-factory -v "$GOPATH:/go" golang:1.13-alpine
```

Download your project (or the example project):
```
git clone git@gitlab.com:empowerlab/example.git $GOPATH/src/gitlab.com/empowerlab/example
```

Under the $GOPATH/src/gitlab.com/empowerlab/example you will find the code of your architecture, which you will be able to update and commit.

Then go into your builder image
```
sudo docker exec -it golang-factory sh
```
And execute the commands you can find in the go-factory.Dockerfile file to set up your builder.

You can issue the commands ```make dep```, ```make build```, and ```make install``` inside each golang services of the project. This will build the binary and install it in the $GOPATH/bin folder, which is used by the containers deployed by Docker Compose.

You can now uncomment in the docker-compose.yml file the links to your volumes, so your containers use the binaries in your local folders.
```
volumes:
    - "$GOPATH:/go"
```

Regarding the React/Node.js applications, like React-Admin, things are simpler since you will just use the Node.js installation inside your containers.
Just uncomment the lines to update the command and map the volume to your local folders.
```
command: ["yarn", "run", "start"]
volumes:
    - "$GOPATH/src/gitlab.com/empowerlab/example/admin:/opt/app-root"
```

By changing the command to ```yarn run start```, the container will not use the pre-built code in the container, but will instead launch a server with hot reloading on your local folders, recompiling the application each time you make a change in your code.
